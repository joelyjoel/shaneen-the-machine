const sequenceDrumTrack = require("../dsp/sequenceDrumTrack.js");
const PointTrack = require("../music/PointTrack.js");
const Sample = require("../dsp/Sample.js");
const make_drums = require("../music/make_drums")

/*var track1 = new PointTrack();
for(var i=0; i<16; i++) {
  var note = new PointTrack.Note();
  note.t = i*4;
  note.d = 4;
  note.sound = "kick";
  track1.notes.push(note);
}

var track2 = new PointTrack();
for(var i=0; i<16; i++) {
  var note = new PointTrack.Note();
  note.t = i*4 + 2;
  note.d = 4;
  note.sound = "kick";
  track2.notes.push(note);
}

track2.makeGamutDisjointTo(track1)
console.log(track2)

track1.mix(track2)*/

var track1 = make_drums();
track1 = track1.loop(track1.d * 2)


sequenceDrumTrack(track1).then(function(thing) {
  Sample.record(thing.y, thing.sequencer.T).save(track1.label + " ("+thing.sequencer.bpm.toFixed(2)+"bpm)")
  console.log("finished recording");
})
