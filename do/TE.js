var clockwork = require("../sound/");
var Sample = clockwork.Sample;
var stereoPairsBinaural = require("../sound/stereoPairsBinaural.js");
var Paragraph = require("../words/Paragraph.js");
var zxsm = require("../sound/zxsm.js");

var pieceDuration = 120 * 44100;
var text = "I lit a green candle to make you jealous of me, but my room filled up with mosquitoes, they heard that my body was free";
text += " " + text;
text += " " + text;
text += " " + text;
text += " " + text;


//GOOO!
//text += " " + text;
//text += " " + text;
text = new Paragraph(text);
//console.log(text);
var samps = text.fetchTTSSamples("en-us").then(function(words) {
    var tape = new clockwork.Sample().blank(pieceDuration, 8);

    var t = 0;
    for(var i in words) {
        t = processAndMixSample(Math.floor(t*0.99), words[i], tape);
    }
    console.log("total:", Math.ceil(t/44100), "s")
    return tape;
}).then(function(tape) {
    var stereoMix = stereoPairsBinaural(tape);
    tape.save("8chan TE.wav");
    stereoMix.save("2chan TE.wav");
});

function processAndMixSample(t, sample, tape) {
    //console.log("before: ", sample.lengthInSamples);
    var processI = Math.floor(Math.random()*processF.length);
    sample = processF[processI](sample);
    if(Math.random()<0.5) {
        var processI = Math.floor(Math.random()*processF.length);
        sample = processF[processI](sample);
    }
    if(sample.rms() == NaN) console.log("oh no NaN", processI, spatialiseI);
    var spatialiseI = Math.floor(Math.random()*spatialiseF.length);
    sample = spatialiseF[spatialiseI](sample);
    if(sample.rms() == NaN) console.log("oh no NaN (B)", processI, spatialiseI);

    //console.log(sample.lengthInSamples, processI, spatialiseI);
    tape.mix(sample, t, 0);
    if(t > pieceDuration) console.log("EEKS gone beyond piece duration", Math.round(t/44100), "s")


    return t + sample.lengthInSamples;
}

processF = [];
processF[0] = function(sample) {
    return sample;
}
processF[1] = function(sample) {
    return zxsm.shuffleGrains(sample, Math.random()*50);
}
processF[2] = function(sample) {
    return zxsm.orderGrainsByLength(sample, Math.random()*10, Math.random()<0.5);
}
processF[3] = function(sample) {
    return zxsm.addSilenceBetweenGrains(sample, 1, Math.random()*50)
}
processF[4] = function(sample) {
    return zxsm.equaliseGrainLengths(sample, 1, Math.random()*0.4 + 0.8);
}
processF[5] = function(sample) {
    return zxsm.wobbleGrainLengths(sample);
}
processF[6] = function(sample) {
    var d = Math.ceil(Math.random()*sample.lengthInSamples-1000)+ 1000;
    var t0 = Math.floor(Math.random()*(sample.lengthInSamples-d));
    var t1 = t0 + d;
    var loop = sample.zXCut(t0, t1);
    loop = loop.loop(Math.floor(Math.random()*8) * loop.lengthInSamples);
    return loop;
}

spatialiseF = [];
spatialiseF[0] = function(sample) {
    var tape = new Sample().blank(sample.lengthInSamples, 8);
    tape.mix(sample, 0, Math.floor(Math.random()*tape.numberOfChannels));
    return tape;
}
spatialiseF[1] = function(sample) {
    var tape = new Sample().blank(sample.lengthInSamples, 8);
    var zX = sample.diceByZeroCrossings(Math.random()*50);
    var t = 0;
    for(var i in zX) {
        tape.mix(zX[i], t, Math.floor(Math.random()*tape.numberOfChannels));
        t += zX[i].lengthInSamples;
    }
    //console.log("RMS:", tape.rms());
    //console.log(tape.lengthInSamples, tape.numberOfChannels, t);
    return tape;
}
