const Sample = require("../dsp3/Sample.js")
const fs = require("fs")
const argv = require("minimist")(process.argv.slice(2))
const path = require("path")

const dir = argv._[0]
const files = fs.readdirSync(dir)
const outdir = argv.o || "/Users/joel/Desktop/WadOut/"

async function main() {
  for(var i in files) {
    if(files[i].slice(-4) != ".wav")
      continue;

    var filepath = path.resolve(dir, files[i])
    console.log("doing ", filepath)

    var samp = await Sample.readFile(filepath)
    var left = new Sample()
    left.channelData[0] = samp.channelData[0]
    var right = new Sample()
    right.channelData[0] = samp.channelData[1]

    console.log("saving left")
    console.log(await left.saveWavFile(outdir+files[i]+".left.wav"))

    console.log("saving right")
    console.log(await right.saveWavFile(outdir+files[i]+".right.wav"))

    delete samp, left, right;
  }

}
main()
