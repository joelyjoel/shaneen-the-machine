const Sample = require("../dsp/Sample.js")
const spectralVocode = require("../dsp/offline/spectralVocode.js")
const argv = require("minimist")(process.argv.slice(2))
const Promise = require("promise")
const path = require("path")
const fs = require("fs")

var numberOfThingsMade = 0;
var numberOfThingsToBeMade = argv._[0] || 1

function makeAThing() {
  Promise.all([
    Sample.choose("drum"),
    Sample.choose("drum"),
  ]).then(function(samples) {

    samples[1].stretch(samples[0].lengthInSamples)

    var dial = 1;//2/3 + Math.random() * 1/3
    var out = spectralVocode(samples[0], samples[1], dial)

    out = out.toPCM()
    var filename = "vocoded ("+samples[0].label+ ") by ("+samples[1].label+") ("+dial.toFixed(3)+").wav"
    console.log("filename:", filename)

    var subPath = path.relative(
      "./resources/samples/drum",
      path.resolve(samples[0].originalFile, "../")
    ).split(path.sep)
    console.log("subPath", subPath)
    var savePath = "./resources/samples/drum/vocoded/"
    for(var i in subPath) {
      savePath = path.resolve(savePath, subPath[i]);
      if(!fs.existsSync(savePath)) {
        fs.mkdirSync(savePath)
      }
    }

    savePath = path.resolve(savePath, filename)
    console.log(savePath)
    out.saveWavFile(savePath)

  }).then(function() {
    numberOfThingsMade++;
    if(numberOfThingsMade < numberOfThingsToBeMade) {
      makeAThing();
    }
  })
}

makeAThing()
