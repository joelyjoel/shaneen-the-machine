const argv = require("minimist")(process.argv.slice(2))
const Sample = require("../dsp/Sample.js")

const inputFilename = argv._[0]
const outputFilename = argv.o || argv._[0] + " stereo mix.wav"

Sample.readFile(inputFilename).then(function(sample) {
  console.log(sample);
})
