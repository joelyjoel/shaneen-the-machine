const RMS = require("../dsp/RMS")
const argv = require("minimist")(process.argv.slice(2))
const Sampler = require("../dsp/synths/SamplerMono.js")
const Sample = require("../dsp/Sample.js")
const FunctionUnit = require("../dsp/FunctionUnit.js")

/*const filename = argv._[0]
console.log("input filename:", filename)
if(!filename)
  throw "please provide file to analyse"*/

Sampler.choose("vocal/speaking").then(function(sampler) {
  sampler.trigger()
  var rms = new RMS(sampler)
  rms.f = 20

  var fUnit = new FunctionUnit()

  Sample.record(rms, sampler.T)
  .save("envelopeTracked")
})
