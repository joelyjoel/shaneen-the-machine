const Sample = require("../dsp/Sample.js")
const Osc = require("../dsp/Osc.js")
const argv = require("minimist")(process.argv.slice(2))
const Sum = require("../dsp/Sum.js")
const connect = require("../dsp/connect.js")
var T = (argv.T || 5) * 44100

var main = Osc.random()

main.p = new Sum(main.p, Osc.lfo(1, 10))

Sample.record(main.y, T, 1).save("testing Oscs")
