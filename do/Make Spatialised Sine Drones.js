const Spatialise = require("../dsp/Spatialise.js")
const CircularMotion = require("../dsp/vector/CircularMotion.js")
const Osc = require("../dsp/Osc.js")
const Sample = require("../dsp/Sample.js")
const Mixer = require("../dsp/Mixer.js")
const argv = require("minimist")(process.argv.slice(2))

var mixer = new Mixer()

var n= argv.n || 1;
var T = 44100 * (argv.T || 10)

var frequencies = []

for(var i=0; i<n; i++) {
  var osc = Osc.random();
  frequencies.push(osc.fNow)
  var space = new Spatialise();
  space.in = osc;
  space.place = CircularMotion.explode([0,0])
  mixer.addInput(space)
}
console.log(frequencies)
Sample.record(mixer, T, 6).fadeOutSelf(T).normalise().addMeta({
  frequencies: frequencies,
}).saveResource(n+" sine explosion", "samples/sfx/spatialised")
