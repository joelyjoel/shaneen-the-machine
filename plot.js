const asciichart = require("asciichart")

function plot(data, config) {
  var width = process.stdout.columns - 10
  config = config || {height:20}
  var lineSep = "\n\n\n"

  //1 dice the data up
  var lines = []
  for(var i=0; i<data.length; i+=width) {
    lines.push(
      "\tX: "+ i+ " - - - >\n"+
      asciichart.plot(
        data.slice(i, i+width),
        config,
      )
    )
  }
  return lines.join(lineSep)
}
module.exports = plot
