const construct = require("./dsp3/construct")
const argv = require("minimist")(process.argv.slice(2))
const RenderStream = require("./dsp3/RenderStream")
const Speaker = require("speaker")
const speaker = new Speaker({
  channels:1,
  bitDepth:32,
})


const duspStr = argv._[0]
const circuit = construct(duspStr)

var toRender = circuit.lastDuspExpression
if(toRender.isUnit)
  toRender = toRender.defaultOutlet

if(!toRender)
  throw "unable to render"

var renderstream = new RenderStream(toRender)
renderstream.pipe(speaker)
