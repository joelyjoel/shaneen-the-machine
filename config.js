const homedir = require("os").homedir()
const path = require("path")

module.exports = {
  sampleFolder: homedir+"/Samples/",
  sampleInboxSubdir: "inbox",
}
