var stereoPan = require("./stereoPan.js");
var Sample = require("./Sample.js");

function panGrains(sample, grainSize) {
    var zX = sample.diceByZeroCrossings(grainSize);
    for(var i in zX) {
        var pan = Math.random()*2-1;
        zX[i] = stereoPan.panSample(zX[i], pan);
    }

    return Sample.concat(zX);
}
module.exports.panGrains = panGrains;

function stepwisePanGrains(sample, grainSize, maxStep) {
    maxStep = maxStep || 0.05;
    var zX = sample.diceByZeroCrossings(grainSize);
    var pan = Math.random()*2-1;
    for(var i in zX) {
        pan += maxStep * (Math.random()*2-1);
        if(pan < -1) pan = 0;
        if(pan > 1) pan = 1;
        zX[i] = stereoPan.panSample(zX[i], pan);
    }

    return Sample.concat(zX);
}
module.exports.stepwisePanGrains = stepwisePanGrains;
