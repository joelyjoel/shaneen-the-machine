var dB = require("./decibel.js");
var Sample = require("./Sample.js");

function stereoPanSample(monoSample, pan, dbCompensate) {
    pan = pan || 0;
    dbCompensate = dbCompensate || 1.5;

    var compensation = dB((1-Math.abs(pan))*dbCompensate);
    var L = (1-pan)/2 * compensation;
    var R = (1+pan)/2 * compensation;

    var out = new Sample().blank(monoSample.lengthInSamples, 2);
    var d = monoSample.lengthInSamples;
    for(var t=0; t<d; t++) {
        out.channelData[0][t] = L * monoSample.channelData[0][t];
        out.channelData[1][t] = R * monoSample.channelData[0][t];
    }
    
    return out;
}
module.exports.panSample = stereoPanSample;
