var Sample = require("./Sample.js");

function stereoPairsBinaural(sample) {
    var n = sample.numberOfChannels;
    var d = sample.lengthInSamples;

    var scaleFactors = [];
    for(var channel=0; channel<n; channel++) {
        var angle = Math.floor(channel/2 + 1)/(n+1) * (channel%2 ? -1 : 1) * Math.PI*2;
        var leftRight = [Math.cos(angle), Math.cos(1-angle)];
        scaleFactors[channel] = leftRight;
    }

    var stereoMix = new Sample().blank(d, 2);

    for(var channel=0; channel<n; channel++) {
        for(var t=0; t<d; t++) {
            stereoMix.channelData[0][t] += scaleFactors[channel][0] * sample.channelData[channel][t];
            stereoMix.channelData[1][t] += scaleFactors[channel][1] * sample.channelData[channel][t];
        }
    }

    return stereoMix;
}
module.exports = stereoPairsBinaural;
