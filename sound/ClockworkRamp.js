var ClockworkUnit = require("./ClockworkUnit.js");

function ClockworkRamp(T, y0, y1, curve) {
    ClockworkUnit.call(this);

    this.t = 0;
    this.y0 = y0 != undefined ? y0 : 0;
    this.y1 = y1 != undefined ? y1 : 1;
    this.T = T != undefined ? T : 44100;
    this.rate = 1;
    this.curve = curve || 0;

    this.loop = false;
    this.flatline = false;
}
ClockworkRamp.prototype = Object.create(ClockworkUnit.prototype);
ClockworkRamp.prototype.constructor = ClockworkRamp;
module.exports = ClockworkRamp;

ClockworkRamp.prototype._tick = function() {
    var y = this.y;
    this.t += this.rate.y;
    if(this.loop && this.t > this.T.y)
        this.t %= this.T.y;
    this.tickSockets();
    return y;
}
ClockworkRamp.prototype.__defineGetter__("y", function() {
    var phase = this.t/this.T.y;
    if(phase < 0)
        return this.flatline ? 0 : this.y0.y;
    else if(phase > 1)
        return this.flatline ? 0 : this.y1.y;
    phase = Math.pow(phase, Math.exp(this.curve.y))
    return this.y0.y + phase * (this.y1.y-this.y0.y);
});

ClockworkRamp.prototype.__defineSocket__("y0");
ClockworkRamp.prototype.__defineSocket__("y1");
ClockworkRamp.prototype.__defineSocket__("T");
ClockworkRamp.prototype.__defineSocket__("rate");
ClockworkRamp.prototype.__defineSocket__("curve");

ClockworkRamp.prototype.randomise = function() {
    this.y0 = Math.random();
    this.y1 = Math.random();
    this.T = Math.random()*44100;
}
