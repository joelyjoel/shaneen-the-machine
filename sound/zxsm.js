// zero crossing sample manipulation
var Sample = require("./Sample.js");

function shuffleGrains(sample, grainSize) {
    var zX = sample.diceByZeroCrossings(grainSize);
    zX = zX.sort(function() { return Math.random()-1/2;});
    return Sample.concat(zX);
}
module.exports.shuffleGrains = shuffleGrains;

function orderGrainsByLength(sample, grainSize, ascending) {
    var zX = sample.diceByZeroCrossings(grainSize);
    if(ascending)
        zX = zX.sort(function(a,b) { return a-b; });
    else
        zX = zX.sort(function(a,b) { return b-a; });
    return Sample.concat(zX);
}
module.exports.orderGrainsByLength = orderGrainsByLength;

function addSilenceBetweenGrains(sample, grainSize, dSilence) {
    dSilence = Math.floor(dSilence || 0);
    grainSize = Math.ceil(grainSize || 1);

    var zX = sample.diceByZeroCrossings(grainSize);
    return Sample.concat(zX, dSilence);
}
module.exports.addSilenceBetweenGrains = addSilenceBetweenGrains;

function equaliseGrainLengths(sample, grainSize, sf) {
    sf = sf || 1;
    var zX = sample.diceByZeroCrossings(grainSize);
    var meanLength = Math.round(sample.lengthInSamples/zX.length);
    for(var i in zX) {
        zX[i] = zX[i].stretch(meanLength * sf);
    }
    return Sample.concat(zX)
}
module.exports.equaliseGrainLengths = equaliseGrainLengths;

function wobbleGrainLengths(sample, grainSize, wobble) {
    wobble = wobble || 0.5;
    var zX = sample.diceByZeroCrossings(grainSize);
    for(var i in zX) {
        var sf = 1 + Math.random()*wobble - wobble/2;
        zX[i] = zX[i].stretch(Math.ceil(zX[i].lengthInSamples * sf));
    }
    return Sample.concat(zX);
}
module.exports.wobbleGrainLengths = wobbleGrainLengths;

function normaliseGrains(sample, grainSize, normalLevel) {
    normalLevel = normalLevel || 1;
    var zX = sample.diceByZeroCrossings(grainSize);
    for(var i in zX)
        zX[i].normalise(normalLevel);
    return Sample.concat(zX);
}
module.exports.normaliseGrains = normaliseGrains;

function granularScrub(sample, grainSize, d, grabRadius) {
    grabRadius = grabRadius || 2;

    var zX = sample.diceByZeroCrossings(grainSize);

    var d = d || sample.lengthInSamples;
    var t = 0;
    var i = 0;
    var list = [];
    while(t < d) {
        i += (Math.random()*2 -1) * grabRadius;
        if(i < 0) i = 0;
        if(i >= zX.length) i = zX.length - 1;
        var grain = zX[Math.floor(i)];
        list.push(grain);
        t += grain.lengthInSamples;
    }
    return Sample.concat(list);
}
module.exports.granularScrub = granularScrub;

function granularReconstruct(sample, grainSize, grabRadius, sf) {
    sf = sf || 1;
    grabRadius = grabRadius || 10;

    var zX = sample.diceByZeroCrossings(grainSize);

    var d = sample.lengthInSamples * sf;
    var t = 0;
    var i = 0;
    var list = [];
    while(t < d) {
        i = (t/d) * zX.length + (Math.random()*2 -1) * grabRadius;
        if(i < 0) i = 0;
        if(i >= zX.length) i = zX.length - 1;
        i = Math.floor(i)
        var grain = zX[i];
        list.push(grain);
        t += grain.lengthInSamples;

        while(grain.lengthInSamples < 256) {
            i++
            var grain = zX[i];
            list.push(grain);
            t += grain.lengthInSamples;
        }
    }
    return Sample.concat(list);
}
module.exports.granularReconstruct = granularReconstruct;

function scatterGrains(sample, grainSize, d) {
    d = d || sample.lengthInSamples;
    var tape = new Sample().blank(d);
    var zX = sample.diceByZeroCrossings(grainSize);
    for(var i in zX) {
        var t = Math.floor(Math.random()*(d-zX[i].lengthInSamples));
        tape.mix(zX[i], t);
    }
    tape.normalise();
    return tape;
}
module.exports.scatterGrains = scatterGrains;
