var ClockworkUnit = require("./ClockworkUnit.js");
var DB = require("./decibel.js");

function ClockworkWaveshaper() {
    ClockworkUnit.call(this);

    this.shape = "sin";
}
ClockworkWaveshaper.prototype = Object.create(ClockworkUnit.prototype);
ClockworkWaveshaper.prototype.constructor = ClockworkWaveshaper;
module.exports = ClockworkWaveshaper;

ClockworkWaveshaper.prototype._tick = function() {
    var y = this.y;
    this.tickSockets();
    return y;
}
ClockworkWaveshaper.prototype.__defineGetter__("y", function() {
    return this.shapeFunction(this.input.y * DB.dbToSf(this.drive.y));
});

ClockworkWaveshaper.prototype.__defineSocket__("input");
ClockworkWaveshaper.prototype.__defineSocket__("drive");

ClockworkWaveshaper.prototype.__defineGetter__("shape", function() {
    return this._shape;
});
ClockworkWaveshaper.prototype.__defineSetter__("shape", function(shape) {
    if(shape.constructor == String) {
        this._shape = shape;
        this.shapeFunction = this.shapeFunctionRef[shape];
    } else if(typeof shape == "function") {
        this._shape = "custom";
        this.shapeFunction = shape;
    }
});

ClockworkWaveshaper.prototype.shapeFunctionRef = {
    "sin": function(x) {
        return Math.sin(Math.PI * x/2);
    }
}
