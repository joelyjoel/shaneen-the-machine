var ClockworkUnit = require("./ClockworkUnit.js");

function ClockworkMixer() {
    ClockworkUnit.call(this);

    this.inputs = [];
    this.levels = [];
}
ClockworkMixer.prototype = Object.create(ClockworkUnit.prototype);
ClockworkMixer.prototype.constructor = ClockworkMixer;
module.exports = ClockworkMixer;

ClockworkMixer.prototype.__defineMultiSocket__("input");
ClockworkMixer.prototype.__defineMultiSocket__("level");

ClockworkMixer.prototype._tick = function() {
    var y = this.y;
    for(var i in this.inputs)
        y += this.inputs[i].y * this.levels[i].y;

    this.tickSockets();

    return y;
}
ClockworkMixer.prototype.__defineGetter__("y", function() {
    var y = 0;
    for(var i in this.inputs) {
        y += this.inputs[i].y * this.levels[i].y;
    }
    return y;
})
