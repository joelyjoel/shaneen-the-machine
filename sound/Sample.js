module.exports = require("../dsp/Sample.js");
/*function Sample(filename) {
    this.channelData = [];
    this.sampleRate = 44100;

    if(filename != undefined && filename.constructor == String) {
        this.readFile(filename)
    }
}
module.exports = Sample;

var AudioContext = require("web-audio-api").AudioContext
, audioctx = new AudioContext ;
var fs = require("fs");
var Promise = require("promise");
var WavEncoder = require("wav-encoder");
//var fft = require("fft-js").fft;
//var fftUtil = require("fft-js").util;
var Complex = require("Complex");
var FFT = require("fft.js");
var organise = require("../organise.js");
//var clockworkify = require("./clockworkify.js");
var findFiles = require("../findFiles.js");

Sample.readFile = function(filename, subdir) {
    return new Promise(function(resolve, reject) {
        var prom = fs.readFile(filename, function(err, data) {
            if(err) reject(err);

            audioctx.decodeAudioData(data, function(data) {
                var samp = new Sample();
                samp.originalFile = filename;
                samp.label = filename.slice(filename.lastIndexOf("/")+1);
                samp.sampleRate = data.sampleRate;
                samp.channelData = data._data;
                resolve(samp);
            }, function(e) {
                reject(e);
            })
        });
    });
}
Sample.choose = function(kind) {
    var path = "./resources/samples/"+(kind || "");
    var files = findFiles(path, [".wav", ".mp3"]);
    if(files.length > 0) {
        var choice = files[Math.floor(Math.random()*files.length)];
        console.log("chosen", choice);
        return Sample.readFile(choice);
    } else {
        console.log("Could not find any samples of kind:", kind);
        return new Promise(function(fulfil) { fulfil(false);});
        // ^^ an empty promise
    }
}
Sample.record = function(what, T) {
    T = T || what.T || 44100;
    var tape = new Sample().blank(T, 1);
    for(var t=0; t<T; t++) {
      what.tick();
      tape.channelData[0][t] = what.y;
      console.log(tape.channelData[0][t]);
    }
    if(what.label)
        tape.label = what.label;
    return tape;
}
Sample.concat = function(samples, spacing) { // spacing may be a ClockworkUnit
    // join an array of samples end to end into one sample
    spacing = spacing || 0;

    var nChannels = 0;
    var d = 0;
    for(var i in samples) {
        d += samples[i].lengthInSamples;
        if(samples[i].numberOfChannels > nChannels)
            nChannels = samples[i].numberOfChannels
    }
    d += spacing * (samples.length-1);
    var outSample = new Sample().blank(d, nChannels);

    var t = 0;
    for(var i in samples) {
        outSample.mix(samples[i], t);
        t += samples[i].lengthInSamples + spacing;
    }

    return outSample;
}

Sample.prototype.isSample = true;
Sample.prototype.saveWavFile = function(filename) {
    var samp = this;
    return new Promise(function(resolve, reject) {
        var audio = {
            "sampleRate": samp.sampleRate,
            "channelData": samp.channelData
        };
        WavEncoder.encode(audio, { float: false, bitDepth: 24 }).then(function(data) {
            fs.writeFile(filename, new Buffer(data), function(err, data) {
                if(err) {
                    reject(err);
                    console.log("failed to save", filename);
                }
                else {
                    console.log("saved ", filename, (samp.lengthInSamples/44100).toFixed(3)+"s");
                    resolve(filename);
                }
            });
        }, function(err) {
            throw err;
        });
    });
}
Sample.prototype.save = function(filename, subdir) {
    filename = filename || (this.label ? this.label+".wav" : ".wav");
    if(filename.slice(filename.length-4) != ".wav")
      filename += ".wav";
    subdir = subdir || "samples";
    var path = organise.chooseFilename(filename, subdir);
    this.saveWavFile(path);
}
Sample.prototype.__defineGetter__("lengthInSamples", function() {
    return this.channelData[0].length;
});
Sample.prototype.__defineGetter__("numberOfChannels", function() {
    return this.channelData.length;
});
Sample.prototype.__defineGetter__("silent", function() {
    for(var channel in this.channelData) {
        for(var t in this.channelData[channel]) {
            if(this.channelData[channel][t])
                return false;
        }
    }
    return true;
})
Sample.prototype.__defineGetter__("d", function() {
    return this.lengthInSamples/this.sampleRate;
});
Sample.prototype.yOfSample = function(t, channel) {
    if(channel == undefined) channel = 0;

    if(t >= 0 && t < this.lengthInSamples
    && channel >= 0 && channel < this.numberOfChannels) {
        if(t%1 == 0) {
            return this.channelData[channel][t];
        } else {
            return this.channelData[channel][Math.ceil(t)] * (t%1)
                + this.channelData[channel][Math.floor(t)] * (1 - t%1);
        }
    } else
        return 0;
}
Sample.prototype.y = function(t, channel) { // alias of .yOfSample
    return this.yOfSample(t, channel);
}
Sample.prototype.rms = function(t0, t1) {
    t0 = t0 || 0;
    t1 = t1 || this.lengthInSamples;
    var sum = 0;
    var n = 0;
    var y;
    for(var c=0; c<this.numberOfChannels; c++)
        for(var t=t0; t<t1; t++) {
            y = this.channelData[c][t];
            sum += Math.pow(y,2);
            n++;
        }
    return Math.sqrt(sum/n);
}
Sample.prototype.min = function(t0, t1) {
    t0 = t0 || 0;
    t1 = t1 || this.lengthInSamples;
    var min = this.channelData[0][t0];
    var y;
    for(var c=0; c<this.numberOfChannels; c++) {
        for(var t=t0; t<t1; t++) {
            y = this.channelData[c][t];
            if(y < min)
                min = y;
        }
    }
    return min;
}
Sample.prototype.max = function(t0, t1) {
    t0 = t0 || 0;
    t1 = t1 || this.lengthInSamples;
    var max = this.channelData[0][t0];
    var y;
    for(var c=0; c<this.numberOfChannels; c++) {
        for(var t=t0; t<t1; t++) {
            y = this.channelData[c][t];
            if(y > max)
                max = y;
        }
    }
    return max;
}
Sample.prototype.peak = function(t0, t1) {
    var min = this.min(t0, t1);
    var max = this.max(t0, t1);
    if(Math.abs(min) > Math.abs(max))
        return min;
    else
        return max;
}
Sample.prototype.nextUpwardZeroCrossing = function(t0, channel) {
    t0 = t0 || 0;
    channel = channel || 0;
    var y, yPrev;
    for(var t=t0; t<this.lengthInSamples; t++) {
        y = this.channelData[channel][t];
        if(y == 0)
            continue;
        if(yPrev < 0 && y > 0)
            return t;

        yPrev = y;
    }
    return this.lengthInSamples;
}
Sample.prototype.nextDownwardZeroCrossing = function(t0, channel) {
    t0 = t0 || 0;
    channel = channel || 0;
    var y, yPrev;
    for(var t=t0; t<this.lengthInSamples; t++) {
        y = this.channelData[channel][t];
        if(y == 0)
            continue;
        if(yPrev > 0 && y < 0)
            return t;

        yPrev = y;
    }
    return this.lengthInSamples;
}
Sample.prototype.previousUpwardZeroCrossing = function(t0, channel) {
    t0 = t0 != undefined ? t0 : this.lengthInSamples;
    channel = channel || 0;
    var y, yPrev, tPrev;
    for(var t=t0-1; t>=0; t--) {
        y = this.channelData[channel][t];
        if(y == 0)
            continue;
        if(yPrev > 0 && y < 0)
            return tPrev;

        yPrev = y;
        tPrev = t;
    }
    return 0;
}
Sample.prototype.previousDownwardZeroCrossing = function(t0, channel) {
    t0 = t0 != undefined ? t0 : this.lengthInSamples;
    channel = channel || 0;
    var y, yPrev, tPrev;
    for(var t=t0-1; t>=0; t--) {
        y = this.channelData[channel][t];
        if(y == 0) continue;
        if(yPrev < 0 && y > 0)
            return tPrev;
        yPrev = y;
        tPrev = t;
    }
    return 0;
}
Sample.prototype.closestZeroCrossing = function(t, channel, downward) {
    channel = channel || 0;
    if(t == undefined) throw "t is required (closestZeroCrossing)";

    if(downward) {
        var nex = this.nextDownwardZeroCrossing(t, channel);
        var pre = this.previousDownwardZeroCrossing(t, channel);
    } else {
        var nex = this.nextUpwardZeroCrossing(t, channel);
        var pre = this.previousUpwardZeroCrossing(t, channel);
    }
    if(Math.abs(t-nex) < Math.abs(t-pre))
        return nex;
    else
        return pre;
}
Sample.prototype.upwardZeroCrossings = function(channel, t0, t1) {
    t0 = t0 || 0;
    t1 = t1 != undefined ? t1 : this.lengthInSamples
    channel = channel || 0;
    var zX = [];
    var t = t0;
    if(t == 0) zX.push(t);
    while((t = this.nextUpwardZeroCrossing(t, channel)) < t1)
        zX.push(t);
    return zX;
}
Sample.prototype.downwardZeroCrossings = function(channel, t0, t1) {
    t0 = t0 || 0;
    t1 = t1 != undefined ? t1 : this.lengthInSamples;
    channel = channel || 0;
    var zX = [];
    var t = t0;
    if(t == 0) zX.push(t);
    while((t = this.nextDownwardZeroCrossing(t, channel)) < t1)
        zX.push(t);
    return zX;
}
Sample.prototype.diceByZeroCrossings = function(grainSize, downward) {
    if(this.numberOfChannels != 1) throw "too many channels!!";
    grainSize = grainSize || 1;
    grainSize = Math.ceil(grainSize);


    var zX = downward ? this.downwardZeroCrossings(0) : this.upwardZeroCrossings(0);
    if(zX[0] != 0) zX.unshift(0);
    if(zX[zX.length-1] != this.lengthInSamples) zX.push(this.lengthInSamples);

    var waveforms = [];
    for(var i=0; i+1<zX.length; i+=grainSize) {
        waveforms.push( this.cut(zX[i], zX[(grainSize+i<zX.length?grainSize+i:zX.length-1)]) );
    }

    return waveforms;
}

Sample.prototype.gain = function(sf, t0, t1) {
    if(!sf) return ;
    t0 = t0 || 0;
    t1 = t1 || this.lengthInSamples;
    for(var c in this.channelData)
        for(var t=t0; t<t1; t++)
            this.channelData[c][t] *= sf;
    return this;
}
Sample.prototype.normalise = function(normal, t0, t1) {
    normal = normal || 1;
    t0 = t0 || 0;
    t1 = t1 || this.lengthInSamples;

    var sf = Math.abs(normal/this.peak(t0, t1));
    //console.log(sf);
    if(sf == Infinity) return sf;
    this.gain(sf, t0, t1);
    return sf;
}

Sample.prototype.blank = function(d, channels) { // d in seconds
    if(channels == undefined) {
        channels = 1;
    }
    //if(sr) this.sampleRate = sr;
    if(d <= 10) {
        console.log("WARNING, very short sample..", d);
    }
    var dInSamples = d;
    this.channelData = [];
    for(var c=0; c<channels; c++) {
        var buffer = new Float32Array(dInSamples);
        for(var t=0; t<dInSamples; t++) {
            buffer[t] = 0;
        }
        this.channelData[c] = buffer;
    }
    return this;
}
Sample.prototype.overwrite = function(samp, t0, channel) {
    t0 = t0 || 0;
    var t1 = t0 + samp.lengthInSamples;
    channel = channel || 0;

    for(var c=channel; c<this.numberOfChannels && c-channel<samp.numberOfChannels; c++) {
        for(var t=t0; t<t1; t++) {
            var y = samp.yOfSample(t-t0, c-channel);
            this.channelData[c][t] = y;
        }
    }

    return this;
}
Sample.prototype.mix = function(samp, t0, channel) {
    t0 = Math.round(t0 || 0);
    var t1 = Math.round(t0 + samp.lengthInSamples);
    channel = channel || 0;

    for(var c=channel; c<this.numberOfChannels && c-channel<samp.numberOfChannels; c++) {
        for(var t=t0; t<t1; t++) {
            var y = samp.yOfSample(t-t0, c-channel);
            this.channelData[c][t] += y;
        }
    }

    return this;
}
Sample.prototype.cut = function(t0, t1) {
    t0 = t0 || 0;
    t1 = t1 || this.lengthInSamples;
    t0 = Math.floor(t0);
    t1 = Math.floor(t1);

    var newSample = new Sample();
    for(var channel in this.channelData) {
        newSample.channelData[channel] = this.channelData[channel].slice(t0, t1);
    }
    return newSample;
}
Sample.prototype.zXCut = function(t0, t1, channel) {
    t0 = t0 || 0;
    t1 = t1 || this.lengthInSamples;

    t0 = this.closestZeroCrossing(t0);
    t1 = this.closestZeroCrossing(t1);
    if(t1 == t0) t1 = this.nextUpwardZeroCrossing(t0);

    return this.cut(t0, t1);
}
Sample.prototype.zXDice = function(subD) {
    if(this.numberOfChannels > 1) throw "woah, too many channels (Sample.prototype.zXDice)";
    if(subD == undefined) throw "subD is required [Sample.prototype.zXDice]";

    var bits = [];
    var t0, t1;
    for(var t=0; t<this.lengthInSamples; t+=subD) {
        t0 = Math.floor(t);
        t1 = Math.floor(t+subD);
        bits.push(this.zXCut(t0, t1));
    }
    return bits;
}
Sample.prototype.loop = function(d) {
    if(d == undefined) throw "d is required (Sample.prototype.loop)";
    var newSamp = new Sample().blank(d, this.numberOfChannels);

    for(var channel in this.channelData) {
        for(var t=0; t<d; t++) {
            newSamp.channelData[channel][t] = this.channelData[channel][t%this.lengthInSamples];
        }
    }
    return newSamp;
}

Sample.prototype.mixDownToMono = function() {
    var d = this.lengthInSamples;
    var monoBuffer = new Float32Array(d);
    var samp;
    for(var t=0; t<d; t++) {
        var samp = 0;
        for(var c in this.channelData) {
            samp += this.channelData[c][t];
        }
        monoBuffer[t] = samp;
    }
    this.channelData = [monoBuffer];

    return this;
}
Sample.prototype.seperateChannels = function() {
    var channels = [];
    for(var c in this.channelData) {
        var chan = new Sample().blank(this.lengthInSamples, 1);
        chan.channelData[0] = this.channelData[c].slice();
        channels.push(chan);
    }
    return channels;
}
Sample.prototype.convertToSampleRate = function(newSampleRate) {
    var oldSampleRate = this.sampleRate;
    if(oldSampleRate == newSampleRate)
        return this;

    var newChannelData = [];
    var newLengthInSamples = this.lengthInSamples * (newSampleRate/oldSampleRate);
    for(var channel in this.channelData) {
        newChannelData[channel] = new Float32Array(newLengthInSamples);
        for(var t=0; t<newLengthInSamples; t++) {
            newChannelData[channel][t] = this.yOfSample(t/newSampleRate*oldSampleRate, channel);
        }
    }
    this.channelData = newChannelData;
    this.sampleRate = newSampleRate;
    return this;
}

Sample.prototype.repitch = function(detune) {
    var rate = Math.pow(2, detune/12);
    var newLengthInSamples = Math.ceil(this.lengthInSamples/rate);

    var newChannelData = [];
    for(var c in this.channelData) {
        newChannelData[c] = new Float32Array(newLengthInSamples);
        for(var t=0; t<newLengthInSamples; t++) {
            newChannelData[c][t] = this.yOfSample(t*rate, c);
        }
    }
    this.channelData = newChannelData;
    return this;
}
Sample.prototype.stretchToD = function(d) {
    var rate = this.lengthInSamples/d;
    var newLengthInSamples = Math.ceil(this.lengthInSamples/rate);

    var newChannelData = [];
    for(var c in this.channelData) {
        newChannelData[c] = new Float32Array(newLengthInSamples);
        for(var t=0; t<newLengthInSamples; t++) {
            newChannelData[c][t] = this.yOfSample(t*rate, c);
        }
    }
    this.channelData = newChannelData;
    return this;
}
Sample.prototype.calculateFFT = function(frameSize) {
    frameSize = frameSize || 4096;

    var fft = new FFT(frameSize);

    var channels = [];
    for(var channel in this.channelData) {
        var frames = [];
        for(var t=0; t < this.lengthInSamples; t+=frameSize) {
            if(t+frameSize < this.lengthInSamples)
                var signal = this.channelData[channel].slice(t, t+frameSize);
            else {
                var signal = new Float32Array(frameSize);
                for(var i=t; i<this.lengthInSamples; i++) {
                    signal[i-t] = this.channelData[channel][i];
                }
            }

            var phasors = fft.createComplexArray(frameSize);
            fft.realTransform(phasors, signal);
            //console.log(phasors.length);

            var complexes = [];
            var magnitudes = [];
            var phases = [];
            for(var i=0; i+1<phasors.length; i+=2) {
                var z = Complex.from(phasors[i], phasors[i+1]);
                complexes[i/2] = z;
                magnitudes[i/2] = z.magnitude()/frameSize;
                phases[i/2] = z.angle()/(2*Math.PI);
            }

            var frame = {
                "phasors": phasors,
                "magnitudes": magnitudes,
                "phases": phases
            };

            frames.push(frame);
        }
        var frequencyBins = [];
        for(var bin=0; bin<phasors.length/2; bin++) {
            var f = this.sampleRate/phasors.length * bin;
            frequencyBins.push(f);
        }
        channels.push({
            "frames": frames,
            "frameSize": frameSize,
            "frequencyBins": frequencyBins
        });
    }
    return channels
}
Sample.prototype.__defineGetter__("fft", function() {
    if(!this._fft)
        this._fft = this.calculateFFT();
    return this._fft;
});*/
