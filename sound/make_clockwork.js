var clockwork = require("./index.js");

exports.ramp = function(T, maxY, minY) {
    T = T || 44100;
    maxY = maxY || 1;
    minY = minY || 0;
    T *= Math.random();
    var y0 = minY + Math.random()*(maxY-minY);
    var y1 = minY + Math.random()*(maxY-minY);
    var ramp = new clockwork.Ramp(T, y0, y1);
    ramp.curve = 2*Math.random()-1;
    return ramp;
}

exports.osc = function(maxP) {
    var osc = new clockwork.Osc();
    osc.randomiseWaveform();
    osc.randomisePitch(maxP);
    return osc;
}

exports.kickSweep = function(T) {
    T = T || Math.random()*44100*3;
    var osc = new clockwork.Osc();
    osc.f = 20 * Math.random();
    osc.pControl = new clockwork.Ramp();
    osc.pControl.y0 = 50 + 50*Math.random();
    osc.pControl.y1 = 0;
    osc.pControl.T = T
    osc.pControl.curve = -Math.random()-1;
    osc.T = T;
    osc.label = "kick sweep";
    return osc;
}
