var ClockworkUnit = require("./ClockworkUnit.js");
var Polynomial = require("./Polynomial.js");
var DB = require("./decibel.js");

function ClockworkPolynomial(T) {
    ClockworkUnit.call(this);

    this.t = 0;
    this.T = T || 44100;
    this.x0 = 0;
    this.x1 = 1;
    this.polynomial = new Polynomial();

    this.A = 1;
    this.dB = 0;
}
ClockworkPolynomial.prototype = Object.create(ClockworkUnit.prototype);
ClockworkPolynomial.prototype.constructor = ClockworkPolynomial;
module.exports = ClockworkPolynomial;

ClockworkPolynomial.prototype.__defineSocket__("A");
ClockworkPolynomial.prototype.__defineSocket__("dB");

ClockworkPolynomial.prototype._tick = function() {
    var y = this.y;
    this.t++;
    return y;
}
ClockworkPolynomial.prototype.__defineGetter__("y", function() {
    var phase = this.t/this.T;
    var x = this.x0 + (this.x1-this.x0)*phase;
    return this.polynomial.y(x) * this.A.y * DB.dbToSf(this.dB.y);
});
ClockworkPolynomial.prototype.normalise = function() {
    this.polynomial.normalise(this.x0, this.x1, 1000);
}

ClockworkPolynomial.prototype.randomise = function(nTerms) {
    this.polynomial.randomise(nTerms);
    this.x0 = Math.random()*100-50;
    this.x1 = Math.random()*100-50;
    this.normalise();
}
