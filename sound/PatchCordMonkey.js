var Osc = require("./ClockworkOsc.js");
var Mixer = require("./ClockworkMixer.js");
var Ramp = require("./ClockworkRamp.js");

function randomOsc() {
    var osc = Osc.randomOsc(100);
    if(Math.random()<0.5)
        osc.pControl = randomModOscGroup();
    osc.A = randomRamp();
    osc.A.y2 = 0;
    return osc;
}
module.exports.randomOsc = randomOsc;

function randomModOsc() {
    var osc = Osc.randomOsc(100);
    if(Math.random()<0.7)
        osc.pControl = randomModOsc();
    if(Math.random()<0.5)
        osc.pControl = randomModOscGroup();
    else if(Math.random()<0.9)
        osc.pControl = randomLFO();
    osc.A = randomRamp();
    return osc;
}
function randomLFO() {
    var osc = Osc.randomLFO();
    osc.A = randomRamp();
    return osc;
}

function randomOscGroup() {
    var mixer = new Mixer();
    var n = 0;
    do {
        mixer.setInput(n, randomOsc());
        mixer.setLevel(n, Math.random());
        n++;
    } while(Math.random()<0.6)
    return mixer;
}
module.exports.randomOscGroup = randomOscGroup;

function randomModOscGroup() {
    var mixer = new Mixer();
    var n = 0;
    do {
        mixer.setInput(n, randomModOsc());
        mixer.setLevel(n, Math.random());
        n++;
    } while(Math.random()<0.6)
    return mixer;
}

function randomRamp(maxT) {
    var T = Math.pow(Math.random(), 5)*44100;
    var y1 = Math.random();
    var y2 = 0;
    return new Ramp(T, y1, y2);
}
