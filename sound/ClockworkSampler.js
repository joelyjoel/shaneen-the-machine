var ClockworkUnit = require("./ClockworkUnit.js");
var Sample = require("./Sample.js");
var Promise = require("promise");

function ClockworkSampler(model) {
    ClockworkUnit.call(this);

    if(model != undefined && model.constructor == String)
        this.sample = new Sample(model);
    else if(model != undefined && model.isSample)
        this.setSample(model);

    this.t = 0;
    this.loop = true;
    this.rate = 1;
    this.detune = 0;
    this.A = 1;

    //this.loop0 = 0;
    //this.loop1 = 0;
    this.loopState = 0;
}
ClockworkSampler.prototype = Object.create(ClockworkUnit.prototype);
ClockworkSampler.prototype.constructor = ClockworkSampler;
module.exports = ClockworkSampler;

ClockworkSampler.prototype.__defineGetter__("y", function() {
    //if(this.t < 0 || this.t >= this.T) return 0;

    return this.sample.yOfSample(this.t) * this.A.y;
});
ClockworkSampler.prototype._tick = function() {
    //console.log("ticking sampler");
    var y = this.y;
    this.t += this.rate.y * Math.pow(2, this.detune.y/12);
    if(this.loopState == 0) {
        // pre loop
        if(this.t >= this.loop0.y && this.t < this.loop1)
            this.loopState = 1;
    } else if(this.loopState == 1) {
        // within loop
        while(this.loop && this.t<this.loop0.y)
            this.t += this.loopD.y;
        while(this.loop && this.t>=this.loop1)
            this.t -= this.loopD.y;
    } else if(this.loopState == 2) {
        // released from loop
    }

    this.tickSockets();

    return y;
}

ClockworkSampler.prototype.__defineSocket__("rate");
ClockworkSampler.prototype.__defineSocket__("detune");
ClockworkSampler.prototype.__defineSocket__("A");
ClockworkSampler.prototype.__defineSocket__("loop0");
ClockworkSampler.prototype.__defineSocket__("loopD");

ClockworkSampler.prototype.__defineGetter__("loop1", function() {
    return this.loop0.y + this.loopD.y;
});
ClockworkSampler.prototype.__defineSetter__("loop1", function(t) {
    this.loopD = t-this.loop0.y;
})

ClockworkSampler.prototype.__defineGetter__("T", function() {
    return this.sample.lengthInSamples;
});
ClockworkSampler.prototype.setSample = function(sample, loop0, loop1) {
    this.sample = sample;
    this.loop0 = loop0 || 0;
    this.loop1 = loop1 || sample.lengthInSamples;
}
ClockworkSampler.prototype.readFile = function(filename, loop0, loop1) {
    var sampler = this;
    return new Promise(function(fulfil, reject) {
        Sample.readFile(filename).then(function(sample) {
            /*sampler.sample = sample;
            sampler.loop0 = loop0 || 0;
            sampler.loop1 = loop1 || sample.lengthInSamples;*/
            sampler.setSample(sample, loop0, loop1);
            fulfil(sampler);
        });
    });
}
ClockworkSampler.prototype.randomiseLoop = function(maxD) {
    var d = (maxD || this.sample.lengthInSamples)*Math.random();
    this.loop0 = Math.random() * (this.sample.lengthInSamples - d);
    this.loopD = d;
}
ClockworkSampler.prototype.randomSkip = function() {
    this.t = Math.random() * this.T;
}
