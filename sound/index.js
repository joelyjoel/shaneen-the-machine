exports.Sample = require("./Sample.js");
exports.Unit = require("./ClockworkUnit.js");
exports.Constant = require("./ClockworkConstant.js")
exports.Osc = require("./ClockworkOsc.js");
exports.Ramp = require("./ClockworkRamp.js");
exports.Sampler = require("./ClockworkSampler.js");
exports.FartMachine = require("./ClockworkFartMachine.js");
exports.Waveshaper = require("./ClockworkWaveshaper.js");
exports.Mixer = require("./ClockworkMixer.js");
exports.Polynomial = require("./ClockworkPolynomial.js");
