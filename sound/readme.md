# CLOCKWORK SYNTH - Readme

The Clockwork Synth is an illogical and inefficient offline dsp library based on my misconceptions of how dsp should work. As I worked on the library I became more and more aware of its naive bad design at a fundamental level, but rather than begin from scratch I persevered in order to discover if this strange system would have any surprising sonic results.

## ClockworkUnit()
Most classes in this directory inherit from the ClockworkUnit class, it lays out a few conventions for passing information between different objects.

.y getter returns the current sample value for the ClockworkUnit object
.tick() advances the unit by one sample an returns the new sample value
.tickSockets() calls .tick() on all of the units which provide signal input to a unit.
.prototype.__defineSocket__() mimics native js convention for defining setters and getters and provides a shortcut for adding signal inputs to a unit.
.prototype.__defineMultiSocket__() is used for adding signal inputs for variable numbers of inputs

## ClockworkConstant()
Returns an flat signal of value y. Used for storing parameter values of other units.

## ClockworkOsc()
