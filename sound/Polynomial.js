function Polynomial() {
    this.terms = [];
}
module.exports = Polynomial;

Polynomial.prototype.y = function(x) {
    var y = 0;
    for(var i=0; i<this.terms.length; i++)
        y += Math.pow(x, i) * this.terms[i];
    return y;
}

Polynomial.prototype.normalise = function(x0, x1, n) {
    // well this doesn't really work!
    var x, y;
    var peak = 0;
    for(var i=0; i<n; i++) {
        x = x0 + i/n * (x1-x0);
        y = this.y(x);
        if(Math.abs(y) > peak)
            peak = Math.abs(y);
    }
    for(var i in this.terms)
        this.terms[i] /= peak;
}

Polynomial.prototype.randomise = function(n) {
    if(n==undefined) {
        n = 0;
        while(Math.random()<0.7)
            n++;
    }
    this.terms = [];
    for(var i=0; i<n; i++)
        this.terms[i] = Math.random();
    return this;
}
