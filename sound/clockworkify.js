var ClockworkUnit = require("./ClockworkUnit.js");
var ClockworkConstant = require("./ClockworkConstant.js");

function clockworkify(thing) {
    if(thing == undefined) return undefined;
    if(typeof thing == "number") {
        return new ClockworkConstant(thing);
    }
    if(thing.isClockwork) return thing;
    else {
        console.log("There has been a problem clockworkifying something");
        return false;
    }
}
module.exports = clockworkify;
