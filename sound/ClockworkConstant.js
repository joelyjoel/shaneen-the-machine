var ClockworkUnit = require("./ClockworkUnit.js");

function ClockworkConstant(value) {
    // a wrapper to allow clockwork synths to use constants
    ClockworkUnit.call(this);
    this.y = value;
}
ClockworkConstant.prototype = Object.create(ClockworkUnit.prototype);
ClockworkConstant.prototype.constructor = ClockworkConstant;
module.exports = ClockworkConstant;

ClockworkConstant.prototype._tick = function() {
    return this.y;
}
