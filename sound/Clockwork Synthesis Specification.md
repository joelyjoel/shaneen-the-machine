#Clockwork Synth spec

This is a set of DSP classes that work in a tick based time system, hence "clockwork".

It is meant for pre-rendering things not for live stuff.

#Standard functions
.tick() function advances the unit along one sample and returns its output
.y returns the output without advancing along a sample


#ClockworkConstant()
#ClockworkUnit()
#ClockworkOsc()
#ClockworkFartMachine();
