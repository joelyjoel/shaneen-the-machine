var ClockworkUnit = require("./ClockworkUnit.js");
var ClockworkOsc = require("./ClockworkOsc.js");

function ClockworkFartMachine() {
    ClockworkUnit.call(this);

    this.osc = new ClockworkOsc();
    this.maxFrequency = 400;
    this.intensity = 0.5; // in milliprobabiltules

    this.randomise();
}
ClockworkFartMachine.prototype = Object.create(ClockworkUnit.prototype);
ClockworkFartMachine.prototype.constructor = ClockworkFartMachine;
module.exports = ClockworkFartMachine;


ClockworkFartMachine.prototype.__defineSocket__("maxFrequency");
ClockworkFartMachine.prototype.__defineSocket__("intensity");

ClockworkFartMachine.prototype.__defineGetter__("y", function() {
    return this.osc.y;
});
ClockworkFartMachine.prototype._tick = function(samps) {
    var y = this.y;

    if(Math.random() < this.intensity.y/1000) {
        this.randomise();
    }

    this.osc.tick();
    this.tickSockets();

    return y;
}
ClockworkFartMachine.prototype.randomise = function() {
    this.osc.f = Math.random()*this.maxFrequency.y;
    this.osc.randomiseVibrato();
    this.osc.randomiseWaveform();
}
