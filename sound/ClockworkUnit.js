// requires at the bottom

function ClockworkUnit() {
    this.tElapsed = 0;
}
module.exports = ClockworkUnit;
ClockworkUnit.prototype.isClockwork = true;



ClockworkUnit.prototype.sockets = [];
ClockworkUnit.prototype.__defineSocket__ = function(name) {
    this.sockets = this.sockets.concat(name);
    this.__defineGetter__(name, function() {
        return this["_"+name];
    });
    this.__defineSetter__(name, function(val) {
        if(typeof val == "number")
            this["_"+name] = new ClockworkConstant(val);
        else
            this["_"+name] = val;
    });
}

ClockworkUnit.prototype.multiSockets = [];
ClockworkUnit.prototype.__defineMultiSocket__ = function(name) {
    this.multiSockets = this.multiSockets.concat(name);

    this.__defineGetter__(name+"s", function() {
        return this["_"+name];
    });
    this.__defineSetter__(name+"s", function(vals) {
        this["_"+name] = [];
        for(var i in vals) {
            if(typeof vals[i] == "number")
                this["_"+name][i] = new ClockworkConstant(vals[i]);
            else
                this["_"+name][i] = vals[i];
        }
    });

    this[name] = function(i) {
        return this["_"+name][i];
    }
    this["set"+name[0].toUpperCase() + name.slice(1)] = function(i, val) {
        if(typeof val == "number")
            this["_"+name][i] = new ClockworkConstant(val);
        else
            this["_"+name][i] = val;
    }
}


ClockworkUnit.prototype.tick = function() {
    this.tElapsed++;
    if(this._tick) return this._tick();
}

ClockworkUnit.prototype.tickUntil = function(t) {
    if(typeof t != "number")
        throw "t must be a number [ ClockworkUnit.prototype.tickUntil() ]";
    var r;
    while(this.tElapsed < t)
        r = this.tick();
    return r;
}

ClockworkUnit.prototype.tickSockets = function() {
    var sockets = this.sockets;
    for(var i in sockets)
        this[sockets[i]].tick();

    var multiSockets = this.multiSockets;
    for(var i in multiSockets) {
        var multiSocket = this[multiSockets[i]+"s"];
        for(var j in multiSocket)
            multiSocket[j].tick();
    }

    return ;
}

var ClockworkConstant = require("./ClockworkConstant.js");
