var ClockworkOsc = require("./ClockworkOsc.js");
var Sample = require("./Sample.js");

module.exports = renderPointTrack = function(track, osc) {
    if(!osc) {
        osc = new ClockworkOsc();
        osc.waveform = "triangle";
        osc.vibrato(5, 0.25);
    }


    var tape = new Sample();
    var bpm = 98;
    var timeUnit = 15/bpm * tape.sampleRate;
    tape.blank(timeUnit * track.d/tape.sampleRate)

    for(var i in track.notes) {
        var note = track.notes[i];
        var tOn = note.t * timeUnit;
        var tOff = note.tOff * timeUnit;
        console.log(note, tOn, tOff);

        if(typeof note.sound != "number")
            continue;

        osc.p = note.sound;
        osc.randomiseWaveform();
        osc.randomiseVibrato(0.5);

        for(var t=tOn; t<tOff; t++) {
            tape.channelData[0][t] += osc.tick() / 8;
        }
    }

    return tape;
}
