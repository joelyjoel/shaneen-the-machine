var ClockworkUnit = require("./ClockworkUnit.js");

function ClockworkOsc(f, waveform, A, equilibrium) {
    ClockworkUnit.call(this);
    this.sampleRate = 44100;

    this.phase = 0;
    console.log("0");
    this.f = f || 440;
    console.log("1");// absolute pitch
    this.pControl = 0;
    console.log("2");
    this.A = A ? A : 1;
    console.log("3");
    this.dB = 0;
    console.log("4");
    this.equilibrium = equilibrium || 0;
    console.log("5");

    this.waveform = waveform ? waveform : "sin";

    console.log("yawhat");
}
ClockworkOsc.prototype = Object.create(ClockworkUnit.prototype);
ClockworkOsc.prototype.constructor = ClockworkOsc;
ClockworkOsc.prototype.isClockworkOsc = true;
module.exports = ClockworkOsc;

var ClockworkConstant = require("./ClockworkConstant.js");
var DB = require("./decibel.js");

ClockworkOsc.prototype.__defineSocket__("p");
ClockworkOsc.prototype.__defineSocket__("pControl");
ClockworkOsc.prototype.__defineSocket__("A");
ClockworkOsc.prototype.__defineSocket__("equilibrium");
ClockworkOsc.prototype.__defineSocket__("dB");

ClockworkOsc.prototype.__defineGetter__("y", function() {
    return this.waveFunction(this.phase) * this.A.y * DB.dbToSf(this.dB.y) + this.equilibrium.y;
});
ClockworkOsc.prototype._tick = function(samps) {
    if(samps == undefined) samps = 1;

    var y = this.y;

    var phasePerSample = this.f/this.sampleRate;
    this.phase += phasePerSample * samps;
    this.phase %= 1;

    this.tickSockets();

    return y;
}

ClockworkOsc.prototype.__defineGetter__("f", function() {
    var p = this.p.y + this.pControl.y;
    return 440 * Math.pow(2, (p-69)/12);
});
ClockworkOsc.prototype.__defineSetter__("f", function(f) {
    //console.log(this.p, f);
    this.p = Math.log(f/440)/Math.log(2) * 12 + 69;
});

ClockworkOsc.prototype.__defineGetter__("waveform", function() {
    return this._waveform;
});
ClockworkOsc.prototype.__defineSetter__("waveform", function(waveform) {
    if(typeof waveform == "string") {
        this.waveFunction = this.waveFunctionRef[waveform];
        this._waveform = waveform;
        return ;
    } else if(typeof waveform == "function") {
        this.waveFunction = waveform;
        this._waveform = "custom";
        return ;
    }
});
ClockworkOsc.prototype.waveFunctionRef = {
    "sin": function(phase) {
        return Math.sin(phase * 2*Math.PI);
    },
    "cos": function(phase) {
        return Math.cos(phase * 2*Math.PI);
    },
    "square": function(phase) {
        return (phase < 0.5) ? 1 : -1;
    },
    "saw": function(phase) {
        return phase*2 - 1;
    },
    "triangle": function(phase) {
        if(phase < 0.25) return phase*4;
        else if(phase < 0.75) return 2 - phase*4;
        else return phase*4 - 4;
    }
}
ClockworkOsc.prototype.setMinMax = function(min, max) {
    this.equilibrium = (min+max)/2;
    this.A = max-min;
}
ClockworkOsc.prototype.randomiseWaveform = function() {
    var list = Object.keys(this.waveFunctionRef);
    this.waveform = list[Math.floor(Math.random()*list.length)];
}
ClockworkOsc.prototype.randomisePitch = function(maxP) {
    maxP = maxP || 100;
    this.p = 17 + Math.random()*(maxP-17);
}
ClockworkOsc.prototype.vibrato = function(f, ammount) {
    if(!this.pControl.isClockworkOsc) {
        this.pControl = new ClockworkOsc();
        this.pControl.waveform = "triangle";
    }
    this.pControl.f = f;
    this.pControl.A = ammount;
}
ClockworkOsc.prototype.randomiseVibrato = function(maxAmmount) {
    if(maxAmmount == undefined) maxAmmount = 12;
    this.vibrato(Math.random()*20, Math.random()*maxAmmount);
}

ClockworkOsc.randomOsc = function(maxP) {
    var osc = new ClockworkOsc();
    osc.randomiseWaveform();
    osc.randomisePitch(maxP);
    return osc;
}
ClockworkOsc.randomLFO = function() {
    var osc = new ClockworkOsc();
    osc.randomiseWaveform();
    //osc.equilibrium = 1;
    osc.A = Math.random();
    osc.f = Math.random()*20;
    return osc;
}
