var argv = require("minimist")(process.argv.slice(2));
var Sample = require("./sound/Sample.js");

var proms = [];
for(var i in argv._) {
    proms.push( Sample.readFile(argv._[i]));
}
Promise.all(proms).then(function(samples) {
    var d = 0;
    var nChannels = 0;
    for(var i in samples) {
        if(samples[i].lengthInSamples > d) {
            d = samples[i].lengthInSamples;
        }
        if(samples[i].numberOfChannels > nChannels)
            nChannels = samples[i].numberOfChannels;
    }
    console.log(d, nChannels);

    var master = new Sample().blank(d, nChannels);
    for(var i in samples) {
        master.mix(samples[i]);
    }

    var chans = master.seperateChannels();

    for(var i in chans) {
        chans[i].normalise();
        var filename = argv._[0] + "." + i + ".wav";
        chans[i].saveWavFile(filename);
    }
})
