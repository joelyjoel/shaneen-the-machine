# §0: Introduction

This document is not intended as comprehensive guide to using the Shaneen Library. Rather it is an explanation of the code for one who wishes to understand the project without reading through over twenty thousand lines of code! It aims to explain the fundamental ideas and principals in a way which is accessible to non-programmers. However some degree of programming literacy will be necessary for understanding the more technical points. Specifically, this document uses basic (object-oriented) programming terms (array, function, class, object, variable, statement, library, etc.) in their technical sense. A thorough description of this project would be impossible without the use of these terms, however a full description of the terms themselves is far beyond the scope of this document. There will also be numerous code extracts and examples in javascript and JSON.

# What is Shaneen?
Shaneen the Machine is a programming project written in node-js.
  1. A library for algorithmic musical composition
  2. A pure node-js digital signal processing engine written from the ground up
  3. A library for algorithmic writing/poetry
  4. A personified digital character and "brand"
