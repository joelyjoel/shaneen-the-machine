# §1: Representing Musical Material

## PointTrack.Note (find code: shaneen/music/PointTrack.js)
This is the class used to represent an individual "note" and fix its position in musical time. Scare-quotes are used because the Note class expands the definition of a note to include chord/mode/tempo changes, song lyrics, or any other piece of musical information that occurs at a fixed point in time, or over some duration. It uses three properties:

  t , a number representing the time (in semiquavers) at which the note begins

  .d , [optional] a number representing the duration of the note in semiquavers. This property may be omitted for sounds which do not sustain (eg percussive sounds) or events which remain valid until interrupted by a similar event (eg Key changes).

  .sound, the value of the note. The sound property can contain a wide range of different objects, representing different types of information. Numbers are assumed to be pitches (using the MIDI standard: 60 = middle C, +/-1 = raise or lower by 1 semitone), arrays of numbers are multiple pitches sounding at once. The sound property can be assigned an object from a number of custom classes that are defined in shaneen to represent harmony symbols, tempo changes, song lyrics etc.

For example, we could define a note of concert-A played on the 3rd beat of bar 1 lasting for 8 semiquavers.

  PointTrack.Note {
    t: 8,
    d: 8,
    sound: 69,
  }

or a triad of C Minor on the 1st beat of bar 1 lasting 1 crotchet:

  PointTrac.Note {
    t: 0,
    d: 4,
    sound: [60, 63, 67],
  }

Any number of Note objects can be combined to represent more or less any music which is representable by a western musical score or MIDI file.

## PointTrack (find code: shaneen/music/PointTrack.js)
If PointTrack.Note represents a single note, then PointTrack itself represents something analogous to a single stave on a score, or a single track in a MIDI file. It is called a PointTrack because it can be thought of as containing a series of time-points encoded with musical information/instructions. A PointTrack object consists of an array (list) of Note objects.

For example, the following PointTrack object represents an ascending scale in the dorian mode.

  PointTrack {
    notes: [
      PointTrack.Note { t:  0, d: 2, sound: 62 },
      PointTrack.Note { t:  2, d: 2, sound: 64 },
      PointTrack.Note { t:  4, d: 2, sound: 65 },
      PointTrack.Note { t:  6, d: 2, sound: 67 },
      PointTrack.Note { t:  8, d: 2, sound: 69 },
      PointTrack.Note { t: 10, d: 2, sound: 71 },
      PointTrack.Note { t: 12, d: 2, sound: 72 },
      PointTrack.Note { t: 14, d: 2, sound: 74 },
    ],
  }

That seems rather longwinded, but this doesn't matter because as these objects are mostly read/written by the computer.

## TrackGroup
We have seen Note objects which represent the individual notes, and PointTrack objects for individual staves. Finally we have the TrackGroup class which can represent the whole score. TrackGroup objects consist of an array of PointTrack objects.

Eg/

  TrackGroup {
    ensembleName: "string quartet"
    tracks: [
      PointTrack { notes: [...], instrumentName: "cello" },
      PointTrack { notes: [...], instrumentName: "viola" },
      PointTrack { notes: [...], instrumentName: "violin" },
      PointTrack { notes: [...], instrumentName: "violin" },
    ]
  }

## Sub-classes and nesting
Both PointTrack and TrackGroup have numerous sub-classes for representing specific channels of musical information. Here are a few:
  - ChordTrack: sub-class of PointTrack, for representing harmony as chord symbols (jazz style or roman numerals).
  - ScaleTrack: sub-class of PointTrack, for representing mode/key changes.
  - DrumTrack: sub-class of PointTrack, for representing percussion/rhythms
  - VoiceTrack: sub-class of PointTrack, for representing melody, however the vanilla form of PointTrack is more frequently used.
  - HarmonyTrack: sub-class of TrackGroup, for combining ScaleTrack and ChordTrack into a more complete representation of harmony.
  - DrumGroup: sub-class of TrackGroup, for multi-percussion/drum-kits

In many contexts, TrackGroups and PointTracks can be treated interchangeably, allowing for identical processes to be applied both to individual tracks or to a group of tracks at once. This also means that a TrackGroup object may contain another TrackGroup object in place of a PointTrack in its track array. This allows for nested structures of musical information. For instance:

  TrackGroup {
    tracks: [

      HarmonyTrack {
        tracks: [
          ScaleTrack { notes: [...] },
          ChordTrack { notes: [...] },
        ],
      },

      PointTrack { notes: [...], instrumentName: "cello" },
      PointTrack { notes: [...], instrumentName: "viola" },
      PointTrack { notes: [...], instrumentName: "violin" },
      PointTrack { notes: [...], instrumentName: "violin" },
    ]
  }

## Methods for manipulating PointTrack/TrackGroup objects
PointTrack, TrackGroup and their sub-classes share a number of methods and properties for basic manipulation of musical material:

  .d [getter/setter]
    Returns/sets the duration of the track/group in semiquavers.

  .sounds [getter/setter]
    Returns a list of all the sound objects in the track/group.
    Passing a list to the setter provides a way to change the sounds in the track irrespective of their rhythmic context.

  .pitches [getter]
    Returns a list of all the fixed numeric pitches in the track/group.

  .soundsAtT(t)
    Returns a list of sounds which are playing at a specific time, t.

  .gamut [getter]
    Returns a list sounds, ommiting duplicates, used by the track/group.

  .octaveProfileStatistics [getter]
    Returns statistics about the frequency with which different pitch classes occur.

  .averagePitch [getter]
    Calculates the mean average pitch of the track/group.

  .monophonic [getter]
    Returns true if no two notes are playing at the same time, otherwise false.

  .polyphonic
    Returns false if no two notes are playing at the same time, otherwise false.

  .Ts [getter/setter]
    Returns a list of times (in semiquavers) where notes begin.
    Use the setter to change the rhythm of the track irrespective of the pitch information.

  .smallestPulse [getter]
    Returns the greatest common subdivision the rhythm of the track/group can be broken into.

  .blank(d[, c])
    Creates an empty track of duration d, and c number of channels (if a group).

  .clear(t0, t1)
    Wipes all musical information between times t0 (inclusively) and t1 (exclusively) leaving a silence. t0 and t1 in semiquavers.

  .emptyFill(d)
    Ensures that the track, and all sub tracks of a group are the same duration with a minimum of d semiquavers.

  .cut(t0, t1)
    Returns a copy of the portion of a track/group between times t0 and t1 (in semiquavers)

  .insert(material, t)
    Inserts a track, material, at time, t semiquavers.

  .insertBlank(d, t)
    Inserts a silence of length d semiquavers at t semiquavers.

  .overwrite(material, t)
    Overwrites a section of the track/group with another track, material.

  .mix(material, t)
    Adds a track, material, to the track/group at t semiquavers, so that both play concurrently.

  .append(material)
    Adds a track, material, at the end of the track/group

  .loop(d),
    Repeat the track/group over and over until its duration is d semiquavers. Cut to fit if d is less than the track/group's duration then make or if a non-integer number of repetitions are required.

  .loopN(n)  
    Repeat the track/group over and over until its duration is n times its original duration. Cut to fit if n is non-integer.

  .transpose(semitones)
    Transposes pitches by the specified number of semitones

  .rotate(timeshift)
    Shift time values by timeshift semiquavers. Times values which afterwards exceed the original duration, or occur before the track begins, pop to the beginning/end a la pacman.

  .stretch(scaleFactor)
    Multiply all time values and durations by a scale factor.

  .stretchToFit(d)
    Multiply all time values and durations so that the new duration of the track is d semiquavers.
