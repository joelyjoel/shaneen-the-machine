# §4: Drums and Rhythms

## Recap: Representation of Rhythms
Like most musical objects in the Shaneen Library, rhythms can be represented using the PointTrack and TrackGroup classes/sub-classes. The PointTrack.Note class contains two floating point numbers .t (time) and .d (duration) which determine when a note begins and how long it lasts.

A previous version of this software represented rhythm using a long list of true/false values, where true indicates a note and false indicates a silence or a continuation of an already sounding note. This system was useful for generating and manipulating rhythms, but lacked the ability to represent rhythms which do not have an underlying pulse or subdivision.

## DrumGroup manipulation
The Shaneen Library has many functions for generating rhythms. However, this chapter will focus on one particular set of functions which are built into the DrumGroup and DrumTrack classes.

The DrumTrack and DrumGroup classes can be imagined as fool-proof interfaces for editing PointTrack/TrackGroup rhythms. They use an analogy of a monkey sitting at a drum sequencer pressing buttons randomly. In order to ensure good results, one must carefully decide which buttons should be exposed to the monkey and which should be hidden.

### "Buttons" (DrumGroup random manipulation methods)
- .poke()
    Imagines a step-sequencer dividing the time into equal subdivisions (the number/size of the subdivisions can be configured to remain the same or change each poke is called). In a DrumGroup the number of monkey-pressable "buttons" will be the number of subdivisions multiplied by the number of tracks. When a button is pressed it first checks whether there is a note starting at the corresponding time, if so it will be removed, otherwise a new note is added.

- .randomSwap()
    Chooses two tracks at random and swaps their contents.

- .randomRotate()
    Chooses a random track and "rotates" it by a random time shift. (See §1 Methods for Manipulating PointTrack/TrackGroup Objects)

- .shadowPoke()
    Chooses two tracks at random, casts a "shadow" of the first track on the second. This is done by making the second track silent whenever the first track is playing.

- .superShadowPoke()
    Chooses one track, casts a shadow on all other tracks in the group using the method described above.

- .makePoke()
    Chooses a random function from another part of the Shaneen Library dedicated to generative rhythms. Puts the resultant rhythm in a random track.

- .omnipoke(n)
    Calls, at random, one of the functions described above. Repeats n times.

These functions work best for making incremental modifications to a small amount of material, usually between 1 and 8 bars long. DrumGroup also defines functions which use the them to generate larger quantities/structures of material:

.evolve(d, maxPokeSize)
  Takes a short DrumGroup object, calls .omnipoke() on it a given number of times then appends it to a new DrumGroup object. This is repeated until the new DrumGroup is d or more semitones long.

.vary(d, maxPokeSize)
  Works in a similar way but forgets about the changes it has made after appending each modified repetition to the new DrumGroup object. The result is many random variations of a similar drum pattern, rather than one constantly mutating one.
