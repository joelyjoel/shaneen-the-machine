# §6 Units and Patches
Note: Throughout this chapter block-caps will be used to refer to inlets and outlets. A notation: [INLETS => OUTLETS] will be used to show the inlets and outlets of a unit/patch.

## Units
All the sub-classes of the Unit class can be found in the directory shaneen/dsp3/components

- Abs [IN => OUT]
    Applies the mathematical operation abs to the IN signal, sending the result to the OUT inlet.

- CircularMotion  [F, RADIUS, CENTRE => OUT]
    Returns a vector signal to OUT following the path of a circle with centre, CENTRE, and radius, RADIUS, completing F cycles per second.

- ConcatChannels  [A, B => OUT]
    Appends the channels of signal B onto signal A.

- CrossFader  [A, B, DIAL => OUT]
    Subclass of SignalCombiner. Cross fades between A and B. DIAL controls the balance of attenuation between the two signals. When DIAL = -1 only A is sent to OUT; when DIAL = 1 only B is sent to out; when DIAL = 0, A and B are mixed together in equal parts.

- DecibelToScale  [IN => OUT]
    Converts a logarithmic decibel signal, IN, to a linear signal, OUT.

- Delay [IN, DELAY => OUT]
    Delays a multichannel signal, IN, by DELAY number of samples. If a DELAY is a multichannel signal, delay each channel in IN by the corresponding channel in DELAY. Uses a write-forwards buffer delay.

- Divide  [A, B => OUT]
    Subclass of SignalCombiner. Divides signal A by B.

- Filter  [IN, F => OUT]
    A Butterworth filter object which can be used as a low-pass, high-pass, band-pass or band-reduce filter. Filters IN using a cutoff/resonance frequency, F.

- Gain  [IN, GAIN => OUT]
    Boosts or attenuates a signal, IN, by GAIN decibels

- GrainCatcher  [IN => null]
    Detects zero crossings, calling the function onGrain() whenever it finds one.

- GreaterThan [IN, VALUE => OUT]
    OUT = 1 when IN is greater than VALUE
    otherwise, OUT = 0

- LessThan  [IN, VALUE => OUT]
    OUT = 1 when IN is less than VALUE,
    otherwise, OUT = 0

- MidiToFrequency [IN => OUT]
    Converts a logarithmic MIDI-number signal to a frequency control signal in Hertz.

- Monitor [IN => null]
    Logs information about IN to the javascript console.

- MonoDelay [IN, DELAY => OUT]
    Delays a monophonic signal, IN, by DELAY samples using a write-forward buffer delay.

- Multiply  [A, B => OUT]
    Subclass of SignalCombiner. Multiplies signal A by signal B.

- Noise [F => OUT]
    Outputs random white noise. F, in Hertz, controls the resample rate.

- Osc [F => OUT]
    A wave-table oscillator class.

- Pan [IN, PAN => OUT]
    Stereo-pans a monophonic signal, IN.
    PAN = -1, pans hard left
    PAN = 0, pans centre
    PAN = 1 pans right

- PickChannel [IN, C => OUT]
    Isolates channel number C of a multichannel signal, IN. Returning a monophonic signal OUT.

- Ramp  [null => OUT]
    A linear envelope class. DEPRECATED! use Shape instead.

- Repeater  [IN => OUT]
    Copies IN to OUT without verbatim. Used only as a structural component in Patch objects.

- Rescale [IN, INLOWER, INUPPER, OUTLOWER, OUTUPPER => OUT]
    Rescales a signal, IN, from the range INLOWER-INUPPER to OUTLOWER-OUTUPPER.

- SampleQ [RATE => OUT]
    Stores a list ("queue") of samples. These may be very short samples for granular synthesis.

- Sampler [RATE, LOOP0, LOOP1 => OUT]
    Plays a sample. Optionally may loop between times LOOP0 and LOOP1.

- SampleRateRedux [IN, AMMOUNT => OUT]
    Mimics the effect of reducing the sampling rate of IN by AMMOUT samples per second.

- SecondsToSamples  [IN => OUT]
    Converts signal of time values in seconds, IN, to a signal of time values in samples, OUT.

- Shape [DURATION, MIN, MAX => OUT]
    An envelope class providing many variously shaped envelopes.

- SignalCombiner  [A, B => OUT]
    A class not useful by itself. It exists so that similar arithmetic/mixing classes may share code.

- Subtract  [A, B => OUT]
    Sub-class of SignalCombiner. Subtracts B from A

- Sum [A, B => OUT]
    Sub-class of SignalCombiner. Returns the sum of A and B.

- VectorMagnitude [IN => OUT]
    Finds the magnitude of a vector signal, IN.

## Patches
All sub-classes of the Patch class can be found in the directory shaneen/dsp3/patches

- ComplexOrbit  [CENTRE, F, R => OUT]
    Makes a vector signal, OUT, by summing randomly generated CircularMotion objects together. CENTRE, F and R can be used to scale all of the CircularMotion at once.

- ComplexOrbitAmbient [F, RADIUS, CENTRE => OUT]
    Spatialises a randomly chosen field recording using ComplexOrbit and Space.

- GrainThing  [IN => OUT] (under development)
    Unfinished. Seperates a signal, IN, into grains by cutting at the zero-crossings. Puts these grains into a SampleQ object to perform some kind of manipulation.

- LFO [F, A, O => OUT]
    A low frequency oscillator with frequency F, amplitude A and equilibrium position O.

- MidiOsc [P => OUT]
    An oscillator which takes a MIDI-number signal, P, rather than frequencies in Hz.

- Mixer [null => OUT]
    Combines any number of signals using a chain of Sum objects.

- OrbittySine [F, SPEED, R, CENTRE => OUT]
    A sine oscillator of frequency F, spatialised by a ComplexOrbit object of frequency SPEED, radius R and centre CENTRE.

- RotatingAmbient [F, R, O => OUT]
    A randomly selected field recording loop, spatialised by a CircularMotion of frequency F, radius R and centre O.

- ScaryPatch  [IN, AMMOUNT => OUT]
    A scary sounding patch which spatialises a signal by using itself as a vector signal.

- SimpleDelay [IN, DELAY, FEEDBACK, DRYWET => OUT]
    A replica of ableton's Simple Delay audio effect.

- SineBoop  [P, DURATION => OUT]
    A sine wave of pitch P with a decay envelope of DURATION seconds.

- SineCloud [F, SPEED, CENTRE => OUT]
    Many OrbittySine object mixed together.

- Space [IN, PLACEMENT => OUT]
    An ambisonix object which spatialises a monophonic signal IN using a vector signal PLACEMENT. Can be used for any speaker configuration. The choice of configuration can be chosen using command line arguments. Applies delay and attenuation to each channel depending on the distance between PLACEMENT and each speakers coordinates.

- SpaceBoop [P, PLACEMENT, DURATION => OUT]
    A SineBoop object spatialized by a Space object.

- SpaceChannel [IN, PLACMENT, SPEAKERPOSITION, DECIBELSPERMETER, SAMPLEDELAYPERMETER => OUT]
    A class used by the Space object to spatialize an individual channel.

- SpaceSampler [PLACEMENT => OUT]
    A Sampler object spatialized by a Space object.

- Speecher [null => OUT]
    A sub-class of TriggerGroup for applying rhythm to synthesised speech. Automatically downloads speech samples from the google-text-to-speech (GTTS) API.

- StereoOsc [P, PCONTROL, GAIN, PAN => OUT]
    An oscillator panned to stereo.

- TriggerGroup [null => OUT]
    A collection of any number of trigger-able objects such as samplers, synthesisers, envelopes etc.
