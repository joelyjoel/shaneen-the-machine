[This document is unfinished]

# §5: DSP Architecture
So far all of the chapters in this document have examined the shaneen/music subdirectory, concerned with algorithmic composition. Now we move our attention towards digital signal processing and algorithmic sound production.

## sound/, dsp/, dsp2/, dsp3
These directories are successive versions of the same code which has been radically revised multiple times. The old versions are kept around for the sake of compatibility. This chapter will only examine the latest version: dsp3/

## DSP3/
DSP3 is an object oriented digital signal processing environment written from the ground up in pure javascript. It is essentially Max MSP but worse in terms of speed, robustness and breadth of application. I wrote it because I needed to be able to do certain things which Max doesn't support.

  1. Integration with the rest of the Shaneen Library
  2. Event/function scheduling
  3. Easy dynamic and greater flexibility in patch construction
  4. Offline rendering

It was also an experiment to benefit my own personal understanding of how a digital signal processing environment works.

### Units
Units are the building blocks of DSP programs, they are similar to objects in Max MSP. The Unit class has many sub-classes, all contained in shaneen/dsp3/components for performing different processes

The active ingredient in a unit is its .tick() function, this contains the code which actually does the signal processing. For example, in the Osc (oscillator) subclass of Unit, the tick function generates a sine wave signal and sends it to the output.

### SignalChunk
In dsp3, signals are represented as a series of SignalChunk objects. These are very short bits of sound (the default is 64 samples, c.1.5ms, long). using chunked signals works out to be much more efficient than processing signals sample by sample (as was the case in previous versions up until dsp3). A SignalChunk object may be monophonic, stereo, or it may contain any number of extra channels.

### Inlets, Outlets
Signals going into a Unit object are called inlets, and those heading out are called outlets. The Inlet and Outlet classes help to manage this flow of information and set-up connections between units. Unit objects can have any number of inlets and outlets, including none at all if desired. The inlet/outlet configuration can be defined in advance (by defining a subclass of Unit) or they can be altered dynamically while the program is running.

In order to enable Inlet and Outlet to share code, they are both sub-classes of the Piglet (not a meaningful name) class.

### Circuit, process-order algorithm
### Patches
### Event/function scheduling
