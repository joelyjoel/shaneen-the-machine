# §2: Harmony

In the previous chapter we have seen a chord can be represented either as a single Note object containing an array of pitches. Or, alternatively, as multiple Note objects which occur simultaneously. However, in each of these cases the pitches have their octave registers fixed.

## HarmonySymbol
The HarmonySymbol class provides a way of representing a collection of pitches abstracted from their octave registers. In the language of post tonal theory this would be known as a 'pitch class set'. This kind of abstraction is also used all the time in tonal theory, where there is a basic conceptual equivalence between all possible versions of, for example, chord IV in C major, regardless of how the chord is voiced.

HarmonySymbol is designed to be applicable to both tonal and atonal music, even going further to allow for applications in microtonal music. In HarmonySymbol, and throughout the Shaneen Library, pitches and pitch classes are represented as floating-point decimals. We could define a HarmonySymbol of a G major triad with just-intoned third and fifth like this:

  HarmonySymbol {
    root: 7,
    colorProfile: [0, 3.863137138648349, 7.019550008653875]
  }

Here the line "root: 7," tells us that the root of the chord is G (counting up 7 semitones from C)

HarmonySymbol has two main sub-classes: Chord and Scale. These are fairly self descriptive. An important note, in this system chords are not defined relative to a scale or mode as in roman numeral notation (where chord ii can be D Minor in C or F minor in Eb). Instead they are defined 'absolutely', as in a lead sheet (C#m7 refers to the same pitch class set irrespective of the key.) The library does include tools for dealing with functional harmony, however, in terms of the way in which data is represented, the function of a chord is derived from the relationship of its absolute pitch within a defined scale, rather than the its absolute pitch being derived from its function and scale. This is necessary to enable consistency in situations where the key is either unknown, ambiguous or unimportant.

A number of methods are defined by the HarmonySymbol class and its sub-classes:

  .octaveProfile (getter)
    Returns an array of the pitch classes of the chord

  .degreeToP(degree)
    Pitches within a HarmonySymbol are indexed by how many pitches the chord or scale contains between the pitch in question and MIDI-zero. This number is called the pitch's 'degree'. This function takes a numeric degree and converts it to a midi pitch number.

  .pToDegree(p)
    Converts a midi pitch back to its degree within the HarmonySymbol. Pitches which are not included in the HarmonySymbol return decimals. This makes it possible to remap modal, chromatic, or microtonal music from one scale to another.

  .degreeShift(p, n)
    Converts a pitch to a degree using .pToDegree, adds n to the result, then puts it back into .degreeToP. Is used extensively by Arp. (see §3)

  .pitchesInRange(lo, hi, hiInclusive)
    Returns a list of all the harmony pitches in the pitch range lo-hi.

  .pitchSetInRange(lo, hi, hiInclusive)
    Returns a probability set of all the harmony pitches in the pitch range lo-hi

  .print [getter/setter]
    Returns or sets the harmony symbol as a shorthand human readable string. Chord objects use jazz notation (eg/ C#m7/E), and Scale objects use an upper or lowercase letter (to denote major or minor respectively) always followed by a colon to distinguish it from a chord (eg/ eb: is E flat minor).

  .containsPitchClasses(listOfPitchClasses)
    Returns true if the HarmonySymbol contains all of a given list of pitch classes, otherwise false.

  .checkPitch(p)
    Checks whether the HarmonySymbol contains a given pitch, p.

Chord only:
  .third
    Returns the third of the Chord as a pitch class. Used to determine whether an unknown Chord is major or minor.

  .inversion
    Calculates the inversion of a Chord. (Chord objects have an additional variable defining the pitch class of the bass note.)

  .allChords() [class function]
    Returns a list of all possible tonal (root position) Chord objects currently defined by the library.

  .allChordsInAllInversions() [class function]
    Returns a list of all possible tonal Chord objects in all possible inversions.

  .fromOctaveProfile(octaveProfile, extraThreshold) [class function]
    Find the tonal chord which best fits a given pitch class set.

  .numeralOrJazz(str) [class function]
    Determines if a given shorthand string representation of a chord uses roman numerals or jazz notation.

  .random() [class function]
    Returns a random tonal chord.

Scale only:
  .pToNumeral(p)
    Takes a pitch number, p, and returns a roman numeral string representing that pitch as a degree of the Scale. Uses accidentals for chromatic pitches.

  .numeralToChord(numeral)
    Takes a roman numeral string representation of a chord and returns a Chord object which represents the pitch class set of that numeral within this Scale.

  .chordToNumeral(chord)
    Takes a Chord object and converts it to a roman numeral string representing the function of that pitch class set in this Scale.

  .random() [class function]
    Returns a random scale or mode.

  .randomMajor() [class function]
    Returns a random major scale.

  .parseScale(str) [class function]
    Parses a string representation of a scale. Returns a Scale object.

  .parseChordOrScale(str) [class function]
    Parses a string representation of a scale or chord (jazz or numeral) and returns either a Scale object or a Chord object.

  .all() [class function]
    Returns a list of all possible Scale objects currently defined by the library.

  .guessFromOctaveProfileStatistics(opStats) [class function]
    Finds the scale which best fits a set of statistics that describe the frequency with which different pitch classes occur in a passage of music.

  .guess(anything) [class function]
    Attempts to guess the underlying scale of a wide range of musical objects.



## HarmonyNetwork
The HarmonyNetwork is essentially an order-1 Markov network class, with additional functions defined for the composition of chord progressions. A Markov network is a statistical structure with a number of states (or nodes) and probabilities defined to describe how to move between states. In a HarmonyNetwork object a set of chords (usually notated as roman numeral strings) are used as nodes, and a table of probabilities describe the chance of moving from any one chord to another. Sequences of chords can be generated by taking a "random walk" around the network, recording each chord/node that you visit.

More advanced processes are also defined. To compose a chord sequence which works well repeated after itself one can use the .loop(n) method, which finds a route through the network that arrives back at where it started after n steps. For sections which link chord progressions together one can use .multiStepSearch(n, pre, nex). This function finds a route across the network from one point (pre) to another (nex) using n steps.

It is also possible to do a kind of arithmetic with the HarmonyNetwork objects themselves. This can be achieved by adding, subtracting or multiplying their probabilities together. In this way one could hypothetical combine the harmonic styles of two different composers (albeit a vastly simplified representation of their styles). However, I have not yet begun experimenting with this theory.

## HarmonyNetwork analysis of Bach
Using the tools described in §1 and §2 it is possible to make a reasonably accurate harmonic analysis of a piece of music automatically. With the following process I built a HarmonyNetwork object based on the way J. S. Bach uses harmony in chorales. This method could easily be applied to any other body of notated music. However Bach chorales work especially well due to their homophonic texture and relative lack of melodic embellishments.

The code for steps 2-8 can be found in shaneen/music/analyseBachChorales.js

1. I downloaded a copy of the Riemenschneider collection of Bach chorales which had been transcribed as MIDI files. [JSBachChorales.net]

2. Using the TrackGroup I wrote a program which opens a MIDI file and parses its contents as a TrackGroup object.

3. The tracks contained in the TrackGroup object are mixed into a single PointTrack object, equivalent to writing a piano reduction on a single stave.

4. PointTrack.prototype.times returns a list of (attack) time values at which notes begin. For each unique time PointTrack.prototype.soundsAtT returns a list of pitch numbers sounding at that time. These are stored in as a list of pitch-sets.

5. The list of pitch-sets is converted to a list of pitch-class-sets.

6. Using Chord.fromOctaveProfile each pitch-class-set is converted to a tonal chord-symbol. Pitch-class-sets which do not fit any of the pre-defined tonal chord-symbols are assigned a null value. Duplicate consecutive chords (of which there will be many, as there is no way to know the harmonic rhythm of the music in advance) are removed.

7. Where there exist consecutive chord-symbols with a reasonable degree of certainty as to their identity, the probability of the progression between the two nodes in the harmony network which represent those chord-symbols is incremented.

8. Steps 2-7 are repeated for all of the chorales in the Riemenschneider, yielding a HarmonyNetwork with the fingerprint of that entire body of music. On a macbook air this process takes around ten minutes to complete.

NOTE: The way I implemented this process ignores the key of each chorale, so the resulting HarmonyNetwork has a peculiar non-functional quality, despite being derived from music which uses exemplary functional harmony.

## Stealing from Bach
The HarmonyNetwork, and the above method, were designed to enable fast and flexible solutions to harmonic problems. Specifically, it was designed to be used inside a max patch for live harmonization of midi input. For the less time-critical problem of composing chord progressions for offline algorithmic composition, a much simpler, arguably less interesting solution has proved more practical: stealing complete chord progressions.

The following method has become my favored way to "generate" chord progressions.

1. Open a random Bach chorale.

2. Use steps 2-6 of the method under the previous subheading to make a symbolic representation of the harmony as Chord objects.

3. Select an uninterrupted sequence of chords according some criteria.

4. If no chord sequence can be found which fits your criteria, choose another random chorale and repeat from step 2.

The fun part of this method is defining the criteria used in step 3. To find a sequence of known length which can loop back on itself, look for a chord which recurs after a certain number of chords and select all the chords in between including the first instance of the duplicate chord. Similarly, two chord progressions can be connected together by selecting the chords found between an instance of the last chord of the first progression and an instance of the first chord of the second progression.

In the Bach chorales, the average length of an uninterrupted sequence of identifiable chords is around 6, and there are very few to be found above 10 chords long. Using less homophonic source material, these numbers are likely to be lower. Therefore it is necessary to piece together a larger harmonic structure by stealing fragments of harmony from many different chorales and building a collage.
