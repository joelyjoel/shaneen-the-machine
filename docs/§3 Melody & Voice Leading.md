# §3: Melody & Voice Leading
This chapter describes two of the main methods for generating melodic lines. Arp is chiefly concerned with generating and decorating complicated melodies, while MelodyProblem is mostly used in voice leading applications.

## Arp
At its core, the Arp class is a notation for describing pitches in relation to their harmonic and melodic context, rather than in reference to some fixed tuning system. It provides methods for converting from this notation into MIDI data and, to some extent, the reverse process.

[aside: In MIDI, pitches are described by counting the number of semitones from C0 (the note 5 octaves below middle-C, also known as MIDI-0). Using this system, middle-C is MIDI-60, and A=440Hz is MIDI-69. A given MIDI pitch will always sound at the same frequency, unless the whole instrument is retuned. MIDI was designed to enable multiple electronic instruments to play in tune together, it has become the established standard to represent note-level musical information inside a computer.]

By contrast, a pitch represented using Arp-notation is not fixed at all, it is entirely dependent on the musical context within which it finds itself. However, importantly, the same Arp notated pitch WILL always yield the same MIDI value if it is placed in the same context. Here are some examples of how pitches can be described using Arp-notation.

<q0     Repeat the previous pitch
h3<     A harmony note, three harmony notes above the next note
s-1>    A note which approaches the next note by step
h0O4    Go to the harmony note closest to the previous note, find the same
        pitch class in MIDI-octave-4

The strings on the left hand side can be embedded in a PointTrack/TrackGroup object as Arp.Q (Arp query) objects in place of a numeric pitches. The Arp class can then take care of gathering information about the harmonic/melodic context and convert the Q's into fixed pitches.

### Operators
Arp notation can be understood as a kind of musical arithmetic, with a set of operators which apply different processes to one pitch, returning another. These operators are notated as a single letter, and a number which effects how the operator is applied. They can be thought of as similar to those of mathematics (+,–,x,÷).

- o_    Octave Skip
          Skip _ octaves upwards. Use negative numbers to skip downwards. Decimals may also be used to jump a fraction of an octave.

- h_    Harmony Skip
          Skip _ degrees in the current sounding chord. If _ is 0 go to the closest chord degree.

- s_    Scale Skip
          Skip _ degrees in the current sounding scale. If _ is 0 go to the closest scale degree.

- q_    Chromatic Skip
          Skip _ semitones.

- O_    Force Octave
          Preserving the pitch-class, go to MIDI-octave-_
          (Note: MIDI-octave-0 = MIDI-numbers 0-11; MIDI-octave-2 = MIDI-numbers 12-23, etc.)

- i_    Force Chord Degree
          Skip to the closest occurrence of a given chord degree. If _ = 0 go to  the root, if _ = 1 go to the third, if _ = 2 go to the fifth etc.

- L     Bass note
          Skip to the closest occurrence of the bass note of the chord.

Q-strings, such as those in the left hand column above, are read left to right, with the output of each operator being fed into into its right-hand neighbor. By default the value of the previous note is fed into the the operator in the Q-string. However this can be changed by the use of modifier characters.

### Modifiers
Unlike with the operators, the order of modifiers (where they occur in a Q-string) is unimportant.

- >   Use next note as reference

- <   Use previous note as reference

- *   "Save me for later"

Many * modifiers can be used in one Q-string. They ensure that the note will be ignored by all Q-strings which don't have at least as many * modifiers. This can be used to ignore ornamentation or to build a large Schenkerian-like structure over a whole piece/passage.

### Inverse process
Complete functionality is provided for converting from Arp-notation into MIDI, however only limited functionality has so far been implemented for the inverse process. A definitive inverse method is a mathematical impossibility because Arp-notation provides many different possible ways to represent the same MIDI data.

So far this remains an experimental feature. However it is worth pursuing as it would enable one to translate melodies from one chord progression into another.

1. Analyze the harmony of the music you wish to translate, either by the method defined in the previous chapter or by some other means.

2. Use an appropriate Arp inverse method to convert from MIDI-notation to Arp-notation.

3. Replace the HarmonyTrack to one representing the new chord progression.

4. Convert the Arp-notated melody back to MIDI-notation using the new chords.



## MelodyProblem
The MelodyProblem class is another approach to melody intended to solve more difficult melodic problems with many possible solutions, such as voice leading and idiomatic instrument writing. Instead of processing individual pitch numbers (as in Arp) MelodyProblem employs a series of “filters” which process a set of probabilities of various possible pitch numbers which may be chosen. This enables us to not only make rules but also define how rigorously they must be adhered to. (Eg/ Choosing a note which brings about parallel fifths may be completely forbidden, whereas choosing a note doubles the third of the chord may be possible, just very unlikely.)

These filters are modular, they can be included or omitted, reordered in any combination and fine tuned with many parameters. The particular set up of the filters is determined by a config object, of which there exist a few ready made. See shaneen/music/MelodyProblem/configs.js

### Filters (see shaneen/music/MelodyProblem/filters.js)
- harmonyNotes
    Only allows chord tones to pass. If there is no chord, all tones are allowed to pass.

- melodicIntervalBellCurve
    Applies gaussian distribution to pitch probabilities depending on the melodic interval they would make with the previous note. If both the previous pitch and the next pitch are known then Gaussian distributions are calculated for both and summed together. The variance of the distribution(s) can be controlled by the config object.

- maxMelodicInterval
    Applies a hard maximum limit to the size of melodic intervals.

- parallels
    Checks for parallel fifths, octaves or any other interval class as defined by the config object.

- banHorizontalUnison
    Forbids the possibility of a two consecutive notes in one track having the same pitch.

- banVerticalUnison
    Forbids the possibility of duplicate pitches occurring in the harmony across all tracks.

- bassNote
    Only permits pitches which have the same pitch class as the bass note of the chord.

- instrumentRange
    Only permits pitches within the range of the instrument specified either in the config object or in the current working PointTrack object.

- instrumentRangeBellCurve
    Applies a gaussian distribution to pitch probabilities across the range of the instrument (favoring the middle of the range) specified either in the config object or in the PointTrack object. This can provide a gentle roll off of probability at the edge of an instruments range, while not excluding the possibility of an unusually high or low note.


- trackAveragePitchBellCurve
    Applies a gaussian distribution to pitch probabilities, matching the mean and variance of the pitches which have been chosen so far in the current PointTrack.

- scoreMelodicIntervals
    Boosts or reduces the probability of selected melodic interval classes as specified in the config object.

- dontDoubleThird
    Forbids doubling the third of the chord.

### Parameters and the config object
It was my intention to expose as many subjective parameters as possible via the config object. As a minimum, the config object contains a list to determine which filters are to be applied in what order, and a variable .initialRange to determine how many pitches to begin with before the filters are applied. The rest of the parameters are mostly self explanatory:
  - maxMelodicInterval  (default: 6)
  - melodicIntervalVariance   (default: 0.1)
  - forbiddenParallels    (default: [0, 7])
  - melodicIntervalScores   (default: {})
  - useIntervalClassesForCheckingParallels    (default: true)
  - instrumentRangeVarianceScaleFactor    (default: 1)
  - trackAveragePitchVariance   (default: 5)

[defaults accurate at the time of writing.]

### Voice-leading
Properly configured and applied, the MelodyProblem class can find convincing solutions to voice-leading problems, including writing parts for a chord progression from scratch. To write parts to a chord progression one must create a MelodyProblem object for each instrument at each chord change. These can then be solved to produce a MIDI-data. Typically some back tracking and re-solving of MelodyProblem objects will be required, for when the algorithm fails by setting itself up for an unsolvable problem it couldn't anticipate. However this is all of this is also taken care of by other methods in the MelodyProblem class.

A shortcut function for SATB choir part writing is to be found in shaneen/music/satbPad.js.
