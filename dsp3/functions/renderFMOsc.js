const FMOsc = require("../patches/FMOsc.js")
const Multiply = require("../components/Multiply.js")
const Mixer = require("../patches/Mixer.js")
const LFO = require("../patches/LFO.js")
const Shape = require("../components/Shape")
const argv = require("minimist")(process.argv.slice(2))
const FrequencyGroup = require("../patches/FrequencyGroup.js")
const StereoDetune = require("../patches/StereoDetune.js")
const pToF = require("../../music/pitch.js").pToF
const config = require("../config.js")
const AHD = require("../components/AHD.js")

const n = (argv.n || 8)
const pConnection = 0.1 // probability of a connection between oscillators
const maxModulationAmmount = 6
const pMix = 1 // probability that an osc ends up in final mix
const maxMixAttenuation = -60
const maxStereoDetune = 0.5

async function renderFMOsc(T, p) {
  var fundamental = pToF(p || 20)
  T /= config.sampleRate
  var freqs = new FrequencyGroup(fundamental)
  freqs.addRandomHarmonics(n, 32, 1)

  var oscs = []
  for(var i in freqs.fOuts) {
    oscs[i] = new FMOsc(
      new StereoDetune(freqs.fOuts[i], Math.random()*maxStereoDetune)
    )
  }

  var mixer = new Mixer()

  for(var i in oscs) {
    for(var j in oscs) {
    //  if(i==j) continue

      if(Math.random() < pConnection)
        oscs[i].addModulator(
          oscs[j],
          //new Multiply(AHD.random(T).trigger(), maxModulationAmmount)
          new Multiply(Shape.randomInRange(T, 0, 1).trigger(), maxModulationAmmount),
        )

      if(Math.random() < pMix)
        mixer.addInput(
          //new Multiply(AHD.random(T).trigger(), maxModulationAmmount)
          new Multiply(Shape.randomDecay(T, 0, 1).trigger(), oscs[i]),
        )
    }
  }

  //var chosenOsc = oscs[Math.floor(Math.random()*oscs.length)]

  var tape = await mixer.OUT.render(T)
  tape.normalise()
  if(tape.lengthInSeconds > 0.4)
    tape.save("FMSound", "FMSounds3")//*/
  return tape
}
module.exports = renderFMOsc
