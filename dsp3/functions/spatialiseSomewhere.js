const Space = require("../patches/Space.js")
const Sampler = require("../components/Sampler.js")
const Sample = require("../Sample.js")
const ComplexOrbit = require("../patches/ComplexOrbit.js")

async function spatialiseSomewhere(sample) {
  if(sample.numberOfChannels > 1) {
    var samps = sample.seperateChannels()
    var outSample = new Sample().blank(sample.lengthInSamples, sample.numberOfChannels)
    for(var i in samps) {
      samps[i] = await spatialiseSomewhere(samps[i])
      outSample.mix(samps[i])
    }
    return outSample
  }


  var sampler = new Sampler()
  sampler.sample = sample
  sampler.trigger()
  var maxDistance = 10

  if(Math.random() < 1)
    var place = [
      (Math.random()*2-1) * maxDistance,
      (Math.random()*2-1) * maxDistance,
    ]
  else
    var place = ComplexOrbit.random(5, 0.5, maxDistance )
  var space = new Space(sampler, place)

  return (await space.OUT.render(sample.lengthInSeconds))
}
module.exports = spatialiseSomewhere
