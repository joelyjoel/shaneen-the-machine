const interpretPointTrack = require("./interpretPointTrack.js")
const renderFMOsc = require("./renderFMOsc.js")

async function fmiffy(track, bpm) {
  return await interpretPointTrack(track, async function(d, p) {
      return await renderFMOsc(d, p)
  }, bpm)
}
module.exports = fmiffy
