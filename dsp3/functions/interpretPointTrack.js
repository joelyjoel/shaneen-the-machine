const Sample = require("../Sample.js")
const config = require("../config.js")

async function interpretPointTrack(track, soundFunction, bpm) {
  var tape = new Sample()

  bpm = bpm || 189

  var samplesPerSemiquaver = config.sampleRate*60 / (bpm*4);

  tape.blank(samplesPerSemiquaver * track.d, 2);

  for(var i in track.notes) {
    console.log("Rendering note", i+"/"+track.notes.length)
    var note = track.notes[i]
    //console.log("note.d * samplesPerSemiquaver:", note.d*samplesPerSemiquaver)
    var sound = await soundFunction(
      note.d * samplesPerSemiquaver,
      note.sound,
      note
    )
    sound.fleshOutChannels(tape.numberOfChannels)
    tape.mix(sound, note.t * samplesPerSemiquaver)
  }

  console.log("numberOfChannels:", tape.numberOfChannels)

  tape.addMeta({bpm: bpm})
  tape.addMetaTags(bpm+"bpm")
  tape.normalise()

  return tape;
}
module.exports = interpretPointTrack
