function checkContinuity(unit, endingPoints, history) {
  if(endingPoints.isUnit)
    endingPoints = [endingPoints]
  if(endingPoints.indexOf(unit) != -1)
    return true

  history = (history || [])
  history.push(unit)
  neighbours = unit.neighbours.filter(u => (history.indexOf(u) == -1))

  for(var i in neighbours) {
    if(checkContinuity(neighbours[i], endingPoints, history))
      return true
  }
  return false
}
module.exports = checkContinuity
