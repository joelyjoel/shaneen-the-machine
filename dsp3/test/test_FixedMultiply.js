const FixedMultiply = require("../components/FixedMultiply.js")
const Osc = require("../components/Osc.js")

var osc1 = new Osc(200)
var mult1 = new FixedMultiply(1/2, osc1)

mult1.OUT.render(1).then((tape) => {
  tape
    .fadeOutSelf()
    .save("testing FixedMultiply")
})
