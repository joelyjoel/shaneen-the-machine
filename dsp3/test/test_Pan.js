const Osc = require("../components/Osc.js")
const Pan = require("../components/Pan.js")
const Multiply = require("../components/Multiply.js")
const render = require("../render.js")

var osc1 = new Osc(200)
var lfo1 = new Osc(3)
var pan1 = new Pan(osc1, new Multiply(lfo1, 0.5))

render(pan1, 1)
.fadeOutSelf()
.save("testing pan (dsp3)")
