const OrbittySine = require("../patches/OrbittySine.js")
const SineCloud = require("../patches/SineCloud.js")
const randomRational = require("../randomRational.js")
const Shape = require("../components/Shape")
const Multiply = require("../components/Multiply.js")

var mySine = new OrbittySine(30)
var cloud1 = new SineCloud(200 + 500*Math.random(), 10)

var shape1 = new Shape("decay", 60)
shape1.trigger()
cloud1.CENTRE = new Multiply(
  [-25, 25],
  shape1,
)

cloud1.addSine(randomRational(16,16, true))
cloud1.addSine(randomRational(16,16, true))
cloud1.addSine(randomRational(16,16, true))
cloud1.addSine(randomRational(16,16, true))
cloud1.addSine(randomRational(16,16, true))

//cloud1.waveform = "square"

cloud1.OUT.render(120).then(function(tape){
  tape
  .normalise()
  .fadeOutSelf()
  .save("thingy")
})
