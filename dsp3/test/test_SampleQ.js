const SampleQ = require("../components/SampleQ.js")
const Sample = require("../Sample.js")
const textToSpeech = require("../../dsp/textToSpeech.js")
const render = require("../render.js")
const config = require("../config.js")
const SampleRateRedux = require("../components/SampleRateRedux.js")

const rate = config.rate || 1000 //* Math.random()
const text = config._[0] || "who are these people who look like clouds"
const T = config.T || 30


var sq = new SampleQ().gtts(text, "en-uk")
sq.playSample(0)

sq.selectionLogic = function(i) {
  this.wait(this.playingSample.lengthInSamples*2, i+1)
}

render(sq, T).then(function(tape) {
  tape
  .normalise()
  .fadeOutSelf()
  .save(text)
})
