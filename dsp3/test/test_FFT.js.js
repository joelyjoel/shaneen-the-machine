const Osc = require("../components/Osc.js")
const Sampler = require("../components/Sampler.js")
const Hopper = require("../components/spectral/Hopper.js")
const Windower = require("../components/spectral/Windower.js")
const FFT = require("../components/spectral/FFT.js")
const IFFT = require("../components/spectral/IFFT.js")
const UnHopper = require("../components/spectral/UnHopper.js")
const config = require("../config.js")
const ReChunk = require("../components/spectral/ReChunk.js")

const windowSize = 4096*4
const hopSize = Math.round(windowSize/4)

var osc1 =new Osc(100)
var hopper1 = new Hopper(hopSize, windowSize)
hopper1.IN = osc1
console.log("standardChunkSize:", osc1.tickInterval)
console.log("hopper tickInterval", hopper1.tickInterval, hopper1.OUT.chunkSize)

var windower1 = new Windower(windowSize, "hamming", hopSize)
windower1.IN = hopper1.OUT

var fft1 = new FFT(windowSize, hopSize)
var ifft1 = new IFFT(windowSize, hopSize)

fft1.IN = windower1.OUT
ifft1.IN = fft1.OUT

var windower2 = new Windower(windowSize, "hamming", hopSize)
windower2.IN = ifft1.OUT

var unhopper1 = new UnHopper(hopSize, windowSize)
unhopper1.IN = windower2.OUT

var rechunk1 = new ReChunk(hopSize, config.standardChunkSize)
rechunk1.IN = unhopper1.OUT

rechunk1.OUT.render(1).then(tape => {
  tape
    .normalise()
    .save("testing hopper", "debugging")
})
