const Osc = require("../components/Osc.js")
const render = require("../render.js")
const Ramp = require("../components/Ramp.js")
const Multiply = require("../components/Multiply")
const Gain = require("../components/Gain.js")
const {sampleRate} = require("../config.js")

var osc1 = new Osc(300)
osc1.waveTable = Osc.triangleTable
var gain1 = new Gain()
gain1.IN = osc1
gain1.schedule(0, function() {
  this.GAIN = gainArray[(gainI++)%gainArray.length]
  return 0.1
})

var gainI = 0
var gainArray = [0, -12, -6]

osc1.schedule(0, function() {
  this.F = 275 + 250*Math.sin(2*Math.PI * 4.01* this.circuit.clock / sampleRate)
  return 0.05
})


render(gain1.OUT, 60)
.normalise()
.fadeOutSelf()
.save("testing events")
