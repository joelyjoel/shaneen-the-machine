const LFO = require("../patches/LFO.js")
const render = require("../render.js")

var lfo1 = new LFO()
render(lfo1.OUT)
.save("testing LFO")
