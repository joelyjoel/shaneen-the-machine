const MultiTapDelay = require("../patches/MultiTapDelay.js")
const Sampler = require("../components/Sampler.js")
const Mixer = require("../patches/Mixer.js")

var sampler1 = new Sampler().choose()
sampler1.trigger()

var delay1 = new MultiTapDelay(2, 1, sampler1)

var mixer = new Mixer()

for(var i=0; i<5; i++)
  mixer.addInput(delay1.addFeedback(Math.random(), Math.random()*0.5, -Math.random()))

mixer.OUT.render(15).then((tape) => {
  tape
    .normalise()
    .save("Testing MultiTapDelay", "Dumb Tests")
})
