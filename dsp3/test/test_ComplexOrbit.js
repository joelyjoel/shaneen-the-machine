const ComplexOrbit = require("../patches/ComplexOrbit.js")
const Osc = require("../components/Osc.js")
const Space = require("../patches/Space.js")
const Monitor = require("../components/Monitor.js")
const Circuit = require("../Circuit.js")
const SampleQ = require("../components/SampleQ.js")

var orbit = ComplexOrbit.random(
  5,
  5,
  0.1,
)
var sampler = new SampleQ().ambient(0.5)
var space = new Space(sampler.OUT, orbit.OUT)

//console.log(orbit)

//new Monitor(orbit.circs[1])

space.OUT.render(10).then(function(sample) {
   sample
   .normalise()
   .save("testing ComplexOrbit.js")
})
//*/
