const Sampler = require("../components/Sampler.js")
const render = require("../render.js")


var sf = (Math.random()*2-1) * 0.1 + 1
var times = []
times[0] = Math.random()
for(var i=1; i<10; i++) {
  times[i] = times[i-1] * sf
  if(times[i] < 0.05)
    break
  if(times[i] > 1)
    break
}

times[0] = 0
for(var i=1; i<times.length; i++) {
  times[i] += times[i-1]
}

console.log("times:", times)
//times = times.reverse()

sampler = Sampler.gtts("I do like to be inside this fancy computer", "random")
//console.log(sampler)
//sampler.trigger()
//  sampler.t0 = sampler.lengthInSamples/2
sampler.schedule(
  times.slice(0, times.length-1),
  function() {
    //this.t0 = this.lengthInSamples * (Math.random())
    this.trigger()
    return times[times.length-1]
  }
)

console.log(sampler.label)

render(sampler.OUT, 10).then(function(tape) {
  tape
  .normalise()
  .fadeOutSelf()
  .save(sampler.label+  " (processed)")
})
