const Osc = require("../components/Osc.js")
const ConcatChannels = require("../components/ConcatChannels.js")
const render = require("../render.js")

var osc1 = new Osc(256)
var osc2 = new Osc(320)

var concat1 = new ConcatChannels(osc1, osc2)

render(concat1, 1)
.save("testing ConcatChannels")
