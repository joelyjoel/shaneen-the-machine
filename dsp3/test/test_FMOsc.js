const FMOsc = require("../patches/FMOsc.js")
const Multiply = require("../components/Multiply.js")

var osc1 = new FMOsc(200)

osc1.addModulator(new FMOsc(250), 12)

console.log(osc1)


osc1.OUT.render(5).then(function(tape) {
  tape.normalise()
  .fadeOutSelf()
  .save("Testing FMOsc")
})
