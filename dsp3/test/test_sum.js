const Osc = require("../components/Osc.js")
const Sum = require("../components/Sum.js")
const render = require("../render.js")

const argv = require("minimist")(process.argv.slice(2))
const T = argv.T || 1

var osc1 = new Osc(200)
var osc2 = new Osc(330)
var osc3 = new Osc(445)
var sum1 = Sum.many([osc1, osc2, osc3])

console.log(sum1)

render(sum1.OUT, T)
.normalise()
.fadeOutSelf()
.save("testing dsp3 sum")
