const FMSynth = require("../patches/OldFMSynth.js")

var synth = new FMSynth(200)
synth.addOsc(1, 1)
synth.addOsc(2)
synth.addOsc(3)
synth.addOsc(4)
synth.addOsc(5)

var matrix = []
for(var i=0; i<5; i++) {
  matrix[i] = []
  for(var j=0; j<5; j++)
    matrix[i][j] = Math.random() * 4
}


synth.applyModulationMatrix(matrix)

synth.OUT.render(3).then(function(tape) {
  tape
    .normalise()
    .fadeOutSelf()
    .save("testing FMSynth")
})
