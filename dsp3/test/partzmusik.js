const interpretPointTrack = require("../functions/interpretPointTrack.js")
const logic = require("logicalexpressions")
const make_logicRhythm = require("../../music/make_logicRhythm.js")
const renderFMOsc = require("../functions/renderFMOsc.js")
const Sample = require("../Sample.js")
const spatialiseSomewhere = require("../functions/spatialiseSomewhere.js")
const config = require("../config.js")

async function make4Bar() {
  do {
    var seed = new logic.Seed(5)
    seed.fastForward(Math.floor(Math.random()*100000000))
    console.log(seed.expression().print())
    var rhythm = make_logicRhythm(seed.expression())
  } while(false && rhythm.d > 8 || rhythm.notes.length == 0)


  rhythm = rhythm.loopN(4)
  rhythm.rollRandomNote(3)
  rhythm.rollRandomNote(3)
  for(var i=0; i+1<rhythm.notes.length; i++) {
    rhythm.notes[i].tOff = rhythm.notes[i+1].t;
    rhythm.notes[i].sound = Math.random()*60 + -10
  }
  rhythm.notes[i].tOff = rhythm.d
  rhythm.notes[i].sound = Math.random()*60 + -10

  for(var i in rhythm.notes) {
    //rhythm.notes[i].d *= 0.5 + 3 * Math.random()
  }


  return await interpretPointTrack(rhythm, async function(d, p) {

    var sound = await makeSound(d, p)

  //  sound = (await spatialiseSomewhere(sound))//.normalise()
    //sound.normalise()
  //  sound.fadeInSelf(0.015)
  //  sound.fadeOutSelf(0.015)
    return sound

  }, 150, true)
}

async function makeSound(d, p) {
  // generate sounds here!
  if(Math.random() < 00)
    return (await Sample.choose("drum"))
  else if(Math.random() < 0.5)
    return await renderFMOsc(d, p)
  else {
    var samp = await Sample.choose("vocal")
    var t0 = Math.floor(Math.random()*(samp.lengthInSamples-d))
    samp = samp.cut(t0, t0+d)
    return samp;
  }
}


async function makeStem() {
  var sections = []
  for(var i=0; i<4; i++)
    sections.push((await make4Bar()).loop(12.8 * config.sampleRate))

  console.log(sections.map((samp) => { return samp.lengthInSeconds}))
  return Sample.concat(sections, 0)
}

makeStem().then(function(tape) {
  tape
    .normalise()
    .save("Prrtz", "partzmusik2")
})
