const Osc = require("../components/Osc.js")
const Sampler = require("../components/Sampler.js")
const Hopper = require("../components/spectral/Hopper.js")

var osc1 = new Osc(100)
var hopper1 = new Hopper(4096/2, 4096)
hopper1.IN = osc1
console.log("standardChunkSize:", osc1.tickInterval)
console.log("hopper tickInterval", hopper1.tickInterval, hopper1.OUT.chunkSize)

hopper1.OUT.render(1).then(tape => {
  tape.normalise()
  .save("testing hopper", "debugging")
})
