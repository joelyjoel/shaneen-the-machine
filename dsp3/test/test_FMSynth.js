const FMSynth = require("../patches/FMSynth.js")
const unDusp = require("../unDusp")
const make_melody = require("../../music/make_melody")



var melody = make_melody.tranceyThing("C``` Am``` F``` G7```").transpose(-24).loopN(8)
melody.bpm = 160

var T = melody.dInSeconds

var seed = FMSynth.randomSeed({
  f: 100,
  duration: 0.25,
  nOscs: 8,
  pConnection: 0.3,
  maxModulationAmmount: 6,
  pMix: 0.5,
  maxStereoDetune: 2
})
seed.mod = unDusp("A"+(T))
var synth = new FMSynth(seed)

/*var times = []
for(var t=0.3; t<T; t+= Math.random())
  times.push(t)
synth.schedule(times, function() {
  this.F = Math.random()*200
  this.trigger()
})*/


synth.scheduleTrack(melody, 160)


console.log(synth.seed)

synth.OUT.render(melody.dInSeconds).then((tape) => {
  tape
    .normalise()
    .fadeOutSelf()
    .save("Testing FMSynth", "dumb tests")
})
