const FixedDelay = require("../components/FixedDelay.js")
const CombFilter = require("../components/CombFilter.js")
const Sampler = require("../components/Sampler.js")

var sampler1 = new Sampler().choose("vocal")
sampler1.trigger()
var comb1 = new CombFilter(1/440, 0.9)
comb1.IN = sampler1

comb1.OUT.render(5).then((tape) => {
  tape
    .normalise()
    .save("Testing CombFilter", "dumb tests")
})
