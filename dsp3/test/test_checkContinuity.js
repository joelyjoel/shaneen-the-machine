const Unit = require("../Unit.js")
const checkContinuity = require("../checkContinuity.js")


var unit1 = new Unit()
unit1.addInlet("in")
unit1.addOutlet("out")

var unit2 = new Unit()
unit2.addInlet("in")
unit2.addOutlet("out")

var unit3 = new Unit()
unit3.addInlet("in")
unit3.addOutlet("out")

var unit4 = new Unit()
unit4.addOutlet("out")

unit2.IN = unit1
unit3.IN = unit2

console.log(checkContinuity(unit1, unit3))

unit2.IN = unit4
console.log(checkContinuity(unit1, unit4, [unit2]))
