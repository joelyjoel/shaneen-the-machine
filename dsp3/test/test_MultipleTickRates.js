const Unit = require("../Unit.js")

var unit1 = new Unit()
var unit2 = new Unit()
unit1._tick = function() {
  console.log("unit1 ticking")
}
unit2._tick = function() {
  console.log("unit2 ticking")
}

unit2.tickInterval = unit1.tickInterval/2
unit2.chainAfter(unit1)
unit2.addOutlet("out")
unit2.OUT.render(1)
