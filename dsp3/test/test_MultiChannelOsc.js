const MultiChannelOsc = require("../components/MultiChannelOsc.js")

var osc1 = new MultiChannelOsc([440, 330])

osc1.OUT.render(5).then(function(tape) {
  tape
    .normalise()
    .fadeOutSelf()
    .save("testing MultiChannelOsc")
})
