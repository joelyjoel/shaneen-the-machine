const construct = require("../construct/constructExpressions.js")
const argv = require("minimist")(process.argv.slice(2))
const dusp = require("../dusp")

var objs = construct(argv._[0] || "[Osc]")
var lastObj = objs[objs.length-1]

var duspVersion = dusp(lastObj)
console.log(duspVersion)
