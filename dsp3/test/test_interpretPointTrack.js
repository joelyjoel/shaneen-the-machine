const interpretPointTrack = require("../functions/interpretPointTrack.js")
const logic = require("logicalexpressions")
const make_logicRhythm = require("../../music/make_logicRhythm.js")
const renderFMOsc = require("../functions/renderFMOsc.js")
const Sample = require("../Sample.js")
const {sampleRate} = require("../config.js")

var seed = new logic.Seed(5)
seed.fastForward(Math.floor(Math.random()*100000))

console.log(seed.expression().print())
var rhythm = make_logicRhythm(seed.expression())


rhythm = rhythm.loopN(4)
for(var i=0; i+1<rhythm.notes.length; i++) {
  rhythm.notes[i].tOff = rhythm.notes[i+1].t;
  rhythm.notes[i].sound = Math.random()*60 + 10
}

interpretPointTrack(rhythm, async function(d, p) {

  // generate sounds here!
  if(Math.random() < 0.1)
    return (await Sample.choose("drum"))
  else if(Math.random() < 0.9)
    return await renderFMOsc(d, p)
  else {
    var samp = await Sample.choose("vocal")
    var t0 = Math.floor(Math.random()*(samp.lengthInSamples-d))
    samp = samp.cut(t0, t0+d)
    return samp;
  }

}).then(function(tape) {
  tape = tape.loop(64 * sampleRate)
  tape.save(seed.expression().print() + " with samples", "Waddesdon 2.0");
})
