const quick = require("../quick.js")
const Osc = require("../components/Osc.js")

var osc1 = new Osc(200)
console.log(quick.clipAbove(2,1))

var clippedOsc = quick.clipAbove(osc1, 0.5)
clippedOsc.render(2).then(tape => {
  tape.save("testing clipping", "dumb tests")
})
