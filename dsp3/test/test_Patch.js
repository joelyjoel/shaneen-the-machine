const Patch = require("../Patch.js")
const Osc = require("../components/Osc.js")
const Sum = require("../components/Sum.js")
const render = require("../render.js")

var osc1 = new Osc(440)
var osc2 = new Osc(330)
var sum1 = new Sum(osc1, osc2)

var patch1 = new Patch()
patch1.aliasInlet(osc1.F)
patch1.aliasInlet(osc2.F)
patch1.aliasOutlet(sum1)


console.log(patch1)

render(patch1, 1)
.normalise()
.fadeOutSelf()
.save("testing patch")
