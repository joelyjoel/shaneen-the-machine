const create = require("../create.js")
const render = require("../render.js")

var unitMap = create([
  "Osc 'Osc1' (F:300)",
  "Osc 'Osc2' (F:400)",
  "Sum 'Sum1' (A: Osc1.OUT, B: Osc2.OUT)",
])

var sum1 = unitMap["Sum1"]
console.log(sum1)
render(sum1).then(tape =>
  tape
    .normalise()
    .fadeOutSelf()
    .save()
)
