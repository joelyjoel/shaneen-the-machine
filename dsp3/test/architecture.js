const Unit = require("../Unit.js")
const Circuit = require("../Circuit.js")

var unit1 = new Unit()
var unit2 = new Unit()
var unit3 = new Unit()
var unit4 = new Unit()

unit1.addOutlet("out")
unit2.addInlet("in")
unit2.addInlet("in2")
unit2.addOutlet("out")


unit2.IN.connect(unit1.OUT)
//unit2.IN2.connect(unit1.OUT)

//console.log(unit2.computeProcessIndex())

var circuit = new Circuit()
circuit.add(unit1)

console.log(circuit)
