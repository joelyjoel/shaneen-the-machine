const SineBoop = require("../patches/SineBoop.js")
const render = require("../render.js")

var boop1 = SineBoop.randomTwinkle(0.25).trigger()


render(boop1, boop1.DURATION.constant).then(function(tape) {
  tape
  .saveResource("Twinkle", "samples/boops/short and high 2")
})
