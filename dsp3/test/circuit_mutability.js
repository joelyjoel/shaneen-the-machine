const Unit = require("../Unit.js")


var unit1 = new Unit()
unit1.addInlet("in")
unit1.addOutlet("out")

var unit2 = new Unit()
unit2.addInlet("in")
unit2.addOutlet("out")

var unit3 = new Unit()
unit3.addInlet("in")
unit3.addOutlet("out")

var unit4 = new Unit()
unit4.addOutlet("out")

unit2.IN = unit1
unit3.IN = unit2
//unit1.IN = unit3

unit3.markAsNecessary()


var units = [unit1, unit2, unit3, unit4]
function check(units) {
  /*for(var i in units) {
    units[i].computeStepsToNecessity()
    console.log("computed", units[i].label, units.map(unit => unit.label + " " + unit.stepsToNecessity))
  }*/
  console.log(units.map(unit => unit.label + " " + unit.stepsToNecessity))
}
unit1.computeStepsToNecessity()
check(units)

console.log("switching unit 1 for unit 4")
unit2.IN = unit4
unit4.computeStepsToNecessity()
check(units)

console.log("unit2.OUT.disconnect()")
unit2.OUT.disconnect()
console.log(unit2.OUT.connections)
check(units)
