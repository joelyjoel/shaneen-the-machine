const FMSynth = require("../patches/FMSynth.js")
const unDusp = require("../unDusp")
const make_melody = require("../../music/make_melody")



var melody = make_melody.tranceyThing("C``` Am``` F``` G7```").transpose(-24).loopN(8)
melody.bpm = 160

var T = melody.dInSeconds

var seed = FMSynth.randomSeed({
  f: 25,
  duration: T,
  nOscs: 8,
  pConnection: 0.7,
  maxModulationAmmount: 6,
  pMix: 1,
  maxStereoDetune: 0
})
seed.mod = 3
var synth = new FMSynth(seed)

/*var times = []
for(var t=0.3; t<T; t+= Math.random())
  times.push(t)
synth.schedule(times, function() {
  this.F = Math.random()*200
  this.trigger()
})*/


synth.scheduleTrack(melody, 160)


console.log(synth.seed)

synth.OUT.render(10).then((tape) => {
  tape
    .normalise()
    .fadeOutSelf()
    .save("Testing wormSeed for FMSynth", "debugging")
})
