const FixedDelay = require("../components/FixedDelay.js")
const Sampler = require("../components/Sampler.js")
const config = require("../config.js")

var sampler1 = new Sampler().choose("vocal")
sampler1.trigger()
var delay1 = new FixedDelay(config.sampleRate)
delay1.IN = sampler1

delay1.OUT.render(5).then((tape) => {
  tape
    .normalise()
    .save("Testing Fixed Delay", "dumb tests")
})
