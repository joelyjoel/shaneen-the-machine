const CircleBufferReader = require("../components/CircleBufferReader.js")
const CircleBufferWriter = require("../components/CircleBufferWriter.js")
const CircleBuffer = require("../CircleBuffer.js")
const Sampler = require("../components/Sampler.js")

var buffer1 = new CircleBuffer(2, 2)

var reader1 = new CircleBufferReader(buffer1, 1)
reader1.postWipe = true
var writer1 = new CircleBufferWriter(buffer1)

var sampler1 = new Sampler().choose("vocal")
sampler1.trigger()

writer1.IN = sampler1

/*reader1.addInlet("chain")
writer1.addOutlet("chain")
reader1.CHAIN = writer1.CHAIN*/

writer1.chain(reader1)

reader1.OUT.render(5).then(function(tape) {
  console.log(tape.meta)
  tape
    .save("Testing Circle Buffer", "Dumb Tests")
})
