const Reverb = require("../patches/Reverb.js")
const Sampler = require("../components/Sampler.js")

var samp1 = new Sampler().choose("vocal")

var reverb = new Reverb()
reverb.IN = samp1.OUT

reverb.OUT.render(5).then((tape) => {
  tape
    .normalise()
    .save("Testing Reverb", "dumb tests")
})
