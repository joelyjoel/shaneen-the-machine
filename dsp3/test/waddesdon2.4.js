const interpretPointTrack = require("../functions/interpretPointTrack.js")
const logic = require("logicalexpressions")
const make_logicRhythm = require("../../music/make_logicRhythm.js")
const renderFMOsc = require("../functions/renderFMOsc.js")
const Sample = require("../Sample.js")
const config = require("../config.js")

async function make4Bar() {
  do {
    var seed = new logic.Seed(5)
    seed.fastForward(Math.floor(Math.random()*100000))
    console.log(seed.expression().print())
    var rhythm = make_logicRhythm(seed.expression())
  } while(rhythm.d > 8 || rhythm.notes.length == 0)


  rhythm = rhythm.loopN(2)
  for(var i=0; i+1<rhythm.notes.length; i++) {
    rhythm.notes[i].tOff = rhythm.notes[i+1].t;
    rhythm.notes[i].sound = Math.random()*60 + 10
  }
  rhythm.notes[i].tOff = rhythm.d
  rhythm.notes[i].sound = Math.random()*60 + 10

  return await interpretPointTrack(rhythm, async function(d, p) {

    // generate sounds here!
    if(Math.random() < 0.1)
      return (await Sample.choose("drum"))
    else if(Math.random() < 0.9)
      return await renderFMOsc(d, p)
    else {
      var samp = await Sample.choose("vocal")
      var t0 = Math.floor(Math.random()*(samp.lengthInSamples-d))
      samp = samp.cut(t0, t0+d)
      return samp;
    }

  }, 150)
}


async function makeStem() {
  var sections = []
  for(var i=0; i<47; i++)
    sections.push((await make4Bar()).loop(12.8 * config.sampleRate))

  console.log(sections.map((samp) => { return samp.lengthInSeconds}))
  return Sample.concat(sections, 0)
}

makeStem().then(function(tape) {
  tape
    .save("Waddesdon Stem", "Waddesdon 2.4")
})
