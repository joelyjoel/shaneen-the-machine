const ScaryPatch = require("../patches/ScaryPatch.js")
const Sampler = require("../components/Sampler.js")

var sampler = new Sampler().gtts("Who are these people who look like clouds", "en-uk")
sampler.trigger()
sampler.RATE = 2/3
var scary = new ScaryPatch(sampler, 10)

scary.OUT.render(5).then((tape)=> {
  tape
  .save("testing ScaryPatch")
})
