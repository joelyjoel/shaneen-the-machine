const RenderStream = require("../RenderStream.js")
const Osc = require("../components/Osc.js")
const Noise = require("../components/Noise.js")
const argv = require("minimist")(process.argv.slice(2))
const quick = require("../quick")
const fs = require("fs")

const Speaker = require("speaker")
const speaker = new Speaker({
  channels:1,
  bitDepth:32,
})

var noise = new Noise()
var stream1 = new RenderStream(noise.OUT)
stream1.pipeWav(fs.createWriteStream("./testing stream.wav"))
//stream1.pipe(speaker)
