const Osc = require("../components/Osc.js")
const Sampler = require("../components/Sampler.js")
const Hopper = require("../components/spectral/Hopper.js")
const Windower = require("../components/spectral/Windower.js")
const FFT = require("../components/spectral/FFT.js")
const IFFT = require("../components/spectral/IFFT.js")
const UnHopper = require("../components/spectral/UnHopper.js")
const config = require("../config.js")
const ReChunk = require("../components/spectral/ReChunk.js")
const undusp = require("../undusp")

const HardBandPass = require("../patches/HardBandPass.js")

const windowSize = config.fft.windowSize
const hopSize = Math.round(config.fft.hopSize)

var osc1 = new Sampler().choose("post-waddesdon").trigger()//new Osc(250)
osc1.loop()
var hopper1 = new Hopper(hopSize, windowSize)
hopper1.IN = osc1
var windower1 = new Windower(windowSize, "hamming", hopSize)
windower1.IN = hopper1.OUT
var fft1 = new FFT(windowSize, hopSize)
fft1.IN = windower1.OUT

var bp1 = new HardBandPass(fft1.OUT, 500, 1000)

var ifft1 = new IFFT(windowSize, hopSize)
ifft1.IN = bp1.OUT

var windower2 = new Windower(windowSize, "hamming", hopSize)
windower2.IN = ifft1.OUT

var unhopper1 = new UnHopper(hopSize, windowSize)
unhopper1.IN = windower2.OUT

var rechunk1 = new ReChunk(hopSize, config.standardChunkSize)
rechunk1.IN = unhopper1.OUT

rechunk1.OUT.render(40).then(tape => {
  tape
    .normalise()
    .save("testing HardBandPass", "debugging")
})
