const FixedDelay = require("../components/FixedDelay.js")
const AllPass = require("../components/AllPass.js")
const Sampler = require("../components/Sampler.js")
const CrossFader = require("../components/CrossFader.js")
const PickChannel = require("../components/PickChannel.js")
const ConcatChannels = require("../components/ConcatChannels.js")
const Filter = require("../components/Filter.js")
const Mixer = require("../patches/Mixer.js")
const PolarityInvert = require("../components/PolarityInvert.js")

var sampler1 = new Sampler().choose("FMSounds3")
sampler1.trigger()

var filter1 = new Filter()
filter1.F = 22000
filter1.IN = sampler1.OUT

const maxDelayTime = 1/1000
const maxFeedbackGain = 0.7
const nFilters = Math.ceil(Math.random()*100)
const maxMixerGain = 1/nFilters

var left = AllPass.manyRandomInSeries(nFilters, maxDelayTime, maxFeedbackGain)
left.list[0].IN = new PickChannel(filter1, 0)


var right = AllPass.manyRandomInSeries(nFilters, maxDelayTime, maxFeedbackGain)
right.list[0].IN = new PickChannel(filter1, 1)

/*var leftMixer = new Mixer()
for(var i in left.list)
  leftMixer.addMultiplied(left.list[i], (Math.random()*2-1) * maxMixerGain)
var rightMixer = new Mixer()
for(var i in right.list)
  rightMixer.addMultiplied(left.list[i], (Math.random()*2-1) * maxMixerGain)*/

var stereo = new ConcatChannels(left.OUT, right.OUT)


var dryWet = new CrossFader(new PolarityInvert(sampler1), stereo, 1)

dryWet.OUT.render(5).then((tape) => {
  tape
    .normalise()
    .fadeOutSelf()
    .save(nFilters + " all pass filters in series", "ambient boops")
}).catch(function(e) {
  console.log("!!", e)
})

//console.log("Reverb time", all1.totalReverbTime)
