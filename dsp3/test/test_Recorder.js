const Osc = require("../components/Osc.js")
const Recorder = require("../components/Recorder.js")
const Circuit = require("../Circuit.js")
const {sampleRate} = require("../config.js")

var osc1 = new Osc(300)
var recorder1 = new Recorder(osc1)

var circuit = new Circuit(recorder1)

circuit.tickUntil(5 * sampleRate).then(function() {

  var tape = recorder1.getSample()
  console.log(tape.lengthInSamples)
})
