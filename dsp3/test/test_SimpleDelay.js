const SampleQ = require("../components/SampleQ.js")
const SimpleDelay = require("../patches/SimpleDelay.js")
const render = require("../render.js")

var sq = new SampleQ().choose("drum")
var delay1 = new SimpleDelay(sq, 0.2, 0.5, 1)
sq.looped = true


render(delay1.OUT, 10).then(function(tape) {
  tape
  .normalise()
  .fadeOutSelf()
  .save("testing SimpleDelay")
})
