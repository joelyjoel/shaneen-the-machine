const Osc = require("../components/Osc.js")
const render = require("../render.js")

var osc1 = new Osc(200, "square")

render(osc1)
.normalise()
.fadeOutSelf()
.save("testing osc")
