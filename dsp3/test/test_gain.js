const Osc = require("../components/Osc.js")
const Gain = require("../components/Gain.js")
const render = require("../render.js")

var osc1 = new Osc(300)
var gain1 = new Gain(-12)

gain1.IN.connect(osc1.OUT)

console.log(gain1.label, gain1.IN.outlet.unit.label)

render(gain1.OUT)
.save("testing gain")
