const SineBoop = require("../patches/SineBoop.js")
const MonoDelay = require("../components/MonoDelay.js")
const Sum = require("../components/Sum.js")
const render = require("../render.js")
const {sampleRate} = require("../config.js")

var boop1 = new SineBoop(60, sampleRate/2).trigger()
var delay1 = new MonoDelay(boop1.OUT, sampleRate/2)
var sum1 = new Sum(boop1.OUT, delay1.OUT)

render(sum1.OUT, 2)
.normalise()
.save("testing MonoDelay")
