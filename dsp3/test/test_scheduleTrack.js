const TriggerGroup = require("../patches/TriggerGroup.js")
const Sampler = require("../components/Sampler.js")
const SpaceSampler = require("../patches/SpaceSampler.js")
const DrumGroup = require("../../music/DrumGroup.js")
const ComplexOrbit = require("../patches/ComplexOrbit.js")


var tg = new TriggerGroup()
tg.handleUnknownTrigger = function(trig) {
  //var sampler = new Sampler().choose("drum")
  var sampler = new SpaceSampler(new Sampler().choose("drum"))
  sampler.PLACEMENT = ComplexOrbit.random()/*[
    (Math.random()*2-1)*25,
    (Math.random()*2-1)*25,
  ]*/
  this.addTrigger(sampler, trig)
  this.trigger(trig)
}

var group = new DrumGroup().blank(16, 16);

group.poke(1);
group = group.evolve(60*4*5*10, 3);
group.poke(120, 0.5);

group.mixDownNumberedTracks()
group = group.tracks[0];


console.log(group)
tg.scheduleTrack(group)

var gamut = group.gamut

/*for(var i in gamut) {
  var sampler = new SpaceSampler(new Sampler().choose("drum"))
  sampler.PLACEMENT = [
    (Math.random()*2-1)*5,
    (Math.random()*2-1)*5,
  ]
  tg.addTrigger(sampler, gamut[i])
}*/




tg.OUT.render(5*60, 1).then(function(tape) {
  tape
  .normalise()
  //.fadeInSelf()
  .fadeOutSelf()
  .save("Drum Pattern")
})
//*/
