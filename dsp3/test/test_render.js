const render = require("../render.js")
const Osc = require("../components/Osc.js")
const Gain = require("../components/Gain.js")
const Multiply = require("../components/Multiply.js")

var osc1 = new Osc(20)
var osc2 = new Osc(1000)
var gain1 = new Gain(-12)
gain1.IN.connect(osc1.OUT)

var mult1 = new Multiply()
mult1.A.connect(osc1.OUT)
mult1.B.connect(osc2.OUT)

render(mult1.OUT, 1)
.save("testing dsp3")
