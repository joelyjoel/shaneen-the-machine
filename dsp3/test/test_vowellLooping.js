const SampleQ = require("../components/SampleQ.js")
const Circuit = require("../Circuit.js")

var sq = new SampleQ().ambient()
console.log(sq)

sq.looped = true

sq.selectionLogic = function(i) {
  return i+5
}

/*var circuit = new Circuit(sq)
console.log(circuit.tickUntil(10))*/

sq.OUT.render(5).then((tape) => {
  tape.normalise()
  .save("testing async rendering")
})
