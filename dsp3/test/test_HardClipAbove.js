const Osc = require("../components/Osc.js")
const HardClipAbove = require("../components/HardClipAbove.js")
const HardClipBelow = require("../components/HardClipBelow.js")

new HardClipBelow(new HardClipAbove(new Osc(), 0.5), -0.5).OUT.render(1).then((tape) => {
  tape
    .save("Test hard clipping", "Dumb Tests")
})
