const Sampler = require("../components/Sampler.js")
const DelayMixer = require("../patches/DelayMixer.js")

var myMixer = new DelayMixer(2, 2)

var sampler1 = new Sampler().choose("vocal")
sampler1.trigger()
var sampler2 = new Sampler().choose("vocal")
sampler2.trigger()

myMixer.addInput(sampler1, 1)
myMixer.addInput(sampler2, 0.5)
myMixer.addInput(sampler1, 1.5)

myMixer.OUT.render(5).then((tape) => {
  tape.normalise()
    .save("Testing DelayMixer", "dumb tests")
})
