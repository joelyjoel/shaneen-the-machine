const RotatingAmbient = require("../patches/RotatingAmbient.js")
const Shape = require("../components/Shape.js")
const Sum = require("../components/Sum.js")

const T = 60
const n = 5

var ambs = []
for(var i=0; i<n; i++) {
  ambs[i] = RotatingAmbient.random(0.5)
}
var mix = Sum.many(ambs)

console.log(mix)

mix.OUT.render(T, 2).then((tape)=> {
  tape
  //.normalise()
  .fadeInSelf()
  .fadeOutSelf()
  .save("Many ambient sounds rotating around")
})
