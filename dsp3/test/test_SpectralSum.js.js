const Osc = require("../components/Osc.js")
const Sampler = require("../components/Sampler.js")
const Hopper = require("../components/spectral/Hopper.js")
const Windower = require("../components/spectral/Windower.js")
const FFT = require("../components/spectral/FFT.js")
const IFFT = require("../components/spectral/IFFT.js")
const UnHopper = require("../components/spectral/UnHopper.js")
const config = require("../config.js")
const ReChunk = require("../components/spectral/ReChunk.js")
const SpectralSum = require("../components/spectral/SpectralSum.js")

const windowSize = config.fft.windowSize
const hopSize = config.fft.hopSize

var osc1 =new Osc(250)
var hopper1 = new Hopper(hopSize, windowSize)
hopper1.IN = osc1
var windower1 = new Windower(windowSize, "hamming", hopSize)
windower1.IN = hopper1.OUT
var fft1 = new FFT(windowSize, hopSize)
fft1.IN = windower1.OUT

var osc2 = new Osc(200)
var hopper2 = new Hopper(hopSize, windowSize)
hopper2.IN = osc2
var windower2 = new Windower(windowSize, "hamming", hopSize)
windower2.IN = hopper2
var fft2 = new FFT(windowSize, hopSize)
fft2.IN = windower2.OUT

var sum1 = new SpectralSum(fft1, fft2, windowSize, hopSize)

var ifft1 = new IFFT(windowSize, hopSize)
ifft1.IN = sum1.OUT

var windower2 = new Windower(windowSize, "hamming", hopSize)
windower2.IN = ifft1.OUT

var unhopper1 = new UnHopper(hopSize, windowSize)
unhopper1.IN = windower2.OUT

var rechunk1 = new ReChunk(hopSize, config.standardChunkSize)
rechunk1.IN = unhopper1.OUT

rechunk1.OUT.render(1).then(tape => {
  tape
    .normalise()
    .save("testing SpectralSum", "debugging")
})
