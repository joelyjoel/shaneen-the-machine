const interpretPointTrack = require("../functions/interpretPointTrack.js")
const logic = require("logicalexpressions")
const make_logicRhythm = require("../../music/make_logicRhythm.js")
const renderFMOsc = require("../functions/renderFMOsc.js")
const Sample = require("../Sample.js")
const config = require("../config.js")

do {
  var seed = new logic.Seed(5)
  seed.fastForward(Math.floor(Math.random()*100000))
  console.log(seed.expression().print())
  var rhythm = make_logicRhythm(seed.expression())
} while(rhythm.d > 8)


rhythm = rhythm.loopN(Math.random() < 0.5 ? 2 : 2.5)
for(var i=0; i+1<rhythm.notes.length; i++) {
  rhythm.notes[i].tOff = rhythm.notes[i+1].t;
  rhythm.notes[i].sound = Math.random()*60 + 10
}

interpretPointTrack(rhythm, async function(d, p) {

  // generate sounds here!
  if(Math.random() < 0.1)
    return (await Sample.choose("drum"))
  else if(Math.random() < 0.9) {
    var fm = await renderFMOsc(d, p)

    if(Math.random() < 0.01)
      fm.reverse()

    return fm
  }
  else {
    var samp = await Sample.choose("vocal")
    var t0 = Math.floor(Math.random()*(samp.lengthInSamples-d))
    samp = samp.cut(t0, t0+d)
    return samp;
  }

}).then(function(tape) {
//  if(tape.lengthInSeconds < 6) {
    tape = tape.loop(12.8 * config.sampleRate)
    tape.save(seed.expression().print(), "post-waddesdon");
  //}
})
