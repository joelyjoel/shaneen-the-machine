const GrainThing = require("../patches/GrainThing.js")
const Osc = require("../components/Osc.js")

var osc1 = new Osc()
var gt = new GrainThing(osc1)

gt.OUT.render(10)
.then(function(tape) {
  tape
  .normalise()
  .save("testing GrainThing")
})
