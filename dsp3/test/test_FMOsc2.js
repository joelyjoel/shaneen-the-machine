const FMOsc = require("../patches/FMOsc.js")
const Multiply = require("../components/Multiply.js")
const Mixer = require("../patches/Mixer.js")
const LFO = require("../patches/LFO.js")
const Shape = require("../components/Shape")
const argv = require("minimist")(process.argv.slice(2))
const FrequencyGroup = require("../patches/FrequencyGroup.js")
const StereoDetune = require("../patches/StereoDetune.js")

const T = Math.random() * (argv.T || 5)
const fundamental = 440
const n = (argv.n || 4)
const pConnection = 0.1 // probability of a connection between oscillators
const maxModulationAmmount = 12
const pMix = 1 // probability that an osc ends up in final mix
const maxMixAttenuation = -60
const maxStereoDetune = 0.05

var freqs = new FrequencyGroup(fundamental)
freqs.addRandomHarmonics(n, 16, 8)

var oscs = []
for(var i in freqs.fOuts) {
  oscs[i] = new FMOsc(
    new StereoDetune(freqs.fOuts[i])
  )
}
/*for(var i=0; i<n; i++) {
  var numerator = Math.ceil(Math.random()*12)
  var demoninator = Math.ceil(Math.random()*(3))
  oscs[i] = new FMOsc([
    numerator / demoninator * fundamental * (1+(Math.random()*2-1)*maxStereoDetune),
    numerator / demoninator * fundamental * (1+(Math.random()*2-1)*maxStereoDetune),
  ])
}*/

var mixer = new Mixer()

for(var i in oscs) {
  for(var j in oscs) {
  //  if(i==j) continue

    if(Math.random() < pConnection)
      oscs[i].addModulator(
        oscs[j],
        new Multiply(Shape.randomInRange(T, 0, 1).trigger(), maxModulationAmmount),
      )

    if(Math.random() < pMix)
      mixer.addInput(
        new Multiply(Shape.randomDecay(T).trigger(), oscs[i]),
      )
  }
}

//var chosenOsc = oscs[Math.floor(Math.random()*oscs.length)]

mixer.OUT.render(T).then(function(tape) {
  tape
    .normalise()
    //.fadeOutSelf()
    .save(argv._[0] || "short sound", "dumb tests")
})
