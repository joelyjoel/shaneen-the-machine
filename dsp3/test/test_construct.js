const constructExpressions = require("../construct/constructExpressions.js")
const constructNumber = require("../construct/constructNumber.js")
const argv = require("minimist")(process.argv.slice(2))
const fs = require("fs")

//const str = argv._[0] || "[Osc F=440 #myOsc]"
const str = fs.readFileSync("dsp3/construct/example.patch", "utf-8")

console.log("Patch:", str)

var index = {}
var objects = constructExpressions(str, {})
var obj = objects[objects.length-1]

if(obj.isUnit)
  obj = obj.defaultOutlet

obj.render(argv.t || 5).then(tape => {
  tape
    .normalise()
    //.fadeOutSelf()
    .save("Testing Parse", "dumb tests")
})
