const GrainCatcher = require("../components/GrainCatcher.js")
const Osc = require("../components/Osc.js")

var osc1 = new Osc(200)
var gc = new GrainCatcher(osc1)
gc.onGrain = function(grain, t) {
  console.log(grain)
}

gc.addOutlet("out")
gc.OUT.render(0.2)
