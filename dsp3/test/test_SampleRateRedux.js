const Sampler = require("../components/Sampler.js")
const SampleRateRedux = require("../components/SampleRateRedux.js")
const LFO = require("../patches/LFO.js")
const render = require("../render.js")

Sampler.choose("vocal").then(function(sampler) {
  sampler.trigger()

  var srr = new SampleRateRedux()
  srr.IN = sampler
  srr.AMMOUNT = new LFO(1, 5, 10)

  render(srr, 5)
  .save(sampler.label)
})
