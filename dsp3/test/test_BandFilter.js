const Sampler = require("../components/Sampler.js")
const BandFilter = require("../patches/BandFilter.js")

var sampler1 = new Sampler().choose("vocal").trigger()
var filter1 = new BandFilter(sampler1, 10000, 22000)

filter1.OUT.render(5).then((tape) => {
  tape.normalise().save("Testing BandFilter", "Dumb Tests")
})
