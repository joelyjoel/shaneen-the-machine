const Osc = require("../components/Osc.js")
const Sampler = require("../components/Sampler.js")
const Hopper = require("../components/spectral/Hopper.js")
const Windower = require("../components/spectral/Windower.js")
const FFT = require("../components/spectral/FFT.js")
const IFFT = require("../components/spectral/IFFT.js")
const UnHopper = require("../components/spectral/UnHopper.js")
const config = require("../config.js")
const ReChunk = require("../components/spectral/ReChunk.js")
const SpectralSum = require("../components/spectral/SpectralSum.js")
const BinShift = require("../components/spectral/BinShift.js")
const SpectralGate = require("../components/spectral/SpectralGate.js")
const undusp = require("../undusp")
const argv = require("minimist")(process.argv.slice(2))

const windowSize = config.fft.windowSize
const hopSize = Math.round(config.fft.hopSize)

var osc1 = new Sampler().choose(argv._[0] || "post-waddesdon").trigger()//new Osc(250)
osc1.loop(0, 12.8).then(sampler => {
  console.log("oh hey")
})
var hopper1 = new Hopper(hopSize, windowSize)
hopper1.IN = osc1
var windower1 = new Windower(windowSize, "hamming", hopSize)
windower1.IN = hopper1.OUT
var fft1 = new FFT(windowSize, hopSize)
fft1.IN = windower1.OUT

var gate1 = new SpectralGate(undusp("10 for 10 then 0"), windowSize, hopSize)
gate1.invert = true
gate1.IN = fft1.OUT

/*var gate2 = new SpectralGate(undusp("(100 for 50 )then ((1-A40^2) * 100 for 40) then A30"), windowSize, hopSize)
gate2.invert = false
gate2.IN = fft1.OUT*/

var sum1 = new SpectralSum(gate1, fft1, windowSize, hopSize)

var ifft1 = new IFFT(windowSize, hopSize)
ifft1.IN = sum1.OUT

var windower2 = new Windower(windowSize, "hamming", hopSize)
windower2.IN = ifft1.OUT

var unhopper1 = new UnHopper(hopSize, windowSize)
unhopper1.IN = windower2.OUT

var rechunk1 = new ReChunk(hopSize, config.standardChunkSize)
rechunk1.IN = unhopper1.OUT

rechunk1.OUT.render(20).then(tape => {
  tape
    .normalise()
    .save("testing DetailBoost", "debugging")
})
