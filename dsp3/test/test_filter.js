const Osc = require("../components/Osc.js")
const Filter = require("../components/Filter.js")
const Ramp = require("../components/Ramp.js")
const render = require("../render.js")
const Monitor = require("../components/Monitor.js")
const Noise = require("../components/Noise.js")
const Shape = require("../components/Shape")


var osc1 = new Noise()
osc1.waveform = "saw"

var filter1 = new Filter()
filter1.IN = osc1
filter1.kind = "HP"

var ramp1 = new Shape("decay", 1, 50, 2000).trigger()

filter1.F = 1000

//new Monitor(ramp1)

render(filter1, 3).then((tape) => {
  tape
    .normalise()
    .save("Testing Filter "+filter1.kind + " f " + filter1.F.printValue, "Dumb Tests")

})
