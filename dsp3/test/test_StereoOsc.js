const StereoOsc = require("../patches/StereoOsc.js")
const render = require("../render.js")
const LFO = require("../patches/LFO.js")

var osc1 = new StereoOsc()
osc1.GAIN = 0
osc1.PAN = new LFO(0.1, 0.5, 0)
osc1.P = 50
osc1.PCONTROL = new LFO(1, 5, 0).OUT

render(osc1, 10)
.save("testing StereoOsc")
