const Osc = require("../components/Osc.js")
const Sampler = require("../components/Sampler.js")
const Hopper = require("../components/spectral/Hopper.js")
const Windower = require("../components/spectral/Windower.js")

const windowSize = 4096
const hopSize = windowSize/2

var osc1 = new Osc(100)
var hopper1 = new Hopper(hopSize, windowSize)
hopper1.IN = osc1
console.log("standardChunkSize:", osc1.tickInterval)
console.log("hopper tickInterval", hopper1.tickInterval, hopper1.OUT.chunkSize)

var windower1 = new Windower(windowSize, "hamming")
windower1.IN = hopper1.OUT

windower1.OUT.render(1).then(tape => {
  tape
    .normalise()
    .save("testing hopper", "debugging")
})
