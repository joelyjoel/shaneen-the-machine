const SpaceChannel = require("../patches/SpaceChannel.js")
const SampleQ = require("../components/SampleQ.js")
const render = require("../render.js")

var sq = new SampleQ().choose(vocal)
var spaceChan = new SpaceChannel()
spaceChan.IN = sq
spaceChan.PLACEMENT = [1,1]
render(spaceChan, 2).then(function(tape) {
  tape
  .normalise()
  .save("testing SpaceChannel")
})
