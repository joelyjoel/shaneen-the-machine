const MidiOsc = require("../patches/MidiOsc.js")
const Ramp = require("../components/Ramp.js")
const render = require("../render.js")
const {sampleRate} = require("../config.js")

var osc1 = new MidiOsc()
var ramp1 = new Ramp(sampleRate, 60, 30)
osc1.P = ramp1.OUT

ramp1.trigger()

render(osc1, 2)
.save("testing ramp")
