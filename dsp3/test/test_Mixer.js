const Mixer = require("../patches/Mixer.js")
const Osc = require("../components/Osc.js")

var mixer1 = new Mixer()
mixer1.addInputs(
  new Osc(200),
  new Osc(10000),
  [
    new Osc(1000),
    new Osc(3000),
    new Osc(2000),
    new Osc(50),
  ],
)

mixer1.schedule(1/10, function() {
  this.removeInput(mixer1.inputs[0])
  return 1/10
})

mixer1.OUT.render(1).then(function(tape) {
  tape
  .normalise()
  .save()
})
