const Retriggerer = require("../components/Retriggerer.js")
const Sampler = require("../components/Sampler.js")

var samp1 = new Sampler().choose('vocal')
var retrig1 = new Retriggerer(samp1, 1)

samp1.render(4).then(tape => {
  tape.save("testing Retriggerer", "dumb tests")
})
