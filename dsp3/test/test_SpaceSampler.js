const SpaceSampler = require("../patches/SpaceSampler.js")

var sampler1 = new SpaceSampler().choose("drum")

sampler1.scheduleTrigger(1)

sampler1.OUT.render(5).then(function(tape) {
  tape
  .normalise()
  .save("testing SpaceSampler")

})
