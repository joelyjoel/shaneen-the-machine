const MOsc = require("../patches/MidiOsc.js")
const Rescale = require("../components/Rescale.js")
const render = require("../render.js")

var osc1 = new MOsc(50)
var rescale1 = new Rescale()
rescale1.IN = osc1.OUT

console.log(rescale1.IN)

render(rescale1.OUT, 1)
.fadeOutSelf()
.save()
//*/
