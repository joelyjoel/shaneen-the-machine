const CrossFader = require("../components/CrossFader.js")
const Osc = require("../components/Osc.js")
const Rescale = require("../components/Rescale.js")
const render = require("../render.js")

var osc1 = new Osc(500*Math.random())
var osc2 = new Osc(200*Math.random())
var lfo1 = new Osc(2*Math.random())
var rescale1 = new Rescale(-1, 1, 0.2, 0.8)
rescale1.IN = lfo1
var crossfader1 = new CrossFader(osc1, osc2, rescale1)

render(crossfader1, 30)
.save("testing CrossFader")
