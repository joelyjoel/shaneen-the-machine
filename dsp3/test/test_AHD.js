const AHD = require("../components/AHD.js")

var envelope = AHD.random(1).trigger()


envelope.OUT.render(4).then((tape) => {
  tape
    .normalise()
    .save("Testing AHD", "dumb tests")
})
