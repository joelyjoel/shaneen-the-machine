const Osc = require("../components/Osc.js")
const Sampler = require("../components/Sampler.js")
const Hopper = require("../components/spectral/Hopper.js")
const Windower = require("../components/spectral/Windower.js")
const FFT = require("../components/spectral/FFT.js")
const IFFT = require("../components/spectral/IFFT.js")
const UnHopper = require("../components/spectral/UnHopper.js")
const config = require("../config.js")
const ReChunk = require("../components/spectral/ReChunk.js")
const SpectralSum = require("../components/spectral/SpectralSum.js")
const BinShift = require("../components/spectral/BinShift.js")
const HardLowPass = require("../components/spectral/HardLowPass.js")
const HardHighPass = require("../components/spectral/HardHighPass.js")
const undusp = require("../undusp")

const windowSize = config.fft.windowSize
const hopSize = Math.round(config.fft.hopSize)

var osc1 = new Sampler().choose("post-waddesdon").trigger()//new Osc(250)
osc1.loop()
var hopper1 = new Hopper(hopSize, windowSize)
hopper1.IN = osc1
var windower1 = new Windower(windowSize, "hamming", hopSize)
windower1.IN = hopper1.OUT
var fft1 = new FFT(windowSize, hopSize)
fft1.IN = windower1.OUT

var lp1 = new HardLowPass(undusp("2100 + A30*20000"))
lp1.IN = fft1.OUT

var hp1 = new HardHighPass(undusp("2000 - A30*2000"))
hp1.IN = lp1.OUT

var ifft1 = new IFFT(windowSize, hopSize)
ifft1.IN = hp1.OUT

var windower2 = new Windower(windowSize, "hamming", hopSize)
windower2.IN = ifft1.OUT

var unhopper1 = new UnHopper(hopSize, windowSize)
unhopper1.IN = windower2.OUT

var rechunk1 = new ReChunk(hopSize, config.standardChunkSize)
rechunk1.IN = unhopper1.OUT

rechunk1.OUT.render(40).then(tape => {
  tape
    .normalise()
    .save("testing HardLowPass", "debugging")
})
