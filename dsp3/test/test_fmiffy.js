const fmiffy = require("../functions/fmiffy.js")
const satbPad = require("../../music/satbPad.js")
const bach = require("../../music/bach")
const stealChordLoopFromBach = require("../../music/stealChordsFromBach.js")


var chorale = bach.random().mixDown()
console.log(chorale)

var pad = satbPad( stealChordLoopFromBach(4))

fmiffy(pad, 189).then(function(tape) {
  console.log(tape)
  tape.normalise()
  tape.save("fmiffy test", "test/fmiffy")
})
