const ReadBackDelay = require("../components/ReadBackDelay.js")
const Osc = require("../components/Osc.js")

var osc1 = new Osc(200)

var delay1 = new ReadBackDelay(osc1, [4410, 2932])


delay1.OUT.render(4).then(function(tape) {
  tape
  .normalise()
  .fadeOutSelf()
  .save("testing ReadBackDelay")
})
