const GlitchySpaceThing = require("../patches/GlitchySpaceThing.js")
const Sampler = require("../components/Sampler.js")

var thing1 = new GlitchySpaceThing(new Sampler().choose("ambient"))

thing1.OUT.render(60).then(function(tape) {
  tape
  .normalise()
  .save("testing GlitchySpaceThing")
})
