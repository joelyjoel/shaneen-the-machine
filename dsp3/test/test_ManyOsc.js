const ManyOsc = require("../patches/ManyOsc.js")
const render = require("../render.js")

//var osc1 = ManyOsc.ofFrequencies(200, [1,2.1,3,4.2,5.4,6,7])
var osc2 = ManyOsc.random(4)

render(osc2, 10)
.normalise()
.fadeOutSelf()
.save()
