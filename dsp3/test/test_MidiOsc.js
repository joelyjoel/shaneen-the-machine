const MidiOsc = require("../patches/MidiOsc.js")
const render = require("../render.js")

var osc1 = new MidiOsc(36)
render(osc1.OUT)
.save("testing midiosc")
