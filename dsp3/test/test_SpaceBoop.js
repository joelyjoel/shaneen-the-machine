const SpaceBoop = require("../patches/SpaceBoop.js")
const Filter = require("../components/Filter.js")
const Monitor = require("../components/Monitor.js")

var boop1 = new SpaceBoop(60, "saw", 0.1, "decay")

console.log(boop1.PLACEMENT)

boop1.schedule(0, function() {
  //console.log("retriggering")
  this.PLACEMENT = [
      (Math.random()*2-1) * 3,
      (Math.random()*2-1) * 3,
  ]
  this.trigger(20 + Math.random() * 40)
  this.waveform = "random"
  return 0.1
})

//var monitor1 = new Monitor(boop1)
//console.log(boop1)

var filter1 = new Filter()

boop1.OUT.render(60)
.save("testing spaceboop")
