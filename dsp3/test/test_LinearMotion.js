const Space = require("../patches/Space.js")
const LinearMotion = require("../components/vector/LinearMotion.js")
const Osc = require("../components/Osc.js")

var osc1 = new Osc(300)

var motion1 = LinearMotion.random()

var space1 = new Space(osc1, motion1)

space1.OUT.render(5).then(function(tape) {
  tape
  //.normalise()
  .save("testing LinearMotion")
})
