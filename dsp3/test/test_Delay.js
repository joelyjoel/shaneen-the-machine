const Delay = require("../components/Delay.js")
const SineBoop = require("../patches/SineBoop.js")
const CrossFader = require("../components/CrossFader.js")
const render = require("../render.js")
const {sampleRate} = require("../config.js")

var boop1 = new SineBoop(30)
boop1.trigger()

var delay1 = new Delay(boop1, [sampleRate, sampleRate/2])

render(delay1, 3, 2).then(function(tape) {
  tape
    .normalise()
    .save("testing delay", "dumb tests")
})
