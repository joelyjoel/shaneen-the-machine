const Noise = require("../components/Noise.js")
const render = require("../render.js")

render(new Noise(400), 1)
.save("testing noise")
