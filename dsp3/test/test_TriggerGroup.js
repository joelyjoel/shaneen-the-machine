const TriggerGroup = require("../patches/TriggerGroup.js")
const Sampler = require("../components/Sampler.js")

var tg = new TriggerGroup()
tg.addTrigger( new Sampler().gtts("who") )
tg.addTrigger( new Sampler().gtts("are").loop() )
tg.addTrigger( new Sampler().gtts("these") )
tg.addTrigger( new Sampler().gtts("people") )
tg.addTrigger( new Sampler().gtts("who") )
tg.addTrigger( new Sampler().gtts("look") )
tg.addTrigger( new Sampler().gtts("like") )
tg.addTrigger( new Sampler().gtts("clouds") )


tg.scheduleTrigger(0, 0)


console.log("all set up ok..")
try {
tg.OUT.render(8).then(function(tape) {
  tape
    .normalise()
    //.fadeOutSelf()
    .save("testing TriggerGroup")
})
} catch(e) {
  console.log(e)
}
