const Sampler = require("../components/Sampler.js")

var samp1 = new Sampler("vocal")
samp1.stopAtEnd = false

samp1.loop()
samp1.trigger()
samp1.OUT.render(5).then(tape => {
  tape.save("testing sampler", "dumb tests")
})
