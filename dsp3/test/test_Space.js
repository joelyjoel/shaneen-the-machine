const Space = require("../patches/Space.js")
const Osc = require("../components/Osc.js")
const CircularMotion = require("../components/vector/CircularMotion.js")
const render = require("../render.js")
const Monitor = require("../components/Monitor.js")
const Sampler = require("../components/Sampler.js")
const Sample = require("../Sample.js")
const SampleQ = require("../components/SampleQ.js")

Sample.ambient().then(function(sample) {


  var sampler1 = new SampleQ()
  sampler1.looped = true
  sampler1.addZXGrains(sample)
  sampler1.playSample(0)


  console.log(sampler1)

  var motion1 = new CircularMotion(1/2, 5)

  var space1 = new Space(sampler1.OUT, [-1,0])
  space1.PLACEMENT = motion1

  console.log(space1.spaceChannels[1].speakerPositionSubtracter.print)

  render(space1, 25).then((tape) => {
    tape.save("Testing Space")
  })
  //.normalise()
  //.fadeOutSelf()
  //.save("testing Space")
})
