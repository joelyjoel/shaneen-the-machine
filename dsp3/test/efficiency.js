const argv = require("minimist")(process.argv.slice(2))
const render = require("../render.js")
const findFiles = require("../../findFiles.js")
const path = require("path")

function getRenderTimeUsage(filePath) {
  try {
    var Constructor = require(filePath)
    var tape = render(new Constructor(), 60, undefined, true)
    return 1/tape.meta.renderRate
  } catch(e) {
    return null
  }
}

var patches = findFiles("./dsp3/patches/", [".js"])
var components = findFiles("./dsp3/components/", [".js"])

console.log("\n\n")
console.log("COMPONENTS:")
console.log("\t", "\b(usage)")
for(var i in components) {
  components[i] = path.resolve(components[i])
  var usage = getRenderTimeUsage(components[i])
  if(usage !== null) {
    var asPercent = (100 * usage).toFixed(2)+"%"
    console.log("\t",  asPercent, "\t", path.relative("./dsp3/components", components[i]))
  } else {
    console.log("\t FAILED\t", path.relative("./dsp3/components", components[i]))
  }
}
console.log("PATCHES:")
for(var i in patches) {
  patches[i] = path.resolve(patches[i])
  var usage = getRenderTimeUsage(patches[i])
  if(usage !== null) {
    var asPercent = (100 * usage).toFixed(2)+"%"
    console.log("\t",  asPercent, "\t", path.relative("./dsp3/patches", patches[i]))
  } else {
    console.log("\tFAILED\t", path.relative("./dsp3/patches", patches[i]))
  }
}

console.log("\n\n")
