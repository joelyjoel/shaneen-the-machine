function randomRational(maxNumerator, maxDenominator, greaterThanOne) {
  maxNumerator = maxNumerator || 16
  maxDenominator = maxDenominator || 16

  var numerator = Math.ceil(Math.random()*maxNumerator)

  if(greaterThanOne && maxDenominator > numerator)
    maxDenominator = numerator

  var denominator = Math.ceil(Math.random()*maxDenominator)

  return numerator/denominator
}
module.exports =randomRational

randomRational.inSemitones = function(maxNumerator, maxDenominator, greaterThanOne) {
  var rational = randomRational(maxNumerator, maxDenominator, greaterThanOne)
  return mToF.fToM(rational)
}
