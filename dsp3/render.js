const Sample = require("./Sample.js")
const Circuit = require("./Circuit.js")
const config = require("./config.js")
const fs = require("fs")
const dusp = require("./dusp")

const sampleRate = config.sampleRate

async function render(outlet, T, numberOfChannels, suppressConsole) {

  var timerStart = new Date().getTime()

  if(outlet.isUnit || outlet.isPatch)
    outlet = outlet.defaultOutlet
  T = (T || 1) * sampleRate
  var outUnit = outlet.unit
  if(outUnit.circuit)
    var circuit = outUnit.circuit
  else
    var circuit = new Circuit(outUnit)

  circuit.vital.push(outUnit)

  var duspStr = dusp(outlet)

//  console.log(circuit.printAsArray)

  var chunkSize = outlet.chunkSize
  console.log("render outlet chunkSize:", chunkSize)
  var tape = new Sample().blank(T, numberOfChannels)
  var lastNotification = 0
  try {

    if(circuit.promises.length > 0) {
      console.log("waiting for", circuit.promises.length, "promises")
      await Promise.all(circuit.promises)
      console.log("promises fulfilled!")
      circuit.promises = []
    }

    for(var t0=0; t0<T; t0+=chunkSize) {
      if(t0 > lastNotification+config.sampleRate && !suppressConsole) {
        console.log(
          "rendered", Math.floor(t0/sampleRate)+"/"+(T/sampleRate)+"s",
          "\t(circuit size:", circuit.units.length+")"
        )
        lastNotification = t0
      }
      var t1 = t0 + chunkSize
      await circuit.tickUntil(t1)
      var chunk = outlet.signalChunk
      while(numberOfChannels == undefined
      && chunk.channelData.length > tape.numberOfChannels)
        tape.addChannel()
      for(var c in chunk.channelData) {
        for(var t=0; t<chunkSize; t++) {
          if(isNaN(chunk.channelData[c][t])) {
            console.log("NaN!! time:", t + t0, "c:", c)
            var culprit = circuit.findNaNCulprit()
            if(culprit)
              console.log("culprit:", culprit.label,)
            throw "render failed because of NaN"
          }
          tape.channelData[c][t+t0] = chunk.channelData[c][t]
        }
      }
    }
  } catch(e) {
    console.log("Error during rendering:\t", "\n", e)
    throw e
  }

  var timerElapse = new Date().getTime() - timerStart
  if(!suppressConsole)
    console.log("recorded ", (T/sampleRate).toFixed(3)+"s of audio in", (timerElapse/1000).toFixed(3)+"s")


  console.log("dusp:\n" + duspStr)
  tape.addMeta({
    renderTime: timerElapse+"ms",
    renderRate: ((T/44.1)/timerElapse),
    renderedByScript: process.argv[1],
    commandLineArguments: process.argv.slice(2).join(" "),
    //renderScript: fs.readFileSync(process.argv[1], "utf-8"),
    hypotheticalLiveProcessorUsageEstimation: (100/(T/44.1)*timerElapse).toFixed(2)+"%",
    renderOutlet: outlet.label,
    colourCode: "#DC3BFE",
    colorCode: "#DC3BFE",
    dusp: duspStr,
    tags: ["shaneen"],
  })
//  tape.meta.tags.push("shaneen")

  tape.label = "recorded_DSP"
  return tape
}
module.exports = render
