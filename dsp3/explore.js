function explore(unit, exclude, history) {
  if(exclude && exclude.constructor != Array)
    exclude = [exclude]
  exclude = exclude || []
  history = history || []
  history.push(unit)

  var neighbours = unit.neighbours.filter(
    u => (history.indexOf(u) == -1) && (exclude.indexOf(u) == -1)
  )

  for(var i in neighbours)
    explore(neighbours[i], exclude, history)

  return history
}
module.exports = explore
