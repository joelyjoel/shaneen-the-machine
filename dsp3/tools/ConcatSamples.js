const argv = require("minimist")(process.argv.slice(2))
const Sample = require("../Sample.js")
const Promise = require("promise")



var proms = []

for(var i in argv._) {
  proms.push( Sample.readFile(argv._[i]) )
}

Promise.all(proms).then(function(samples) {
  var newSample = new Sample()

  for(var i in samples) {
    newSample.channelData = newSample.channelData.concat(samples[i].channelData)
  }

  newSample.save("concatted channels")
})
