const Unit = require("../Unit.js")
const Sample = require("../Sample.js")
const textToSpeech = require("../../dsp/textToSpeech.js")
const config = require("../config.js")


function SampleQ() {
  Unit.call(this)
  this.addInlet("rate", {mono: true, zero:1, type:"scalar"})
  this.addOutlet("out", {type:"audio"})

  this.samples = []
  this.playingIndex = null
  this.startIndex = 0
  this.playingSample
  this.t = 0
  this.i = 0
  this.playing = true
  this.waitTime = 0
  this.looped = false
  this.waitAtEndForMore = true

  this.selectionLogic = function(i) {
    if(this.grainGap) {
      this.wait(this.grainGap * this.playingSample.lengthInSamples, i+1)
    } else
      return i+1
  }
  this.RATE = 1
}
SampleQ.prototype = Object.create(Unit.prototype)
SampleQ.prototype.constructor = SampleQ
module.exports = SampleQ

SampleQ.prototype.isSampleQ = true

SampleQ.prototype.weird = function(sample) {
  var sq = this
  sq.addZXGrains(sample, 2)
  sq.onPlaySample = function(sample) {
    this.RATE = (0.002)/sample.lengthInSeconds
  }
  sq.playSample(0)
  return sq
}

SampleQ.prototype.choose = function(kind) {
  var sq = new SampleQ()
  sq.addPromise(Sample.choose(kind).then(function(sample) {
    sq.addZXGrains(sample)
    sq.playSample(0)
  }))
  return sq
}
SampleQ.prototype.ambient = function(lengthInSeconds) {
  var sq = this
  sq.addPromise(Sample.ambient(lengthInSeconds).then(function(sample) {
    sq.addZXGrains(sample)
    sq.playSample(0)
    sq.looped = true
  }))
  return sq
}
SampleQ.prototype.gtts = function(text, lang, grainSize) {
  lang = lang || "en-uk"
  grainSize = grainSize || 3

  var sq = this
  sq.addPromise(textToSpeech(text, lang).then(function(sample) {
    sq.addZXGrains(sample, grainSize)
  }))
  return sq
}

SampleQ.prototype._tick = function(clock) {
  var chunkSize = this.OUT.chunkSize

  if(this.awaitingSample != null) {
    if(this.samples[this.awaitingSample])
      this.playSample(this.awaitingSample)
    else
      return ;
  }

  for(var tChunk=0; tChunk<chunkSize; tChunk++) {
    if(this.playing) {

      if(this.waitTime) {
        this.waitTime--
        if(!this.waitTime && this.onFinishWaitTime) {
          if(this.onFinishWaitTime.constructor == Function)
            this.onFinishWaitTime()
          else if(this.onFinishWaitTime.constructor == Number) {
            this.playSample(this.onFinishWaitTime)
          }
        }
        for(var c=0; c<this.out.length; c++)
          this.out[c][tChunk] = 0

      } else if(this.playingSample) {

        for(var c=0; c<this.playingSample.numberOfChannels; c++)
          this.out[c][tChunk] = (1-this.t%1) * this.playingSample.channelData[c][Math.floor(this.t)]
                                + (this.t%1) * this.playingSample.channelData[c][Math.ceil(this.t)]
        for(c=c; c<this.numberOfChannels; c++)
          this.out[c][tChunk] = 0

        this.t += this.rate[tChunk]
        if(this.t > this.playingSample.lengthInSamples-1) {
          var nextI = this.selectionLogic(this.i)
          if(nextI){
            this.i = nextI
            this.playSample(this.i)
          }
          if(this.awaitingSample)
            break;
        }

      }

    }
  }
}

SampleQ.prototype.trigger = function() {
  this.playSample(this.startIndex)
}

SampleQ.prototype.playSample = function(sampleIndex) {
  // resetting stuff
  this.t %= 1
  this.waitTime = 0
  this.onFinishWaitTime = null
  this.awaitingSample = null
  this.playingIndex = null
  this.playingSample = null

  this.i = sampleIndex
  sampleIndex = Math.floor(sampleIndex)

  // what if its outside sample range?
  if(this.looped)
    sampleIndex %= this.samples.length

  if(sampleIndex >= this.samples.length) {
    if(this.waitAtEndForMore)
      this.awaitingSample = sampleIndex
    else
      this.stop()

    return null
  } else {
    this.playingIndex = sampleIndex
    this.playingSample = this.samples[sampleIndex]
  }

  if(!this.playingSample) {
    this.stop()
    return null;
  }

  if(this.onPlaySample)
    this.onPlaySample(this.playingSample)
}
SampleQ.prototype.wait = function(tSamples, then) {
  this.waitTime = tSamples
  this.onFinishWaitTime = then
}
SampleQ.prototype.stop = function() {
  this.playing = false
}

SampleQ.prototype.addSample = function(sample) {
  while(this.out.length < sample.numberOfChannels)
    this.out.push(new Float32Array(this.OUT.chunkSize))
  this.samples.push(sample)
  if(this.samples.length == 1 && this.onAddFirstSample)
    this.onAddFirstSample()
  if(this.onAddSample)
    this.onAddSample(sample)
}
SampleQ.prototype.addSamples = function(samples) {
  for(var i in samples) {
    this.addSample(samples[i])
  }
}
SampleQ.prototype.addZXGrains = function(sample, grainSize) {
  var grains = sample.firstChannel().diceByZeroCrossings(grainSize)[0]
  this.addSamples(grains)
}

SampleQ.prototype.shuffle = function() {
  this.samples = this.samples.sort((a,b) => {
    return Math.random()*2-1
  })
}
