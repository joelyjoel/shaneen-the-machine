const Unit = require("../Unit.js")
const Sample = require("../Sample.js")

function GrainCatcher(input) {
  Unit.call(this)

  this.addInlet("in", {mono: true, type:"audio"})

  this.lastVal = 0
  this.grainBuffer = new Float32Array(this.maxGrainSize)
  this.grainBufferI = 0

  this.IN = input || 0
}
GrainCatcher.prototype = Object.create(Unit.prototype)
GrainCatcher.prototype.constructor = GrainCatcher
module.exports = GrainCatcher

GrainCatcher.prototype.maxGrainSize = 1024

GrainCatcher.prototype._tick = function(clock) {
  for(var t=0; t<this.in.length; t++) {
    this.grainBufferI++
    if(this.grainBufferI > this.maxGrainSize) {
      this.grainBufferI = 0
    }
    this.grainBuffer[this.grainBufferI] = this.in[t]
    if(this.lastVal < 0 && this.in[t] > 0) {
      var grain = this.grainBuffer.slice(0, this.grainBufferI)
      var sample = new Sample([grain])
      if(this.onGrain)
        this.onGrain(sample, clock+t-this.grainBufferI)
      this.grainBufferI = 0
    }
    if(this.in[t] != 0)
      this.lastVal = this.in[t]
  }
}
