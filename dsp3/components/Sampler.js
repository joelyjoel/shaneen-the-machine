const Unit = require("../Unit.js")
const Sample = require("../Sample.js")
const textToSpeech = require("../../dsp/textToSpeech.js")

function Sampler(choose) {
  Unit.call(this)

  this.loopBuffer = false
  this.stopAtEnd = true

  this.addInlet("rate", {mono: true, type:"scalar"})
  this.addInlet("loopStart", {mono: true, type:"time", unit:"seconds"})
  this.addInlet("loopDuration", {mono: true, type:"duration", unit:"seconds"})
  this.addInlet("xf", {mono: true, type:"duration", unit:"seconds"}) // crossfade time
  this.addOutlet("out", {type:"audio"})

  this.samplePromise = new Promise(fulfil => {
    this.__defineSetter__("sample", function(sample) {
      console.log("Setting sample!")
      delete this.sample
      this.sample = sample
      this.awaitingSample = false
      for(var c=0; c<sample.numberOfChannels; c++)
        this.out[c] = this.out[c] || new Float32Array(this.OUT.chunkSize)
      this.lengthInSamples = sample.lengthInSamples
      this.numberOfChannels = sample.numberOfChannels
      this.lengthInSeconds = sample.lengthInSeconds
      if(this.onSetSample)
        this.onSetSample(sample)
      fulfil(sample)
    })
    this.__defineGetter__("sample", function() {
      return this.samplePromise
    })
  })

  this.awaitingSample = true
  this.t = 0 // in samples
  this.tSeconds = 0 // in seconds
  this.playing = false
  this.loopsCompleted = 0
  this.lockT0ToLoopStart = false

  if(choose)
    this.choose(choose)

  this.RATE = 1 // rate multiplier
  this.t0 = 0 // in seconds
  this.LOOPSTART = 0 // in seconds
  this.LOOPDURATION = 1 // in seconds
  this.loopN = 0 // number or infinity
  this.XF = 0
}
Sampler.prototype = Object.create(Unit.prototype)
Sampler.prototype.constructor = Sampler
module.exports = Sampler

Sampler.prototype.dusp = {
  flagFunctions: {
    trigger: function() {
      this.trigger()
    },
    loop: function() {
      this.loopN = Infinity
      this.loopBuffer = true
      this.stopAtEnd = false
      this.lockT0ToLoopStart = true
    },
    loopAll: function() {
      this.loop()
    }
  }
}

Sampler.prototype.choose = function(kind) {
  var sampler = this
  sampler.addPromise(Sample.choose(kind).then(function(sample) {
    sampler.sample = sample
  }))
  console.log(sampler.promises)
  return sampler
}
Sampler.prototype.gtts = function(text, lang) {
  var sampler = this
  sampler.addPromise(textToSpeech(text, lang).then(function(sample) {
    sampler.sample = sample
  }))
  return sampler
}

Sampler.prototype._tick = function() {
  var numberOfChannels = this.sample.numberOfChannels
  for(var tChunk=0; tChunk<this.out[0].length; tChunk++) {
    if(this.playing) {
      var t = this.t
      if(this.loopBuffer) {
        while(t > this.lengthInSamples)
          t -= this.lengthInSamples
        while(t < 0)
          t += this.lengthInSamples
      }
      var tSeconds = t * this.sampleRate
      var xfTSamples = xfT *this.sampleRate
      var xfT = tSeconds - this.loopDuration[tChunk]
      if(this.loopsCompleted < this.loopN && this.xf[tChunk] && xfT > this.loopStart[tChunk]-this.xf[tChunk]) {
        var xfProgress = (xfT-this.loopStart[tChunk] + this.xf[tChunk])/this.xf[tChunk]
        var xfTSamples = xfT *this.sampleRate
      } else
        var xfProgress = 0

      for(var c=0; c<numberOfChannels; c++) {
        if(xfProgress)
          this.out[c][tChunk] = xfProgress * this.sample.y(xfTSamples, c) + (1-xfProgress) * this.sample.y(t,c)
        else
          this.out[c][tChunk] = this.sample.y(t, c)
      }

      this.t += this.rate[tChunk]
      this.tSeconds = this.t * this.samplePeriod

      if(this.loopsCompleted < this.loopN) {
      /*  if(this.tSeconds < this.loopStart[tChunk]) {
          this.skip(this.loopStart + this.loopDuration[tChunk])
          this.loopsCompleted++
        }*/
        if(this.tSeconds > this.loopStart[tChunk] + this.loopDuration[tChunk]) {
          this.skip(this.loopStart[tChunk])
          this.loopsCompleted++
        }
      }
      if(this.stopAtEnd && this.t > this.sample.lengthInSamples-1 ) {
        this.stop()
        this.finish()
      }

    } else {
      for(var c=0; c<this.numberOfChannels; c++)
        this.out[c][tChunk] = 0
    }
  }
}

Sampler.prototype.trigger = function() {
  this.playing = true
  if(this.lockT0ToLoopStart)
    this.t0 = this.loopStart[0]
  this.skip(this.t0)
  return this
}
Sampler.prototype.play = function() {
  this.playing = true
  return this
}
Sampler.prototype.stop = function() {
  if(this.lockT0ToLoopStart)
    this.t0 = this.loopStart[0]
  this.skip(this.t0)
  this.loopsCompleted = 0
  this.playing = false
  return this
}
Sampler.prototype.pause = function() {
  this.playing = false
  return this
}
Sampler.prototype.skip = function(tSeconds) {
  //console.log("skipping", this.tSeconds, "to", tSeconds)
  this.t = tSeconds * this.sampleRate
  this.tSeconds = tSeconds
}

Sampler.prototype.loop = async function(loopStart, loopDuration, loopN) {
  await this.sample
  loopStart = loopStart || 0
  loopDuration = loopDuration || this.lengthInSeconds
  loopN = loopN || Infinity

  this.stopAtEnd = false

  this.LOOPSTART = loopStart
  this.LOOPDURATION = loopDuration
  this.loopN = loopN
  return this
}
Sampler.prototype.breakLoop = function() {
  this.loopsCompleted = this.loopN
  return this
}

Sampler.prototype.__defineGetter__("lengthInSamples", function() {
  if(this.sample)
    return this.sample.lengthInSamples
})
Sampler.prototype.__defineGetter__("lengthInSeconds", function() {
  if(this.sample)
    return this.sample.lengthInSeconds
})

Sampler.prototype.randomLoop = async function(duration) {
  await this.sample

  duration = duration || this.lengthInSeconds * Math.random()
  var loopStart = Math.random()*(this.lengthInSeconds-duration)
  this.loop(loopStart, duration, Infinity)
}
