const Unit = require("../Unit.js")
const Sample = require("../Sample.js")

function Recorder(input) {
  Unit.call(this)

  this.addInlet("in", {type:"audio"})

  this.IN = input || 0

  this.recording = true
  this.chunks = []
}
Recorder.prototype = Object.create(Unit.prototype)
Recorder.prototype.construtor = Recorder
module.exports = Recorder

Recorder.prototype._tick = function() {
  if(this.recording) {
    this.chunks.push(this.IN.signalChunk.duplicateChannelData())
    console.log("record.chunks.length", this.chunks.length)
  }
}

Recorder.prototype.start = function() {
  this.recording = true
}
Recorder.prototype.stop = function() {
  this.recording = false
}

Recorder.prototype.getSample = function() {
  var chunks = []
  for(var i in this.chunks) {
    var sampleChunk = new Sample()
    sampleChunk.channelData = this.chunks[i]
    chunks.push(sampleChunk)
  }
  tape = Sample.concat(chunks)
  tape.label = this.IN.outlet.label + " recorded"
  return tape
}
