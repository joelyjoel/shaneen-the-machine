const fs = require("fs").promises
const path = require("path")
const findFiles = require("../../findFiles.js")

const patchDirectory = path.resolve("dsp3/patches")

async function compilePatchList() {

  var jsFiles = findFiles(patchDirectory, [".js"])
  var relPaths = jsFiles.map(absPath => path.relative(__dirname, absPath))
  var localPaths = jsFiles.map(absPath => path.relative(patchDirectory, absPath))

  var index = {}
  console.log("\nIndexing patches from", jsFiles.length, "files")
  for(var i in relPaths) {
    try {
      var mod = require(relPaths[i])
    } catch(e) {
      console.warn("\t", localPaths[i], "contains errors")
    }

    if(mod && mod.prototype && mod.prototype.isPatch) {
      var name = mod.name
      if(!index[name]) {
        index[name] = localPaths[i]
        console.log("\tFOUND PATCH:", name, "at", index[name])
      } else
        console.warn("\tDUPLICATE PATCH:", name, "; discarding", localPaths[i], "in favour of", index[name])

    } else
      console.log("\tNOT A PATCH:", localPaths[i])
  }

  console.log(JSON.stringify(index, null, 4))

  await fs.writeFile(
    path.resolve(patchDirectory, "patch_index.json"),
    JSON.stringify(index, null, 4)
  )
  console.log("Updated index", path.resolve(patchDirectory, "patch_index.json"))
}

compilePatchList()
