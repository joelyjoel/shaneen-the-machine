const Patch = require("../Patch.js")
const Sampler = require("../components/Sampler.js")
const Space = require("./Space.js")
const config = require("../config.js")

function GlitchySpaceThing(sampler) {
  Patch.call(this)

  if(sampler && sampler.constructor == String)
    sampler = new Sampler().choose(sampler)

  this.addUnits(
    this.sampler = sampler || new Sampler(),
    this.space = new Space(this.sampler),
  )

  this.schedule(0, function() {
    var interval = 0.1
    this.sampler.t0 = Math.random()*(this.sampler.lengthInSeconds - interval) * config.sampleRate
    this.sampler.trigger()

    this.space.PLACEMENT = [
      (Math.random()*2-1) * 5,
      (Math.random()*2-1) * 5,
    ]

    return interval
  })

  this.aliasOutlet(this.space.OUT, "out")
}
GlitchySpaceThing.prototype = Object.create(Patch.prototype)
GlitchySpaceThing.prototype.constructor = GlitchySpaceThing
module.exports = GlitchySpaceThing
