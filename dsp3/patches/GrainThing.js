const Patch = require("../Patch.js")
const GrainCatcher = require("../components/GrainCatcher.js")
const SampleQ = require("../components/SampleQ.js")
const config = require("../config.js")


function GrainThing(input, delay) {
  Patch.call(this)

  this.addUnits(
    this.catcher = new GrainCatcher(),
    this.q = new SampleQ(),
  )

  this.catcher.addOutlet("chain")
  this.q.addInlet("chain")
  this.q.CHAIN = this.catcher.CHAIN

  this.catcher.q = this.q
  this.catcher.onGrain = function(grain, t) {
    this.q.addSample(grain)
    if(this.ownerPatch.onGrain)
      this.ownerPatch.onGrain(grain, t)
  }

  

  this.alias(this.catcher.IN)
  this.alias(this.q.OUT)

  this.IN = input || 0
}
GrainThing.prototype = Object.create(Patch.prototype)
GrainThing.prototype.constructor = GrainThing
module.exports = GrainThing
