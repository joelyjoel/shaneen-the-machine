const Patch = require("../Patch.js")
const Repeater = require("../components/Repeater")
const Multiply = require("../components/Multiply")
const Divide = require("../components/Divide.js")
const Sampler = require("../components/Sampler")

class ThePablo extends Patch {
  constructor(chooseString="vocal", frequency=50, loopDuration=0.1) {
    super()

    this.addUnits(
      this.fToD = new Divide(1, frequency),
      this.sampler = new Sampler(chooseString),

    )
    this.sampler.stopAtEnd = false
    this.sampler.lockT0ToLoopStart
    this.sampler.loopBuffer = true
    this.sampler.loop(0, this.fToD)
    this.sampler.XF = this.fToD
    //this.sampler.RATE = this.durationTimesFrequency
    this.sampler.trigger()

    this.aliasInlet(this.fToD.B, "f")
    this.aliasInlet(this.sampler.LOOPSTART, "t")
    this.aliasOutlet(this.sampler.OUT)

    this.F = frequency
  }
}
module.exports = ThePablo
