const Patch = require("../Patch.js")
const Space = require("./Space.js")
const Sampler = require("../components/Sampler.js")

function SpaceSampler(sampler) {
  Patch.call(this)

  this.addUnits(
    this.sampler = sampler||new Sampler(),
    this.space = new Space(),
  )

  this.space.IN = this.sampler.OUT
  this.aliasInlet(this.space.PLACEMENT)
  this.aliasOutlet(this.space.OUT)
}
SpaceSampler.prototype = Object.create(Patch.prototype)
SpaceSampler.prototype.constructor = SpaceSampler
module.exports = SpaceSampler

SpaceSampler.prototype.trigger = function() {
  this.sampler.trigger()
}
SpaceSampler.prototype.play = function() {
  this.sampler.play()
}
SpaceSampler.prototype.pause = function() {
  this.sampler.pause()
}
SpaceSampler.prototype.stop = function() {
  this.sampler.stop()
}
SpaceSampler.prototype.loop = function(loop0, loop1, loopN) {
  this.sampler.loop(loop0, loop1, loopN)
  return this
}
SpaceSampler.prototype.choose = function(kind) {
  this.sampler.choose(kind)
  return this
}
