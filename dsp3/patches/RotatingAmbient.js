const Patch = require("../Patch.js")

const SampleQ = require("../components/SampleQ.js")
const Space = require("./Space.js")
const CircularMotion = require("../components/vector/CircularMotion.js")

function RotatingAmbient(f, r, o) {
  Patch.call(this)

  this.addUnits(
    this.ambient = new SampleQ().ambient(1),
    this.motion = new CircularMotion(),
    this.space = new Space(this.ambient, this.motion),
  )

  this.aliasInlet(this.motion.F)
  this.aliasInlet(this.motion.RADIUS)
  this.aliasInlet(this.motion.CENTRE)
  this.aliasOutlet(this.space.OUT)

  this.F = f || 10
  this.RADIUS = r || 3
  this.CENTRE = o || [0, 0]
}
RotatingAmbient.prototype = Object.create(Patch.prototype)
RotatingAmbient.prototype.constructor = RotatingAmbient
module.exports = RotatingAmbient

RotatingAmbient.random = function(fMax, rMax, oMax) {
  return new RotatingAmbient(
    Math.random() * (fMax || 2),
    Math.random() * (rMax || 5),
    [
      (Math.random()*2-1) * (oMax || 5),
      (Math.random()*2-1) * (oMax || 5),
    ],
  )
}

RotatingAmbient.prototype.shuffle = function() {
  this.ambient.shuffle()
}
