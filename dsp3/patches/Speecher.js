const TriggerGroup = require("./TriggerGroup.js")
const SampleQ = require("../components/SampleQ.js")

function Speecher(lang) {
  TriggerGroup.call(this)

  this.lang = lang || "en-uk"
}
Speecher.prototype = Object.create(TriggerGroup.prototype)
Speecher.prototype.constructor = Speecher
module.exports = Speecher

Speecher.prototype.handleUnknownTrigger = function(which) {
  var sampler = new SampleQ().gtts(which, this.lang)

  sampler.selectionLogic = function(i) {
    return i + 0.3
  }

  this.addTrigger(sampler, which)
  this.trigger(which)
  console.log(this.triggers[which])
}
