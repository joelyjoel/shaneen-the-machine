const patches = require("./patch_index.json")

for(var i in patches) {
  exports[i] = require("./"+patches[i])
}
