const Patch = require("../Patch.js")

const SampleQ = require("../components/SampleQ.js")
const Space = require("./Space.js")
const CircularMotion = require("../components/vector/CircularMotion.js")
const ComplexOrbit = require("../ComplexOrbit.js")

function ComplexOrbitAmbient(f, r, o) {
  Patch.call(this)

  this.addUnits(
    this.ambient = new SampleQ().ambient(1),
    this.motion = new CircularMotion(),
    this.space = new Space(this.ambient, this.motion),
  )

  this.aliasInlet(this.motion.F)
  this.aliasInlet(this.motion.RADIUS)
  this.aliasInlet(this.motion.CENTRE)
  this.aliasOutlet(this.space.OUT)

  this.F = f || 10
  this.RADIUS = r || 3
  this.CENTRE = o || [0, 0]
}
ComplexOrbitAmbient.prototype = Object.create(Patch.prototype)
ComplexOrbitAmbient.prototype.constructor = ComplexOrbitAmbient
module.exports = ComplexOrbitAmbient

ComplexOrbitAmbient.random = function(fMax, rMax, oMax) {
  return new ComplexOrbitAmbient(
    Math.random() * (fMax || 2),
    Math.random() * (rMax || 5),
    [
      (Math.random()*2-1) * (oMax || 5),
      (Math.random()*2-1) * (oMax || 5),
    ],
  )
}

ComplexOrbitAmbient.prototype.shuffle = function() {
  this.ambient.shuffle()
}
