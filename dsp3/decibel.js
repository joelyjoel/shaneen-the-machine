function dbToSf(db) { // decibel to scale factor (for amplitude calculations)
  return Math.pow(10, db/20);
}
function sfToDb(sf) { // scale factor to decibel (amplitude)
  return 20 * Math.log10(sf)
}
function dbToSf_power(db) { // decibel to scale factor (for power calculations)
  return Math.pow(10, db/10);
}
function sfToDb_power(sf) { // scale factor to decibel (power)
  return 10 * Math.log10(sf)
}
function logScale(x) {
  return Math.pow(10, (x-1) * 3)
}

module.exports = dbToSf;
module.exports.dbToSf = dbToSf;
module.exports.sfToDb = sfToDb;
module.exports.dbToSf_power = dbToSf_power;
module.exports.sfToDb_power = sfToDb_power;
module.exports.logScale = logScale
