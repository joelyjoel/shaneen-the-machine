const DspUnit = require("./DspUnit.js");

// var mAvL1 = 0.443 / (config.bassCutoff / sampleRate) // (moving average length)
function LowPass(f) {
  // A moving average low pass filter
  DspUnit.call(this);

  this.buffer = new Float32Array(1024);
  this.sum = 0;
  this.sumStart = 0;
  this.sumEnd = 0

  this.f = f || 1000;
}
LowPass.prototype = Object.create(DspUnit.prototype);
LowPass.prototype.constructor = LowPass;

LowPass.prototype.__defineSocket__("in") // input
LowPass.prototype.__defineSocket__("f", function(f) {
  this.movingAverageLength = 0.443 * this.sampleRate / f;
  return f;
});

LowPass.prototype.__definePlug__("y");

/*LowPass.prototype.__defineGetter__("movingAverageLength", function() {
  return 0.443 * this.sampleRate / this.f;
});*/

LowPass.prototype._tick = function() {
  // copy incoming signal to buffer
  this.buffer[this.clock%this.buffer.length] = this.in;

  // advance end of sum to current time
  while(this.sumEnd < this.clock) {
    this.sum += this.buffer[this.sumEnd%this.buffer.length];
    this.sumEnd++;
  }
  // advance (or backtrack) beginning of sum to current time - averagelength
  var targetSumStart = Math.floor(this.clock - this.movingAverageLength);
  if(targetSumStart < 0)
    targetSumStart = 0;
  while(this.sumStart < targetSumStart) {
    this.sum -= this.buffer[this.sumStart%this.buffer.length];
    this.sumStart++;
  }
  while(this.sumStart > targetSumStart && this.sumStart > 0) {
    this.sum += this.buffer[this.sumStart%this.buffer.length];
    this.sumStart--;
  }

  this.y = this.sum / (this.sumEnd - this.sumStart);
}

module.exports = LowPass;
