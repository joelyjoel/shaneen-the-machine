const DspUnit = require("./DspUnit.js");
const mToF = require("./midiToFrequency.js")

function Noise(nChannels, f) {
  DspUnit.call(this);

  this.f = f || this.sampleRate
  this.numberOfChannels = nChannels || 1
  this.wait = 0

  this.min = -1
  this.max = 1
}
Noise.prototype = Object.create(DspUnit.prototype);
Noise.prototype.constructor = Noise;
module.exports = Noise;

Noise.prototype.__defineSocket__("f", function(f) {
  this.interval = Math.ceil(this.sampleRate/f)
  return f
})
Noise.prototype.__defineSocket__("p", function(p) {
  this.f = mToF(p)
  return p
})
Noise.prototype.__defineSocket__("min")
Noise.prototype.__defineSocket__("max")
Noise.prototype.__definePlug__("y")

Noise.prototype._tick = function() {
  this.wait--
  if(this.wait <= 0) {
    this.wait = this.interval

    var min = this.min
    var max = this.max

    var y = []
    for(var c=0; c<this.numberOfChannels; c++)
      y[c] = Math.random()*(max-min) + min
    this.y = y
  }
}

Noise.prototype.__defineGetter__("fNow", function() {
  // (for compatibility)
  return this.f
})
