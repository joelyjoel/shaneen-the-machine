const DspUnit = require("./DspUnit.js");

function Multiply(A, B) {
  DspUnit.call(this);
  this.A = A;
  this.B = B;
}
Multiply.prototype = Object.create(DspUnit.prototype);
Multiply.prototype.constructor = Multiply;
module.exports = Multiply;

Multiply.prototype.__defineSocket__("A");
Multiply.prototype.__defineSocket__("B");
Multiply.prototype.__definePlug__("y");

Multiply.prototype._tick = function() {
  this.y = (this.A * this.B) || 0;
}
