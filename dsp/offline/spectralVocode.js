const Sample = require("../Sample.js")
const SpectralSample = require("../SpectralSample.js")

function spectralVocode(sample1, sample2, dial, frameSize) {
  // make a SpectralSample with the magnitudes from sample1 and the phases from sample2
  if(sample1.isSample)
    sample1 = sample1.toSpectralSample(frameSize || 256)
  if(sample2.isSample)
    sample2 = sample2.toSpectralSample(sample1.frameSize)

  dial = dial==undefined ? 1 : dial
  console.log("dial:", dial)

  var out = new SpectralSample().blank(
    sample1.lengthInSamples < sample2.lengthInSamples ? sample1.lengthInSamples : sample2.lengthInSamples,
    sample1.numberOfChannels < sample2.numberOfChannels ? sample1.numberOfChannels : sample2.numberOfChannels,
    sample1.frameSize
  )

  for(var c in out.channelData) {
    for(var frame in out.channelData[c]) {
      for(var bin in out.channelData[c][frame].phasors) {
        var magnitude = dial*sample1.channelData[c][frame].phasors[bin].magnitude + (1-dial)*sample2.channelData[c][frame].phasors[bin].magnitude
        var phase = dial*sample2.channelData[c][frame].phasors[bin].phase + (1-dial)*sample1.channelData[c][frame].phasors[bin].phase
        out.channelData[c][frame].phasors[bin].setPolar(
          magnitude,
          phase,
        )
      }
    }
  }


  out.label = "vocoded sample"
  return out;
}

module.exports = spectralVocode
