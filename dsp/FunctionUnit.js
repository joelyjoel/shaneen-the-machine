const DspUnit = require("./DspUnit.js")

function FunctionUnit(plug, func) {
  DspUnit.call(this)
  this.func = func || (x, c) => { return y }
  this.in = plug || 0
}
FunctionUnit.prototype = Object.constructor(DspUnit.prototype)
FunctionUnit.prototype.constructor = FunctionUnit
module.exports = FunctionUnit

FunctionUnit.prototype.__defineSocket__("in", null, "array")
FunctionUnit.prototype.__definePlug__("y", "array")

FunctionUnit.prototype._tick = function() {
  var x = this.in
  var y = []
  if(this.func)
    for(var c in x)
      y[c] = this.func(x[c], c)
  this.y = y
}
