const DspUnit = require("./DspUnit.js")

function Butterworth() {
  DspUnit.call(this)

  this.type = "LP"
  this.x1 = []
  this.x2 = []
  this.y1 = []
  this.y2 = []

  this.f = 22000
  this.bandwidth = 1
}
Butterworth.prototype = Object.create(DspUnit.prototype)
Butterworth.prototype.constructor = Butterworth
module.exports = Butterworth

Butterworth.prototype.__defineSocket__("in", null, "array")
Butterworth.prototype.__defineSocket__("f", function(f) {
  switch(this.type) {
    case "LP":
      var lamda = 1/Math.tan(Math.PI * f/this.sampleRate)
      var lamdaSquared = lamda * lamda
      this.a0 = 1/(1 + 2*lamda + lamdaSquared)
      this.a1 = 2 * this.a0
      this.a2 = this.a0
      this.b1 = 2 * this.a0 * (1 - lamdaSquared)
      this.b2 = this.a0 * (1 - 2 * lamda + lamdaSquared)
      break;
    case "HP":
      var lamda = Math.tan(Math.PI * f / this.sampleRate)
      var lamdaSquared = lamda * lamda
      this.a0 = 1/(1 + 2*lamda + lamdaSquared)
      this.a1 = 2 * this.a0
      this.a2 = this.a0
      this.b1 = 2 * this.a0 * (lamdaSquared-1)
      this.b2 = this.a0 * (1 - 2*lamda + lamdaSquared)
      break;
    case "BP":
      var lamda = 1/Math.tan(Math.PI * this.bandwidth/this.sampleRate)
      var phi = 2 * Math.cos(2*Math.PI * f/this.sampleRate)
      this.a0 = 1/(1+lamda)
      this.a1 = 0
      this.a2 = -this.a0
      this.b1 = - lamda * phi * this.a0
      this.b2 = this.a0 * (lamda - 1)
      break;
    case "BR":
      var lamda = Math.tan(Math.PI * this.bandwidth/this.sampleRate)
      var phi = 2 * Math.cos(2*Math.PI * f/this.sampleRate)
      this.a0 = 1/(1+lamda)
      this.a1 = - phi * this.a0
      this.a2 = this.a0
      this.b1 = - phi * this.a0
      this.b2 = this.a0 * (lamda - 1)
      console.log(f, this)
      break;
  }
  return f
})
Butterworth.prototype.__definePlug__("y", "array")

Butterworth.prototype._tick = function() {
  var x = this.in
  var y = []
  for(var c in x) {
    y[c] = x[c] - this.a2 * (this.x2[c] || 0) - this.b1 * (this.y1[c] || 0) - this.b2 * (this.y2[c] || 0)
    this.y2[c] = this.y1[c] || 0
    this.y1[c] = y[c]
    this.x2[c] = this.x1[c] || 0
    this.x1[c] = x[c]
  }
  console.log(y)
  this.y = y
}
