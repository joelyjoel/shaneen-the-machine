const DspUnit = require("./DspUnit.js")
const dB = require("./decibel.js")

function Shaper(plug) {
  DspUnit.call(this)
  this.gain = 0
  this.postGain = 0
  this.in = plug || 0
  this.compensate = false
  this.compensationNeedsCalculating = true
}
Shaper.prototype = Object.create(DspUnit.prototype)
Shaper.prototype.constructor = Shaper
module.exports = Shaper

Shaper.prototype.__defineSocket__("in", null, "array")
Shaper.prototype.__defineSocket__("gain", function(gain) {
  this.gainSf = dB(gain)
  this.compensationNeedsCalculating = true
  return gain
})
Shaper.prototype.__defineSocket__("postGain", function(gain) {
  this.postGainSf = dB(gain)
  return gain
})
Shaper.prototype.__definePlug__("y", "array")

Shaper.prototype._tick = function() {
  var x = this.in
  var y = []
  for(var c in x) {
    y[c] = this.F(this.gainSf * x[c]) * this.postGainSf
    if(this.compensate) {
      if(this.compensationNeedsCalculating)
        this.calculateCompensation()
      y[c] *= this.compensation
    }
  }
  this.y = y
}
Shaper.prototype.calculateCompensation = function() {
  if(this.compensate)
    this.compensation = this.F(1)/this.F(this.gainSf)
  else
    this.compensation = 0
  this.compensationNeedsCalculating = false
  return this.compensation
}

Shaper.prototype.F = function(x) {
  return Math.sin(x*Math.PI)
}
