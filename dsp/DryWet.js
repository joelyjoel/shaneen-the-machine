const DspUnit = require("./DspUnit.js");

function DryWet() {
  DspUnit.call(this);
  this.dryIn = 0;
  this.wetIn = 0;
  this.balance = 0;
}
DryWet.prototype = Object.create(DspUnit);
DryWet.prototype.constructor = DryWet;
module.exports = DryWet;

DryWet.prototype.__defineSocket__("dryIn", undefined, "array")
DryWet.prototype.__defineSocket__("wetIn", undefined, "array")
DryWet.prototype.__defineSocket__("balance", function(balance) {
  this.drySf = 1 - balance;
  this.wetSf = balance;
})
DryWet.prototype.__definePlug__("y", "array")

DryWet.prototype._tick = function() {
  var y = [];

  var nC = (this.dryIn.length > this.wetIn.length) ? this.dryIn.length : this.wetIn.length;
  for(var c=0; c<nC; c++)
    y[c] = this.drySf * (this.dryIn[c] || 0) + this.wetSf * (this.wetIn[c] || 0);
  this.y = y;
}
