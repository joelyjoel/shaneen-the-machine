var Osc = require("./Osc.js");
var Ramp = require("./Ramp.js");
var PointTrackPlayer = require("./PointTrackPlayer.js");

function SimpleSynth() {
  this.osc = new Osc();
  this.envelope = Ramp.randomDecay(4410);
  this.envelope.setToEnd();
  this.sequencer = new PointTrackPlayer();
  this.sequencer.osc = this.osc;
  this.sequencer.ramp = this.envelope
  this.sequencer.onNoteOn = function(note) {
    this.osc.p = note.sound;
    this.ramp.trigger();
    //this.osc.randomiseWaveform();
  }
  this.osc.A = this.envelope.y;
  this.osc.chain(this.sequencer);
}
module.exports = SimpleSynth;

SimpleSynth.prototype.__defineGetter__("track", function() {
  return this.sequencer.track;
});
SimpleSynth.prototype.__defineSetter__("track", function(track) {
  this.sequencer.track = track;
});

SimpleSynth.prototype.__defineGetter__("waveform", function() {
  return this.osc.waveform;
});
SimpleSynth.prototype.__defineSetter__("waveform", function(waveform) {
  this.osc.waveform = waveform;
})

SimpleSynth.prototype.__defineGetter__("y", function() {
  return this.osc.y;
});
