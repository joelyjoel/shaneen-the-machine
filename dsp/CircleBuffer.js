function CircleBuffer(lengthInSamples, numberOfChannels) {
  this.lengthInSamples = lengthInSamples || 44100;
  this.numberOfChannels = numberOfChannels || 1
  this.channelData = [];
  this.linearInterpolateCursors = [];
  for(var c=0; c<this.numberOfChannels; c++)
    this.channelData[c] = new Float32Array(this.lengthInSamples);

  this.t = 0;
}
module.exports = CircleBuffer;

CircleBuffer.prototype.shift = function() {
  var y = [];
  var t = this.t % this.lengthInSamples;
  for(var c in this.channelData) {
    y[c] = this.channelData[c][t];
    this.channelData[c][t] = 0;
  }

  this.y = y;
  this.t++;
  return y;
}
CircleBuffer.prototype.push = function(samp, channel) {
  channel = channel || 0
  if(samp.constructor == Array)
    for(var c=0; c<samp.length; c++)
      this.channelData[(c+channel)%this.numberOfChannels][this.t] = samp[c]
  else
    this.channelData[channel][this.t] = samp;
  this.t++
}

CircleBuffer.prototype.read = function(t, c) {
  return this.channelData[c][t%this.lengthInSamples]
}

CircleBuffer.prototype.last = function(n) {
  n = n || this.lengthInSamples;
  var t0 = this.t-n;

  var out = [];
  for(var c=0; c<this.numberOfChannels; c++) {
    out[c] = new Float32Array(n)
    for(var t=t0; t<this.t; t++)
      out[c][t] = this.channelData[c][t%this.lengthInSamples];
  }
  return out
}

CircleBuffer.prototype.resize = function(lIS, nOC, preserveEnd) {
  var newChannelData = []
  nOC = nOC || this.numberOfChannels
  for(var c=0; c<nOC; c++) {
    newChannelData[c] = new Float32Array(lIS)
    if(this.channelData[c])
      if(preserveEnd)
        for(var t=1; t<=lIS && t<=this.lengthInSamples; t++)
          newChannelData[c][lIS-t] = this.channelData[c][(this.t-t)%this.lengthInSamples]
      else
        for(var t=0; t<lIS && t<this.lengthInSamples; t++)
          newChannelData[c][t] = this.channelData[c][(this.t+t)%this.lengthInSamples]
  }
  this.t = 0
  this.channelData = newChannelData
  this.numberOfChannels = nOC
  this.lengthInSamples = lIS
}
CircleBuffer.prototype.addChannels = function(n) {
  n = n || 1;
  for(var i=0; i<n; i++) {
    this.channelData.push(new Float32Array(this.lengthInSamples))
    this.numberOfChannels++;
  }
}

CircleBuffer.prototype.mixAhead = function(samp, dT, channel) {
  if(dT%1 != 0) {
    samp = samp || 0;
    this.mixAhead(samp * (1-dT%1), Math.floor(dT), channel);
    this.mixAhead(samp * (dT%1),   Math.ceil(dT), channel);
    return;
  }
  if(dT > this.lengthInSamples) {
    console.log("buffer not large enough!")
    return;
  }
  //console.log("samp:", samp, "dT:", dT, "channel:", channel)
  var t = (this.t + (dT || 1)) % this.lengthInSamples;
  this.channelData[channel || 0][t] += (samp || 0);
}
CircleBuffer.prototype.overwriteAhead = function(samp, dT, channel) {
  if(dT > this.lengthInSamples) {
    console.log("buffer not large enough!")
    return;
  }
  //console.log("samp:", samp, "dT:", dT, "channel:", channel)
  var t = (this.t + (dT || 1)) % this.lengthInSamples;
  this.channelData[channel || 0][Math.floor(t)] = samp || 0;
}
CircleBuffer.prototype.linearInterpolateMixAhead = function(s1, dT, c) {
  var T = this.lengthInSamples
  var t1 = this.t + dT
  var t0 = this.linearInterpolateCursors[c] || 0 // must be int
  var s0 = this.channelData[c][t0%T]

  var k = (s1-s0)/(t1-t0) // efficiency saving constant
  if(t1 > t0) {
    for(var t=t0+1; t<=t1; t++) {
      this.channelData[c][t%T] += (t-t0) * k + s0
    }
    this.linearInterpolateCursors[c] = Math.floor(t1)
  } else if(t1 < t0) {
    for(var t=t0-1; t>=t1; t--) {
      this.channelData[c][t%T] += (t-t0) * k + s0
    }
    this.linearInterpolateCursors[c] = Math.ceil(t1)
  }
}
