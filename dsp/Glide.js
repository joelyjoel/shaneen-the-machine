const DspUnit = require("./DspUnit.js")

function Glide() {
  // simple linear interpolation DspUnit
  DspUnit.call(this)

  this.glideTime = 4410;
  this.x = [0]
}
Glide.prototype = Object.create(DspUnit.prototype)
Glide.prototype.constructor = Glide
module.exports = Glide

Glide.prototype.__defineSocket__("x", function(x) {
  this.m = []
  var y = this._y || []
  for(var i in x)
    this.m[i] = (x[i] - (y[i] || x[i]))
  this.progress = 0
  return x
}, "array")
Glide.prototype.__defineSocket__("glideTime", function(gt) {
  this.rate = 1/(gt || 1)
  return gt
})
Glide.prototype.__definePlug__("y", "array")

Glide.prototype._tick = function() {
  var y = this._y || []
  for(var i in this.m)
    y[i] = (y[i] || 0) + this.m[i] * this.rate
  this.y = y

  this.progress += this.rate


  if(this.progress >= 1) {
    this.m = []
    if(this.onFinish)
      this.onFinish()
  }
}
