var Phasor = require("./Phasor.js");

function SpectralFrame(phasors) {
  this.phasors = phasors || [];
}
module.exports = SpectralFrame;


SpectralFrame.prototype.duplicate = function() {
  var newPhasors = this.phasors.map(function(phasor) {
    return phasor.duplicate();
  })
  return new SpectralFrame(newPhasors);
}

SpectralFrame.prototype.map = function(F) {
  var newPhasors = this.phasors.map(F);
  return new SpectralFrame(newPhasors);
}

SpectralFrame.prototype.multiplyByFrame = function(frame) {
  var newPhasors = [];
  for(var bin in frame.phasors) {
    newPhasors[bin] = Phasor.multiply(frame.phasors[bin], this.phasors[bin]);
  }
  return new SpectralFrame(newPhasors);
}
