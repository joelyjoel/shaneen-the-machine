const Poly = require("./Poly.js")
const SamplerMono = require("./SamplerMono.js")
const Sample = require("../Sample.js")

function SamplerPoly(sample) {
  Poly.call(this, SamplerMono)

  this.A = 441
  this.D = 4410
  this.S = 0
  this.R = 4410
  this.sample = sample
}
SamplerPoly.prototype = Object.create(Poly.prototype)
SamplerPoly.prototype.constructor = SamplerPoly
module.exports = SamplerPoly

SamplerPoly.choose = function(kind) {
  return Sample.choose(kind).then(function(sample) {
    return new SamplerPoly(sample)
  })
}

SamplerPoly.prototype.__defineRepeater__("A")
SamplerPoly.prototype.__defineRepeater__("D")
SamplerPoly.prototype.__defineRepeater__("S")
SamplerPoly.prototype.__defineRepeater__("R")

SamplerPoly.prototype.setUpMono = function(mono) {
  mono.sample = this.sample
  mono.tuning = this.tuning

  /*mono._envelope.A = this.APlug
  mono._envelope.D = this.DPlug
  mono._envelope.S = this.SPlug
  mono._envelope.R = this.RPlug*/
}

SamplerPoly.prototype.__defineGetter__("label", function() {
  return this._label || this.sample.label
})
SamplerPoly.prototype.__defineSetter__("label", function(label) {
  this._label = label
})

SamplerPoly.prototype.tuneSample = function() {
  var pNatural = this.sample.estimateP() // f is for frequency
  this.tuning = -pNatural%12 - 60
  console.log(this.tuning)
}
