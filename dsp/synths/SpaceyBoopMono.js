const Mono = require("./Mono.js")
const Osc = require("../Osc.js")
const ADSR = require("../ADSR.js")
const Spatialise = require("../Spatialise.js")

function SpaceyBoop() {
  Mono.call(this)

  this.osc = new Osc()
  this.osc.waveform = "saw"
  this.envelope = new ADSR(1, 44100, 0, 44100)
  this.envelope.synth = this
  this.envelope.onFinish = function() {
    this.synth.finish()
  }
  this.osc.A = this.envelope
  this.space = Spatialise.random(this.osc)
  this.in = this.space
}
SpaceyBoop.prototype = Object.create(Mono.prototype)
SpaceyBoop.prototype.constructor = SpaceyBoop
module.exports = SpaceyBoop

SpaceyBoop.prototype.__defineSocket__("in", null, "array")
SpaceyBoop.prototype.__definePlug__("y", "array")

SpaceyBoop.prototype._tick = function() {
  this.y = this.in
}

SpaceyBoop.prototype.trigger = function(p, note) {
  this.osc.p = p
  this.envelope.trigger()
}
SpaceyBoop.prototype.release = function(p, note) {
  this.envelope.release()
}
