const Poly = require("./Poly.js")
const SimpleSineMono = require("./SimpleSineMono.js")
const Osc = require("../Osc.js")

function SimpleSinePoly(A, D, S, R) {
  Poly.call(this, SimpleSineMono)

  this.A = A || 44100
  this.D = D || 44100
  this.S = S || 0
  this.R = R || 44100
}
SimpleSinePoly.prototype = Object.create(Poly.prototype)
SimpleSinePoly.prototype.constructor = SimpleSinePoly
module.exports = SimpleSinePoly

SimpleSinePoly.prototype.__defineRepeater__("A")
SimpleSinePoly.prototype.__defineRepeater__("D")
SimpleSinePoly.prototype.__defineRepeater__("S")
SimpleSinePoly.prototype.__defineRepeater__("R")

SimpleSinePoly.prototype.setUpMono = function(mono) {
  mono._envelope.A = this.APlug
  mono._envelope.D = this.DPlug
  mono._envelope.S = this.SPlug
  mono._envelope.R = this.RPlug
}
