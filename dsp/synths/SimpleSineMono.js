const Mono = require("./Mono.js")
const Osc = require("../Osc.js")
const ADSR = require("../ADSR.js")

function SimpleSineMono(A,D,S,R) {
  Mono.call(this)

  this.osc = new Osc()
  this.envelope = new ADSR(
    A || 10,
    D || 44100/3,
    S || 0.5,
    R || 44100/3,
  )
}
SimpleSineMono.prototype = Object.create(Mono.prototype)
SimpleSineMono.prototype.constructor = SimpleSineMono
module.exports = SimpleSineMono

SimpleSineMono.prototype.__defineSocket__("osc")
SimpleSineMono.prototype.__defineSocket__("envelope")

SimpleSineMono.prototype._tick = function() {
  if(this._envelope && this._envelope.finished)
    this.finish()

  this.y = [this.osc * this.envelope]
}

SimpleSineMono.prototype.trigger = function(p, note) {
  this._osc.p = p
  this._envelope.trigger()
}

SimpleSineMono.prototype.release = function(p, note) {
  this._envelope.release()
}
