const Synth = require("./Synth.js")

function Mono() {
  // a mother class for monophonic synthesisers
  Synth.call(this)
}
Mono.prototype = Object.create(Synth.prototype)
Mono.prototype.constructor = Mono
module.exports = Mono

Mono.prototype.isMonoSynth = true
