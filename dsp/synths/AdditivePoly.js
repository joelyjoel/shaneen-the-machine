const Poly = require("./Poly.js")
const AdditiveMono = require("./AdditiveMono.js")

function AdditivePoly(partials, A, D, S, R) {
  Poly.call(this, AdditiveMono)

  this.partials = partials || [1,2,3]
  this.A = A || 44
  this.D = D || 22050
  this.S = 0
  this.R = 22050
}
AdditivePoly.prototype = Object.create(Poly.prototype)
AdditivePoly.prototype.constructor = AdditivePoly
module.exports = AdditivePoly

AdditivePoly.prototype.__defineRepeater__("A")
AdditivePoly.prototype.__defineRepeater__("D")
AdditivePoly.prototype.__defineRepeater__("R")
AdditivePoly.prototype.__defineRepeater__("S")

AdditivePoly.prototype.setUpMono = function(mono) {
  mono.envelope.A = this.APlug
  mono.envelope.D = this.DPlug
  mono.envelope.S = this.SPlug
  mono.envelope.R = this.RPlug
  //mono._osc.partials = this.partials
}
