const Mono = require("./Mono.js")
const Osc = require("../Osc.js")
const Filter = require("../Filter.js")
const ADSR = require("../ADSR.js")
const Add = require("../AddNumbers.js")
const Multiply = require("../Multiply.js")
const Glide = require("../Glide.js")

function AcidMono() {
  Mono.call(this)


  // non replaceable
  //this.filterEnvelopeScaler = new Multiply(1, 1)
  this.osc = new Osc()
  this.osc.waveform = "saw"
  this.osc.p = new Glide()
  this.cutOffSum = new Add(0, 0)
  this.filter = new Filter()
  this._filter.p = this.cutOffSum
  this._filter.in = this.osc.y

  // replaceable
  this.filterEnvelope = new ADSR()
  this.envelopeAmmount = 100
  this.pCutOff = 36
}
AcidMono.prototype = Object.create(Mono.prototype)
AcidMono.prototype.constructor = AcidMono
module.exports = AcidMono

AcidMono.prototype.__defineSocket__("filter", null, "array")

AcidMono.prototype._tick = function() {
  this.y = this.filter
}
AcidMono.prototype.trigger = function(p, note) {
  this.osc._p.x = p
  this.filterEnvelope.trigger()
}
AcidMono.prototype.release = function(p, note) {
  this.filterEnvelope.release()
}


AcidMono.prototype.__defineSetter__("filterEnvelope", function(env) {
  var ammount = this.envelopeAmmount || 0
  this.cutOffSum.A = env
  this.envelopeAmmount = ammount
})
AcidMono.prototype.__defineGetter__("filterEnvelope", function() {
  return this.cutOffSum._A
})
AcidMono.prototype.__defineSetter__("pCutOff", function(fCutOff) {
  this.cutOffSum.B = fCutOff
})
AcidMono.prototype.__defineGetter__("pCutOff", function() {
  return this.cutOffSum.B
})
AcidMono.prototype.__defineGetter__("_pCutOff", function() {
  return this.cutOffSum._B
})
AcidMono.prototype.__defineSetter__("envelopeAmmount", function(ammount) {
  this.filterEnvelope.master = ammount
})
AcidMono.prototype.__defineGetter__("envelopeAmmount", function() {
  return this.filterEnvelope._master
})
