const DspUnit = require("../DspUnit.js")

function Synth() {
  // a mother class for synthesisers
  DspUnit.call(this)
}
Synth.prototype = Object.create(DspUnit.prototype)
Synth.prototype.constructor = Synth
module.exports = Synth

Synth.prototype.isSynth = true

Synth.prototype.__definePlug__("y", "array")

Synth.prototype.trigger = function(p, noteDetails) {
  // play a note
  console.log("trigger has not been defined yet", this.label)
}

Synth.prototype.release = function(p, noteDetails) {
  // stop playing a note
  console.log("release() has note been defined yet", this.label)
}

Synth.prototype.scheduleNote = function(note, semiquaverInSamples, t0) {
  semiquaverInSamples = semiquaverInSamples || 5512.5
  t0 = t0 || 0
  var p = note.p
  var tOn = note.t*semiquaverInSamples + t0
  var tOff = note.tOff * semiquaverInSamples + t0

  if(!isNaN(tOn))
    this.schedule(tOn, function() {
      this.trigger(p, note)
    })
  if(!isNaN(tOff))
    this.schedule(tOff, function() {
      this.release(p, note)
    })
}

Synth.prototype.scheduleTrigger = function(t, p, note) {
  this.schedule(t, function() {
    this.trigger(p, note)
  })
}
Synth.prototype.scheduleRelease = function(t, p, note) {
  console.log("scheduling release", t, p)
  this.schedule(t, function() {
    this.release(p, note)
  })
}

Synth.prototype.scheduleTrack = function(track, bpm, t0) {
  var bpm = bpm || track.bpm || 120
  console.log("using bpm:", bpm)
  var semiquaverInSamples = this.sampleRate * 60/4 / bpm
  var t0 = t0 || 0
  track = track.splitArraySounds()

  for(var i in track.notes) {
    this.scheduleNote(track.notes[i], semiquaverInSamples, t0)
  }
}
