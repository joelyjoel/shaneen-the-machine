const Mono = require("./Mono.js")
const ADSR = require("../ADSR.js")
const MultiOsc = require("../MultiOsc.js")

function AdditiveMono(partials, attack, decay, sustain, release) {
  Mono.call(this)

  partials = partials || [1,2,3]

  this.osc = new MultiOsc(partials)
  this.envelope = new ADSR(attack, decay, sustain, release)
  this.envelope.onFinish = function() {
    this.mono.finish()
  }
  this.envelope.mono = this
  this._osc.A = this.envelope
}
AdditiveMono.prototype = Object.create(Mono.prototype)
AdditiveMono.prototype.construtor = AdditiveMono
module.exports = AdditiveMono

AdditiveMono.prototype.__defineSocket__("osc")
AdditiveMono.prototype.__definePlug__("y")

AdditiveMono.prototype._tick = function() {
  this.y = this.osc
}

AdditiveMono.prototype.trigger = function(p, note) {
  this._osc.p = p
  this.envelope.trigger()
}
AdditiveMono.prototype.release = function(p, note) {
  this.envelope.release()
}
