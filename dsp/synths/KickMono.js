const Mono = require("./Mono.js")
const Osc = require("../Osc.js")
const Ramp = require("../Ramp.js")

function KickMono(T, p, pHi) {
  Mono.call(this)
  this.osc = new Osc()
  this.pitchEnvelope = new Ramp(this.TPlug, this.pHiPlug, this.pPlug, this.sweepCurvePlug)
  this.amplitudeEnvelope = new Ramp(this.TPlug, 1, 0, 0.5)
  this.amplitudeEnvelope.kick = this
  this.amplitudeEnvelope.onFinish = function() {
    this.kick.finish
  }
  this.osc.p = this.pitchEnvelope.y
  this.osc.A = this.amplitudeEnvelope
  this.mix = this.osc

  this.T = T || 44100
  this.p = p || 20
  this.pHi = pHi || 100
  this.sweepCurve = -2
}
KickMono.prototype = Object.create(Mono.prototype)
KickMono.prototype.constructor = KickMono
module.exports = KickMono

KickMono.random = function() {
  var kick = new KickMono()
  kick.T = Math.random()*5*44100
  kick.pHi = Math.random()*70 + 60
  kick.p = Math.random()*30
  kick.sweepCurve = -Math.random()*3 - 1
  return kick
}

KickMono.prototype.__defineRepeater__("T")
KickMono.prototype.__defineRepeater__("pHi")
KickMono.prototype.__defineRepeater__("p")
KickMono.prototype.__defineRepeater__("sweepCurve")
KickMono.prototype.__defineSocket__("mix", null, "array")

KickMono.prototype.trigger = function(p, mono) {
  if(p)
    this.p = p
  this.pitchEnvelope.trigger()
  this.amplitudeEnvelope.trigger()
}

KickMono.prototype._tick = function() {
  this.y = this.mix
}
