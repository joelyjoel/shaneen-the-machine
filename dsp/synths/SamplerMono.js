const Mono = require("./Mono.js")
const Sample = require("../Sample.js")
const textToSpeech = require("../textToSpeech.js")
const mToF = require("../midiToFrequency.js")
const ADSR = require("../ADSR.js")

function SamplerMono(sample) {
  Mono.call(this)

  this.sample = sample
  this.channel = 0
  this.t = 0
  this.t0 = 0
  this.rate = 1
  this.envelope = new ADSR(1, 1, 1, 44100)
  this.playing = false
  this.looping = false
  this.loop0 = 0
  this.loopD = 44100
  this.loopX = 0

  this.tuning = 0 // constant in semitones to tune the sample
}
SamplerMono.prototype = Object.create(Mono.prototype)
SamplerMono.prototype.constructor = SamplerMono
module.exports = SamplerMono

SamplerMono.prototype.__definePlug__("y", "array")
SamplerMono.prototype.__defineSocket__("t0")
SamplerMono.prototype.__defineSocket__("rate")
SamplerMono.prototype.__defineSocket__("p", function(p) {
  this.rate = Math.pow(2, (p + this.tuning)/12)
})
SamplerMono.prototype.__defineSocket__("envelope")
SamplerMono.prototype.__defineSocket__("loop0")
SamplerMono.prototype.__defineSocket__("loopD")
SamplerMono.prototype.__defineSocket__("loopX")

SamplerMono.prototype._tick = function() {
  if(this.t > this.sample.lengthInSamples) {
    if(this.looping && this.loop0+this.loopD >= this.sample.lengthInSamples)
      this.t = this.loop0
    else {
      this.playing = false
      this.t = this.t0
      this.finish()
    }
  }

  if(this.looping) {
    if(this.t > this.loop0 + this.loopD ) {
        this.t = this.loop0
        if(this.onLoop)
          this.onLoop()
    } else if(this.rate < 0 && this.t < this.loop0) {
      this.t = this.loop0 + this.loopD
      if(this.onLoop)
        this.onLoop()
    }
  }
  if(this.playing) {
    this.t += this.rate
    var y = this.sample.col(this.t)
    if(this.looping && this.loopX && this.t > this.loop0 + this.loopD - this.loopX) {
      var tFade = this.t - this.loop0 - this.loopD + this.loopX
      var y2 = this.sample.col(tFade + this.loop0)
      for(var c=0; c<y.length||c<y2.length; c++)
        y[c] = (y[c] || 0) * (this.loopX - tFade)/this.loopX +
                (y2[c] || 0) * tFade/this.loopX
    }
    for(var c in y)
      y[c] *= this.envelope
    if(this._envelope.finished)
      this.finish()
    this.y = y
  } else
    this.y = [0]
}

SamplerMono.prototype.__defineGetter__("numberOfChannels", function() {
  return this.sample.numberOfChannels
})
SamplerMono.prototype.__defineGetter__("T", function() {
  return this.sample.lengthInSamples
})
SamplerMono.prototype.trigger = function(p, note) {
  this.t = this.t0
  this.playing = true
  if(p)
    this.p = p
  if(this._envelope.isDspUnit)
    this._envelope.trigger()
}
SamplerMono.prototype.release = function() {
  if(this._envelope.isDspUnit)
    this._envelope.release()
}

SamplerMono.prototype.loop = function(loop0, d, X) {
  loop0 = loop0 || 0
  d = d || (this.T-loop0)
  X = X || 0

  this.loop0 = loop0
  this.loopD = d
  this.loopX = X // cross fade
  this.looping = true
  return this
}

SamplerMono.prototype.skipBack = function(dT) {
  dT = dT || Math.random()*44100/4
  this.t -= dT
}
SamplerMono.prototype.glitchLoop = function(d) {
  this.loop(this.t, Math.random()*44100/2, 4410*Math.random())
  if(d)
    this.schedule(this.clock + d, function() {
      this.looping = false
    })
}

SamplerMono.prototype.__defineGetter__("label", function() {
  return this._label || this.sample.label
})
SamplerMono.prototype.__defineSetter__("label", function(label) {
  this._label = label
})

SamplerMono.prototype.tuneSample = function() {
  var pNatural = this.sample.estimateP() // f is for frequency
  this.tuning = -pNatural%12 - 60
}

SamplerMono.fromFile = function(filename) {
  return Sample.readFile(filename).then(function(sample) {
    var sampler = new SamplerMono()
    sampler.sample = sample
    return sampler
  })
}
SamplerMono.choose = function(kind) {
  return Sample.choose(kind).then(function(sample) {
    var sampler = new SamplerMono()
    sampler.sample = sample
    return sampler
  })
}
SamplerMono.gtts = function(text, lang) {
  return textToSpeech(text, lang).then(function(sample) {
    var sampler = new SamplerMono()
    sampler.sample = sample
    return sampler
  })
}
