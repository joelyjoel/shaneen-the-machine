const Synth = require("./Synth.js")
const SimpleSineMono = require("./SimpleSineMono.js")
const Mixer = require("../Mixer.js")

function Poly(MonoConstructor) {
  Synth.call(this)
  this.MonoConstructor = MonoConstructor || SimpleSineMono

  this.mix = new Mixer()
  this._mix.poly = this
  //this._mix.master = this.masterPlug
  this.playing = {}

  this.master = 1
}
Poly.prototype = Object.create(Synth.prototype)
Poly.prototype.construtor = Poly
module.exports = Poly

Poly.prototype.__defineSocket__("mix", null, "array")
//Poly.prototype.__defineRepeater__("master")

Poly.prototype._tick = function() {
  this.y = this.mix
}

Poly.prototype.trigger = function(p, note) {
  if(!p)
    console.log("warning p is ", p)
  var mono = new this.MonoConstructor()
  mono.syncClocks(this.clock)
//  console.log("adding mono\n\tmono clock:", mono.clock, "poly clock:", this.clock)
  if(this.setUpMono)
    this.setUpMono(mono)
  mono.poly = this
//  console.log("post set up mono\n\tmono clock:", mono.clock, "poly clock:", this.clock, "mixer clock:", this._mix.clock)
  mono.polyMixerChannel = this._mix.addInput(mono)
  mono.onFinish = function() {
    this.polyMixerChannel.remove()
  }
  if(this.playing[p]) {
  //  console.log("triggered an already playing note at t=", this.clock/this.sampleRate, "seconds")
    this.release(p)
  }
  this.playing[p] = mono
  mono.trigger(p, note)
}
Poly.prototype.release = function(p, note) {
//  console.log("Releasing", p, this.label, "t:", this.clock)
  if(this.playing[p]) {
    this.playing[p].release()
    this.playing[p] = null
  }
}
