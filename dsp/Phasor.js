function Phasor(real, imaginary) {
  this.real = real || 0;
  this.imaginary = imaginary || 0;
}
module.exports = Phasor;

Phasor.polar = function(magnitude, phase) {
  return new Phasor(
    magnitude * Math.cos(phase),
    magnitude * Math.sin(phase),
  )
}
Phasor.multiply = function(a, b) {
  return new Phasor(
    a.real * b.real - a.imaginary * b.imaginary,
    a.real * b.imaginary + a.imaginary * b.real
  )
}
Phasor.sum = function(a, b) {
  return new Phasor(
    a.real + b.real,
    a.imaginary + b.imaginary
  )
}
Phasor.subtract = function(a, b) {
  return new Phasor(
    a.real - b.real,
    a.imaginary - b.imaginary,
  )
}
Phasor.conjugate = function(a) {
  return new Phasor(
    a.real,
    -a.imaginary
  )
}

Phasor.prototype.isPhasor = true;
Phasor.prototype.duplicate = function() {
  return new Phasor(this.real, this.imaginary);
}

// fft compatibility
Phasor.prototype.__defineGetter__("0", function() {
  return this.real
})
Phasor.prototype.__defineGetter__("1", function() {
  return this.imaginary
})

Phasor.prototype.setPolar = function(magnitude, phase) {
  this.real = Math.cos(phase) * magnitude;
  this.imaginary = Math.sin(phase) * magnitude;
}

Phasor.prototype.randomise = function() {
  this.real = Math.random()*2 - 1;
  this.imaginary = Math.random()*2 - 1;
}

Phasor.prototype.__defineGetter__("magnitude", function() {
  return Math.sqrt(Math.pow(this.real, 2) + Math.pow(this.imaginary, 2))
})
Phasor.prototype.__defineSetter__("magnitude", function(mag) {
  var sf = mag/this.magnitude;
  if(isNaN(mag)) {
    this.real = mag
    this.imaginary = 0
    return;
  } else {
    this.real *= sf
    this.imaginary *= sf
  }
})
Phasor.prototype.__defineGetter__("phase", function() {
  var m = this.real / this.imaginary
  return Math.atan(m) + (this.real > 0 ? 0 : Math.PI)
})
Phasor.prototype.__defineSetter__("phase", function(phase) {
  var mag = this.magnitude
  this.real = mag * Math.cos(phase)
  this.imaginary = mag * Math.sin(phase)
})
Phasor.prototype.__defineGetter__("conjugate", function() {
  return new Phasor( this.real , - this.imaginary )
})

Phasor.prototype.multiply = function(phasor) {
  if(phasor.isPhasor) {
    this.real = this.real * phasor.real - this.imaginary * phasor.imaginary;
    this.imaginary = this.real * phasor.imaginary + this.imaginary * phasor.real;
  } else if(phasor.constructor == Number) {
    this.real *= phasor;
    this.imaginary *= phasor;
  }
  return this;
}
