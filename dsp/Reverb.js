const DspUnit = require("./DspUnit.js")
const Sample = require("./Sample.js")

function Reverb() {
  DspUnit.call(this)

  this.endScale = 1;
  this.ambientSample;
  this.buffer
  this.tBuffer = 0;
}
Reverb.prototype = Object.create(DspUnit.prototype)
Reverb.prototype.constructor = Reverb;
module.exports = Reverb;

Reverb.prototype.__defineSocket__("in");
Reverb.prototype.__definePlug__("yWet");

Reverb.prototype._tick = function() {
  this.buffer.setCol(this.tBuffer, this.in);
  var monoSum = 0;
  var yChans = [];
  for(var c=0; c<this.buffer.numberOfChannels; c++) {
    var channelSum = 0;
    var tA = this._ambientSample.lengthInSamples - this.tBuffer;
    for(var tD=0; tD<this.tBuffer; tD++) {
      channelSum += this.buffer.channelData[c][tD] * this._ambientSample.channelData[c][tA]
      tA++;
    }
    tA = 0;
    for(var tD=this.tBuffer+1; tD<this.buffer.lengthInSamples; tD++) {
      channelSum += this.buffer.channelData[c][tD] * this._ambientSample.channelData[c][tA]
      tA++;
    }
    yChans[c] = channelSum * this.endScale;
    monoSum += yChans[c];
  }
  this.yWet = monoSum;
  this.tBuffer = (this.tBuffer + 1) % this.buffer.lengthInSamples;
}

Reverb.prototype.__defineSetter__("ambientSample", function(samp) {
  this._ambientSample = samp;
  this.buffer = new Sample().blank(samp);
  this.endScale = 1 / samp.absSum();
})
