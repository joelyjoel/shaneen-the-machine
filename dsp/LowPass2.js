const DspUnit = require("./DspUnit.js");

function LowPass2(f) {
  DspUnit.call(this);

  this.f = f;
  this.sum = 0;
  this.sumSize = 0;
}
LowPass2.prototype = Object.create(DspUnit.prototype);
LowPass2.prototype.constructor = LowPass2;
module.exports = LowPass2;

LowPass2.prototype.__defineSocket__("in");
LowPass2.prototype.__defineSocket__("f", function(f) {
  this.k = 1 - (f / (0.443 * this.sampleRate));
})
LowPass2.prototype.__definePlug__("y");

LowPass2.prototype._tick = function() {
  this.sum += this.in;
  this.sumSize++;

//  var sf = this.idealSumSize/this.sumSize
  this.sum *= this.k;
  this.sumSize *= this.k;

  this.y = this.sum / this.sumSize;
}
