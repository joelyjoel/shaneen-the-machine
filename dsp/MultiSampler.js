const DspUnit = require("./DspUnit.js")
const Sampler = require("./Sampler.js")
const Promise = require("promise")

function MultiSampler() {
  DspUnit.call(this);

  this.samplers = {};
}
MultiSampler.prototype = Object.create(DspUnit.prototype);
MultiSampler.prototype.constructor = MultiSampler;
MultiSampler.prototype.isMultiSampler = true;
module.exports = MultiSampler;

MultiSampler.choose = function(kinds) {
  var proms = [];
  var refs = {};
  for(var i in kinds) {
    refs[i] = proms.length;
    proms.push(Sampler.choose(kinds[i]));
  }
  return Promise.all(proms).then(function(samplers) {
    var multiSampler = new MultiSampler();

    for(var i in refs) {
      multiSampler.samplers[i] = samplers[refs[i]];
    }

    return multiSampler;
  });
}

MultiSampler.of = function(samples) {
  var proms = [];
  var refs = {};
  for(var i in samples) {
    refs[i] = proms.length;
    proms.push(samples[i]);
  }
  return Promise.all(proms).then(function(samplers) {
    var multiSampler = new MultiSampler();

    for(var i in refs)
      multiSampler.samplers[i] = samplers[refs[i]];

    return multiSampler;
  });
}

MultiSampler.prototype.trigger = function(note) {
  var sampler = this.samplers[note];
  if(sampler)
    sampler.trigger();
}

MultiSampler.prototype._tick = function() {
  var y = [];
  for(var i in this.samplers) {
    this.samplers[i].tickUntil(this.clock)
    var samplerY = this.samplers[i].out.y
    for(var c in samplerY) {
      if(isNaN(samplerY[c]))
        throw "Yuk NaN!\nsmp:\t"+ i +
          "\nc:\t"+ c +
          "\ny:\t"+ samplerY + "\t(" + (typeof samplerY) + ")\n" +
          "t:\t" + this.clock + "\n~~~\n";
      y[c] = (y[c] || 0) + (samplerY[c] || 0);
    }
  }
//  console.log(y);
  this.y = y;
}

MultiSampler.prototype.__defineGetter__("extras", function() {
  return Object.values(this.samplers)
})


MultiSampler.prototype.__definePlug__("y", "array");
