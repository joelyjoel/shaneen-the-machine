const DspUnit = require("./DspUnit.js");
const dB = require("./decibel.js")

function Gain(plug, gain) {
  DspUnit.call(this)
  this.gain = gain || 0
  this.sf = 1
  this.in = plug || 0
}
Gain.prototype = Object.create(DspUnit.prototype)
Gain.prototype.constructor = Gain
module.exports = Gain

Gain.prototype.__defineSocket__("in", function(x) {
  var y = []
  for(var c in x) {
    y[c] = this.gainSf * this.sf * x[c]
  }
  this.y = y
  return x
}, "array")
Gain.prototype.__defineSocket__("gain", function(gain) {
  this.gainSf = dB(gain)
  return gain
})
Gain.prototype.__definePlug__("y", "array")

Gain.prototype.__defineSocket__("sf")
