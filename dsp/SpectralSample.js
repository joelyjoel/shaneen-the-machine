const fft = require("fft-js").fft
const ifft = require("fft-js").ifft
const fftUtil = require("fft-js").util
const Sample = require("./Sample.js")
const Phasor = require("./Phasor.js")
const SpectralFrame = require("./SpectralFrame.js")
const hann = require("./hann.js")

function SpectralSample() {
  this.channelData = [];
  this.frameSize = 256;
  this.frequencyBins = [];
  this.sampleRate = 44100
  this.hop = 1/4
}
module.exports = SpectralSample;

SpectralSample.prototype.fromPCM = function(samp, frameSize) {
  if(frameSize)
    this.frameSize = frameSize

  this.channelData = [];

  for(var c in samp.channelData) {
    this.channelData[c] = []; // frames
    var t1
    for(var t0=-this.hopSize; t0<samp.lengthInSamples; t0+=this.hopSize) {

      t1 = t0 + this.frameSize

      var signal = new Float32Array(this.frameSize)
      signal.fill(0)
      for(var t=(t0>=0 ? t0:0); t<t1 && t<samp.lengthInSamples; t++)
        signal[t-t0] = samp.channelData[c][t]

      // WINDOWING GOES HERE
      for(var t=0; t<signal.length; t++) {
        signal[t] *= hann(t, signal.length)
      }

      var phasors = fft(signal)
      phasors = phasors.map(function(a) {
        return new Phasor(a[0], a[1])
      });

      this.channelData[c].push(new SpectralFrame(phasors))
    }
  }
}

SpectralSample.prototype.toPCM = function() {
  var tape = new Sample().blank(this.lengthInSamples, this.numberOfChannels);

  for(var c in this.channelData) {
    var t = 0
    for(var frame in this.channelData[c]) {
      var signal = ifft(this.channelData[c][frame].phasors)
      for(var i=0; i<signal.length; i++) {
        var before = tape.channelData[c][t];
        tape.channelData[c][t] += signal[i][0] * hann(i, signal.length);
        t++
      }
      t -= Math.floor(this.frameSize-this.hopSize)
    }
  }

  return tape;
}

SpectralSample.prototype.blank = function(lengthInSamples, numberOfChannels, frameSize) {
  if(frameSize)
    this.frameSize = frameSize;
  lengthInSamples = lengthInSamples || this.lengthInSamples || 44100;
  numberOfChannels = numberOfChannels || this.numberOfChannels || 1;
  var nFrames = Math.ceil(lengthInSamples/this.hopSize);

  this.channelData = [];
  for(var c=0; c<numberOfChannels; c++) {
    this.channelData[c] = [];
    for(var frame=0; frame<nFrames; frame++) {
      var phasors = [];
      for(var bin=0; bin<this.frameSize; bin++) {
        phasors[bin] = new Phasor(0,0);
      }
      this.channelData[c][frame] = new SpectralFrame(phasors);
    }
  }

  return this;
}

SpectralSample.prototype.__defineGetter__("frameSize", function() {
  return this._frameSize;
})
SpectralSample.prototype.__defineSetter__("frameSize", function(frameSize) {
  this._frameSize = frameSize;
  // set bin frequencies
  this.frequencies = [];
  var step = this.sampleRate / frameSize;
  for(var i=0; i<this.frameSize; i++) {
    this.frequencies[i] = i * step;
  }
})
SpectralSample.prototype.__defineGetter__("hopSize", function() {
  return Math.floor(this.hop * this.frameSize)
})
SpectralSample.prototype.__defineGetter__("numberOfChannels", function() {
  return this.channelData.length;
})
SpectralSample.prototype.__defineGetter__("lengthInSamples", function() {
  if(!this.channelData[0])
    return undefined;
  return this.channelData[0].length * this.hopSize;
});
