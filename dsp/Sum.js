const DspUnit = require("./DspUnit.js");

function Sum(A, B) {
  DspUnit.call(this);
  this.A = A || 0;
  this.B = B || 0;
}
Sum.prototype = Object.create(DspUnit.prototype);
Sum.prototype.constructor = Sum;
module.exports = Sum;

Sum.prototype.__defineSocket__("A", undefined, "array");
Sum.prototype.__defineSocket__("B", undefined, "array");
Sum.prototype.__definePlug__("y", undefined, "array");

Sum.prototype._tick = function() {
  var y = [];
  for(var c=0; c<this.A.length || c<this.B.length; c++) {
    y[c] = (this.A[c] || 0) + (this.B[c] || 0)
  }
  this.y = y
}
