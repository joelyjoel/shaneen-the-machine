function Sample(channelData) {
    this.channelData = channelData || [];
    this.sampleRate = 44100;
    this.meta = {}
}
module.exports = Sample;

var AudioContext = require("web-audio-api").AudioContext
, audioctx = new AudioContext ;
const fs = require("fs");
var Promise = require("promise");
var WavEncoder = require("wav-encoder");
//var fft = require("fft-js").fft;
//var fftUtil = require("fft-js").util;
var Complex = require("Complex");
var FFT = require("fft.js");
var hann = require("./hann.js");
var organise = require("../organise.js");
//var clockworkify = require("./clockworkify.js");
var findFiles = require("../findFiles.js");
var SpectralSample = require("./SpectralSample.js")
const Ramp = require("./Ramp.js")
const dB = require("./decibel.js")
const mToF = require("./midiToFrequency.js")
const plot = require("../plot.js")

Sample.readFile = function(filename, subdir) {
    return new Promise(function(resolve, reject) {
        var prom = fs.readFile(filename, function(err, data) {
            if(err) reject(err);

            audioctx.decodeAudioData(data, function(data) {
                var samp = new Sample();
                samp.originalFile = filename;
                var label
                label = filename.slice(filename.lastIndexOf("/")+1);
                label = label.slice(0, label.lastIndexOf("."))
                samp.label = label;
                samp.sampleRate = data.sampleRate;
                samp.channelData = data._data;
                resolve(samp);
            }, function(e) {
                reject(e);
            })
        });
    });
}
Sample.choose = function(kind) {
    var path = "/Users/joel/Samples/"+(kind || "");
    var files = findFiles(path, [".wav", ".mp3", ".aif"]);
    if(files.length > 0) {
        var choice = files[Math.floor(Math.random()*files.length)];
        console.log("chosen", choice);
        return Sample.readFile(choice);
    } else {
        console.log("Could not find any samples of kind:", kind);
        return new Promise(function(fulfil) { fulfil(false);});
        // ^^ an empty promise
    }
}
Sample.ambient = function(loopSize) {
  loopSize = loopSize || 6
  return Sample.choose("ambient/short").then(function(sample) {
    loopSize *= sample.sampleRate
    var t0 = Math.random() * (sample.lengthInSamples-loopSize)
    var t1 = t0 + loopSize
    return sample.cut(t0, t1)
  })
}
Sample.record = function(what, T, numberOfChannels) {
    var timerStart = new Date().getTime()

    if(what && what.isDspUnit) {
      what = what[what.defaultPlug];
    }

    T = T || what.unit.T || 44100;
    //numberOfChannels = numberOfChannels || 1;
    var tape = new Sample().blank(T, numberOfChannels || 1);
    var y = 0;
    var lastReportT = 0
    for(var t=0; t<T; t++) {
      if(t%44100 == 0) {
        lastReportT = new Date().getTime()
        var renderSpeed = t/(lastReportT-timerStart)
        var eta = (T-t)/renderSpeed
        console.log(
          "recording..\t",
          (t/44100).toFixed(3) + "/" + (T/44100).toFixed(3), "s\t",
          "ETA:",(eta/1000).toFixed(2), "seconds"
        )
      }
      what.unit.tickUntil(t);
      //console.log(what.unit.out)
      y = what.unit.out[what.out]
      if(y == null)
        console.log("Sample.record problem, y:",y)
      if(y.constructor == Number)
        y = [y];

      for(var c=0; c<y.length; c++) {
        if(c >= tape.numberOfChannels) {
          console.log("adding channel")
          tape.addChannel();
        }
        if(isNaN(y[c])) throw "Tried to record NaN, at t=" + t + " and c="+c
            + "\n sample size: " + [T, numberOfChannels];
        if(y[c] == Infinity) {
          console.log("Sample render encountered infinity\n\tt:", t, "\tc:", c)
          throw "cannot record infinity";
        }
        tape.channelData[c][t] = y[c];
      }
    }
    if(what.label)
        tape.label = what.label;

    var timerElapse = new Date().getTime() - timerStart;
    console.log("recorded", (T/44100).toFixed(3)+"s", "of sound in", (timerElapse/1000).toFixed(3)+"s")

    tape.meta.renderTime = timerElapse
    tape.meta.renderRate = (T/44.1)/timerElapse
    tape.meta.renderedByScript = process.argv[1]
    tape.meta.commandLineArguments = process.argv.slice(2).join(" ")
    tape.meta.renderScript = fs.readFileSync(process.argv[1], "utf-8")

    tape.label = "recorded_DSP"
    return tape;
}
Sample.concat = function(samples, spacing) { // spacing may be a ClockworkUnit
    // join an array of samples end to end into one sample
    spacing = spacing || 0;

    var nChannels = 0;
    var d = 0;
    for(var i in samples) {
        d += samples[i].lengthInSamples;
        if(samples[i].numberOfChannels > nChannels)
            nChannels = samples[i].numberOfChannels
    }
    d += spacing * (samples.length-1);
    var outSample = new Sample().blank(d, nChannels);

    var t = 0;
    for(var i in samples) {
        outSample.mix(samples[i], t);
        t += samples[i].lengthInSamples + spacing;
    }

    return outSample;
}

Sample.prototype.isSample = true;

Sample.prototype.encodeWav = function() {
  var audio = {
      "sampleRate": this.sampleRate,
      "channelData": this.channelData
  }
  return WavEncoder.encode(audio, {float:false, bitDepth:16 })
}

// SAVING
Sample.prototype.saveWavFile = function(filename) {
    var samp = this;
    return new Promise(function(resolve, reject) {
        var audio = {
            "sampleRate": samp.sampleRate,
            "channelData": samp.channelData
        };
        WavEncoder.encode(audio, { float: false, bitDepth: 24 }).then(function(data) {
            fs.writeFile(filename, Buffer.from(data), function(err, data) {
                if(err) {
                    reject(err);
                    console.log("failed to save", filename);
                }
                else {
                    console.log("saved ", filename, (samp.lengthInSamples/44100).toFixed(3)+"s");
                    samp.updateBasicMetadata()
                    samp.saveMetadata(filename+".json")
                    //samp.saveDusp(filename+".dusp")
                    resolve(filename);
                }
            });
        }, function(err) {
            console.log("WavEncoder error")
            throw err;
        });
    })
}
Sample.prototype.save = function(filename, subdir) {
    if(this.isSilent) {
      console.log("Skipping save because sample is silent:", this.label)
      return ;
    }
    filename = filename || (this.label ? this.label+".wav" : ".wav");
    if(filename.slice(filename.length-4) != ".wav")
      filename += ".wav";
    //subdir = subdir || "sampleDump";
    var path = organise.chooseFilename(filename, subdir);
    this.saveWavFile(path);
}
Sample.prototype.saveResource = function(filename, subdir) {
  if(this.isSilent) {
    console.log("Skipping saveResource because sample is silent:", this.label)
    return ;
  }
  filename || (this.label ? this.label+".wav" : ".wav")
  if(filename.slice(filename.length-4) != ".wav")
    filename += ".wav"
  subdir = subdir || "samples/uncategorised";
  var path = organise.chooseResourceFilename(filename, subdir)
  this.saveWavFile(path);
}


// METADATA
Sample.prototype.updateBasicMetadata = function() {
  this.meta.lengthInSamples = this.lengthInSamples
  this.meta.numberOfChannels = this.numberOfChannels
  this.meta.label = this.label
}
Sample.prototype.addMetaTags = function() {
  this.meta.tags = this.meta.tags || []
  for(var i in arguments) {
    if(arguments[i].constructor == String && this.meta.tags.indexOf(arguments[i]) == -1)
      this.meta.tags.push(arguments[i])
  }
}
Sample.prototype.saveMetadata = function(path) {
  fs.writeFile(
    path,
    JSON.stringify(this.meta, null, 4),
    (err) => {
      if(err) console.error(err);
      else console.log("saved metadata:", path)
    }
  )
}
Sample.prototype.saveDusp = function(path) {
  if(this.meta && this.meta.dusp) {
    fs.writeFile(
      path,
      this.meta.dusp,
      (err) => {
        if(err) console.error(error);
        else console.log("saved dusp:", path)
      }
    )
  } else {
    console.log("no dusp to save")
    return null
  }
}
Sample.prototype.fetchMetadata = function(path) {
  path = path || this.originalFile+".json";
  var samp = this
  return new Promise(function(fulfil, reject) {
    if(fs.existsSync(path)) {
      fs.readFile(path, (err, data) => {
        if(err) reject(err);
        else fulfil(data)
      })
    } else
      fulfil("")
  }).then(function(data) {
    data = JSON.parse(data)
    samp.meta = Object.assign({}, samp.meta, data)
    return samp
  })
}
Sample.prototype.addMeta = function(meta) {
  Object.assign(this.meta, meta)
  return this // returns self for daisy chain purposes
}

// MEASUREMENTS
Sample.prototype.__defineGetter__("lengthInSamples", function() {
    return this.channelData[0].length;
});
Sample.prototype.__defineGetter__("T", function() {
  // alias of .lengthInSamples
  return this.lengthInSamples;
})
Sample.prototype.__defineGetter__("lengthInSeconds", function() {
  return this.lengthInSamples / this.sampleRate;
})
Sample.prototype.__defineGetter__("numberOfChannels", function() {
    return this.channelData.length;
});
Sample.prototype.__defineGetter__("silent", function() {
    for(var channel in this.channelData) {
        for(var t in this.channelData[channel]) {
            if(this.channelData[channel][t])
                return false;
        }
    }
    return true;
})
Sample.prototype.__defineGetter__("d", function() {
    return this.lengthInSamples/this.sampleRate;
});
Sample.prototype.yOfSample = function(t, channel) {
    if(channel == undefined) channel = 0;

    if(t >= 0 && t < this.lengthInSamples
    && channel >= 0 && channel < this.numberOfChannels) {
        if(t%1 == 0) {
            return this.channelData[channel][t];
        } else {
            return (this.channelData[channel][Math.ceil(t)] || 0) * (t%1)
                + (this.channelData[channel][Math.floor(t)] || 0) * (1 - t%1);
        }
    } else
        return 0;
}
Sample.prototype.y = function(t, channel) { // alias of .yOfSample
    return this.yOfSample(t, channel);
}
Sample.prototype.col = function(t) {
  var col = [];

  for(var c in this.channelData) {
    col[c] = this.y(t, c);
  }

  return col
}
Sample.prototype.setCol = function(t, y) {
  for(var c=0; c<this.numberOfChannels; c++) {
    this.channelData[c][t] = y;
  }
}
Sample.prototype.rms = function(t0, t1) {
    t0 = t0 || 0;
    t1 = t1 || this.lengthInSamples;
    var sum = 0;
    var n = 0;
    var y;
    for(var c=0; c<this.numberOfChannels; c++)
        for(var t=t0; t<t1; t++) {
            y = this.channelData[c][t];
            sum += Math.pow(y,2);
            n++;
        }
    return Math.sqrt(sum/n);
}
Sample.prototype.absSum = function(t0, t1) {
  t0 = t0 || 0;
  t1 = t1 || this.lengthInSamples
  var sum = 0;
  for(var c=0; c<this.numberOfChannels; c++) {
    for(var t=t0; t<t1; t++) {
      sum += Math.abs(this.channelData[c][t]);
    }
  }
  return sum;
}
Sample.prototype.min = function(t0, t1) {
    t0 = t0 || 0;
    t1 = t1 || this.lengthInSamples;
    var min = this.channelData[0][t0];
    var y;
    for(var c=0; c<this.numberOfChannels; c++) {
        for(var t=t0; t<t1; t++) {
            y = this.channelData[c][t];
            if(y < min)
                min = y;
        }
    }
    return min;
}
Sample.prototype.max = function(t0, t1) {
    t0 = t0 || 0;
    t1 = t1 || this.lengthInSamples;
    var max = this.channelData[0][t0];
    var y;
    for(var c=0; c<this.numberOfChannels; c++) {
        for(var t=t0; t<t1; t++) {
            y = this.channelData[c][t];
            if(y > max)
                max = y;
        }
    }
    return max;
}
Sample.prototype.peak = function(t0, t1) {
    var min = this.min(t0, t1);
    var max = this.max(t0, t1);
    if(Math.abs(min) > Math.abs(max))
        return min;
    else
        return max;
}
Sample.prototype.__defineGetter__("isSilent", function() {
  return !this.hasSound
})
Sample.prototype.__defineGetter__("hasSound", function() {
  for(var c=0; c<this.numberOfChannels; c++)
    for(var t=0; t<this.lengthInSamples; t++)
      if(this.channelData[c][t])
        return true

  return false
})
Sample.prototype.autocorrelate = function(t, c, W, lag) {
  t = Math.floor(t)
  var sum = 0
  for(var j=t+1; j<t+W-lag; j++) {
    sum += this.channelData[c][j] * this.channelData[c][j+lag]
  }
  return sum
}
Sample.prototype.findPeriodUsingAutocorrelation = function(t, c, W) {
  c = c || 0
  W = W || 1000
  var minLag = 1
  var maxLag = W

  var r0 = this.autocorrelate(t, c, W, 0)
  var r, rLast, rDelta, currentPeakR, currentPeakLag
  var peaks = []
  for(var lag=Math.floor(minLag); lag<=maxLag; lag++) {
    r = this.autocorrelate(t, c, W, lag)
    rDelta = (r-rLast) || 0
    rLast = r

    if(rDelta > 0) {
      if(!currentPeakR || r > currentPeakR) {
        currentPeakR = r
        currentPeakLag = lag
      }
    } else if(rDelta < 0) {
      if(currentPeakR) {
        peaks.push({
          lag: currentPeakLag,
          r: currentPeakR,
        })
        currentPeakR = null
        currentPeakLag = null
      }
    }
  }

  if(peaks.length == 1) {
    return peaks[0].lag
  } else if(peaks.length == 0) {
    return null
  }

  // find mode difference between peaks
  var counts = []
  for(var i=1; i<peaks.length; i++) {
    var dif = peaks[i].lag-peaks[i-1].lag
    counts.push(dif)
  }
  counts = counts.sort()
  var median = counts[Math.floor(counts.length/2)]

  return median
}
Sample.prototype.plotAutocorrelation = function(t, c, W) {
  c = c || 0
  W = W || 1000
  var minLag = 0
  var maxLag = W
  var points = []

  for(var lag=Math.floor(minLag); lag<=maxLag; lag++) {
    points[lag] = this.autocorrelate(t, c, W, lag)
  }

  return plot(points)
}
Sample.prototype.trackPeriodWithAutocorrelation = function(W, hop, c) {
  if(this.meta.periodTracking && hop == undefined && c==undefined && W==undefined)
    return this.meta.periodTracking.periods
  c = c || 0
  W = W || 1000
  hop = hop || W/2


  if(this.meta.periodTracking
    && this.meta.periodTracking.hop == hop
    && this.meta.periodTracking.W == W
    && this.meat.periodTracking.c == c)
    return this.meta.periodTracking.periods

  var trackedPeriods = {}
  for(var t=0; t+W<this.lengthInSamples; t+=hop) {
    var period = this.findPeriodUsingAutocorrelation(t,c,W)
    if(!period) {
      trackedPeriods[t] = null
    } else {
      trackedPeriods[t] = period
    }
  }

  this.meta.periodTracking = {
    method: "autocorrelation",
    W: W,
    hop: hop,
    channelTracked: c,
    periods: trackedPeriods
  }

  return trackedPeriods
}
Sample.prototype.estimatePeriod = function() {
  var periods = this.trackPeriodWithAutocorrelation()
  var ordered = Object.values(periods).sort((a,b) => {return a-b})

  // remove upper and lower quartile
  ordered = ordered.slice(
    Math.floor(ordered.length/4),
    Math.ceil(3/4 * ordered.length),
  )

  var sum = 0
  var n = 0
  for(var i in ordered) {
    if(ordered[i]) {
      sum += ordered[i]
      n++
    }
  }

  var meanPeriod = sum/n

  //var p = mToF.fToM(this.sampleRate/meanPeriod)
  return meanPeriod
}
Sample.prototype.estimateF = function() {
  return this.sampleRate / this.estimatePeriod()
}
Sample.prototype.estimateP = function() {
  return mToF.fToM(this.estimateF())
}


// COLLAGE/CUTITING
Sample.prototype.blank = function(d, channels) { // d in seconds
    if(d != undefined && d.isSample) {
      // using another sample as a template
      channels = d.numberOfChannels;
      d = d.lengthInSamples;
    }
    if(channels == undefined) {
        channels = 1;
    }
    //if(sr) this.sampleRate = sr;
    if(d <= 10)
      console.log("WARNING, very short sample..", d);

    var dInSamples = d;
    this.channelData = [];
    for(var c=0; c<channels; c++) {
        var buffer = new Float32Array(dInSamples);
        for(var t=0; t<dInSamples; t++) {
            buffer[t] = 0;
        }
        this.channelData[c] = buffer;
    }
    return this;
}
Sample.prototype.addChannel = function() {
  this.channelData.push(new Float32Array(this.lengthInSamples).fill(0))
  return this;
}
Sample.prototype.fleshOutChannels = function(n) {
  if(n > this.numberOfChannels) {
    var channelsToAdd = n - this.numberOfChannels
    var oldNumberOfChannels = this.numberOfChannels
    console.log("duplicating", channelsToAdd, "channels")

    for(var i=0; i<channelsToAdd; i++)
      this.channelData.push(this.channelData[i%oldNumberOfChannels].slice())
  }
  return this
}
Sample.prototype.overwrite = function(samp, t0, channel) {
    t0 = t0 || 0;
    var t1 = t0 + samp.lengthInSamples;
    channel = channel || 0;

    for(var c=channel; c<this.numberOfChannels && c-channel<samp.numberOfChannels; c++) {
        for(var t=t0; t<t1; t++) {
            var y = samp.yOfSample(t-t0, c-channel);
            this.channelData[c][t] = y;
        }
    }

    return this;
}
Sample.prototype.mix = function(samp, t0, channel) {
    t0 = Math.round(t0 || 0);
    var t1 = Math.round(t0 + samp.lengthInSamples);
    channel = channel || 0;

    for(var c=channel; c<this.numberOfChannels && c-channel<samp.numberOfChannels; c++) {
        for(var t=t0; t<t1; t++) {
            var y = samp.yOfSample(t-t0, c-channel);
            this.channelData[c][t] += y;
        }
    }

    return this;
}
Sample.prototype.cut = function(t0, t1) {
    t0 = t0 || 0;
    t1 = t1 || this.lengthInSamples;
    t0 = Math.floor(t0);
    t1 = Math.floor(t1);

    var newSample = new Sample();
    for(var channel in this.channelData)
        newSample.channelData[channel] = this.channelData[channel].slice(t0, t1);
    return newSample;
}
Sample.prototype.cutSelf = function(t0, t1) {
  t0 = t0 || 0
  t1 = t1 || this.lengthInSamples
  t0 = Math.floor(t0)
  t1 = Math.floor(t1)

  for(var channel in this.channelData)
    this.channelData[channel] = this.channelData[channel].slice(t0, t1)
  return this
}
Sample.prototype.cutOneChannel = function(c, t0, t1) {
  c = c || 0
  t0 = Math.floor(t0 || 0)
  t1 = Math.floor(t1 || this.lengthInSamples)

  var newSample = new Sample()
  newSample.channelData[0] = this.channelData[c].slice(t0, t1)
  return newSample
}
Sample.prototype.loop = function(d) {
    if(d == undefined) throw "d is required (Sample.prototype.loop)";
    var newSamp = new Sample().blank(d, this.numberOfChannels);

    for(var channel in this.channelData) {
        for(var t=0; t<d; t++) {
            newSamp.channelData[channel][t] = this.channelData[channel][t%this.lengthInSamples];
        }
    }
    newSamp.label = this.label + " looped"
    return newSamp;
}


// ZERO CROSSINGS
Sample.prototype.nextUpwardZeroCrossing = function(t0, channel) {
    t0 = t0 || 0;
    channel = channel || 0;
    var y, yPrev;
    for(var t=t0; t<this.lengthInSamples; t++) {
        y = this.channelData[channel][t];
        if(y == 0)
            continue;
        if(yPrev < 0 && y > 0)
            return t;

        yPrev = y;
    }
    return this.lengthInSamples;
}
Sample.prototype.nextDownwardZeroCrossing = function(t0, channel) {
    t0 = t0 || 0;
    channel = channel || 0;
    var y, yPrev;
    for(var t=t0; t<this.lengthInSamples; t++) {
        y = this.channelData[channel][t];
        if(y == 0)
            continue;
        if(yPrev > 0 && y < 0)
            return t;

        yPrev = y;
    }
    return this.lengthInSamples;
}
Sample.prototype.previousUpwardZeroCrossing = function(t0, channel) {
    t0 = t0 != undefined ? t0 : this.lengthInSamples;
    channel = channel || 0;
    var y, yPrev, tPrev;
    for(var t=t0-1; t>=0; t--) {
        y = this.channelData[channel][t];
        if(y == 0)
            continue;
        if(yPrev > 0 && y < 0)
            return tPrev;

        yPrev = y;
        tPrev = t;
    }
    return 0;
}
Sample.prototype.previousDownwardZeroCrossing = function(t0, channel) {
    t0 = t0 != undefined ? t0 : this.lengthInSamples;
    channel = channel || 0;
    var y, yPrev, tPrev;
    for(var t=t0-1; t>=0; t--) {
        y = this.channelData[channel][t];
        if(y == 0) continue;
        if(yPrev < 0 && y > 0)
            return tPrev;
        yPrev = y;
        tPrev = t;
    }
    return 0;
}
Sample.prototype.closestZeroCrossing = function(t, channel, downward) {
    channel = channel || 0;
    if(t == undefined) throw "t is required (closestZeroCrossing)";

    if(downward) {
        var nex = this.nextDownwardZeroCrossing(t, channel);
        var pre = this.previousDownwardZeroCrossing(t, channel);
    } else {
        var nex = this.nextUpwardZeroCrossing(t, channel);
        var pre = this.previousUpwardZeroCrossing(t, channel);
    }
    if(Math.abs(t-nex) < Math.abs(t-pre))
        return nex;
    else
        return pre;
}
Sample.prototype.upwardZeroCrossings = function(channel, t0, t1) {
    t0 = t0 || 0;
    t1 = t1 != undefined ? t1 : this.lengthInSamples
    channel = channel || 0;
    var zX = [];
    var t = t0;
    if(t == 0) zX.push(t);
    while((t = this.nextUpwardZeroCrossing(t, channel)) < t1)
        zX.push(t);
    return zX;
}
Sample.prototype.downwardZeroCrossings = function(channel, t0, t1) {
    t0 = t0 || 0;
    t1 = t1 != undefined ? t1 : this.lengthInSamples;
    channel = channel || 0;
    var zX = [];
    var t = t0;
    if(t == 0) zX.push(t);
    while((t = this.nextDownwardZeroCrossing(t, channel)) < t1)
        zX.push(t);
    return zX;
}
Sample.prototype.zeroCrossingLengths = function(channel, t0, t1, downward) {
  if(channel == undefined) {
    var lengths = []
    for(var c=0; c<this.numberOfChannels; c++) {
      lengths = lengths.concat( this.zeroCrossingLengths(c, t0, t1, downward) )
    }
    return lengths
  }

  if(downward)
    zX = this.downwardZeroCrossings(channel, t0, t1);
  else
    zX = this.upwardZeroCrossings(channel, t0, t1);

  var zXLengths = [];
  for(var i=0; i<zX.length; i++) {
    var prev = zX[i-1] || 0;
    zXLengths.push(zX[i]-prev);
  }
  if(zX[zX.length-1] < this.lengthInSamples)
    zXLengths.push(this.lengthInSamples - zX[zX.length-1]);

  return zXLengths;
}
Sample.prototype.diceByZeroCrossings = function(grainSize, downward) {
    grainSize = grainSize || 1;
    grainSize = Math.ceil(grainSize);
    var waveforms = []
    for(var c=0; c<this.numberOfChannels; c++) {
      var zX = downward ? this.downwardZeroCrossings(c) : this.upwardZeroCrossings(c)
      if(zX[0] != 0) zX.unshift(0)
      if(zX[zX.length-1] != this.lengthInSamples) zX.push(this.lengthInSamples)

      waveforms[c] = []
      for(var i=0; i+1<zX.length; i+=grainSize)
          waveforms[c].push( this.cutOneChannel(c, zX[i], zX[(grainSize+i<zX.length?grainSize+i:zX.length-1)]) )
    }
    return waveforms;
}
Sample.prototype.zXCut = function(t0, t1, channel) {
    t0 = t0 || 0;
    t1 = t1 || this.lengthInSamples;

    t0 = this.closestZeroCrossing(t0);
    t1 = this.closestZeroCrossing(t1);
    if(t1 == t0) t1 = this.nextUpwardZeroCrossing(t0);

    return this.cut(t0, t1);
}
Sample.prototype.zXDice = function(subD) {
    if(this.numberOfChannels > 1) throw "woah, too many channels (Sample.prototype.zXDice)";
    if(subD == undefined) throw "subD is required [Sample.prototype.zXDice]";

    var bits = [];
    var t0, t1;
    for(var t=0; t<this.lengthInSamples; t+=subD) {
        t0 = Math.floor(t);
        t1 = Math.floor(t+subD);
        bits.push(this.zXCut(t0, t1));
    }
    return bits;
}
Sample.prototype.estimatePitchByZeroCrossings = function(t0, t1) {
  var zxLs = this.zeroCrossingLengths(t0, t1)
  var period = 0
  for(var i in zxLs) {
    period = zxLs[i]
    if(zxLs[i] < 50) {
      console.log("skipped")
      continue
    } else {
      var impliedFrequency = this.sampleRate/period
      var impliedPitchClass = mToF.fToPitchClass(impliedFrequency)

      console.log({
        period: period,
        impF: impliedFrequency,
        impPC: impliedPitchClass,
      })
      //period = 0
    }
  }
  console.log("not FINISHEED")
}

// CHANNELS/MULTITRACKING
Sample.prototype.mixDownToMono = function() {
    var d = this.lengthInSamples;
    var monoBuffer = new Float32Array(d);
    var samp;
    for(var t=0; t<d; t++) {
        var samp = 0;
        for(var c in this.channelData) {
            samp += this.channelData[c][t];
        }
        monoBuffer[t] = samp;
    }
    this.channelData = [monoBuffer];

    return this;
}
Sample.prototype.firstChannel = function() {
  var sample = new Sample()
  sample.channelData = [
    this.channelData[0].slice(),
  ]
  return sample
}
Sample.prototype.seperateChannels = function() {
    var channels = [];
    for(var c in this.channelData) {
        var chan = new Sample().blank(this.lengthInSamples, 1);
        chan.channelData[0] = this.channelData[c].slice();
        channels.push(chan);
    }
    return channels;
}
Sample.prototype.convertToSampleRate = function(newSampleRate) {
    var oldSampleRate = this.sampleRate;
    if(oldSampleRate == newSampleRate)
        return this;

    var newChannelData = [];
    var newLengthInSamples = this.lengthInSamples * (newSampleRate/oldSampleRate);
    for(var channel in this.channelData) {
        newChannelData[channel] = new Float32Array(newLengthInSamples);
        for(var t=0; t<newLengthInSamples; t++) {
            newChannelData[channel][t] = this.yOfSample(t/newSampleRate*oldSampleRate, channel);
        }
    }
    this.channelData = newChannelData;
    this.sampleRate = newSampleRate;
    return this;
}

// TIME/PITCH
Sample.prototype.repitch = function(detune) {
    var rate = Math.pow(2, detune/12);
    var newLengthInSamples = Math.ceil(this.lengthInSamples/rate);

    var newChannelData = [];
    for(var c in this.channelData) {
        newChannelData[c] = new Float32Array(newLengthInSamples);
        for(var t=0; t<newLengthInSamples; t++) {
            newChannelData[c][t] = this.yOfSample(t*rate, c);
        }
    }
    this.channelData = newChannelData;
    return this;
}
Sample.prototype.stretch = function(d) {
    var rate = this.lengthInSamples/d;
    var newLengthInSamples = Math.ceil(this.lengthInSamples/rate);

    var newChannelData = [];
    for(var c in this.channelData) {
        newChannelData[c] = new Float32Array(newLengthInSamples);
        for(var t=0; t<newLengthInSamples; t++) {
            newChannelData[c][t] = this.yOfSample(t*rate, c);
        }
    }
    this.channelData = newChannelData;
    return this;
}
Sample.prototype.reverse = function() {
  for(var i in this.channelData) {
    this.channelData[i].reverse()
  }
}

// GAIN/ATTENUATION
Sample.prototype.fadeOutSelf = function(T) {
  T = T || this.sampleRate
  var t0 = this.lengthInSamples-T
  this.meta.fadeOutBegins = t0

  for(var c in this.channelData)
    for(var t=0; t<T; t++)
      this.channelData[c][t0+t] *= dB(t/T * -60)
  return this
}
Sample.prototype.fadeInSelf = function(T) {
  T = T || this.sampleRate
  this.meta.fadeInEnds = T
  var t0 = 0;
  for(var c in this.channelData) {
    for(var t=0; t<T; t++)
      this.channelData[c][t] *= dB((T-t)/T * -60)
  }
  return this
}
Sample.prototype.gain = function(sf, t0, t1) {
    if(!sf) return ;
    t0 = t0 || 0;
    t1 = t1 || this.lengthInSamples;
    for(var c in this.channelData)
        for(var t=t0; t<t1; t++)
            this.channelData[c][t] *= sf;
    return this;
}
Sample.prototype.normalise = function(normal, t0, t1) {
    normal = normal || 0.95;
    t0 = t0 || 0;
    t1 = t1 || this.lengthInSamples;

    var sf = Math.abs(normal/this.peak(t0, t1));
    console.log("normalising, scale-factor:", sf)
    if(sf == Infinity)
      console.log("sample cannot be normalised because it is silent")
    else
      this.gain(sf, t0, t1);
    return this;
}

// SPECTRAL
Sample.prototype.calculateFFT = function(frameSize) {
    frameSize = frameSize || 4096;

    var fft = new FFT(frameSize);

    var channels = [];
    for(var channel in this.channelData) {
        var frames = [];
        for(var t=0; t < this.lengthInSamples; t+=frameSize) {
            if(t+frameSize < this.lengthInSamples)
                var signal = this.channelData[channel].slice(t, t+frameSize);
            else {
                var signal = new Float32Array(frameSize);
                for(var i=t; i<this.lengthInSamples; i++) {
                    signal[i-t] = this.channelData[channel][i];
                }
            }

            // PUT WINDOWING HERE
            for(var t2=0; t2<signal.length; t2++) {
              signal[t2] *= hann(t2, signal.length)
            }

            var phasors = fft.createComplexArray(frameSize);
            fft.realTransform(phasors, signal);
            //console.log(phasors.length);

            var complexes = [];
            var magnitudes = [];
            var phases = [];
            for(var i=0; i+1<phasors.length; i+=2) {
                var z = Complex.from(phasors[i], phasors[i+1]);
                complexes[i/2] = z;
                magnitudes[i/2] = z.magnitude()/frameSize;
                phases[i/2] = z.angle()/(2*Math.PI);
            }

            var frame = {
                "phasors": phasors,
                "magnitudes": magnitudes,
                "phases": phases
            };

            frames.push(frame);
        }
        var frequencyBins = [];
        for(var bin=0; bin<phasors.length/2; bin++) {
            var f = this.sampleRate/phasors.length * bin;
            frequencyBins.push(f);
        }
        channels.push({
            "frames": frames,
            "frameSize": frameSize,
            "frequencyBins": frequencyBins
        });
    }
    return channels
}
Sample.prototype.toSpectralSample = function(frameSize) {
  var specsamp = new SpectralSample()
  specsamp.fromPCM(this, frameSize)
  return specsamp
}
Sample.prototype.__defineGetter__("fft", function() {
    if(!this._fft)
        this._fft = this.calculateFFT();
    return this._fft;
});

Sample.prototype.__defineGetter__("vectorSamples", function() {
  var samps = []
  var T = this.lengthInSamples
  for(var t=0; t<T; t++) {
    var vector = []
    for(var c in this.channelData) {
      vector[c] = this.channelData[c][t]
    }
    samps.push(vector)
  }
  return samps
})

Sample.prototype.trimSelf = function(threshold) {
  threshold = dB(threshold || -60)
  console.log("calling trimSelf, threshold:", threshold)


  var sound = false
  for(var t0=0; t0<this.lengthInSamples && !sound; t0++)

    for(var c=0; c<this.numberOfChannels; c++) {
      if(Math.abs(this.channelData[c][t0]) > threshold)
        sound = true
    }

  sound = false
  for(var t1=this.lengthInSamples-1; t1>=0 && !sound; t1--)
    for(var c=0; c<this.numberOfChannels; c++) {
      if(Math.abs(this.channelData[c][t1]) > threshold)
        sound = true
    }

  return this.cutSelf(t0, t1)
}
