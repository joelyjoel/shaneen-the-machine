var gtts = require("node-gtts"); // REQUIRES INTERNET CONNECTION!!
var Promise = require("promise");
var fs = require("fs");
var Sample = require("./Sample.js");

module.exports = textToSpeech = function(text, lang) {
    // !! simplify text
    lang = lang || "en";
    if(lang == "random")
        lang = textToSpeech.randomLang();
        //lang = langs[Math.floor(Math.random()*langs.length)];

    var filename = "./resources/samples/gtts/"+lang+"-"+text.slice(0,100)+".wav";
    //console.log("filename:", filename);
    return new Promise(function(fulfil, reject) {
        if(!fs.existsSync(filename)) {
            //console.log(filename, "doesn't exist");
            saveTextToSpeech(filename, text, lang).then(function() {
                //console.log("saved text to speech");
                Sample.readFile(filename).then(function(samp) {
                    //console.log("read ", filename);
                    if(samp.sampleRate != 44100) {
                        console.log("converting from sr:", samp.sampleRate);
                        samp.convertToSampleRate(44100);
                        samp.trimSelf(-50)
                        samp.meta.lang = lang
                        samp.meta.text = text
                        samp.saveWavFile(filename);
                    }
                    fulfil(samp);
                });
            });
        } else {
            // file does exist
            //console.log(filename, "DOES exist");
            Sample.readFile(filename).then(function(samp) {
                //console.log("read ", filename);
                //console.log("converting sampleRate");
                if(samp.sampleRate != 44100) {
                    console.log("converting from sr:", samp.sampleRate);
                    samp.convertToSampleRate(44100);
                    samp.trimSelf(-50)
                    samp.saveWavFile(filename);
                }
                fulfil(samp);
            });
        }
    })
    .catch((err) => {
      console.log(err)
    })
}

saveTextToSpeech = function(filename, txt, lang) {
    return new Promise(function(fulfil, reject) {
        console.log("requesting from gtts:", lang, txt);
        gtts(lang).save(filename, txt, function() {
            fulfil();
        }, function(e) {
            console.log(e);
        });
    });
}
textToSpeech.saveTextToSpeech = saveTextToSpeech;

textToSpeech.langs = ["af", "ar", "zh", "hr", "da",
    "en-us", "en-au", "fi", "fr", "de", "hu",
    "id", "it", "ko", "mk", "no", "pt-br", "pt",
    "ro", "ru", "sr", "sk", "es", "es-us", "sv",
    "tr", "vi", "sq", "ca", "zh-cn", "cs", "nl",
    "en", "el", "hi", "ja", "pl", "th"];
textToSpeech.randomLang = function() {
    return textToSpeech.langs[Math.floor(Math.random() * textToSpeech.langs.length)];
}
