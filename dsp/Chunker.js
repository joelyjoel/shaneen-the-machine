const DspUnit = require("./DspUnit.js")
const CircleBuffer = require("./CircleBuffer.js")

function Chunker() {
  DspUnit.call(this)

  this.chunkSize = 512
  this.hopSize = 512
}
Chunker.prototype = Object.create(DspUnit.prototype)
Chunker.prototype.constructor = Chunker
module.exports = Chunker

Chunker.prototype.__defineSocket__("in", undefined, "array")

Chunker.prototype.__defineSetter__("chunkSize", function(chunkSize) {
  if(!this.buffer)
    this.buffer = new CircleBuffer()
  this.buffer.resize(chunkSize, undefined, false);
  this._chunkSize = chunkSize;
})
Chunker.prototype.__defineGetter__("chunkSize", function() {
  return this._chunkSize;
})

Chunker.prototype._tick = function() {
  if(this.buffer.numberOfChannels < this.in.length)
    this.buffer.addChannels(this.in.length-this.buffer.numberOfChannels)
  this.buffer.push(this.in)
  if(this.clock % this.hopSize == 0)
    if(this.onChunk)
      this.onChunk(this.buffer.last());
}
