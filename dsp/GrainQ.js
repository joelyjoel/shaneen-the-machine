const DspUnit = require("./DspUnit.js")

function GrainQ(grains) {
  DspUnit.call(this)

  if(grains && grains.isSample) {
    grains = grains.diceByZeroCrossings()
  }
  this.grains = grains || [] // an array of samples
  this.playGrain(0)
  this.rate = 1
}
GrainQ.prototype = Object.create(DspUnit.prototype)
GrainQ.prototype.constructor = GrainQ
module.exports = GrainQ

GrainQ.prototype.__defineSocket__("rate")
GrainQ.prototype.__definePlug__("y")

GrainQ.prototype._tick = function() {
  this.y = this.currentGrain.y(this.t)
  this.t += this.rate
  if(this.t >= this.currentGrain.lengthInSamples)
    this.finishGrain()
}

GrainQ.prototype.finishGrain = function() {
  this.t -= this.currentGrain.lengthInSamples
  this.playing = false
  if(this.onFinishGrain)
    this.onFinishGrain(this.grainI, this.currentGrain)
}

GrainQ.prototype.playGrain = function(i) {
  i = Math.floor(i)
  if(i >= this.grains.length || i < 0)
    return
  this.currentGrain = this.grains[i]
  this.grainI = i
  if(this.onPlayGrain)
    this.onPlayGrain(this.grainI, this.currentGrain)
  this.t = this.t || 0
  this.playing = true
}
