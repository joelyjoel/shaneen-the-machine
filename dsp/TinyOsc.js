function TinyOsc(f, phase) {
  this.phase = phase || 0
  this.f = f || 440

  this.sampleRate = 44100
  this.sampleRateOver2Pi = 22050 / Math.PI
}

TinyOsc.prototype.tick = function(f) {
  this.phase += (f || this.f)/this.sampleRateOver2Pi
  return Math.sin(this.phase)
}
