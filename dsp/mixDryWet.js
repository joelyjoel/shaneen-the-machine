function mixDryWet(ratio, dry, wet) {
  dry = dry || 0;
  wet = wet || 0;
  return (1-ratio) * dry + ratio * wet;
}
module.exports = mixDryWet;
