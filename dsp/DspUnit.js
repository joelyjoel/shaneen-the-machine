// Generic signal-processing class. Not useful by itself, used as a template for inheritance.
// set prototype._tick in inheritting classes for sample-by-sample process

/*
    SOCKET REFORM!
    Currently, socket data is split between 5 objects
      this.sockets = {} // contains the unit/plugs feeding the sockets
      this.socketConversionFunctions = {} // contains functions for converting
                                           // socket input types
      this.vals = {} // contains current socket values
      prototype.socketF = {} // for preprocessing socket input
      prototype.socketFormat


    The new solution will have a single object (this.sockets) which will
    contain a series of Socket objects
      Socket {
        plug:
        currentVal:
        conversionFunction:
        inputFunction: // replacing  socketFs
        format:
      }

    This is conceptually simple, but will require a few hours work because of
    the fiddly redoing of lots of already implemented code. It will necessarily
    need to be done in one go because everything will stop working while work
    is underway.
*/
const Socket = require("./Socket.js")

function DspUnit() {
  this.clock = -1 // samples since processing began. Don't meddle!
  this.tickClock = 0
  this.tickedUntil = -1
  //this.sockets = {} // contains the unit/plugs feeding the sockets
  //this.socketConversionFunctions = {} // contains functions for converting socket input types
  //this.vals = {} // contains current socket values
  this.chains = []
  this.out = {} // output values
  this.events = []
  this.finished = false

  this.label = "Unlabelled_" + this.constructor.name

  this.sockets = {}
  for(var i in this.socketTemplate) {
    this.sockets[i] = new Socket(this.socketTemplate[i])
    this.sockets[i].owner = this
  }
}
module.exports = DspUnit

DspUnit.connect = require("./connect.js")

// standard variables
DspUnit.prototype.isDspUnit = true
DspUnit.prototype.sampleRate = 44100
DspUnit.prototype.tickInterval = 1

// Defining plugs and sockets
//DspUnit.prototype.socketFs = {}
//DspUnit.prototype.socketFormats = {}
DspUnit.prototype.__defineSocket__ = function(name, F, format) {
  // Define a socket for the prototype, equivalent to max 'inlet'
  // `name` is the handle of the socket
  // `F` is an optional function which processes new data on its way into the socket.
  // getting a socket returns its current value
  // setting a socket sets it to the new value permanently, handles shaneen-dsp objects

  format = format || "number"

  var socket = new Socket()
  socket.format = format
  socket.name = name
  socket.inputFunction = F

  var temp = {}
  temp[name] = socket
  this.socketTemplate = Object.assign({}, this.socketTemplate, temp)


  this.__defineGetter__(name, function() {
    return this.sockets[name].val
  })
  this.__defineSetter__(name, function(val) {
    if(val && val.isDspUnit) {
      val = val[val.defaultPlug]
    }
    if(val && val.isPlug) {
      val.unit.syncClocks(this.clock)
      this.sockets[name].plug = val
      this.sockets[name].tickClockAtLastRead = -1
      if(val.format != this.sockets[name].format) {
        var convStr = val.format + "_to_" + this.sockets[name].format
        var conversionFunction = DspUnit.conversionFunctions[convStr]
        this.sockets[name].conversionFunction = conversionFunction
        if(conversionFunction == undefined) {
          throw ("no conversion function for " + convStr
            + "\nunit: " + this.label + ",\tsocket: "+name)
        }
      } else {
        this.sockets[name].conversionFunction = null
      }
    } else {

      this.sockets[name].plug = null
      this.sockets[name].conversionFunction = false
      var valType = typeof val
      if(valType == "object") valType = "array"
      if(valType != this.sockets[name].format){

        var convStr = valType + "_to_" + this.sockets[name].format
        var conversionFunction = DspUnit.conversionFunctions[convStr]
        if(conversionFunction)
          val = conversionFunction(val)
        else
          throw ("no conversion function for " + convStr +
          "\nunit: " + this.label + ",\tsocket: " + name)
      }
      if(this.sockets[name].inputFunction)
        this.sockets[name].val = this.sockets[name].inputFunction.call(this, val)
      else
        this.sockets[name].val = val
    }
  })
  this.__defineGetter__("_"+name, function() {
    if(this.sockets[name].plug)
      return this.sockets[name].plug.unit
    else
      return this.sockets[name].val
  })
}
DspUnit.prototype.__definePlug__ = function(name, format) {
  // define a plug for the prototype, equivalent to an 'outlet' in max
  // `name` is the plugs handle
  // setting a plug exposes the value to objects whos sockets read from the plug
  // getting a plug returns a pointer object, which can be passed to a socket

  format = format || "number"

  this.__defineGetter__(name, function() {
    return {
      unit: this,
      out: name,
      format: format,
      isPlug: true
    }
  })
  this.__defineGetter__("_"+name, function() {
    return this.out[name]
  })
  this.__defineSetter__(name, function(v) {
    this.out[name] = v
  })
}
DspUnit.prototype.defaultPlug = "y"

DspUnit.prototype.__defineRepeater__ = function(socketName, plugName, format) {
  plugName = plugName || (socketName + "Plug")
  this.__definePlug__(plugName, format)
  this.__defineSocket__(socketName, function(x) {
    this[plugName] = x
    return x
  }, format)
}

DspUnit.conversionFunctions = {
  number_to_number: function(n) {
    return n
  },
  array_to_array: function(n) {
    return n
  },
  number_to_array: function(n) {
    return [n]
  },
  array_to_number: function(n) {
    return n[0]
  },
  array_to_vector: function(n) {
    return {
      x: n[0],
      y: n[1],
    }
  },
  vector_to_array: function(n) {
    return [n.x, n.y]
  },
  object_to_vector: function(n) {
    return n
  },
  vector_to_vector: function(n) {
    return n
  }
}

// Chaining
DspUnit.prototype.chain = function(unit) {
  if(this.chains.indexOf(unit) == -1) {
    this.chains.push(unit)
  }
  return this
}

// Ticking
DspUnit.prototype.tick = function(dT) {
  this.clock += this.tickInterval
  this.tickClock++
  while(this.events[0] && this.events[0].t <= this.clock) {
    var evnt = this.events.shift()
    var eventReturn = evnt.F.call(this)
    if(eventReturn > 0) {
      this.schedule(this.clock + eventReturn, evnt.F)
    }
  }
  this.tickSocketsUntil(this.clock)
  if(this._tick)
    this._tick(dT || 1)
}
DspUnit.prototype.tickUntil = function(t) {
  if(t > this.tickedUntil || true) {
    this.tickedUntil = t
    var n = 0
    while(this.clock + this.tickInterval <= t) {
      n++
      //console.log("tickuntil::", this.label, n)
      this.tick(this.tickInterval)
    }
  }
}
DspUnit.prototype.tickSocketsUntil = function(t) {
  for(var i in this.chains) {
    this.chains[i].tickUntil(t)
  }
  for(var socketName in this.sockets) {
    //console.log(this.label, "Ticking..", socketName, "until", t)
    var socket = this.sockets[socketName]
    var cat = socket
    if(!socket.plug)
      continue

    socket.plug.unit.tickUntil(t)

    if(!socket.plug)
      continue

    if(socket.plug.unit.clock > t) {
      console.log(
        "A DspUnit has got ahead by",
        socket.plug.unit.clock-t,
        "samples",
        socket.plug.unit.label,
        "(t=", socket.plug.unit.clock,")",
        "\n\tbeing called by",
        this.label, "(t=",t,")",
      )
      //throw "ARGH"
    }
    if(socket.tickClockAtLastRead < socket.unit.tickClock) {
      var val = socket.plug.unit.out[socket.plug.out]
      if(socket.conversionFunction)
        val = socket.conversionFunction(val)
      socket.tickClockAtLastRead = socket.unit.tickClock
      if(socket.inputFunction) {
        socket.val = socket.inputFunction.call(this, val)
      } else
        socket.val = val
    } else {
      /*console.log(
        "a different kind of problem",
        socket.tickClockAtLastRead,
        socket.unit.tickClock
      )
      throw "wtf"*/
    }
  }
}

// Scheduling
DspUnit.prototype.schedule = function(t, F) {
  // schedule a function, F, to be called at when the clock reaches t
  // if F returns a number, the event will be rescheduled that number of samples later

  var evnt = {
    t: t,
    F: F,
  }

  var inserted = false
  for(var i=0; i<this.events.length; i++) {
    if(this.events[i].t > evnt.t) {
      this.events.splice(i, 0, evnt)
      inserted = true
      break
    }
  }
  if(!inserted) {
    this.events.push(evnt)
  }
}
DspUnit.prototype.scheduleAhead = function(dT, F) {
  this.schedule(this.clock + dT, F)
}
DspUnit.prototype.doAndReschedule = function(F) {
  this.schedule(this.clock, F)
}
DspUnit.prototype.__defineGetter__("lastScheduleTime", function() {
  if(this.events.length == 0)
    return 0
  return this.events[this.events.length-1].t
})

DspUnit.prototype.__defineGetter__("tickRate", function() {
  // ticks per second
  return this.sampleRate / this.tickInterval
})
DspUnit.prototype.__defineSetter__("tickRate", function(tr) {
  this.tickInterval = Math.ceil(this.sampleRate/tr)
})

DspUnit.prototype.exploreCircuit = function(components) {
  var components = components || []
  if(components.indexOf(this) == -1)
    components.push(this)
  else return components

  for(var i in this.sockets) {
  //  console.log(this.sockets[i])
    if(this.sockets[i].plug) {
      components = this.sockets[i].unit.exploreCircuit(components)
    }
  }

  for(var i in this.chains) {
    components = this.chains[i].exploreCircuit(components)
  }

  var extras = this.extras
  if(extras)
    for(var i in extras)
      components = this.extras[i].exploreCircuit(components)

  return components
}
DspUnit.prototype.syncClocks = function(t) {
  t = t || this.clock
  var components = this.exploreCircuit()
  //console.log("syncing clocks", t, components.length)
  for(var i in components) {
    components[i].clock = t
  }
  return t
}

DspUnit.prototype.__defineGetter__("unpluggedSockets", function() {
  var circuit = this.exploreCircuit()
  var list = []
  for(var i in circuit) {
    for(var socketName in circuit[i].sockets) {
      var socket = circuit[i].sockets[socketName]
      var unplugged = socket.plug == null
      if(unplugged)
        list.push(socket)
    }
  }
  return list
})

// Events
DspUnit.prototype.finish = function() {
  if(this.onFinish)
    this.onFinish()
  this.finished = true
}

/*DspUnit.prototype.__defineGetter__("label", function() {
  if(this._label)
    return this._label
  else
    return "Unlabelled_"+this.constructor.name
})*/
