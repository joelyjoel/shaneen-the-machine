const Shaper = require("./Shaper.js")
const Packer = require("./PackMixer.js")
const Osc = require("./Osc.js")

function PolynomialShaper(plug) {
  Shaper.call(this, plug)
  this.coefficients = [0, 1, 2, 1]
  this.compensate = true
}
PolynomialShaper.prototype = Object.create(Shaper.prototype)
PolynomialShaper.prototype.constructor = PolynomialShaper
module.exports = PolynomialShaper

PolynomialShaper.random = function(plug, n) {
  n = n || 5
  var shaper = new PolynomialShaper(plug)
  var coeffs = []
  for(var i=0; i<n; i++) {
    coeffs.push(Math.random())
  }
  shaper.coefficients = coeffs
  return shaper
}
PolynomialShaper.morphing = function(plug, n, fMax) {
  n = n || 5
  var shaper = new PolynomialShaper(plug)
  var packer = new Packer()
  for(var i=0; i<n; i++) {
    var osc = Osc.randomLFO(fMax, 0.9, 1)
    packer.addInput(osc)
  }
  shaper.coefficients = packer
  return shaper
}

PolynomialShaper.prototype.__defineSocket__("coefficients", function(coefficients) {
  this.compensationNeedsCalculating = true
  return coefficients
}, "array")

PolynomialShaper.prototype.F = function(x, channel) {
  var poly = this.coefficients
  var y = 0
  for(var i=0; i<poly.length; i++) {
    y += poly[i] * Math.pow(x, i*2+1)
  }
  return y
}
