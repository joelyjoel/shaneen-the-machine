const DspUnit = require("./DspUnit.js");

function AddNumbers(A, B) {
  DspUnit.call(this);
  this.A = A || 0;
  this.B = B || 0;
}
AddNumbers.prototype = Object.create(DspUnit.prototype);
AddNumbers.prototype.constructor = AddNumbers;
module.exports = AddNumbers;

AddNumbers.prototype.__defineSocket__("A");
AddNumbers.prototype.__defineSocket__("B");
AddNumbers.prototype.__definePlug__("y");

AddNumbers.prototype._tick = function() {
  this.y = this.A + this.B
}
