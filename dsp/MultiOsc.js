const DspUnit = require("./DspUnit.js")
const Osc = require("./Osc.js")
const Mixer = require("./Mixer")
const Gain = require("./Gain.js")
const mToF = require("./midiToFrequency.js")

function MultiOsc(frequencies) {
  DspUnit.call(this)
  this.oscs = []
  this.mixer = new Mixer()
  this.mix = new Gain(this.mixer)
  this.A = 1

  if(frequencies && frequencies.constructor == Array) {
    for(var i in frequencies) {
      this.add(frequencies[i])
    }
  }
}
MultiOsc.prototype = Object.create(DspUnit.prototype)
MultiOsc.prototype.constructor = MultiOsc
module.exports = MultiOsc

MultiOsc.random = function(n) {
  n = n || 2
  var osc = new MultiOsc()
  for(var i=0; i<n; i++) {
    var f = Math.random() + 1/2
    var A = Math.random()
    osc.add(f, A)
  }
  return osc
}
MultiOsc.randomHarmonic = function(n, maxNumerator, maxDenominator, maxDetune) {
  maxNumerator = maxNumerator || 16
  maxDenominator = maxDenominator || 1
  maxDetune = maxDetune || 0
  n = n || 2
  var osc = new MultiOsc()
  for(var i=0; i<n; i++) {
    var f =Math.ceil(Math.random()*maxNumerator)
      / Math.ceil(Math.random()*maxDenominator)
      * mToF((Math.random()*2-1)*maxDetune)
    var A = Math.random()
    osc.add(f, A)
  }
  return osc
}

MultiOsc.prototype.__defineRepeater__("p")
MultiOsc.prototype.__defineSocket__("A")
MultiOsc.prototype.__defineSocket__("mix")
MultiOsc.prototype.__definePlug__("y")

MultiOsc.prototype._tick = function() {
  this.y = this.mix * this.A
}

MultiOsc.prototype.add = function(f, A) {
  var osc = new Osc()
  osc.f = f
  osc.p = this.pPlug
  osc.A = A || 1
  this.mixer.addInput(osc)
  this._mix.sf = 1/this.mixer.inputs.length
  this.oscs.push(osc)
}

MultiOsc.prototype.__defineSetter__("partials", function(partials) {
  this.oscs = []
  this.mixer.clear()
  for(var i in partials) {
    this.add(partials[i])
  }
})
