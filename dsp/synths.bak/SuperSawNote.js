const DspUnit = require("../DspUnit.js")
const Mixer = require("../Mixer.js")
const Osc = require("../Osc.js")
const Spatialise = require("../Spatialise.js")
const Sum = require("../Sum.js")
const ADSR = require("../ADSR.js")

function SuperSawNote(p, n, maxDetune) {
  DspUnit.call(this)
  n = n || Math.floor(Math.random()*10)
  maxDetune = maxDetune || 0.1

  this.p = p || 36

  this.oscs = []
  this.oscSockets = []
  var mixer = new Mixer()
  for(var i=0; i<n; i++) {
    var detune = (Math.random()*2-1) * maxDetune
    var osc = new Osc()
    osc.waveform = "saw"
    var oscSocket = new Sum(this.pOut, detune)
    this.oscSockets.push(oscSocket)
    osc.p = oscSocket

    var space = Spatialise.random()
    space.in = osc


    this.oscs.push(osc)
    mixer.addInput(space)
  }
  this.envelope = new ADSR()
  this.mixer = mixer
  this.oscMix = mixer
}
SuperSawNote.prototype = Object.create(DspUnit.prototype)
SuperSawNote.prototype.constructor = SuperSawNote
module.exports = SuperSawNote

SuperSawNote.prototype.__defineSocket__("oscMix", null, "array")
SuperSawNote.prototype.__definePlug__("y", "array")
SuperSawNote.prototype.__defineSocket__("p", function(p) {
  this.pOut = p
})
SuperSawNote.prototype.__definePlug__("pOut")

SuperSawNote.prototype._tick = function() {
  this.y = this.oscMix
}
