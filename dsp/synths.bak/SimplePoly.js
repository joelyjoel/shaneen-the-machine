const Poly = require("./Poly.js")
const SimpleNote = require("./SimpleNote.js")

function SimplePoly(A, D, S, R) {
  Poly.call(this)

  this.A = A || 1
  this.D = D || 1
  this.S = S || 1
  this.R = R || 1
}
SimplePoly.prototype = Object.create(Poly.prototype)
SimplePoly.prototype.constructor = SimplePoly
module.exports = SimplePoly

SimplePoly.prototype.__defineSocket__("A")
SimplePoly.prototype.__defineSocket__("D")
SimplePoly.prototype.__defineSocket__("S")
SimplePoly.prototype.__defineSocket__("R")

SimplePoly.prototype.makeNote = function(p) {
  return new SimpleNote(p, this.A, this.D, this.S, this.R)
}
