const Poly = require("./Poly.js")
const SuperSawNote = require("./SuperSawNote.js")

function SuperSawPoly() {
  Poly.call(this)
}
SuperSawPoly.prototype = Object.create(Poly.prototype)
SuperSawPoly.prototype.constructor = SuperSawPoly
module.exports = SuperSawPoly

SuperSawPoly.prototype.makeNote = function(p) {
  return new SuperSawNote(p)
}
