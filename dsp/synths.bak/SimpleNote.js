const DspUnit = require("../DspUnit.js")
const Osc = require("../Osc.js")
const ADSR = require("../ADSR.js")

function SimpleNote(p, A, D, S, R) {
  DspUnit.call(this)
  this.osc = new Osc()
  this.osc.p = p || 69
  console.log(A)
  this.env = new ADSR(A, D, S, R)

  this.chain(this.osc)

  var thisNote = this;

}
SimpleNote.prototype = Object.create(DspUnit.prototype)
SimpleNote.prototype.constructor = SimpleNote
module.exports = SimpleNote

SimpleNote.prototype.__definePlug__("y", "array")
SimpleNote.prototype._tick = function() {
  if(isNaN(this.osc._y))
    console.log(this.osc._y, this.env._y, this.env)
  this.y = [this.osc._y]
}

SimpleNote.prototype.trigger = function() {
  this.env.trigger()
}
SimpleNote.prototype.release = function() {
  this.env.release()
}

SimpleNote.prototype.__defineGetter__("env", function() {
  return this.osc._A
})
SimpleNote.prototype.__defineSetter__("env", function(env) {
  this.osc.A = env
  var thisNote = this
  env.onFinish = function() {
    if(thisNote.onFinish)
      thisNote.onFinish()
  }
})
