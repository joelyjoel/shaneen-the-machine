const DspUnit = require("../DspUnit.js")
const Mixer = require("../Mixer.js")
const SimpleNote = require("./SimpleNote.js")

function Poly() {
  DspUnit.call(this)
  this.playing = {}
  this.mixer = new Mixer()
  this.mix = this.mixer
}
Poly.prototype = Object.create(DspUnit.prototype)
Poly.prototype.constructor = Poly
module.exports = Poly

Poly.prototype.__defineSocket__("mix", function(mix) {
  this.y = mix
}, "array")
Poly.prototype.__definePlug__("y", "array")

Poly.prototype.trigger = function(p, details) {
  var note = this.makeNote(p)
  note.trigger()
  var chan = this.mixer.addInput(note)
  if(this.playing[p])
    this.playing[p].release()
  this.playing[p] = note
  note.onFinish = function() {
    chan.remove()
  }
  return note
}
Poly.prototype.release = function(p, details) {
  if(this.playing[p])
    this.playing[p].release()
}

Poly.prototype.makeNote = function(p) { // to be replaced
  var note = new SimpleNote(p)
  return note
}

Poly.prototype.__defineGetter__("sequencer", function() {
  return this._sequencer
})
Poly.prototype.__defineSetter__("sequencer", function(sequencer) {
  this._sequencer = sequenecer
})
