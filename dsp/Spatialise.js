const DspUnit = require("./DspUnit.js");
const dB = require("./decibel.js");
const CircleBuffer = require("./CircleBuffer.js")
const CircularMotion = require("./vector/CircularMotion.js")

function Spatialise(place) {
  DspUnit.call(this);
//  this.speakerConfig = Spatialise.speakerConfigs.fivePointOne;/*
  this.speakerConfig = Spatialise.speakerConfigs.stereo //*/

  this.sf = [];
  this.delays = [];
  this.decibelsPerMeter = 6;
  this.sampleDelayPerMeter = 44100 / 343;
  this.place = place || [0,0]

  this.lastIn = 0
  this.lastOut = []
  this.differentiate = false
  //this.integrationDecay = 64 // decibels per second
  this.integrationFilterF = 1
  //this.integrationFilterA = 0.0021348549947766138 // 15Hz
  //this.integrationFilterB = 0.9978651450052234 // 15Hz

  this.interpolate = true

  this.buffer = new CircleBuffer(44100, this.speakerConfig.length);
}
Spatialise.prototype = Object.create(DspUnit.prototype);
Spatialise.prototype.constructor = Spatialise;
module.exports = Spatialise;

Spatialise.random = function(input, maxRadius) {
  var r = Math.random() * (maxRadius || 2) // radius
  var angle = Math.random() * 2 * Math.PI
  var place = [
    r * Math.cos(angle),
    r * Math.sin(angle),
  ]

  var spatialise = new Spatialise()
  spatialise.place = place
  spatialise.in = input
  return spatialise
}

Spatialise.randomComplexOrbit = function(input, n, fMax, rMax, O) {
  var space = new Spatialise()
  space.place = CircularMotion.recursive(n, fMax, rMax, O)
  space.in = input
  return space
}

Spatialise.speakerConfigs = {
  fivePointOne: [
    [-1, 1],  // left
    [1,1],   // right
    [0, Math.sqrt(2)], // centre
    [0,0],   // sub
    [-1,-1], // surround left
    [1,-1],  // surround right
  ],
  stereo: [
    [-1,0],
    [1,0],
  ]
}

Spatialise.prototype.__defineSocket__("in");
Spatialise.prototype.__defineSocket__("place", function(place) {
  for(var c in this.speakerConfig) {
    var xD = (place[0] || 0) - this.speakerConfig[c][0];
    var yD = (place[1] || 0) - this.speakerConfig[c][1];
    var distance = Math.sqrt(xD*xD + yD*yD);
    this.sf[c] = dB(-distance*this.decibelsPerMeter)
    this.delays[c] = distance * this.sampleDelayPerMeter;
  }
  return place
}, "array");
Spatialise.prototype.__definePlug__("y", "array");

Spatialise.prototype._tick = function() {
  var x = this.in
  if(this.differentiate) {
    x = x-this.lastIn
    this.lastIn = x
  }
  if(this.interpolate)
    for(var c in this.sf)
      this.buffer.linearInterpolateMixAhead( (this.sf[c] * x) || 0, this.delays[c], c )
  else
    for(var c in this.sf)
      this.buffer.mixAhead( (this.sf[c] * x) || 0, this.delays[c], c)
  var y = this.buffer.shift();
  if(this.differentiate) {
    for(var c=0;c<y.length; c++) {
      y[c] = y[c] + (this.lastOut[c] || 0)
      var before = y[c]
      y[c] = this.integrationFilterA * y[c] - this.integrationFilterB * (this.lastOut[c] || 0)
      var after = y[c]
      //console.log(after-before)
    }
    this.lastOut = y//.slice() // is slice necessary??
    //y[c] = this.a * x[c] - this.b * (y[c] || 0);
  }
  this.y = y;

}
/*Spatialise.prototype.sfF = function(distance) {
  // attenuation function
  return dB(-distance*decibelsPerMeter)
}*/

/*Spatialise.prototype.__defineSetter__("integrationDecay", function(dbPerSecond) {
  this.integrationScaleFactor = dB(-dbPerSecond/this.sampleRate)
  console.log("integrationScaleFactor:", this.integrationScaleFactor)
})*/
Spatialise.prototype.__defineSetter__("integrationFilterF", function(f) {
  var costh = 2 - Math.cos(2 * Math.PI * f / this.sampleRate);
  var b = Math.sqrt(costh * costh - 1) - costh;
  this.integrationFilterA = 1 + b;
  b *= -1;
  this.integrationFilterB = b;
  this._integrationFilterF = f
})
