const MultiSampler = require("./MultiSampler.js");
const PointTrackPlayer = require("./PointTrackPlayer.js");
const parseSound = require("../music/parseSound.js");

function sequenceDrumTrack(pattern) {
  var percset = pattern.percset;

  var chooseQ = {}
  for(var i in percset) {
    var label = parseSound(percset[i]).label//percset[i].slice(0, percset[i].indexOf("_"));
    chooseQ[percset[i]] = chooseTable[label] || "drum";
  }

  return MultiSampler.choose(chooseQ).then(function(sampler) {
    sampler.sequencer = new PointTrackPlayer();
    sampler.sequencer.track = pattern;
    sampler.sequencer.onNoteOn = function(note) {
      sampler.trigger(note.sound)
    }
    sampler.chain(sampler.sequencer);
    //console.log(sampler)
    return sampler;
  })
}
module.exports = sequenceDrumTrack;

const chooseTable = {
  "kick": "drum/kick",
  "k": "drum/kick",
  "hh": "drum/hh",
  "clap": "drum/clap",
  "snare": "drum/snare",
  "sn": "drum/snare",
  "perc": "drum/percussion",
  "shaker": "drum/percussion/shaker",
  "sh": "drum/percussion/shaker",
}
