function mToF(m) {
  return Math.pow(2, ((m-69)/12)) * 440
}
module.exports = mToF

var twelveOverLog2 = 12/Math.log(2)
function fToM(f) {
  return twelveOverLog2 * Math.log(f/440) + 69
}
module.exports.fToM = fToM

function fToPitchClass(f) {
  var p = fToM(f)
  p %= 12
  if(p<0)
    p += 12
  return p
}
module.exports.fToPitchClass = fToPitchClass
