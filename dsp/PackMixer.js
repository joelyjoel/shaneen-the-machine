const Mixer = require("./Mixer.js")

function PackMixer() {
  Mixer.call(this)
}
PackMixer.prototype = Object.create(Mixer.prototype)
PackMixer.prototype.constructor = PackMixer
module.exports = PackMixer

PackMixer.prototype.mixFunction = function(a, b) {
  return a.concat(b)
}
