var DspUnit = require("./DspUnit.js");

function PointTrackPlayer() {
  DspUnit.call(this);
  this.track = undefined;

  this.rate = 1;
  this.bpm = 139;

  this.tTrack = 0;
  this.soundingNotes = [];
  this.nextNote;

  this.synth = null
}
PointTrackPlayer.prototype = Object.create(DspUnit.prototype);
PointTrackPlayer.prototype.constructor = PointTrackPlayer;
module.exports = PointTrackPlayer;

PointTrackPlayer.prototype.__defineSocket__("rate");
PointTrackPlayer.prototype.__defineGetter__("bpm", function() {
  return this.dPerSample * 15 * this.sampleRate;
})
PointTrackPlayer.prototype.__defineSetter__("bpm", function(bpm) {
  this.dPerSample = bpm / (15 * this.sampleRate);
});
PointTrackPlayer.prototype.__defineGetter__("T", function() {
  return this.rate * this.track.d/this.dPerSample;
})

PointTrackPlayer.prototype._tick = function() {
  this.tTrack += this.rate * this.dPerSample;
  if(this.nextNote && this.tTrack >= this.nextNote.t)
    this.noteOn(this.nextNote);
  if(this.soundingNotes[0] && this.tTrack >= this.soundingNotes[0].tOff)
    this.noteOff(this.soundingNotes[0]);
}
PointTrackPlayer.prototype.noteOn = function(note) {
  if(this.synth)
    this.synth.trigger(note.sound, note)
  if(this.onNoteOn) this.onNoteOn(note);
  for(var i=0; i<this.soundingNotes.length; i++)
    if(this.soundingNotes[i].d == undefined ||
      this.soundingNotes[i].tOff > note.tOff) {
      i--;
      break;
    }
  this.soundingNotes.splice(i, 0, note);

  this.nextNote = this._track.notes[++this.nextNoteI];
}
PointTrackPlayer.prototype.noteOff = function(note) {
  if(this.synth)
    this.synth.release(note.sound, note)
  if(this.onNoteOff) this.onNoteOff(note);
  this.soundingNotes.splice(this.soundingNotes.indexOf(note), 1);
}

PointTrackPlayer.prototype.__defineGetter__("track", function() {
  return this._track;
})
PointTrackPlayer.prototype.__defineSetter__("track", function(track) {
  this._track = track;
  this.findNextNote();
})
PointTrackPlayer.prototype.findNextNote = function() {
  if(!this._track) {
    this.nextNote = undefined;
    this.nextNoteI = undefined;
    return undefined;
  }

  for(var i=0; i<this._track.notes.length; i++) {
    if(this._track.notes[i].t >= this.tTrack) {
      this.nextNote = this._track.notes[i];
      this.nextNoteI = i;
      return i;
    }
  }
}
