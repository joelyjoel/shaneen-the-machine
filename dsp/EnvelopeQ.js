const DspUnit = require("./DspUnit.js")

function EnvelopeQ() {
  DspUnit.call(this)

  this.envelopes = []
  this.i = -1
  for(var i in arguments) {
    this.envelopes.push(arguments[i])
  }
}
EnvelopeQ.prototype = Object.create(DspUnit.prototype)
EnvelopeQ.prototype.constructor = EnvelopeQ
module.exports = EnvelopeQ

EnvelopeQ.prototype.__defineSocket__("currentEnvelope")
EnvelopeQ.prototype.__definePlug__("y")

EnvelopeQ.prototype.nextEnvelope = function() {
  this.i++
  if(this.i < this.envelopes.length) {
    var env = this.envelopes[this.i]
    env.trigger()
    this.currentEnvelope = env
  } else {
    this.finish()
    this.i = -1
    this.currentEnvelope = 0
  }
}

EnvelopeQ.prototype.trigger = function() {
  this.i = -1
  this.nextEnvelope()
}

EnvelopeQ.prototype._tick = function() {
  if(this._currentEnvelope.finished) {
    this.nextEnvelope()
  }
  this.y = this.currentEnvelope
}
