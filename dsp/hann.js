function hann(n, N) {
  return (1/2) * (1 - Math.cos(2 * Math.PI * n / (N-1)))
}
module.exports = hann;
