const DspUnit = require("./DspUnit.js");
const mToF = require("./midiToFrequency")

function Filter(f, q) {
  DspUnit.call(this);
  this.highPass = false;
  this.yNMinus1 = 0;
  this.f = f || 22000;
  this.q = q || 5 // this is a mislabelling !!
  this.history = []
}
Filter.prototype = Object.create(DspUnit.prototype);
Filter.prototype.constructor = Filter;
module.exports = Filter;

Filter.prototype.__defineSocket__("in", undefined, "array");
Filter.prototype.__defineSocket__("f", function(f) {
  var costh = 2 - Math.cos(2 * Math.PI * f / this.sampleRate);
  var b = Math.sqrt(costh * costh - 1) - costh;
  this.a = 1 + b;
//  if(this.highPass)
//    b *= -1;
  this.b = b;

  return f;
});
Filter.prototype.__defineSocket__("p", function(p) {
  this.f = mToF(p)
  return p
})
Filter.prototype.__definePlug__("y", "array");

Filter.prototype._tick = function() {
  var x = this.in.slice();
  for(var q=0; q<this.q; q++) {
    var y = this.history[q] || [];
    for(var c in x) {
      y[c] = this.a * x[c] - this.b * (y[c] || 0)
      x[c] = y[c]
    }
    this.history[q] = y
  }
  if(this.highPass)
    for(var c in y)
      y[c] = this.in[c]-y[c]
  this.y = y;
}
