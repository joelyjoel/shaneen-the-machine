const DspUnit = require("./DspUnit.js")

function Rescale() {
  DspUnit.call(this)

  this.inLo = 0
  this.inHi = 1
  this.outLo = 0
  this.outHi = 1
}
Rescale.prototype = Object.create(DspUnit.prototype)
Rescale.prototype.constructor = Rescale
module.exports = Rescale

Rescale.prototype.__defineSocket__("in", null, "array")
Rescale.prototype.__defineSocket__("inLo")
Rescale.prototype.__defineSocket__("inHi")
Rescale.prototype.__defineSocket__("outLo")
Rescale.prototype.__defineSocket__("outHi")
Rescale.prototype.__definePlug__("y", "array")

Rescale.prototype._tick = function() {
  var y = []
  for(var c in this.in)
    y[c] = (this.in[c]-this.inLo) / (this.inHi-this.inLo) *
    (this.outHi-this.outLo) + this.outLo
  this.y = y;
}
console.log("WARNING: Rescale() is untested!!")
