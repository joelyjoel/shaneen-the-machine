const DspUnit = require("./DspUnit.js")

function BitCrusher(bitRate) {
  DspUnit.call(this)

  this.bitRate = bitRate || 8
}
BitCrusher.prototype = Object.create(DspUnit.prototype)
BitCrusher.prototype.constructor = BitCrusher
module.exports = BitCrusher

BitCrusher.prototype.__defineSocket__("in", null, "array")
BitCrusher.prototype.__defineSocket__("pointDivisions")
BitCrusher.prototype.__defineSocket__("bitRate", function(bitRate) {
  this.pointDivisions = Math.pow(2, bitRate)
})
BitCrusher.prototype.__definePlug__("y", "array")

BitCrusher.prototype._tick = function() {
  var x = this.in
  var y = []
  var pd = this.pointDivisions/2
  for(var c in x) {
    y[c] = Math.round(x[c]*pd)/pd
  }
/*  console.log(
    "y:", y,
    "pd:", pd,
    "x:", x,
  )*/
  this.y = y
}
