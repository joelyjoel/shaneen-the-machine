const DspUnit = require("./DspUnit.js")

function ZeroX(plug) {
  DspUnit.call(this)
  this.in = plug

  this.prev = [] // stores previous sample for comparison
  this.buffer = []

  this.countInterval = 441
  this.counts = []

  this.minWavelength = 10 // in samples
}
ZeroX.prototype = Object.create(DspUnit.prototype)
ZeroX.prototype.constructor = ZeroX
module.exports = ZeroX

ZeroX.prototype.__defineSocket__("in", null, "array")
ZeroX.prototype.__definePlug__("fEstimate", "array")

ZeroX.prototype._tick = function() {
  var x = this.in
  for(var c in x) {
    if(this.prev[c] < 0 && x[c] > 0) {
      if(this.buffer[c].length > this.minWavelength)
        this.zeroCrossing(
          this.buffer[c], // WAVEFORM
          this.clock-this.buffer[c].length, // tStart
          c, // channel
        )
      this.buffer[c] = []
    }
    if(x[c] != 0)
      this.prev[c] = x[c]
    if(!this.buffer[c])
      this.buffer[c] = []
    this.buffer[c].push(x[c])
  }
  if(this.countInterval && this.clock%this.countInterval == 0) {
    console.log("Count A:", this.clock, this.counts)
    var f = [0]
    for(var c in this.counts) {
      console.log("B", c)
      f[c] = 44100/this.countInterval * this.counts[c]
      console.log("C")
      this.counts[c] = null
    }
    this.fEstimate = f
  }
}

ZeroX.prototype.zeroCrossing = function(waveletBuffer, tStart, c) {
  if(this.countInterval)
    this.counts[c] = (this.counts[c] || 0) + 1
  if(this.onZeroCrossing)
    this.onZeroCrossing(waveletBuffer, tStart, c, this.clock /* (tNow) */)
}
