const DspUnit = require("./DspUnit.js");

function SixPan() {
  DspUnit.call(this);
}
SixPan.prototype = Object.create(DspUnit.prototype);
SixPan.prototype.constructor = SixPan;

SixPan.random = function() {
  this.left   = Math.random();
  this.right  = Math.random();
  this.centre = Math.random()
  this.sub    = Math.random();
  this.surroundLeft = Math.random();
  this.surroundRight = Math.random();
}

SixPan.prototype.__definePlug__("y", "array");
SixPan.prototype.__defineSocket__("in");
SixPan.prototype.__defineSocket__("left"); // left
SixPan.prototype.__defineSocket__("right"); // right
SixPan.prototype.__defineSocket__("centre"); // centre
SixPan.prototype.__defineSocket__("sub"); // sub
SixPan.prototype.__defineSocket__("surroundLeft");
SixPan.prototype.__defineSocket__("surroundRight");
SixPan.prototype.__defineSocket__("master");


SixPan.prototype._tick = function() {
  var inScaled = this.in * this.master;
  this.y = [
    inScaled * this.left,
    inScaled * this.right,
    inScaled * this.centre,
    inScaled * this.sub,
    inScaled * this.surroundLeft,
    inScaled * this.surroundRight,
  ]
}
