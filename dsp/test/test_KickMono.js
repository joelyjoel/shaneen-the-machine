const KickMono = require("../synths/KickMono.js")
const render = require("../Sample.js").record
const make_kick = require("../../music/make_rhythm.js").kick
const Spatialise = require("../Spatialise.js")
const mixx = require("../mixx.js")

var kick = new KickMono()
kick.T = 44100
kick.p = 10 + 20 * Math.random()
kick.pHi = 50 + Math.random()*70
kick.sweepCurve = Math.random() * -5

//kick.scheduleTrack(make_kick(32).loop(128))
kick.trigger()

/*var space1 = Spatialise.random(kick)
var space2 = Spatialise.random(kick)
var space3 = Spatialise.random(kick)

var mix = mixx(space1, space2)
mix = mixx(mix, space3)*/


render(kick, kick.lastScheduleTime + kick.T)
.normalise()
.save("kick?")
