const Osc = require("../Osc.js")
const RMS = require("../RMS.js")
const Sample = require("../Sample.js")
const Sampler = require("../Sampler.js")

Sampler.choose("vocal/speaking").then(function(sampler) {
  var osc = Osc.random()
  var rms = new RMS(sampler)
  osc.A = rms
  sampler.trigger()
  Sample.record(osc.y, sampler.T).save("testing rms")
})
