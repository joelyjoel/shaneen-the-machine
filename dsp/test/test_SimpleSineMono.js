const argv = require("minimist")(process.argv.slice(2))
const SimpleSineMono = require("../synths/SimpleSineMono.js")
const Noise = require("../Noise.js")
const Sample = require("../Sample.js")
const ADSR = require("../ADSR.js")
const make_melody = require("../../music/make_melody")
//console.log(make_melody)

//const T = (argv.T || 1) * 44100

var melody = make_melody.tranceyThing()
melody.transpose(24)

var synth = new SimpleSineMono()
synth.envelope = ADSR.random(44100/4)
synth.osc = new Noise()
//synth._osc.waveform = "saw"
synth.scheduleTrack(melody, 120)

var T = synth.lastScheduleTime + 44100

Sample.record(synth, T).save("testing simpleSineMono")
