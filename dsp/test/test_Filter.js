const Filter = require("../Filter.js");
const Sampler = require("../Sampler.js");
const Sample = require("../Sample.js");
const Ramp = require("../Ramp.js");
const Osc = require("../Osc.js");
const Noise = require("../Noise.js")
const argv = require("minimist")(process.argv.slice(2))

/*Sampler.choose("vocal/speaking").then(function(sampler) {
  var filt = new Filter();
  filt.f = 3//new Ramp(sampler.T, 200, 0);
  filt.in = sampler.y;
  var T = sampler.T;
  console.log(T);
  sampler.playing = true;

  Sample.record(filt.y, T).save("testing filter");
})*/

var n = argv.n || 0
var f = argv.f || 22000
var q = argv.q || 15
var h = argv.h || false

var noise = new Noise()

var filter = new Filter(f)
filter.p = new Osc(1/3, 50, 69)
filter.in = noise
filter.highPass = h
filter.q = q
//filter.f = f
console.log(filter)
/*filter.schedule(44100, function() {
  this.f /= 2
  return 44100
})*/

for(var i=1; i<n; i++) {
  var filter2 = new Filter(f)
  filter2.in = filter
  filter2.q = q
  filter2.highPass = h
/*  filter2.schedule(44100, function() {
    this.f *= 1/2
    return 44100
  })*/
  filter = filter2
}

Sample.record(filter, 44100*10).normalise().save("testing filter")
