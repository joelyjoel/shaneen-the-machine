const Transform = require("../vector/Transform.js")
const CircularMotion = require("../vector/CircularMotion.js")
const render = require("../Sample.js").record

var circ = new CircularMotion()
circ.f = 44100/100

var transform = new Transform()
transform.translate = [0, 0]
transform.rotation = Math.PI/2
transform.in = circ

var tape = render(transform, 100)
console.log(tape.vectorSamples)
