const Osc = require("../Osc.js")
const LowPass2 = require("../LowPass2.js")
const Ramp = require("../Ramp.js")
const Sample = require("../Sample.js")
const argv = require("minimist")(process.argv.slice(2))

var T = 44100 * (argv.T || 1);

var myOsc = new Osc(undefined, new Ramp(T, 1, 0, 1/2));
myOsc.p = 40;
myOsc.waveform = "saw";

var myFilter = new LowPass2(50);
var lfo = new Osc(1/20, 500, 1000);
//myFilter.f = new Osc(3, 1000, 2000).y;
myFilter.f = new Ramp(T/2, 1000, 200, -1/2);
myFilter.in = myOsc.y;

var tape = Sample.record(myFilter.y, T)
tape.save("debug lowpass 2");
