var Noise = require("../Noise.js");
var Sample = require("../Sample.js");
const Ramp = require("../Ramp.js")
const argv = require("minimist")(process.argv.slice(2))

var nChannels = argv.c || 6
var T = (argv.T || 1) * 44100
var myNoiseGen = new Noise(nChannels, 1);
myNoiseGen.p = 40

Sample.record(myNoiseGen, T, nChannels)
.save("testing noise");
