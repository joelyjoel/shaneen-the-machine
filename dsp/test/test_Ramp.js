const Osc = require("../Osc.js");
const Ramp = require("../Ramp.js");
const Sample = require("../Sample.js").record;

var T = 44100;

var myOsc = new Osc(440, new Ramp(T, 1, 0, -2))
render(myOsc, T).save("debugging ramp")
