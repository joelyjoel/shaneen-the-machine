var Sample = require("../Sample.js");
var make_melody = require("../../music/make_melody.js");
var SimpleSynth = require("../SimpleSynth.js");

var myPT = make_melody.tranceyThing(); // this works (9/4/18)
myPT = myPT.loop(32*8);
console.log("smallestPulse:: ", myPT.smallestPulse)

//myPT.save("tranceyThing test");/*

var synth = new SimpleSynth();
synth.waveform = "sin";
synth.track = myPT;

var T = synth.sequencer.T;
console.log("T:", T);

var tape = Sample.record(synth.y, T);
tape.save("Testing Simple Synth");
//*/
