const Sampler = require("../Sampler.js")
const RMS = require("../RMS.js")
const Sample = require("../Sample.js")
const Filter = require("../Filter.js")

Sampler.choose("").then(function(sampler) {
  console.log(sampler)
  sampler.t0 = sampler.T
  sampler.rate = -1

  var rms = new RMS()
  rms.in = sampler
  sampler.trigger()

  var filter = new Filter()
  filter.f = 50
  filter.in = rms

  var tape = Sample.record(rms, sampler.T)
  .normalise()

  tape.channelData.unshift(sampler.sample.channelData[0])
  tape.save("testing envelope tracker, "+sampler.sample.label)
})
