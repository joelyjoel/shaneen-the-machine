const SimplePoly = require("../synths/SimplePoly.js")
const Sample = require("../Sample.js")

var poly = new SimplePoly(44100, 22050, 0.5, 88200)

var tAttack = 44100

poly.schedule(tAttack, function() {
  this.trigger(60)
  this.trigger(64)
  console.log("triggered", this.exploreCircuit())
})
poly.schedule(tAttack + 1, function() {
  console.log("one sample later", this.exploreCircuit())
})

poly.schedule(44100*2, function() {
  console.log("releasing");
  this.release(60);
  this.release(64);
})

Sample.record(poly, 10*44100).normalise().save("testing poly")
