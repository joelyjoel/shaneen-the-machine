const Osc = require("../Osc.js");
const Sample = require("../Sample.js");
const ADSR = require("../ADSR.js");
const argv = require("minimist")(process.argv.slice(2))

var osc1 = new Osc(440);
var env1 = ADSR.random();
osc1.A = env1.y;

env1.schedule(44100/2, function() {
  this.trigger()
  console.log(this);
})
env1.schedule(44100, function() {
  this.release();
  console.log(this)
})

var T = 44100 * (argv.T || 5)
Sample.record(osc1.y, T).save("testing adsr")
