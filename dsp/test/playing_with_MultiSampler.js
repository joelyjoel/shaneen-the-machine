const MultiSampler = require("../MultiSampler.js")
const Sample = require("../Sample.js")
const textToSpeech = require("../textToSpeech.js")
const Osc = require("../Osc.js")

const argv = require("minimist")(process.argv.slice(2))
const T = argv.T || 1

new MultiSampler.of({
  0: Sample.choose("vocal"),
}).then(function(mult) {

/*  mult.schedule(0, function() {
    this.trigger(2)
    return 44100
  })*/
  console.log("hm")

  Sample.record(new Osc(), T * 44100)
})
