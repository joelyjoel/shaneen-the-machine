const BitCrusher = require("../BitCrusher.js")
const Osc = require("../Osc.js")
const Sample = require("../Sample.js")
const argv = require("minimist")(process.argv.slice(2))

const T = 44100 * (argv.T || 1)

var osc1 = new Osc(200)
var bitcrusher1 = new BitCrusher()
bitcrusher1.in = osc1
bitcrusher1.bitRate = 4

Sample.record(bitcrusher1, T)
.save("testing bitcrusher")
