const SamplerPoly = require("../synths/SamplerPoly.js")
const Sample = require("../Sample.js")
const argv = require("minimist")(process.argv.slice(2))
const make_melody = require("../../music/make_melody.js")
const make_harmony = require("../../music/make_harmony.js")
const SimpleSineMono = require("../synths/SimpleSineMono.js")
const Mixer = require("../Mixer.js")
const Pan = require("../Pan.js")
const PointTrack = require("../../music/PointTrack.js")
const midi = require("../../music/midi.js")
const Promise = require("promise")
const bach = require("../../music/bach")

const T = 44100 * (argv.T || 0)
const bpm = argv.bpm || Math.round(Math.random()*99 + 50)

//var harmony = make_harmony(32)
var bach = bach.random()
console.log(bach)
var melody = bach.mixDownNumberedTracks()//make_melody.tranceyThing(harmony).transpose(24)
console.log(melody.notes)
console.log(bach.d, melody.d)
//throw "stop early"


SamplerPoly.choose("keys").then(function(sampler) {
  sampler.tuneSample()
  sampler.sample.fadeOutSelf(4410)
  sampler.scheduleTrack(melody, bpm)
//  sampler.scheduleTrigger(44100, 60)
//  sampler.scheduleRelease(88200, 60)

  var synth = new SimpleSineMono(10, 44100/3, 0)
//  synth.scheduleRelease(44100, 60)
  synth.scheduleTrack(melody, bpm)
/*  for(var i in synth.events) {
    console.log(synth.events[i].t)
  }*/
  var synthPan = new Pan()
  synthPan.in = synth

  var mixer = new Mixer()

  mixer.addInput(synthPan)
  mixer.addInput(sampler)
  try {
    var tape = Sample.record(sampler,  sampler.lastScheduleTime)
  } catch(e) {
    console.error(e)
    return
  }

  tape.addMeta({
    originalSample: sampler.sample.originalFile,
    samplerDetune: sampler.tuning,
    bpm: bpm,
  })
  .normalise()
  .save("testing SamplerPoly: "+sampler.label)
})
