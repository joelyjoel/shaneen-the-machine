const Sample = require("../Sample.js");
const argv = require("minimist")(process.argv.slice(2));

var filename = argv._[0] || "./resources/exampleSample.wav";

Sample.readFile(filename).then(function(sample) {
  var fftData = sample.calculateFFT();
  console.log(fftData[0].frames[0]);
})
