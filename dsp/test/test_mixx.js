const mixx = require("../mixx.js")
const Osc = require("../Osc.js")
const Sample = require("../Sample.js")
const Mixer = require("../Mixer.js")
const Multiply = require("../MultiplyScalar.js")
const argv = require("minimist")(process.argv.slice(2))

const T = (argv.T || 1) * 44100
const n = argv.n || 4
const pMasterSend = 0
const pCrossConnect = 0.5

var oscs = Osc.bulkRandom(n)
var master = new Mixer()

for(var i=0; i<oscs.length; i++) {
  if(Math.random() < pMasterSend || i==0) {
    var masterSend = new Multiply(Math.random(), oscs[i])
    master.addInput(masterSend)
    console.log("sending ",i, "to master")
  }

  for(var j=0; j<oscs.length; j++) {
    if(j == i)
      continue
    if(Math.random() < pCrossConnect && j<i) {
      console.log("cross connecting", i, "to", j)
      var modulationSend = new Multiply(Math.random()*12, oscs[i])
      var kdfn = mixx(oscs[j]._p, modulationSend)
      oscs[j].p = kdfn
    }
  }
}

console.log("unpluggedSockets:", master.unpluggedSockets)

/*Sample.record(master, T)
.normalise()
.save("fm experiment")*/
