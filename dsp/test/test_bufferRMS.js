const Osc = require("../Osc.js")
const BufferRMS = require("../BufferRMS.js")
const Sample = require("../Sample.js")
const Sampler = require("../Sampler.js")

Sampler.choose("vocal/speaking").then(function(sampler) {
  var osc = Osc.random()
  var rms = new BufferRMS(sampler, 256)
  osc.A = rms
  sampler.trigger()
  Sample.record(osc, sampler.T).save("testing rms buffer")
})
