var Sample = require("../Sample.js");
var make_melody = require("../../music/make_melody.js");
var SimpleSynth = require("../SimpleSynth.js");
const Reverb = require("../Reverb.js")
const Sum = require("../Sum.js")
const Multiply = require("../Multiply.js")

var myPT = make_melody.tranceyThing(64); // this works (9/4/18)
//myPT = myPT.loop(32 * 2);

//myPT.save("tranceyThing test");/*

var synth = new SimpleSynth();
synth.waveform = "sin";
synth.track = myPT;
synth.envelope.y0 = 0.3;

var T = synth.sequencer.T;
console.log("T:", T);


//console.log(myOsc);

rev = new Reverb();
var ambSamp = new Sample().blank(4410);
for(var t=0; t<ambSamp.lengthInSamples; t++) {
  ambSamp.channelData[0][t] = Math.random() * 0.4 * (1- t/ambSamp.lengthInSamples);
}
rev.ambientSample = ambSamp;
//console.log(rev._ambientSample)
rev.in = synth.y;

//var mix = new Sum(0, rev.yWet);
var mix = new Sum(new Multiply(synth.y, 0.2).y, rev.yWet);


var tape = Sample.record(mix.y, T);
console.log("..normalising");
tape.normalise();
tape.save("188bpm bassline");
