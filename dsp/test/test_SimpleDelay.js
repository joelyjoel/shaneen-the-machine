var Sample = require("../Sample.js");
var make_melody = require("../../music/make_melody.js");
var SimpleSynth = require("../SimpleSynth.js");
var SimpleDelay = require("../SimpleDelay.js");
const Osc = require("../Osc.js");
const Ramp = require("../Ramp.js");

var myPT = make_melody.tranceyThing(); // this works (9/4/18)
myPT = myPT.loop(32*32);

//myPT.save("tranceyThing test");/*

var synth = new SimpleSynth();
synth.waveform = "sin";
synth.track = myPT;

var T = synth.sequencer.T;
console.log("T:", T);

var delay1 = new SimpleDelay();
delay1.in = synth.y;
delay1.dryWet = 0.25;
delay1.p = new Osc(1, 20, 40);
delay1.feedback = new Ramp(T/2, 0, 0.9)

var delay2 = new SimpleDelay();
delay2.in = delay1;
delay2.dryWet = 0.2;
delay2.delay = 44100/3;

var tape = Sample.record(delay2.y, T);
tape.save("Quite intense bassline");
//*/
