const Spatialise = require("../Spatialise.js");
const Osc = require("../Osc.js");
const argv = require("minimist")(process.argv.slice(2))
const Sample = require("../Sample.js")

//console.log(Spatialise)

var osc = new Osc();
osc.p = Math.random()*30 + 40;
osc.waveform = "saw";


var space = new Spatialise();
space.in = osc.y;

var r = 20;

space.schedule(0, function() {
  var t = this.clock/this.sampleRate;

  var x = r * Math.sin(t * 2 * Math.PI)
  var y = x;

  this.place = {
    x: x,
    y: y,
  }
//  console.log("ee")
  return 4410;
})

var T = (argv.T || 1) * 44100;

Sample.record(space.y, T, 6).normalise(0.5).save("testing spatialise (5.1)");
