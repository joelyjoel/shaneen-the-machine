const Chunker = require("../Chunker.js");
const Sampler = require("../Sampler.js");
const Sample = require("../Sample.js");

Sampler.choose("vocal/speaking").then(function(sampler) {

  console.log("sampler.T:", sampler.T)
  sampler.playing = true;

  var chunker = new Chunker();
  chunker.in = sampler.y;
  chunker.onChunk = function(chunk) {
    console.log("woo a chunk", chunk[0].length);
  }


  chunker.tickUntil(sampler.T);
  console.log("complete")
})
