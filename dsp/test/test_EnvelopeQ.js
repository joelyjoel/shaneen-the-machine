const EnvelopeQ = require("../EnvelopeQ")
const Ramp = require("../Ramp.js")
const render = require("../Sample.js").record

var env = new EnvelopeQ(
  new Ramp(44100*1/2, 0, 1),
  new Ramp(44100*1, 1, 0),
)
console.log(env)

env.trigger()
env.envelopes[1].schedule(44100/2, function() {
  console.log(this.x, this._y)
  return 4410
})
env.schedule(22051, function() {
  console.log(this.sockets.currentEnvelope)
})

render(env, 44100*4)
.save("envelopeQ")
