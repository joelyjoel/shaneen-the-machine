const Spatialise = require("../Spatialise.js")
const CircularMotion = require("../vector/CircularMotion.js")
const Osc = require("../Osc.js")
const Sample = require("../Sample.js")
const Mixer = require("../Mixer.js")
const argv = require("minimist")(process.argv.slice(2))
const Noise = require("../Noise.js")

var mixer = new Mixer()

var n= argv.n || 1;
var T = 44100 * (argv.T || 1)

var frequencies = []

for(var i=0; i<n; i++) {
  var osc = new Noise()
  osc.p = 20 + Math.random()*70
  frequencies.push(osc.f)
  var space = new Spatialise();
  space.in = osc;
  space.place = CircularMotion.explode([0,0])
  mixer.addInput(space)
}
console.log(frequencies)
Sample.record(mixer, T, 6).fadeOutSelf(T).normalise().addMeta({
  frequencies: frequencies,
}).save(n+" noise explosion")
