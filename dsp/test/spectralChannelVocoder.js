const Sample = require("../Sample.js")
const SpectralSample = require("../SpectralSample.js")
const Promise = require("promise")
const spectralVocode = require("../offline/spectralVocode.js");

Promise.all([
  Sample.choose("vocal/speaking"),
  Sample.choose("vocal/speaking"),
]).then(function(samples) {
  /*if(samples[0].lengthInSamples < samples[1].lengthInSamples)
    samples[0] = samples[0].loop(samples[1].lengthInSamples);
  else
    samples[1] = samples[1].loop(samples[0].lengthInSamples)

  console.log(
    "input lengths/s:",
    samples[0].lengthInSeconds,
    samples[1].lengthInSeconds
  )

  var frameSize = 256*16;
  var in1 = samples[0].toSpectralSample(frameSize)
  var in2 = samples[1].toSpectralSample(frameSize)

  var out = new SpectralSample().blank(
    in1.lengthInSamples < in2.lengthInSamples ? in1.lengthInSamples : in2.lengthInSamples,
    in1.numberOfChannels < in2.numberOfChannels ? in1.numberOfChannels : in2.numberOfChannels,
    frameSize
  );

  for(var c in out.channelData) {
    for(var frame in out.channelData[c]) {
      for(var bin in out.channelData[c][frame].phasors) {
        out.channelData[c][frame].phasors[bin].setPolar(
          in1.channelData[c][frame].phasors[bin].magnitude,
          in2.channelData[c][frame].phasors[bin].phase,
        )
      }
    }
  }*/

  var out = spectralVocode(samples[0], samples[1])
  console.log(out)

  out.toPCM().save("testing spectral channel vocoder" )


})
