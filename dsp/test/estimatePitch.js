const Sample = require("../Sample.js");
const argv = require("minimist")(process.argv.slice(2));

var filename = argv._[0] || "./resoucrs/exampleSample.wav";

Sample.readFile(filename).then(function(sample) {
  var zxl = sample.zeroCrossingLengths();
  //console.log(zxl);

  var sum = 0;
  var sum2 = 0;
  var sum2n = 0;
  var histogram = {};
  for(var i in zxl) {
    sum += zxl[i];
    histogram[zxl[i]] = (histogram[zxl[i]] || 0) + 1
  }
  for(var i in histogram) {
    histogram[i] /= zxl.length
    if(histogram[i] > 0.1) {
      sum2 += parseFloat(i) * histogram[i];
      sum2n += histogram[i];
    }
  }
  console.log(histogram)
  var mean = sum/zxl.length;
  var f = 44100/mean;
  console.log("mean grain length:", sum/zxl.length)
  console.log("frequency", f+"Hz")

  var altMean = sum2/sum2n;
  var altF = 44100/altMean;
  console.log("alternative frequency =", altF+"Hz", altMean)
})
