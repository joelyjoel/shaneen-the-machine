const SuperSawNote = require("../synths/SuperSawNote.js")
const Sample = require("../Sample.js")
const argv = require("minimist")(process.argv.slice(2))
const Mixer = require("../Mixer.js")
const Osc = require("../Osc.js")

var T = 44100 * (argv.T || 1)

var p = Math.floor(Math.random()*40) + 20
var pitches = [p, p + 3, p + 7]

var mixer = new Mixer()
for(var i=0; i<pitches.length; i++) {
//  var lfo = new Osc(Math.random(), 0.1, pitches[i])
  var note = new SuperSawNote(pitches[i], null, 0.5)
  mixer.addInput(note)
}

Sample.record(mixer, T, 6).normalise().fadeOutSelf().save("super saws")
