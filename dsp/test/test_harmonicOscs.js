const Osc = require("../Osc.js")
const Mixer = require("../Mixer.js")
const Sample = require("../Sample.js")

const argv = require("minimist")(process.argv.slice(2))
const T = (argv.T || 1) * 44100

var oscs = Osc.bulkHarmonic(4, new Osc(5*Math.random(), 1, 20 + 40*Math.random()))

var mixer = new Mixer()
for(var i in oscs) {
  mixer.addInput(oscs[i])
}

Sample.record(mixer, T).normalise().fadeInSelf().fadeOutSelf().save("additive")
