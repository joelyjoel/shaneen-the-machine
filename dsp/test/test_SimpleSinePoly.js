const SimpleSinePoly = require("../synths/SimpleSinePoly.js")
const Sample = require("../Sample.js")
const satbPad = require("../../music/satbPad.js")
const make_harmony = require("../../music/make_harmony.js")
const argv = require("minimist")(process.argv.slice(2))
const Osc = require("../Osc.js")

const T = (argv.T || 1) * 44100

var synth1 = new SimpleSinePoly()

synth1.A = new Osc(0.5, 44100, 44101)

//synth1.trigger(60)
//synth1.scheduleRelease(44100, 60)
//synth1.trigger(70)
//synth1.scheduleRelease(44100, 70)
/*synth1.schedule(44100, function() {
  this.trigger(69)
})*/

synth1.scheduleTrack(satbPad(make_harmony(64)).loop(256))


Sample.record(synth1.y, synth1.lastScheduleTime + 44100)
.normalise()
.save("testing repeaters")
//*/
