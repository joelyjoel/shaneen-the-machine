const SpaceyBoop = require("../synths/SpaceyBoopMono.js")
const Sample = require("../Sample.js")
const make_melody = require("../../music/make_melody")
const Poly = require("../synths/Poly.js")

const bach = require("../../music/bach")


var bach = bach.random()
console.log(bach)
var melody = bach.mixDownNumberedTracks()//make_melody.tranceyThing(harmony).transpose(24)

//var melody = make_melody.tranceyThing().transpose(36)
for(var i in melody.notes) {
  //melody.notes[i].d *= 0.5
}

var synth = new Poly(SpaceyBoop)
synth.scheduleTrack(melody)

Sample.record(synth, synth.lastScheduleTime+44100)
.normalise().save("testing spacey boop")

console.log(synth._mix)
