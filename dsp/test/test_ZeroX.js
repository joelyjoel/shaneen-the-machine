const ZeroX = require("../ZeroX.js")
const Sampler = require("../synths/SamplerMono.js")
const Sample = require("../Sample.js")
const Osc = require("../Osc.js")

Sampler.choose("vocal").then(function(sampler) {
  var zx = new ZeroX(sampler)
  sampler.trigger()

  var osc = new Osc()
  osc.f = zx.fEstimate

  Sample.record(osc, sampler.T).normalise().addMeta({
    madeFromSample: sampler.sample.originalFile
  }).save("zero crossing pitch tracking ("+sampler.label+")")
})
