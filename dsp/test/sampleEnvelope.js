const Sampler = require("../synths/SamplerMono.js")
const Osc = require("../Osc.js")
const render = require("../Sample.js").record
const argv = require("minimist")(process.argv.slice(2))
const mixx = require("../mixx.js")


const T = 44100 * (argv.T || 1)

Sampler.choose("").then(function(sampler) {
  sampler.tuneSample()
  sampler.p = 69
  sampler.rate = 1/440
  sampler.t0 = Math.random()*sampler.T
  sampler.envelope = 600
  sampler.trigger()
  sampler.glitchLoop()

  var osc = new Osc()
  osc.p = mixx(sampler, 60)

  render(osc, T)
  .normalise()
  .save("testing sampled envelopes")
})
