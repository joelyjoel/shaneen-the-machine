const Sample = require("../Sample.js")
const Osc = require("../Osc.js");
const fft = require("fft-js").fft;
const ifft = require("fft-js").ifft
//console.log(fft);

//var T = 8192 * 10;
Sample.choose("vocal").then(function(samp) {
  //samp = samp.loop(44100 * 20)

  var frameSize = 128;

  var reSynthedFrames = [];
  //console.log(samp);
  for(var i=0; i < samp.lengthInSamples; i+=frameSize) {
    var signal1 = samp.channelData[0].slice(i, i+frameSize)
    if(signal1.length < frameSize) {
      var sig = new Float32Array(frameSize);
      for(var t in signal1) {
        sig[t] = signal1[t];
      }
      signal1 = sig;
    }
    //console.log(signal1.length);
    var phasors = fft(signal1)
    var top = phasors.slice(0, phasors.length/2)
    var bottom = phasors.slice(phasors.length/2)
    phasors = top.concat(bottom)

    // Transform the phasors
  /*  for(var bin=0; bin<phasors.length; bin++) {
      var sf = 1;
      if(bin%4 != 0) {
        //sf = 1/8
      }
      sf = Math.random()
      phasors[bin][0] *= sf;
      phasors[bin][1] *= sf;
    }*/


    //console.log(phasors);

    var signal2 = ifft(phasors)

    var samp2 = new Sample().blank(signal2.length);
    for(var t in signal2) {
      samp2.channelData[0][t] = signal2[t][0];
      //console.log(t,":", signal2[t][0])
    }
    reSynthedFrames.push(samp2);
    reSynthedFrames.push(samp2);
  }

  var master = Sample.concat(reSynthedFrames);
  master.save("fft test2: " + samp.label)
})
