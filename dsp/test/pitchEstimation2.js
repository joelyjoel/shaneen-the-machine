const Sample = require("../Sample.js")
const plot = require("../../plot.js")
const mToF = require("../midiToFrequency.js")
const Osc = require("../Osc.js")
const argv = require("minimist")(process.argv.slice(2))

const W = 1000
const hopSize = 333
const kind = argv.k || ""

console.log("kind:", kind)
Sample.choose(kind).then(function(sample) {
  var estimatedP = sample.estimatePeriod()
  console.log("period:", estimatedP)
  console.log(sample.label, "(done)")
})
