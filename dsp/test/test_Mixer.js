const Mixer = require("../Mixer.js")
const Osc = require("../Osc.js")
const Sample = require("../Sample.js")
const argv = require("minimist")(process.argv.slice(2))


var mixer = new Mixer()

var T = (argv.T || 1) * 44100
var n = argv.n || Math.random() * 16
n=0
for(var i=0; i<n; i++) {
  var osc = Osc.random();
  osc.A = 1/n
  var input = mixer.addInput(osc.y)
  input.schedule(T/n * i, function() {
    this.remove()
  })
}

Sample.record(mixer.y, T).save("testing mixer")
