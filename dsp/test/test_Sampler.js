const Sampler = require("../Sampler.js");
const Sample = require("../Sample.js");
const SimpleDelay = require("../SimpleDelay.js");
const Ramp = require("../Ramp.js");
const Osc = require("../Osc.js");
const Multiply = require("../Multiply.js");
const argv = require("minimist")(process.argv.slice(2))
const ADSR = require("../ADSR.js")
const Spatialise = require("../Spatialise.js")
const CircularMotion = require("../vector/CircularMotion.js")

var T = 44100 * (argv.T || 10);

Sampler.gtts(
  "Testing, testing, 1, 2, 3",
  argv.l || textToSpeech.randomLang(),
).then(function(sampler) {
//  sampler.t0 = sampler.T/2
  //sampler.envelope = new ADSR(44100*2, 44100, 0, 4410)
  var loopD = 44100/(Math.random()*40)
  sampler.loop()
  //sampler.t0 = sampler.loop0
  sampler.schedule(0, function() {
    this.trigger();
    //this.rate = Math.random()*3;
    //return 3517;
  })

  sampler.schedule(44100*Math.random(), function() {
    //this.rate = Math.random()
    //this.skipBack(44100*Math.random()/4 * this.rate)
    this.glitchLoop(44100)
    console.log("glitch loop")
    return 44100*Math.random();
    //console.log("skipping back")
  })
  console.log("No. Chans", sampler.numberOfChannels)

  /*delay1 = new SimpleDelay();
  delay1.in = sampler;
  delay1.feedback = new Ramp(T/2, 0, 0.9);
  delay1.delay = new Osc(1/3, 100, 400);
  delay1.dryWet = 0.3;

  delay2 = new SimpleDelay();
  delay2.in = delay1;
  delay2.feedback = 0.3;
  delay2.delay = 3517 * 0.1;
  delay2.dryWet = 1;/**/

  //var rm = new Multiply(delay2.wet, delay1);

  var space = Spatialise.random()
  space.place = CircularMotion.recursive(4, 1/5).xy
  space.in = sampler

  try {
    Sample.record(sampler, T, 1).fadeOutSelf().save(sampler.sample.label + " glitched");
  } catch (e) {
    console.log(e)
  }

});
