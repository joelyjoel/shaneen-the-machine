const Sample = require("../Sample.js");
const SpectralSample = require("../SpectralSample.js");
const Phasor = require("../Phasor.js");
const Promise = require("promise")

Promise.all([
  Sample.choose("vocal/"),
  Sample.readFile("./resources/maidenImp1.wav")
]).then(function(samples) {
  var sample = samples[0]
  var frameSize = 8192*8;
  var impulseSample = samples[1].toSpectralSample(frameSize);

  var ss = sample.toSpectralSample(frameSize);

  console.log("finished fft", impulseSample);
  //console.log(ss);

  for(var c in ss.channelData) {
    for(var frame in ss.channelData[c]) {
      for(var bin in ss.channelData[c][frame].phasors) {
        var phasor = ss.channelData[c][frame].phasors[bin];
        var impulsePhasor = impulseSample.channelData[c][0].phasors[bin];
        var newPhasor = Phasor.polar(
          phasor.magnitude * impulsePhasor.magnitude,
          phasor.phase+impulsePhasor.phase,
        )
        ss.channelData[c][frame].phasors[bin] = newPhasor;
      }
    }
  }

  console.log("resynthesising")
  //ss.hopSize = 1; console.log("SILLYY!!");
  var resynthed = ss.toPCM().normalise();
  //resynthed.mix(sample)
  resynthed.save("testing SpectralSample: " + sample.label);
})


/*var samp = new SpectralSample().blank(44100 * 60, 1, 8192);
for(var frame=0; frame<samp.channelData[0].length; frame++) {
  for(var bin=0; bin<samp.frameSize; bin++)
    samp.channelData[0][frame].phasors[bin].setPolar(1, 2*Math.PI * Math.random())
}
var pcmSamp = samp.toPCM()
pcmSamp.normalise()
console.log("RSM:", pcmSamp.rms())
pcmSamp.save("testing fft synth")
*/
