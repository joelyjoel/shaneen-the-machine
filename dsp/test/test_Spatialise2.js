const Spatialise = require("../Spatialise.js");
const Osc = require("../Osc.js");
const argv = require("minimist")(process.argv.slice(2))
const Sample = require("../Sample.js")
const CircularMotion = require("../vector/CircularMotion.js")
const Sum = require("../Sum.js");
const Mixer = require("../Mixer.js")


var mixer1 = new Mixer();

var n = argv.n || 4

var fundamental = Math.random()*20 + 20
//console.log(Spatialise)
for(var i=0; i<n; i++) {
  var osc1 = new Osc(fundamental * Math.ceil(Math.random()*i * 2))
  osc1.waveform = "sin";
  var space1 = new Spatialise();
  space1.in = osc1.y;
  space1.place = CircularMotion.recursive(3, new Osc(0.1)).xy;
  mixer1.addInput(space1)
  //space1.decibelsPerMeter =
}


/*var osc2 = new Osc(500 * Math.random());
osc2.waveform = "sin";
var space2 = new Spatialise();
space2.in = osc2.y;
space2.place = CircularMotion.random(3).xy;*/

var T = (argv.T || 1) * 44100;

var tape = Sample.record(mixer1.y, T, 2).fadeOutSelf(T/10).fadeInSelf(T/10).normalise()

tape.save("testing spatialise (stereo)");
