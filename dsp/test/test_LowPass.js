const Osc = require("../Osc.js")
const LowPass = require("../LowPass.js")
const Ramp = require("../Ramp.js")
const Sample = require("../Sample.js")

var T = 441000 * 2;

var myOsc = new Osc();
myOsc.p = 36;
myOsc.waveform = "saw";

var myFilter = new LowPass();
var lfo = new Osc(1/20, 500, 1000);
myFilter.f = new Ramp(T/2, 1000, 100); //new Osc(1/20, 500, 1000).y;
myFilter.in = myOsc.y;

var tape = Sample.record(myFilter.y, T)
tape.save("debug lowpass");
