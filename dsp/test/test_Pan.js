const Osc = require("../Osc.js");
const Pan = require("../Pan.js");
const Sample = require("../Sample.js");
const argv = require("minimist")(process.argv.slice(2));
const SimpleDelay = require("../SimpleDelay.js");

var T = (argv.T || 1) * 44100


var osc1 = new Osc(500*Math.random());

delay1 = new SimpleDelay();
delay1.in = osc1;
delay1.p = Math.random()*40;

var pan1 = new Pan();
pan1.pan = new Osc(500 * Math.random(), new Osc(1/10));
pan1.in = delay1.y;

Sample.record(pan1, T, 2).save("debug panning")
