const Osc = require("../Osc.js")
const Shaper = require("../Shaper.js")
const PolynomialShaper = require("../PolynomialShaper.js")
const render = require("../Sample.js").record
const Kick = require("../synths/KickMono.js")
const argv = require("minimist")(process.argv.slice(2))
const Ramp = require("../Ramp.js")

const T = 44100 * (argv.T || 1)

var kick1 = new Kick()
kick1.trigger()
kick1.schedule(22050, function() {
  this.trigger()
  return 22050
})
var shaper1 = PolynomialShaper.random(kick1, 20)
//shaper1.in = kick1
console.log(shaper1.coefficients)
shaper1.gain = 0
//shaper1._gain.trigger()

render(shaper1, T)
.normalise()
.save("thibng")
