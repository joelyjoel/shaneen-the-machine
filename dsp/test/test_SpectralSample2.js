const Sample = require("../Sample.js");
const SpectralSample = require("../SpectralSample.js");
const Phasor = require("../Phasor.js");
const Promise = require("promise")

Promise.all([
  Sample.choose("vocal"),
]).then(function(samples) {
  var sample = samples[0]

  sample = sample.toSpectralSample(256);
//  console.log(sample.frameFrequency, sample.frequencies)

/*  var framePeriod = sample.frameSize;
  for(var i in sample.frequencies) {
    var binPeriod = 44100/sample.frequencies[i]
    console.log(framePeriod/binPeriod)
  }*/

  console.log(sample)

  sample.toPCM().normalise().save("testing hop size")
})


/*var samp = new SpectralSample().blank(44100 * 60, 1, 8192);
for(var frame=0; frame<samp.channelData[0].length; frame++) {
  for(var bin=0; bin<samp.frameSize; bin++)
    samp.channelData[0][frame].phasors[bin].setPolar(1, 2*Math.PI * Math.random())
}
var pcmSamp = samp.toPCM()
pcmSamp.normalise()
console.log("RSM:", pcmSamp.rms())
pcmSamp.save("testing fft synth")
*/
