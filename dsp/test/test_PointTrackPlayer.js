var make_melody = require("../../music/make_melody.js");
var PointTrackPlayer = require("../PointTrackPlayer.js");
var Sample = require("../Sample.js");
var Osc = require("../Osc.js");

var myPT = make_melody.tranceyThing(); // this works (9/4/18)
//myPT.save("tranceyThing test");

var myPlayer = new PointTrackPlayer();
//console.log("myPT", myPT);
myPlayer.track = myPT;
myPlayer.bpm = 188;
myPlayer.onNoteOn = function(note) {
  //console.log("noteon", note.sound);
  myOsc.p = note.sound;
}

var T = 44100 * 5;
var myOsc = new Osc();
myOsc.waveform = "square";


myOsc.chain(myPlayer);
console.log(myOsc);

var tape = Sample.record(myOsc.y, myPlayer.T);
tape.save("testing PointTrackPlayer");
