const GrainQ = require("../GrainQ.js")
const Sample = require("../Sample.js")
const Spatialise = require("../Spatialise.js")
const CircularMotion = require("../vector/CircularMotion.js")
const Mixer = require("../Mixer.js")
const argv = require("minimist")(process.argv.slice(2))
const Osc = require("../Osc.js")
const mToF = require("../midiToFrequency.js")

const n = argv.n || 1
const T = 44100 * (argv.T || 1)
const bpm = argv.bpm || Math.floor(Math.random()*60) + 80
const crotchet = 44100*60/bpm
const retrigInterval = crotchet * 4
const kind = argv.k || argv.kind || ""
const fundamental = argv.f || 20

Sample.choose(kind).then(function(sample) {
  var zx = sample.diceByZeroCrossings()[0]



  var mixer = new Mixer()
  for(var i=0; i<n; i++) {
    var grainer = new GrainQ(zx)
    grainer.onFinishGrain = function(i) {
      this.playGrain((i + 0.75+ (Math.random()*2-1) * 4)%this.grains.length)
    //  this.playGrain((i+1)%this.grains.length)
    }
    grainer.onPlayGrain = function() {
      var impliedFrequency = this.sampleRate/this.currentGrain.lengthInSamples
      var interval = impliedFrequency/fundamental
      this.rate = impliedFrequency/(Math.ceil(interval)*fundamental)
      console.log(
        Math.floor(this.currentGrain.lengthInSamples * (this._rate-1)),
        "/",
        this.currentGrain.lengthInSamples,
        "interval:", (interval),
        "implied f:", impliedFrequency,
      )
    }

    var space = new Spatialise()
    space.place = CircularMotion.recursive(undefined, undefined, new Osc(0.2, 2, 2))/*.explode()
    space._place.schedule(retrigInterval, function() {
      this.reExplode()
      return retrigInterval
    })*/
    space.in = grainer
    mixer.addInput(space)
  }



  Sample.record(grainer, T)
  .normalise()
  .fadeOutSelf()
  .addMeta({
    bpm: bpm,
    originalSample: sample.originalFile,
  })
  .save("testing GrainQ: " + sample.label)
})
