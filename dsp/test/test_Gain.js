const Osc = require("../Osc.js")
const Gain = require("../Gain.js")
const Sample = require("../Sample.js")
const argv = require("minimist")(process.argv.slice(2))

var T = (argv.T || 1) * 44100

var osc1 = new Osc(440);
var gain1 = new Gain();
gain1.in = osc1;

gain1.schedule(0, function() {
  this.gain = this.clock / T * -18
  return 100;
})

Sample.record(gain1, T).save("testing gain")
