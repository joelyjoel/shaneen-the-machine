const Glide = require("../Glide.js")
const Osc = require("../Osc.js")
const Sample = require("../Sample.js")

var osc1 = new Osc()
var glide1 = new Glide()

osc1.p = glide1
glide1.schedule(0, function() {
  this.x = Math.random()* 137
  this.glideTime = Math.random()*4410
  return this.glideTime
})

Sample.record(osc1, 44100*10).normalise().save("testing glide")
