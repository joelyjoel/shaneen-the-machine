const argv = require("minimist")(process.argv.slice(2))
const AcidMono = require("../synths/AcidMono.js")
const Noise = require("../Noise.js")
const Sample = require("../Sample.js")
const ADSR = require("../ADSR.js")
const Osc = require("../Osc.js")
const MultiSampler = require("../MultiSampler.js")
const make_melody = require("../../music/make_melody")
const Mixer = require("../Mixer.js")
const make_drums = require("../../music/make_drums")
//console.log(make_melody)

//const T = (argv.T || 1) * 44100

MultiSampler.choose({
  kick: "drum/kick",
  kick2: "drum/kick",
  hh: "drum/hh",
  snare: "drum/snare",
}).then(function(sampler) {
  var bpm = argv.bpm || 139
  var crotchet = 44100*60/bpm

  var melody = make_melody.tranceyThing()
  melody = melody.loop(melody.d * 16)
  melody.transpose(0)

  var synth = new AcidMono()
  synth.filterEnvelope = ADSR.random(44100)
  synth.filterEnvelope.S = 0
  synth.filterEnvelope.A = 1
  synth.fCutOff = new Osc(0.25, 36, 36)
  synth.envelopeAmmount = 100 * Math.random()
  //synth._osc.waveform = "saw"
  synth.scheduleTrack(melody, bpm)

  var T = synth.lastScheduleTime

  sampler.schedule(crotchet*16, function() {
    this.trigger("kick")
    return crotchet
  })
  sampler.schedule(crotchet*32, function() {
    this.trigger("hh")
    return crotchet/4
  })
  sampler.schedule(crotchet * 48, function() {
    this.trigger("kick2")
    return crotchet*2
  })
  sampler.schedule(crotchet * 48 + crotchet, function() {
    this.trigger("snare")
    return crotchet * 2
  })

  var mixer = new Mixer()
  mixer.addInput(sampler.y)
  mixer.addInput(synth.y)

  //console.log(mixer.exploreCircuit().length)
  var tape = Sample.record(mixer.y, T)
  .normalise()
  .addMeta({
    bpm: bpm,
  })
  .save("testing acidMono")
})
