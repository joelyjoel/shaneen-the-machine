const Poly = require("../synths/Poly.js")
const Sample = require("../Sample.js")
const make_melody = require("../../music/make_melody.js")
const argv = require("minimist")(process.argv.slice(2))

const T = 44100 * (argv.T || 1)

var poly = new Poly()

poly.scheduleTrack(
  make_melody.tranceyThing().transpose(24)
)
poly.setUpMono = function(mono) {
  mono._envelope
}

Sample.record(poly, T)
.normalise()
.save("testing poly")
