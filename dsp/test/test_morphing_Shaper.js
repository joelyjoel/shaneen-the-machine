const Osc = require("../Osc.js")
const Shaper = require("../Shaper.js")
const PolynomialShaper = require("../PolynomialShaper.js")
const render = require("../Sample.js").record
const Kick = require("../synths/KickMono.js")
const argv = require("minimist")(process.argv.slice(2))
const Ramp = require("../Ramp.js")
const Sampler = require("../synths/SamplerMono.js")
const make_kick = require("../../music/make_rhythm").kick
const mixx = require("../mixx.js")
const Pan = require("../Pan.js")

const T = 44100 * (argv.T || 1)
const d = argv.d || 1

var rhythm = make_kick(argv.T).loopN(d)

Sampler.choose(argv.kind || "").then(function(sampler) {
  sampler.scheduleTrack(rhythm)
  sampler.t0 = Math.random()*sampler.T
  sampler.envelope = 0.5
  sampler.rate = Math.random() + 1/2

  var kick = Kick.random()
  kick.scheduleTrack(rhythm)

  var mix = mixx(new Pan(kick), sampler)
  mix.master = 1/2

  var shaper = PolynomialShaper.morphing(mix, 7)


  render(shaper, sampler.lastScheduleTime)
  .normalise()
  .save("Polynomial Distortion")
})
/*var kick1 = new Kick()
kick1.trigger()
kick1.schedule(22050, function() {
  this.trigger()
  return 22050
})*/
//var shaper1 = PolynomialShaper.morphing(kick1, 20, 1)
//shaper1.in = kick1
//console.log(shaper1.coefficients)
//shaper1.gain = 0
//shaper1._gain.trigger()

/*render(shaper1, T)
.normalise()
.save("thibng")*/
