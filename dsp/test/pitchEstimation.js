const Sample = require("../Sample.js")
const plot = require("../../plot.js")
const mToF = require("../midiToFrequency.js")
const Osc = require("../Osc.js")
const argv = require("minimist")(process.argv.slice(2))

const W = 1000
const hopSize = 333
const kind = argv.k || ""

/*Sample.choose(kind).then(function(sample) {
  //sample.estimatePitchByZeroCrossings()
  console.time("tracking pitches")
  //var period = sample.findPeriodUsingAutocorrelation(Math.random()*(sample.lengthInSamples-W), 0, W)
  try {
    var trackedPeriods = sample.trackPeriodWithAutocorrelation(W, hopSize)
  }catch(e){
    console.log(e)

  }
  console.timeEnd("tracking pitches")
  //console.log(period, 44100/period + "Hz")

  var osc1 = new Osc()
  for(var t in trackedPeriods) {
    var period = trackedPeriods[t]
    if(period)
      osc1.schedule(t, function() {
        this.f = this.sampleRate/trackedPeriods[this.clock]
        this.A = sample.rms(this.clock, this.clock + W)
      })
  }
  console.log(sample.label)

  var oscTape = Sample.record(osc1, sample.lengthInSamples)
  Sample.concat([sample, oscTape], 44100).save("testing pitch tracking: "+sample.label)
})*/
