const Noise = require("../Noise.js")
const Butterworth = require("../Butterworth.js")
const Sample = require("../Sample.js")
const argv = require("minimist")(process.argv.slice(2))
const Osc = require("../Osc.js")

const T = (argv.T || 1) * 44100


var noise = new Noise()

var filter = new Butterworth()
filter.in = noise
filter.type = "BR"
filter.f = 5000//new Osc(1, 4000, 8000)

Sample.record(filter, T)
.normalise()
.save("testing Butterworth filters")
