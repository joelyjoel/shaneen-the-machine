const Sampler = require("../Sampler.js")
const Sample = require("../Sample.js")
const Osc = require("../Osc.js")
const Spatialise = require("../Spatialise.js")

Sampler.choose("").then(function(sampler) {
  var osc = new Osc(440)
  var space = new Spatialise()
  space.place = sampler.y;
  space.in = sampler.y

  sampler.loop()
  sampler.trigger()

  Sample.record(space.y, sampler.T , 6).addMeta({
    originalFile: sampler.sample.originalFile
  }).save("experiment: "+sampler.sample.label)
})
