var Osc = require("../Osc.js");
var NoiseGen = require("../Noise.js")
var Ramp = require("../Ramp.js");
var Sample = require("../Sample.js");
var argv = require("minimist")(process.argv.slice(2));

var T = (argv.T || 1) * 44100;

var myOsc = new Osc();
myOsc.label = "carrier"
var mod = new Osc(150, 12, 60).y
mod.unit.label = "the MOD";
//mod.unit.tick();
//console.log(mod);
myOsc.p = mod;
myOsc.A = Ramp.randomDecay(T).y;

console.log(myOsc);
console.log(mod.unit);
//console.log(myOsc, mod.unit);
//console.log(Osc.prototype.socketFs)

Sample.record(myOsc.y, T)
.save("testing osc");
