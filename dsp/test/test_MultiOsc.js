const MultiOsc = require("../MultiOsc.js")
const render = require("../Sample.js").record
const Osc = require("../Osc.js")
const AdditiveMono = require("../synths/AdditiveMono.js")

var synth = new AdditiveMono([1,2,3,4], 4410, 44100/2, 0, 44100)
var p = 30
synth.schedule(0, function() {
  p+=2
  this.trigger(p)
  return 44100/5
})

render(synth, 44100 * 60)
//.normalise()
.save()
