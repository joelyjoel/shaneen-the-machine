const Osc = require("../Osc.js");
const Sample = require("../Sample.js");
const argv = require("minimist")(process.argv.slice(2))
const LowPass = require("../LowPass.js");
const Ramp = require("../LowPass.js");

var T = (argv.T || 1) * 44100;

var myOsc = new Osc();
myOsc.p = 60;
myOsc.schedule(4410, function() {
  this.p = Math.random()*50 + 20;
  return 4410;
})
myOsc.schedule(500, function() {
  this.randomiseWaveform();
  return 8820;
})

var filter = new LowPass(500);
filter.in = myOsc.y;
filter.schedule(T/100, function() {
  this.f -= 10
  return T/100
})


Sample.record(filter.y, T).save("debugging scheduling");
