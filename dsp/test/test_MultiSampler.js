const MultiSampler = require("../MultiSampler.js")
const Sample = require("../Sample.js")
const argv = require("minimist")(process.argv.slice(2))

var bpm = Math.floor(80 + Math.random()*80)
var crotchet = 44100*60/bpm;//22050
var bar = 4 * crotchet

var T = (argv.T || 1) * bar

MultiSampler.choose({
  0: "drum/vocoded/kick",
  1: "drum/percussion/shaker",
  2: "drum",//"drum/clap",
  vocal: "",//"vocal",
  hh: "drum/hh",
  5: "drum/kick",
  bigKick: "drum/kick",
  bigSnare: "drum",//"drum/snare",
  bigKick2: "drum/kick",
  snare2: "drum/vocoded",
  anotherKick: "drum/kick",
  cymb: "drum/cymbal",
}).then(function(mySampler) {

  mySampler.samplers["vocal"].rate = 1/2 + Math.random();
  mySampler.schedule(crotchet*Math.random(), function() {
    this.trigger(1);
    return crotchet
  })
  mySampler.schedule(0, function() {
    this.trigger(0);
    return crotchet
  })
  var subd = Math.ceil(Math.random()*5)
  mySampler.schedule(crotchet*Math.random(), function(){
    this.trigger(1);
    return crotchet/subd
  })
  mySampler.schedule(crotchet*Math.random(), function(){
    this.trigger(2);
    return crotchet
  })
  mySampler.schedule(crotchet*2, function() {
    this.trigger("vocal");
    return crotchet*4;
  })
  mySampler.schedule(crotchet/5*Math.random(), function() {
    this.trigger(4);
    return crotchet*4/10;
  })
  mySampler.schedule(32*crotchet + Math.random()*crotchet/4, function() {
    this.trigger("hh");
    return crotchet/4
  })
  mySampler.schedule(0, function() {
    this.trigger(5);
    return crotchet*8
  })
  mySampler.schedule(bar * 16, function() {
    this.trigger("bigKick");
    return crotchet;
  })
  mySampler.schedule(bar * 16 + Math.floor(Math.random()*8)*crotchet + Math.random()*crotchet, function() {
    this.trigger("bigKick2")
    return 8 * crotchet
  })
  mySampler.schedule(bar * 16 + crotchet, function() {
    this.trigger("bigSnare");
    return 2 * crotchet;
  })

  for(var i=0; i<16; i++) {
    if(Math.random()<0.5) {
      mySampler.schedule(bar * 32 + crotchet/4 * i, function() {
        this.trigger("snare2")
        return bar;
      })
    }
  }
  mySampler.schedule(bar * 40, function() {
    this.trigger("anotherKick")
    return crotchet;
  })
  mySampler.schedule(bar * 48 + crotchet/2, function() {
    this.trigger("cymb")
    if(this.clock+crotchet < bar * 56)
      return crotchet;
  })

  try {
    var tape = Sample.record(mySampler, T, 2)
  } catch (e) {
    console.log("error!")
    console.error(e)
    for(var i in mySampler.samplers) {
      console.log(mySampler.samplers[i].clock)
    }
  }
  tape.meta.bpm = bpm;
  tape.fadeOutSelf(bar).normalise().save("syncopated rhythms ("+bpm+"bpm)");
})
