const SimpleNote = require("../synths/SimpleNote.js")
const Sample = require("../Sample.js")
const ADSR = require("../ADSR.js")

var note1 = new SimpleNote()
note1.env = ADSR.random()
note1.schedule(44100, function() {
  this.trigger();
})
note1.schedule(44100*5, function() {
  this.release();
})

note1.onFinish = function() {
  console.log("finished..", this.clock)
}

Sample.record(note1, 44100*10).save("testing SimpleNote")
