const DspUnit = require("./DspUnit.js")
const mToF = require("./midiToFrequency.js")
const fToM = mToF.fToM

const PHI = Math.PI * 2;

// TODO : add triangle waveform
// TODO : add array based waveforms

function Osc(f, A, o, waveform) {
  DspUnit.call(this);

  this.phase = 0;
  this.f = f || 1; // frequency (defualt C0)
  this.A = A==undefined ? 1 : A; // amplitude
  this.o = o || 0; // equilibrium level
  this.p = -36.37631656229591;
  this.waveform = waveform || "sin";
  this.y = 0
}
Osc.prototype = Object.create(DspUnit.prototype);
Osc.prototype.constructor = Osc;
module.exports = Osc;

Osc.random = function(waveform) {
  var osc = new Osc();
  osc.p = 15 + Math.random()*85// 15-100
  osc.waveform = waveform || "sin"
  return osc
}
Osc.randomAudible = function(waveform) {
  var osc = new Osc()
  osc.p = 15+ Math.random()*122 // 15-137
  return osc
}
Osc.lfo = function(f, A, o, waveform, tickRate) {
  var osc = new Osc(f, A, o, waveform)
  osc.tickInterval = tickRate || 44
  //osc.clock = -osc.tickRate
  return osc
}
Osc.randomLFO = function(fMax, AMax, o) {
  var pMax = fToM(fMax || 20)
  var pMin = fToM(0.1)

  var osc = new Osc()
  osc.p = Math.random()*(pMax-pMin) + pMin
  osc.A = Math.random() * (AMax || 1)
  osc.o = o || 0
  osc.phase = Math.random()
  return osc
}

Osc.bulkRandom = function(n) {
  var oscs = [];
  n = n || 1
  for(var i=0; i<n; i++)
    oscs.push( Osc.random() )
  return oscs
}
Osc.bulkHarmonic = function(n, p, maxNumerator, maxDenominator) {
  n = n || 1
  p = p || 69
  maxNumerator = maxNumerator || 16
  maxDenominator = maxDenominator || 16
  var oscs = []
  for(var i=0; i<n; i++) {
    var osc = new Osc()
    osc.f = Math.ceil(Math.random() * maxNumerator) / Math.ceil(Math.random() * maxDenominator)
    osc.p = p
    oscs.push(osc)
  }
  return oscs
}

Osc.prototype.__defineSocket__("f", function(f) {
  this.phasePerSample = f/this.sampleRate;
  return f;
})
Osc.prototype.__defineSocket__("A")
Osc.prototype.__defineSocket__("o")
Osc.prototype.__defineSocket__("p", function(p) {
  this.phaseRateSF = Math.pow(2, p/12);
  return p;
});
Osc.prototype.__definePlug__("y")

Osc.prototype._tick = function(dT) {
  var phaseRate = this.phasePerSample * this.phaseRateSF * 8.1757989148
  this.phase = (this.phase + dT * phaseRate)%1;
  this.y = this.o + this.A * this.F(this.phase);
}

Osc.prototype.__defineGetter__("waveform", function() {
  return this._waveform;
});
Osc.prototype.__defineSetter__("waveform", function(waveform) {
  this.F = this.waveFunctions[waveform];
  this._waveform = waveform;
});
Osc.prototype.waveFunctions = {
  sin: function(phase) {
    return Math.sin(PHI * phase);
  },
  saw: function(phase) {
    return phase * 2 - 1;
  },
  square: function(phase) {
    return phase > 0.5 ? 1 : -1;
  },
  sine8bit: function(phase) {
    return Math.round(Math.sin(PHI*phase) * 128)/128
  },
  sine4bit: function(phase) {
    return Math.round(Math.sin(PHI*phase) * 8)/8
  },
  /* To Do: Triangle, ¿noise? */
}
Osc.prototype.randomiseWaveform = function() {
  var waveforms = Object.keys(this.waveFunctions);
  this.waveform = waveforms[Math.floor(Math.random()*waveforms.length)];
}
/*Osc.prototype.__defineGetter__("y", function() {
  return this.o + this.A * Math.sin(Math.PI * 2 * this.phase);
});*/

Osc.prototype.__defineGetter__("fNow", function() {
  return this.phasePerSample * this.phaseRateSF*this.sampleRate
})
