const DspUnit = require("./DspUnit.js");

function RMS(plug, windowSize) {
  DspUnit.call(this);
  this.highPass = false;
  this.yNMinus1 = 0;
  if(this.windowSize)
    this.windowSize = windowSize
  else
    this.f = 1000
  this.in = plug || [0]
}
RMS.prototype = Object.create(DspUnit.prototype);
RMS.prototype.constructor = RMS;
module.exports = RMS;

RMS.prototype.__defineSocket__("in", undefined, "array");

RMS.prototype.__defineSetter__("windowSize", function(l) {
  this.f = this.sampleRate/l;
})
RMS.prototype.__defineSetter__("f", function(f) {
  var costh = 2 - Math.cos(2 * Math.PI * f / this.sampleRate);
  var b = Math.sqrt(costh * costh - 1) - costh;
  this.a = 1 + b;
  this.b = b;

  return f;
});
RMS.prototype.__definePlug__("y", "array");

RMS.prototype._tick = function() {
  var x = this.in;
  var xSquared;
  var lastYSquared = this.lastYSquared || [];
  var y = []
  var ySquared = []
  for(var c in x) {
    xSquared = x[c] * x[c]
    if(lastYSquared[c] == undefined)
      lastYSquared[c] = xSquared
    ySquared[c] = this.a * xSquared - this.b * (lastYSquared[c] || 0)
    y[c] = Math.sqrt(ySquared[c])
  }
  this.lastYSquared = ySquared
  this.y = y
}
