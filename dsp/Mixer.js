const DspUnit = require("./DspUnit.js")
const Gain = require("./Gain.js")

function Mixer() { // a-rate unit for mixing any number of plug outputs
  DspUnit.call(this)

  this.inputs = [] // an object containing plugs
  this.master = 1
}
Mixer.prototype = Object.create(DspUnit.prototype)
Mixer.prototype.constructor = Mixer
module.exports = Mixer

Mixer.prototype.__definePlug__("y", "array")
Mixer.prototype.__defineSocket__("master")

Mixer.prototype._tick = function() {
  var y = []
  var inputs = this.inputs.slice() // in case one gets removed
  for(var i=0; i<inputs.length; i++) {
    var input = inputs[i]
    input.tickUntil(this.clock)
    var inpY = input.out.y
    y = this.mixFunction(y, inpY)
  //  for(var c in inpY)
  //    y[c] = (y[c] || 0) + (inpY[c] || 0) * this.master
  }
  this.y = y
}
Mixer.prototype.mixFunction = function(one, two) {
  var three = []
  for(var c=0; c<one.length || c< two.length; c++)
    three[c] = (one[c] || 0) + (two[c] || 0) * this.master
  return three
}

Mixer.prototype.addInput = function(plug) {
  var chan = new MixerChannel(plug, this)
  chan.syncClocks(this.clock)
  this.inputs.push(chan)
  chan.syncClocks(this.clock)
  //console.log("newly synced channel clock:", chan.clock, "mixer clock:", this.clock)
  //console.log("poly clock:",this.poly.clock)
//  throw "!"
  return chan
}
Mixer.prototype.removeInput = function(input) {
  console.log("removing input", this.inputs.length)
  if(input.constructor == Number) {
    this.inputs.splice(input, 1)
    console.log("\tby number")
  }
  if(input.isMixerChannel) {
    var i = this.inputs.indexOf(input)
    this.inputs.splice(i, 1)
    console.log("\tby channel", i, this.inputs.length)
  }
}
Mixer.prototype.clear = function() {
  this.inputs = []
}

Mixer.prototype.__defineGetter__("extras", function() {
  return this.inputs
})

function MixerChannel(plug, mixer) {
  DspUnit.call(this)
  this.mixer = mixer
  this.syncClocks(this.mixer.clock)
  this.in = plug
  if(mixer)
    this.clock = mixer.clock
}
MixerChannel.prototype = Object.create(DspUnit.prototype)
MixerChannel.prototype.constructor = MixerChannel
MixerChannel.prototype.isMixerChannel = true

MixerChannel.prototype.__defineSocket__("in", function(x) {
  this.y = x
}, "array")
MixerChannel.prototype.__definePlug__("y", "array")

MixerChannel.prototype.remove = function() {
  this.mixer.removeInput(this)
}

Mixer.prototype.__defineGetter__("report", function() {
  var inpts = []
  for(var i in this.inputs) {
    inpts[i] = "\t"+i+": "+this.inputs[i]._in.label
  }
  return "Mixer report:\n" + inpts.join("\n")
})
