function Socket(model) {
  if(model) {
    this.inputFunction = model.inputFunction;
    this.format = model.format;
    this.name = model.name;
  }
  this.plug = null;
  this.conversionFunction = undefined;
  this.val = this.format=="array" ? [0] : 0;
  this.tickClockAtLastRead = -1;
}
module.exports = Socket;

Socket.prototype.__defineGetter__("unit", function() {
  if(this.plug) {
    return this.plug.unit
  }
})
