const DspUnit = require("./DspUnit.js")

function BufferRMS(plug, windowSize) {
  DspUnit.call(this)

  this.windowSize = windowSize || 2205
  this.in = plug || [0]

  this.squareSum = 0;
}
BufferRMS.prototype = Object.create(DspUnit.prototype)
BufferRMS.prototype.constructor = BufferRMS
module.exports = BufferRMS

BufferRMS.prototype.__defineSocket__("in", undefined, "array")
BufferRMS.prototype.__definePlug__("y")

BufferRMS.prototype._tick = function() {
  this.squareSum -= this.buffer[this.clock%this.l]
  var x = this.in

  var sampleSum = 0
  for(var c=0; c<x.length; c++)
    sampleSum += x[c] * x[c]
  this.squareSum += sampleSum
  this.buffer[this.clock%this.l] = sampleSum
  this.y = Math.sqrt(this.squareSum / this.l)
}

BufferRMS.prototype.__defineSetter__("windowSize", function(l) {
  this.buffer = new Float32Array(l)
  this.buffer.fill(0)
  this.l = this.buffer.length
})
