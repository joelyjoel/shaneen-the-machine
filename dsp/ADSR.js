const DspUnit = require("./DspUnit.js")

function ADSR(A, D, S, R) {
  DspUnit.call(this);

  this.A = A || 1;
  this.D = D || 1;
  this.S = (S!=undefined) ? S : 1;
  this.R = R || 1;
  this.master = 1;

  this.state = 0; // 0:off, 1:attacking, 2:decaying, 3:sustaining, 4:releasing
  this.level = 0;
}
ADSR.prototype = Object.create(DspUnit.prototype);
ADSR.prototype.constructor = ADSR;
module.exports = ADSR;

ADSR.prototype.isEnvelope = true
ADSR.prototype.isADSR = true

ADSR.random = function(dMax) {
  dMax = dMax || 2*44100
  return new ADSR(
    Math.random() * dMax/3, // attack
    Math.random() * dMax/3,
    Math.random(),
    Math.random() * dMax/3,
  );
}
ADSR.randomDecay = function(maxDecay) {
  return new ADSR(
    Math.random()*4410,
    Math.random()*maxDecay,
    0,
    Math.random()*maxDecay
  )
}

ADSR.prototype.__defineSocket__("A", function(A) {
  A = A || 1
  this.attackGradient = 1/(A);
  return A
})
ADSR.prototype.__defineSocket__("D", function(D) {
  D = D || 1
  this.decayGradient = ((this.S||1)-1)/(D);
  return D
})
ADSR.prototype.__defineSocket__("S", function(S) {
  this.decayGradient = ((S)-1)/(this.D)
  this.releaseGradient = -(S||1)/this.R
  return S;
})
ADSR.prototype.__defineSocket__("R", function(R) {
  R = R || 1
  this.releaseGradient = -(this.S || 1)/R
  return R;
})
ADSR.prototype.__defineSocket__("master")
ADSR.prototype.__definePlug__("y")

ADSR.prototype._tick = function() {
  switch(this.state) {
    case 0: // off
      break;
    case 1: // attacking
      this.level += this.attackGradient;
      if(this.level >= 1) {
        this.state++;
        this.level = 1;
        if(this.onDecay) this.onDecay()
      }
      break;
    case 2: // decaying
      this.level += this.decayGradient;
      if((this.level <= this.S && this.S <= 1)
          || (this.level >= this.S && this.S >= 1)) {
        this.state++;
        this.level = this.S;
        if(this.onSustain) this.onSustain()
      }
      break;
    case 3: // sustaining
      break;
    case 4: // releasing
      this.level += this.releaseGradient;
      if(this.level <= 0) {
        this.level = 0;
        this.state = 0;
        this.finish()
      }
      break;
  }

  this.y = this.level * this.master;
}

ADSR.prototype.trigger = function() {
  this.state = 1; // attacking
}
ADSR.prototype.release = function() {
  this.state = 4; // releasing
}
