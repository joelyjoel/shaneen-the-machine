const DspUnit = require("./DspUnit.js")
const dB = require("./decibel.js");

function Pan(plug) {
  // unit for stereo panning a mono signal
  DspUnit.call(this);
  this.dbCompensate = 1.5
  this.pan = 0;
  this.in = plug || 0
}
Pan.prototype = Object.create(DspUnit.prototype);
Pan.prototype.constructor = Pan;
module.exports = Pan;

Pan.prototype.__defineSocket__("in")
Pan.prototype.__defineSocket__("pan", function(pan) {
  var compensation = dB((1-Math.abs(pan))*this.dbCompensate);
  this.sfL = (1-pan)/2 * compensation;
  this.sfR = (1+pan)/2 * compensation;
  return pan;
});
Pan.prototype.__definePlug__("y", "array");
Pan.prototype.__definePlug__("L");
Pan.prototype.__definePlug__("R");

Pan.prototype._tick = function() {
  var L = this.sfL * this.in;
  var R = this.sfR * this.in;
  this.L = L;
  this.R = R;
  this.y = [L, R];
}
