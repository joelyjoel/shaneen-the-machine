const Poly = require("./synths/Poly.js")
const Sample = require("./Sample.js")
const ADSR = require("./ADSR.js")

function demoTrack(track) {
  if(track.isPointTrack) {
    var synth = new Poly()
    synth.setUpMono = function(mono) {
      //mono.envelope = new ADSR(441, 4410, 0, 0)
    //  mono._osc.waveform = "sine4bit"
    //  mono.envelope = new ADSR(4410, 44100/2, 1, 4410)
    }
    synth.scheduleTrack(track)

    return Sample.record(synth, synth.lastScheduleTime + 44100).normalise()
  }
}
module.exports = demoTrack
