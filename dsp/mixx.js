const Mixer = require("./Mixer.js")

function mixx(a, b) {
  if(a.isDspUnit)
    a = a[a.defaultPlug]
  if(b.isDspUnit)
    b = b[b.defaultPlug]


  if(a.unit && a.unit.isMixer && b.uni && b.unit.isMixer) {
    console.log("what do we do if they are both")
  } else if(a.unit && a.unit.isMixer) {
    a.unit.addInput(b)
    return a
  } else if(b.unit && b.unit.isMixer) {
    b.unit.addInput(b)
    return b
  } else {
    var mixer = new Mixer()
    mixer.addInput(a)
    mixer.addInput(b)
    return mixer
  }
}
module.exports = mixx
