const DspUnit = require("./DspUnit.js")
const TinyOsc = require("./TinyOsc.js")

function AdditiveOsc() {
  DspUnit.call(this)
}
AdditiveOsc.prototype = Object.create(DspUnit.prototype)
AdditiveOsc.prototype.constructor = AdditiveOsc
module.exports = AdditiveOsc
