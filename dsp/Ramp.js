var DspUnit = require("./DspUnit.js");
const dB = require("./decibel")

function Ramp(T, y0, y1, curve) {
  DspUnit.call(this);
  this.T = T || 44100;
  this.y0 = y0 != undefined ? y0 : 1;
  this.y1 = y1 != undefined ? y1 : 0;
  this.x = 0;
  this.curve = curve || 0; // <0 decreases faster
  this.edge = "flat"; // "zero" || "flat" || "extend"
  this.logarithmic = false

  this.waitBefore = 0
  this.waitAfter = 0
}
Ramp.prototype = Object.create(DspUnit.prototype);
Ramp.prototype.constructor = Ramp;
module.exports = Ramp;

Ramp.random = function(T) {
  var ramp = new Ramp();
  ramp.y0 = Math.random();
  ramp.y1 = Math.random();
  ramp.T = T || Math.random()*44100*10;
  ramp.curve = (Math.random()*2-1)/2;
  return ramp;
}
Ramp.randomDecay = function(T) {
  var ramp = Ramp.random(T);
  ramp.y0 = 1;
  ramp.y1 = 0;
  return ramp;
}
Ramp.randomAttack = function(T) {
  var ramp = Ramp.random(T);
  ramp.y0 = 0;
  ramp.y1 = 1;
  return ramp;
}

Ramp.prototype.__definePlug__("y")
Ramp.prototype.__defineSocket__("y0")
Ramp.prototype.__defineSocket__("y1")
Ramp.prototype.__defineSocket__("curve")
Ramp.prototype.__defineSocket__("T")
Ramp.prototype.__defineSocket__("waitBefore")
Ramp.prototype.__defineSocket__("waitAfter")

Ramp.prototype._tick = function() {
  this.x++;
  if(this.x >= this.T && !this.finished)
    this.finish()

  var phase = (this.x-this.waitBefore) / (this.T-this.waitAfter-this.waitBefore);
  if(phase >= 0 && phase <= 1) {
    //console.log("phase:", phase)
    //throw phase
    this.y = this.F(phase);
  } else {
    switch(this.edge) {
      case "zero":
        this.y = 0;
        break;
      case "flat":
        if(phase < 0) this.y = this.y0;
        else this.y = this.y1;
        break;
      case "extend":
        this.y = this.F(phase);
        break;
    }
  }

}
Ramp.prototype.F = function(phase) {
  phase = Math.pow(phase, Math.exp(this.curve))
  if(this.logarithmic)
    phase = dB.logScale(phase)
  return this.y0 + phase * (this.y1-this.y0)
}

Ramp.prototype.trigger = function() {
  this.x = 0;
}
Ramp.prototype.setToEnd = function() {
//  this.x = this.T;
}
