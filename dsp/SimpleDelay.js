const DspUnit = require("./DspUnit.js")
const mixDryWet = require("./mixDryWet.js");
const mToF = require("./midiToFrequency.js");

// TODO: add p socket, for controlling pitch of feedback loop

function SimpleDelay() {
  // a simple delay unit
  DspUnit.call(this);

  this.buffer = new Float32Array(44100);
  this.delay = 4410;
  this.dryWet = 1;
  this.feedback = 0;
}
SimpleDelay.prototype = Object.create(DspUnit.prototype);
SimpleDelay.prototype.constructor = SimpleDelay;
module.exports = SimpleDelay;

SimpleDelay.random = function() {
  var delay = new SimpleDelay();
  delay.delay = Math.random()*44100
  this.dryWet = Math.random()
  this.feedback = Math.random()
  return delay;
}

SimpleDelay.prototype.__definePlug__("y");
SimpleDelay.prototype.__definePlug__("wet");
SimpleDelay.prototype.__defineSocket__("in");
SimpleDelay.prototype.__defineSocket__("delay", function(delay) {
  if(delay > this.buffer.length) {
    var newBufferLength = this.bufferLength;
    this.resizeBuffer(delay * 1.5);
  }
  return delay;
});
SimpleDelay.prototype.__defineSocket__("p", function(p) {
  this.delay = 44100/mToF(p);
})
SimpleDelay.prototype.__defineSocket__("feedback");

SimpleDelay.prototype.resizeBuffer = function(newSize) {
  var newBuffer = new Float32Array(Math.ceil(newSize));
  var fromT = newSize < this.buffer.length ? newSize : this.buffer.length;
  fromT = this.clock - fromT;
  if(fromT < 0)
    fromT = 0;
  for(var t=fromT; t <= this.clock; t++) {
    newBuffer[t%newBuffer.length] = this.buffer[t%this.buffer.length];
  }
  this.buffer = newBuffer;
}

SimpleDelay.prototype._tick = function() {
  //console.log("delay in:", this.in)
  var tRead = (this.clock-this.delay)%this.buffer.length;
  if(tRead < 0)
    tRead += this.buffer.length;
  this.wet = this.buffer[Math.floor(tRead)];
  this.y = mixDryWet(this.dryWet, this.in, this._wet);
  console.log("if there is an error, it is possibly coming from here (_wet)")
  this.buffer[this.clock%this.buffer.length] = this.in + this.feedback * this._wet;
}
