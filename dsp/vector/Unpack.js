const DspUnit = require("../DspUnit.js")

function Unpack() {
  DspUnit.call(this)
}
Unpack.prototype = Object.create(DspUnit.prototype)
Unpack.prototype.constructor = Unpack
module.exports = Unpack

Unpack.prototype.__defineSocket__("in", function(xy) {
  this.x = xy.x
  this.y = xy.y
  this.z = xy.z
  return xy
} "array")
Unpack.prototype.__definePlug__("x")
Unpack.prototype.__definePlug__("y")
Unpack.prototype.__definePlug__("z")
