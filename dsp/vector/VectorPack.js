const DspUnit = require("../DspUnit.js");

function VectorPack() {
  DspUnit.call(this);
}
VectorPack.prototype = Object.create(VectorPack);
VectorPack.prototype.constructor = VectorPack;
module.exports = VectorPack;

VectorPack.prototype.__defineSocket__("x")
VectorPack.prototype.__defineSocket__("y")
VectorPack.prototype.__definePlug__("xy", "array")

VectorPack.prototype._tick = function() {
  this.xy = [this.x, this.y]
}
