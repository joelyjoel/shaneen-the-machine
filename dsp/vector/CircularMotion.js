const DspUnit = require("../DspUnit.js")
const Multiply = require("../Multiply.js")
const Ramp = require("../Ramp.js")

function CircularMotion(f, r) {
  DspUnit.call(this);

  this.phase = 0;
  this.f = f || 1;
  this.r = r || 1
  this.O = [0,0]
}
CircularMotion.prototype = Object.create(DspUnit.prototype);
CircularMotion.prototype.constructor = CircularMotion;
module.exports = CircularMotion;

CircularMotion.prototype.defaultPlug = "xy"
CircularMotion.prototype.tickInterval = 1

CircularMotion.random = function(fMax, rMax) {
  var cm = new CircularMotion()
  if(fMax && (fMax.isDspUnit || fMax.isPlug))
    cm.f = new Multiply(fMax, Math.random())
  else
    cm.f = Math.random()*(fMax || 5);
  if(rMax && (rMax.isDspUnit || rMax.isPlug))
    cm.r = new Multiply(rMax, Math.random())
  else
    cm.r = Math.random()*5;
  return cm;
}
CircularMotion.recursive = function(n, fMax, rMax, O) {
  n = n || Math.floor(Math.random()*10)
  var bottom = CircularMotion.random(fMax, rMax);
  var top = bottom;
  for(var i=1; i<n; i++) {
    var cm = CircularMotion.random(fMax, rMax);
    top.O = cm.xy;
    top = cm;
  }
  return bottom;
}
CircularMotion.explode = function(O, n, fMax, rMax) {
  n = n || Math.ceil(Math.random()*10)
  fMax = fMax || Math.random()*10
  rMax = new Ramp(44100*5*Math.random(), 0, rMax || 20, -0.25)
  O = O || [0,0]
  var motion = CircularMotion.recursive(n, fMax, rMax, 0)
  motion.ramp = rMax
  motion.reExplode = function() {
    rMax.trigger()
  }
  return motion
}

CircularMotion.prototype.__defineSocket__("f", function(f) {
  this.phasePerSample = f/this.sampleRate * 2 * Math.PI;
})
CircularMotion.prototype.__defineSocket__("r")
CircularMotion.prototype.__defineSocket__("O", undefined,"array")
CircularMotion.prototype.__definePlug__("xy", "array")

CircularMotion.prototype._tick = function(dT) {
  this.phase += this.phasePerSample * dT
  this.xy = [
    this.r * Math.cos(this.phase) + this.O[0],
    this.r * Math.sin(this.phase) + this.O[1],
  ]
}
