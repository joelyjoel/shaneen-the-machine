const DspUnit = require("../DspUnit.js")

function Transform() {
  DspUnit.call(this)

  this.translate = [0, 0]
  this.rotation = 0
}
Transform.prototype = Object.create(DspUnit.prototype)
Transform.prototype.constructor = Transform
module.exports = Transform

Transform.prototype.__definePlug__("y", "array")
Transform.prototype.__defineSocket__("in", function(x) {
  var translate = this.translate
  var y = []
  for(var c=0; c<x.length || x<translate.length; c++)
    y[c] = (translate[c] || 0) + (x[c] || 0)

  var transformed = []
  for(var row in y) {
    transformed[row] = 0
    for(var col in this.matrix[row])
      transformed[row] += this.matrix[row][col] * y[col]
  }

  this.y = transformed
}, "array")
Transform.prototype.__defineSocket__("rotation", function(angle) {
  this.matrix = [
    [Math.cos(angle), Math.sin(angle)],
    [-Math.sin(angle), Math.cos(angle)]
  ]
  return angle
})
Transform.prototype.__defineSocket__("translate", null, "array")
