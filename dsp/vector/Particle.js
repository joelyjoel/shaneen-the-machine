const DspUnit = require("../DspUnit.js")

function Particle() {
  DspUnit.call(this);

  this.position = [0,0]
  this.velocity = [0,0]
  //this.friction = 1
}
Particle.prototype = Object.create(DspUnit.prototype)
Particle.prototype.constructor = Particle
module.exports = Particle

Particle.bulk_exploding = function(O, n, maxVelocity) {
  O = O || [0,0] // should not be plug
  n = n || 0
  maxVelocity = maxVelocity || 2
  var particles = []
  for(var i=0; i<n; i++) {
    var particle = new Particle()
    particle.position = O.slice()
    particle.velocity = [
      (2*Math.random()-1) * maxVelocity,
      (2*Math.random()-1) * maxVelocity,
    ]
  }
}

Particle.prototype.__defineSocket__("friction")
Particle.prototype.__definePlug__("out", "array")

Particle.prototype._tick = function(dT) {
  var secondsElapsed = dT / this.sampleRate
  if(this.velocity) {
    this.position.x += this.velocity.x * secondsElapsed
    this.position.y += this.velocity.y * secondsElapsed
    /*if(this.friction) {
      this.velocity.x *= this.friction;
      this.velocity.y *= this.friction;
    }*/
  }
  this.out = this.position
}
