//var saveTextToSpeech = require("./sound/saveTextToSpeech.js");
var textToSpeech = require("./sound/textToSpeech.js");
var argv = require('minimist')(process.argv.slice(2));
var fs = require("fs");

var lang = argv.l || "en";
var txt = argv._[0];
/*var filename = "./resources/samples/gtts/"+txt+".wav";

if(!fs.existsSync(filename)) {
    saveTextToSpeech(filename, txt)
    .then( function() {
        console.log("saved to ", filename);
    });
} else {
    console.log("file already exists ", filename)
}
*/
console.log("ok");


textToSpeech(txt, lang).then(function(data) {
    console.log("seems to have worked", data.sampleRate);
});
