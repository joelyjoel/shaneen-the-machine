var fs = require("fs");
var logFilePath = "./output/logbook.md";

function logbook(body) {
    var date = new Date().toLocaleString();
    var path = process.argv[1];
    path = path.slice(path.indexOf("/shaneen/"));
    var header = "# " + date + " ("+path+")\n";

    var str = header + "\n" + body + "\n\n~\n";

    fs.appendFile(logFilePath, str, function() {
        console.log("logged to", logFilePath)
    });
    return str;
}
module.exports = logbook;
