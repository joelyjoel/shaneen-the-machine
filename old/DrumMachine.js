var VoiceTrack = require("./VoiceTrack.js");
var RhythmTrack = require("./RhythmTrack.js");
var WeightedSet = require("./WeightedSet.js");

function DrumTrack() {
    this.tracks = new Array();
}

DrumTrack.prototype.__defineGetter__("print", function() {
    var lines = new Array();
    for(var i in this.tracks) {
        lines.push(this.tracks[i].print);
    }
    return lines.join("\n");
});

DrumTrack.prototype.toVoiceTrack = function() {
    var p = 36;
    var track = new VoiceTrack().blank(this.d);
    for(var i in this.tracks) {
        track.mix(0, this.tracks[i].toVoiceTrack(p));
        p += this.tracks[i].max;
    }
    return track;
}

DrumTrack.prototype.blank = function(d, numberOfChannels) {
    if(numberOfChannels == undefined) {
        numberOfChannels = 1;
    }

    this.tracks = new Array();

    for(var channel=0; channel<numberOfChannels; channel++) {
        this.tracks.push( new RhythmTrack() );
    }
    this.emptyFill(d);

    return this;
}
DrumTrack.prototype.clear = function(d) {
    for(var i in this.tracks) {
        this.tracks[i].clear();
    }
}
DrumTrack.prototype.emptyFill = function(d) {
    if(d == undefined) {
        d = this.d;
    }
    for(var i in this.tracks) {
        this.tracks[i].emptyFill(d);
    }
    return this;
}

DrumTrack.prototype.addEmptyTracks = function(n) {
    if(n == undefined) {
        n = 1;
    }
    for(var i=0; i<n; i++) {
        this.tracks.push(new RhythmTrack());
    }
    this.emptyFill();
}

DrumTrack.prototype.__defineGetter__("d", function() {
    var d = 0;
    for(var i in this.tracks) {
        if(this.tracks[i].d > d) {
            d = this.tracks[i].d;
        }
    }
    return d;
});

DrumTrack.prototype.toggle = function(channel, t) {
    this.tracks[channel].toggle(t);
}
DrumTrack.prototype.rotate = function(angle) {
    if(angle == undefined) {
        angle = Math.floor(Math.random()*this.d);
    }

    for(var i in this.tracks) {
        this.tracks[i].rotate(angle);
    }
}
DrumTrack.prototype.invert = function() {
    for(var i in this.tracks) {
        this.tracks[i].invert();
    }
}

DrumTrack.prototype.clearCol = function(t) {
    if(t == undefined) {
        t = Math.floor(Math.random()*this.d);
    }
    for(var i in this.tracks) {
        this.tracks[i].steps[t] = false;
    }
}

DrumTrack.prototype.cut = function(t, v) {
    if(t == undefined) {
        t = 0;
    }
    if(v == undefined) {
        t = this.d;
    }

    var newDT = new DrumTrack(this);
    for(var i in newDT.tracks) {
        newDT.tracks[i].cut(t, v);
    }
}
DrumTrack.prototype.loop = function(d) {
    if(d == undefined) {
        d = this.d*2;
    }
    for(var i in this.tracks) {
        this.tracks[i].loop(d);
    }
}
DrumTrack.prototype.overwrite = function(channel, t, what) {
    if(channel == undefined) {
        channel = 0;
    }
    if(t == undefined) {
        t = 0;
    }
    if(what == undefined) {
        what = false;
    }

    if(what.constructor == DrumTrack) {
        for(var i=0; i<what.tracks.length; i++) {
            if(channel+i > this.tracks.length) {
                this.addEmptyTracks(1);
            }
            this.tracks[channel+i].overwrite(t, what.tracks[i]);
        }
    } else {
        this.tracks[channel].overwrite(t, what);
    }

    return this;
}
DrumTrack.prototype.mix = function(channel, t, what) {
    if(channel == undefined) {
        channel = 0;
    }
    if(t == undefined) {
        t = 0;
    }
    if(what == undefined) {
        what = false;
    }

    if(what.constructor == ShDrumTrack) {
        for(var i=0; i<what.tracks.length; i++) {
            if(channel+i > this.tracks.length) {
                this.addEmptyTracks(1);
            }
            this.tracks[channel+i].mix(t, what.tracks[i]);
        }
    } else {
        this.tracks[channel].mix(t, what);
    }

    return this;
}
DrumTrack.prototype.append = function(what) {
    if(this.tracks.length < what.tracks.length) {
        this.addEmptyTracks(what.tracks.length-this.tracks.length);
    }
    this.emptyFill();
    for(var i=0; i<what.tracks; i++) {
        this.tracks[i] = this.tracks[i].append(what.tracks[i]);
    }
}
DrumTrack.prototype.insertBlank = function(t, d) {
    if(t == undefined) {
        t = 0;
    }
    if(d == undefined) {
        d = 1;
    }
    for(var i in this.tracks) {
        this.tracks[i].insertBlank(t, d);
    }
    return this;
}
DrumTrack.prototype.insert = function(channel, t, what) {
    if(t == undefined) {
        t = 0;
    }
    if(channel == undefined) {
        channel = 1;
    }
    this.emptyFill();

    var d;
    if(what.isAStepTrack) {
        d = what.d;
    } else {
        d = 1;
    }

    this.insertBlank(t, d);
    this.overwrite(channel, t, what);
}



// Imaginative functions!!
DrumTrack.prototype.toggleMix = function(channel, t, what) {
    // an imaginative function
    if(channel == undefined) {
        channel = 0;
    }
    if(t == undefined) {
        t = 0;
    }
    if(what == undefined) {
        return this;
    }

    if(what.constructor == DrumTrack) {
        for(var i=0; i<what.tracks.length; i++) {
            var chan = (i + channel) % this.tracks.length;
            this.tracks[chan].toggleMix(t, what);
        }
    } else {
        this.tracks[channel].toggleMix(t, what);
    }
}

//poke functions
DrumTrack.prototype.poke = function(n) {
    var t = Math.floor(Math.random()*this.d);
    var channel = Math.floor(Math.random()*this.tracks.length);

    this.toggle(channel, t);

    if(n > 1) {
        this.poke(n-1);
    }
}
DrumTrack.prototype.clearColPoke = function(n) {
    this.clearCol();

    if(n > 1) {
        this.clearColPoke(n-1);
    }
}
DrumTrack.prototype.clearRowPoke = function(n) {
    var channel = Math.floor(Math.random()*this.tracks.length);

    this.tracks[channel].clear();

    if(n > 1) {
        this.clearColPoke(n-1);
    }
}
DrumTrack.prototype.soloNotePoke = function(n) {
    var channel = Math.floor(Math.random()*this.tracks.length);
    var t = Math.floor(Math.random()*this.d);

    this.clearCol(t);
    this.tracks[channel].toggle(t);

    if(n > 1) {
        this.soloNotePoke(n-1);
    }
}
DrumTrack.prototype.rotatePoke = function(n) {
    var angle = Math.floor(Math.random()*this.d);

    for(var i in this.tracks) {
        if(Math.random() < 0.2) {
            this.tracks[i].rotate(angle);
        }
    }

    if(n>1) this.rotatePoke(n-1);
}
DrumTrack.prototype.swapPoke = function(n) {
    var c1 = Math.floor(Math.random()*this.tracks.length);
    var c2 = Math.floor(Math.random()*(this.tracks.length-1));
    if(c2 >= c1) c2++;

    var hold = new RhythmTrack(this.tracks[c1]);
    this.tracks[c1].overwrite(0, this.tracks[c2]);
    this.tracks[c2].overwrite(0, hold);

    if(n>1) this.swapPoke(n-1);
}
DrumTrack.prototype.invertPoke = function(n) {
    var c = Math.floor(Math.random()*this.tracks.length);
    this.tracks[c].invert();

    if(n>1) this.invertPoke(n-1);
}
DrumTrack.prototype.shadowPoke = function(n) {
    var c1 = Math.floor(Math.random()*this.tracks.length);
    var c2 = Math.floor(Math.random()*(this.tracks.length-1));
    if(c2 >= c1) c2++;

    for(var t=0; t<this.tracks[c2].steps.length; t++) {
        if(this.tracks[c1].steps[t]) {
            this.tracks[c2].steps[t] = false;
        }
    }

    if(n>1) this.shadowPoke(n-1);
}
DrumTrack.prototype.superShadowPoke = function(n) {
    var c = Math.floor(Math.random()*this.tracks.length);

    var hold;
    for(var t=0; t<this.tracks[c].steps.length; t++) {
       if(this.tracks[c].steps[t]) {
           hold = this.tracks[c].steps[t];
           this.clearCol(t);
           this.tracks[c].steps[t] = hold;
       }
    }

    if(n>1) this.superShadowPoke(n-1);
}

DrumTrack.prototype.pokeSet = new WeightedSet();
DrumTrack.prototype.pokeSet.increment(["poke" , "soloNotePoke", "rotatePoke","swapPoke","shadowPoke","superShadowPoke"]);

DrumTrack.prototype.omnipoke = function(n) {
    var pokeFunction = this.pokeSet.roll();
    console.log(pokeFunction);
    this[pokeFunction]();

    if(n>1) this.omnipoke(n-1);
}

module.exports = DrumTrack;
