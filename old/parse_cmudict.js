var fs = require("fs");
var cmudictKeys = require("./poet/cmudictKeys.js");

var mysql = require("mysql");
var connection = mysql.createConnection({
    "host":"localhost",
    "user":"root",
    "password":"",
    "database":"shaneen_resources"
});
connection.connect();

var file = fs.readFileSync("./resources/cmudict/cmudict.0.7a.txt", "utf-8");

var rawLines = file.split("\n");

var lines = [];
for(var i in rawLines) {
    if(rawLines[i].match(/;;;/))
        continue;
    if(rawLines[i] == "")
        continue;

    lines.push(rawLines[i]);
}
console.log("skipped "+ (rawLines.length-lines.length), "invalid lines");

var largest = "";
var largestW;
var phonemeCount = {};
for(var i in lines) {
    var line = lines[i];

    var spleet = line.split("  ");
    if(spleet.length != 2) {
        console.log("yikes", spleet.length)
    }

    var w = spleet[0];
    w = w.split("(")[0];
    var pron = spleet[1];
    var pronCompressed = cmudictKeys.compress(pron);
    var phonemes = pron.split(" ");
    var stressPattern = [];
    for(var j in phonemes) {
        if(phonemes[j].match(/[AEIOU]/)) {
            if(phonemes[j].indexOf("1") != -1)
                stressPattern.push("^");
            else if (phonemes[j].indexOf("2") != -1)
                stressPattern.push(">");
            else if(phonemes[j].indexOf("0") != -1)
                stressPattern.push("-");
            else {
                console.log("WHAHAAT", phonemes[j], w);
            }
        }
    }

    lines[i] = {
        "w": w,
        "phonemes": pron,
        "data": pronCompressed,
        "stressPattern": stressPattern.join("")
    }
    //console.log(lines[i]);
    if(stressPattern.length > largest.length) {
        largest = stressPattern;
        largestW = w;
    }

    var wSql = connection.escape(w.toLowerCase());
    var phonSql = connection.escape(pronCompressed);
    var stressSql = connection.escape(stressPattern.join(""));
    var sylSql = connection.escape(stressPattern.length);
    var sqlVals = [wSql, phonSql, sylSql, stressSql].join(", ");
    var q = "INSERT INTO `phonology` (`w`, `phonetic`, `syllable_n`, `stress`) VALUES ("+sqlVals+")";

    //var q = "INSERT INTO `phonology` (`w`, `phonetic`, `syllable_n`, `stress`) VALUES ("+wSql+", "+phonSql+")";
    //var q = "UPDATE `phonology` SET `phonetic` = " + phonSql + " WHERE `w` = "+wSql;
    //connection.query(q);
    //console.log(q);
    //break;
}
connection.end();

console.log(largest, largest.length, largestW);

/*var keys = {};
var iKeys = {};
for(var i in phonemeCount) {
    var c = i[0];
    while(keys[c]) {

        var n = c.charCodeAt(0)-1;
        if(n <= 32)
            n = 126;
        c = String.fromCharCode(n);
    }
    keys[c] = i;
    iKeys[i] = c;
}

var refFile = {
    "keys": keys,
    "iKeys": iKeys
}
fs.writeFileSync("ipaKeys.json", JSON.stringify(refFile));
//JSON.stringify(keys) + "\n" + JSON.stringify(iKeys);*/
