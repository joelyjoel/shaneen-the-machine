var de = "Nein, aus dem Park. Wer will denn hinein? Bleibt einmal einer vor dem Gitter stehen, winke ich mit der Hand aus dem Fenster und er läuft davon. Aber hinaus, hinaus wollen alle. Nach Mitternacht kannst du alle Grabesstimmen um mein Haus versammelt sehn."
var en = "No, out of the park. Who'd want to come in? If ever anyone stops at the railing I beckon to him from the window and he runs away. But out! Everyone wants to get out. After midnight you can see all the voices from the grave assembled around my house.";

var tts = require("./sound/textToSpeech.js");

for(var i in tts.langs) {
    lang = tts.langs[i];
    var samp = tts.saveTextToSpeech(lang+"_original_philippDemo.wav", de, lang);
    var samp2 = tts.saveTextToSpeech(lang+"_translation_philippDemo.wav", en, lang);
    console.log(lang);
}
