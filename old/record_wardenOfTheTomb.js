var tts = require("./sound/textToSpeech.js");
var fs = require("fs");

tx = fs.readFileSync("./resources/texts/Warden.txt", "utf-8");


var directionRegex = /\(.*\)/g;
var lines = tx.split("\n");
//console.log(lines.length);
var n = 0;
var speakers = {};
var directionsVoice = tts.randomLang();

for(var i in lines) {
    var spleet = lines[i].split(/[ :]/);
    var speaker = spleet[0].split(" ")[0];
    var said = spleet.slice(1).join(" ");

    if(speaker[0] == "." || speaker == '') continue;

    n++;

    var directions = said.match(directionRegex);
    if(directions) console.log(directions);
    var spoken = said.split(directionRegex);

    if(speakers[speaker] == undefined)
        speakers[speaker] = tts.randomLang();

    break;
}
console.log(speakers);
console.log(n);
