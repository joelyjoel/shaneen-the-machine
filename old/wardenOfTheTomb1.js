var clockwork = require("../sound/");
var poet = require("../words/");

var text = new poet.Paragraph("Oh my! This is very fun");
text.stripPunctuation();
console.log(text);

text.fetchTTSSamples("random").then(function(samps) {
    var samplers = [];
    for(var i in samps) {
        samplers[i] = new clockwork.Sampler(samps[i]);
        /*samplers[i].loop0 = Math.random()*samps[i].lengthInSamples;
        samplers[i].loopD = Math.random()*4410;*/
        samplers[i].randomiseLoop(44100/20);
        samplers[i].t = samplers[i].loop0.y;
        //console.log("loop:",samplers[i].loop0.y, samplers[i].loop1)
        samplers[i].detune = Math.random()*24 - 12;
    }
    //console.log(samplers);
    var T = 44100*5;
    var tape = new clockwork.Sample().blank(T,2);
    for(var t=0; t<tape.lengthInSamples; t++) {
        if(t%44100==0)
            console.log(t/44100, "seconds done");
        for(var i in samplers) {
            if(t%Math.floor(44100*60/126)==0) {
                samplers[i].randomiseLoop(44100);
                samplers[i].rate = 0.2;
                samplers[i].loop0 = new clockwork.Osc(Math.random(), "triangle", samplers[i].loop0.y/10, samplers[i].loop0.y);
                //console.log(samplers[i].loopD);
                //samplers[i].detune = Math.random()*12-36;
                samplers[i].t = samplers[i].loop0.y;
                samplers[i].loopState = 0;
                //console.log("randomised", i)
            }
            //console.log(samplers[i].tick());
            tape.channelData[parseInt(i)%2][t] += samplers[i].tick()/samplers.length;
        }
    }
    console.log("normalising");
    tape.normalise();
    tape.saveWavFile(Math.floor(Math.random()*1000)+"wardenThing.wav");
});
