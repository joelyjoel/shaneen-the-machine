var argv = require("minimist")(process.argv.slice(2));
var Paragraph = require("./words/Paragraph.js");
var Word = require("./words/Word.js");
var Promise = require("promise");

var txt = new Paragraph(argv._[0]);
txt.fetchInfo().then(function() {

    var proms = [];

    for(var i in txt.bits) {
        var bit = txt.bits[i];
        if(!bit.isWord)
            continue;
        console.log(txt.bits[i].w, ":", txt.bits[i]._pos)
        if(bit._pos.length == 1 && bit._pos[0] == "adj") {
        //if(bit._pos.length == 1) {
            proms.push(Word.randomWordFromPOS(bit._pos[0]).then(function(word) {
                this.w = word.w;
            }.bind(bit)));
        }
    }

    proms = Promise.all(proms).then(function() {
        console.log("output:", txt.print);
    })
    //console.log(txt.print);
    //console.dir(txt);
});
//console.log(txt);
