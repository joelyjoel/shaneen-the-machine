var HarmonyNetwork = require("./HarmonyNetwork.js");
var Chord = require("./Chord.js");
var fs = require("fs");

var myHN = new HarmonyNetwork("./resources/hmaps/bachChoralAbsoluteAnalysis.hmap");
console.log("warning deadends:", myHN.deadends);

//console.log(myHN.encodeFlattenedJSONVersion());
var obj = myHN.encodeFlattenedJSONVersion();
var obj2 = {};
for(var i in obj) {
    c = new Chord(i);
    if(c.color == "") {
        c.color = "major";
    }
    c = c.print;

    obj2[c] = [];
    for(var j in obj[i]) {
        var d = new Chord(obj[i][j]);
        if(d.color == "") {
            d.color = "major";
        }
        d = d.print;
        obj2[c].push(d);
    }
}






var data = JSON.stringify(obj2);

fs.writeFileSync("./resources/hmaps/bachHmapForMaxMsp.json", data);
console.log("saved to ", "./resources/hmaps/bachHmapForMaxMsp.json");
