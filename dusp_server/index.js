const http = require("http")
const url = require("url")
const construct = require("../dsp3/construct")
const render = require("../dsp3/render")
const RenderStream = require("../dsp3/RenderStream.js")
const dusp = require("../dsp3/dusp")

const Speaker = require("speaker")
const speaker = new Speaker({
  channels:1,
  bitDepth:32,
})

var renderstream = null

http.createServer(function(req, res) {

  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');

  var parsedUrl = url.parse(req.url, true)
  var q = parsedUrl.query

  if(parsedUrl.pathname == "/render.wav") {
    if(q.circuit) {
      console.log("circuit:", q.circuit)
      var circuit = construct(q.circuit)
      var toRender = circuit.lastDuspExpression
      if(toRender.isUnit)
        toRender = toRender.defaultOutlet

      var d = q.d || 1

      toRender.render(d).then(async function(tape) {
        tape.normalise()
        tape.fadeOutSelf()
        tape.meta.inputDusp = q.circuit
        if(q.save)
          tape.save(q.save, "duspUI")

        var wav = await tape.encodeWav()
        wav = Buffer.from(wav)
        console.log(typeof wav, wav)
        res.write(wav)
        console.log("!")
        res.end()
      }).catch(e => {
        res.write(e.message)
        res.end()
      })
    } else {
      console.log("Request made with no circuit attribute", req.url)
      res.end()
    }

  } else if(parsedUrl.pathname == "/play") {
    console.log("playing!")
    if(renderstream) {
      //renderstream.stop()
      renderstream.unpipe(speaker)
    //  speaker.end(0)
    }
    console.log("circuit:", q.circuit)
    var circuit = construct(q.circuit)
    var toRender = circuit.lastDuspExpression
    console.log(dusp(toRender))

    renderstream = new RenderStream(toRender)
    renderstream.pipe(speaker)
  } else if(parsedUrl.pathname == "/stop")
    if(renderstream) {
    //  renderstream.stop()
      renderstream.unpipe(speaker)
    //  speaker.end()
    }

}).listen(8080)
console.log("ready")
