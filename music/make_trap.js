var make_trapHats = require("./make_trapHats.js");
var TrackGroup = require("./TrackGroup.js");
var RhythmTrack = require("./RhythmTrack.js");
var PointTrack = require("./PointTrack.js");
var convertTracks = require("./convertTracks.js");
var midi = require("./midi.js");

function make_trapBeat(d) {
    if(d == undefined) {
        d = 16 * 8;
    }

    var hats = make_trapHats(d);
    var kick = new RhythmTrack().blank(d);
    kick.poke(d/8);
    kick.steps[0] = 1;
    //console.log(kick.steps);
    for(var i=0; i<kick.steps.length; i++) {
        if(kick.steps[i]) {
            kick.steps[i] = 36;
        }
    }

    var snare = new RhythmTrack().blank(16);
    snare.steps[8] = 1;
    snare.loop(d);
    snare.poke(d/16);
    for(var i=0; i<snare.steps.length; i++) {
        if(snare.steps[i]) {
            snare.steps[i] = 37;
        }
    }

    var beat = new PointTrack();
    beat.label = "trap beat";
    beat.mix(hats);
    kick = convertTracks.StepTrack_to_PointTrack(kick);
    beat.mix(kick);
    snare = convertTracks.StepTrack_to_PointTrack(snare);
    beat.mix(snare);

    return beat;
}

var beat = make_trapBeat(32);
//beat.saveMidiFile("trapBeat!.mid");
midi.PointTrack_saveMidiFile(beat, "trap beat (in progress).mid");
