function make_gliss(d, startP, endP, opOrHS, closed) {/* octaveProfile or HarmonySymbol */
    if(d == undefined) {
        d = 1;
    }

    if(startP == undefined || endP == undefined) {
        throw "must define start/end pitches";
    }

    var hs;
    if(opOrHS == undefined) {
        hs = new Scale("C*:")
        //op = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
    } else if(opOrHS.constructor == Array) {
        hs = new HarmonySymbol();
        hs.colorProfile = opOrHS;
    } else if(typeof opOrHS == "object") {
        hs = opOrHS;
    } else if(typeof opOrHS == "string") {
        hs = Scale.parseChordOrScale(opOrHS);
    }

    if(startP < endP) {
        var pitchset = hs.pitchesInRange(startP, endP);
    } else {
        var pitchset = hs.pitchesInRange(endP, startP).reverse();
    }

    var subD = d/pitchset.length;
    var t = 0;

    var track = new PointTrack();
    for(var i=0; i<pitchset.length; i++) {
        var note = new PointTrack.Note();
        note.t = t;
        note.d = subD;
        note.sound = pitchset[i];
        track.notes.push(note);

        t += subD;
    }

    if(closed) {
        var note = new PointTrack.Note();
        note.t = d;
        note.sound = endP;
        if(typeof closed == "number") {
            note.d = closed;
        }
        track.notes.push(note);
    }

    track.label = "gliss";
    if(hs.print != undefined) {
        track.label += " "+hs.print;
    }
    return track;
}
module.exports = make_gliss;

var Scale = require("./Scale.js");
var Chord = require("./Chord.js");
var HarmonySymbol = require("./HarmonySymbol.js");
var PointTrack = require("./PointTrack.js");
