var make_arp = require("./make_arp.js");
var make_harmony = require("./make_harmony.js");
var Score = require("./Score.js");
var Arp = require("./Arp.js");
const HarmonyTrack = require("./HarmonyTrack.js")

exports.tranceyThing = function(harmony) {
    harmony = harmony || 32;
    if(harmony.constructor == Number)
      harmony = make_harmony(harmony);
    if(harmony.constructor == String)
      harmony = new HarmonyTrack(harmony)
    var d = harmony.d;

    var track = make_arp.tranceyThing(d);


    Arp(track, harmony);

    return track;
}

exports.riff = function(harmony) {
  harmony = harmony || 32;
  if(harmony.constructor == Number)
      harmony = make_harmony(harmony);
  if(harmony.constructor == String)
    harmony = new HarmonyTrack(harmony)
  var d = harmony.d;

  var track = make_arp.riff(d);


  Arp(track, harmony);

  return track;
}

exports.arpeggiatedChords = function() {

}
