/*
  Generate a list of times from the sequence (PHI*n)%1, make a weighted
  probability set and choose times at random to be the start/end of notes in a
  piece.
*/

const make_petalPattern = require("./make_petalPattern")

let sounds = []
for(var i=0; i<10; i++)
  sounds[i] = Math.floor(Math.random()*60+30)
let piece = make_petalPattern({sounds: sounds})
piece.save()
