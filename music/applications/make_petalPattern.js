/*
  Generate a list of times from the sequence (PHI*n)%1, make a weighted
  probability set and choose times at random to be the start/end of notes in a
  piece.
*/

const WeightedSet = require("weighted-set")
const PointTrack = require("../PointTrack.js")


const PHI = 1.61803398875

function make_petalPattern({
  pFunction = previous => previous*0.9,
  numberOfAnchorPoints = 34,
  duration = 30,
  sounds,
}={}) {
  if(!sounds)
    throw "sounds is a required argument for make_petalPattern"

  // assemble the probability set
  var set = new WeightedSet
  var p = 1
  for(var n=0; n<numberOfAnchorPoints; n++) {
    var t = duration * ((PHI*n)%1) // time
    p = pFunction(p)
    set.increment(t, p)
  }

  // make the piece
  var piece = new PointTrack()
  for(var i in sounds) {
    var a = set.roll()
    var b = set.roll()

    var note = new PointTrack.Note()
    note.t = Math.min(a, b)
    note.d = Math.abs(a - b)

    // this is not going to work in many circumstances
    if(typeof sounds[i] == "object")
      Object.assign(note, sounds[i])
    else
      note.sound = sounds[i]
    piece.mix(note)
  }

  return piece
}
module.exports = make_petalPattern
