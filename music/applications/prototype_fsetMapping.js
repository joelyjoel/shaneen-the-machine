/*
  Generate a microtonal chord setquence by making each frequency set a function
  of the one before it.

  This file is a prototype.
*/



// Parameters
const min_frequency = 50
const max_frequency = 10000
const startingSet = [{f:440, A:1}] // {frequency:amplitude, f:A, ...}
const numberOfFrames = 20
const densityPlan = (new Array(100).fill(8))
const step_duration = 1 // semiquavers
const normaliseEachFrame = true
const articulation = "block_chords" // block_chords or petal_pattern
const bpm = 140
const stepFunction = (f, A, previous, next) => {
  // frequency, amplitude, previous set, next set (incomplete)

  // split up and down by fifths
  if(Math.random() < 0.5)
    return [{f: f*1.5, A:A/2}, {f: f/1.5, A:A/2}]

  // detune
  if(Math.random() < 0.5)
    return {f: f * Math.pow(2,( Math.random()*2-1)/48), A:A}

  // split up and down by third
  if(Math.random() < 0.2)
    return [{f: f*(5/4), A:A/2}, {f: f*(4/5), A:A/2}]
  // harmonic leap
  if(Math.random() < 0) {
    let numerator = Math.ceil(Math.random()*16)
    let denominator = Math.ceil(Math.random()*8)
    let h = numerator / denominator
    return {f: f*h, A:A}
  }

  // repeat
  return {f:f, A:A}
}

// generate the sequence
let sequence = []
for(var frame in densityPlan) {
  console.log("generating frame", frame)
  let next_set = []
  let previous_set = sequence[sequence.length-1] || startingSet
  while(next_set.length < densityPlan[frame]) {
    let i = Math.floor(Math.random()*previous_set.length)
    if(previous_set[i].f < min_frequency || previous_set[i].f > max_frequency)
      continue
    let result = stepFunction(previous_set[i].f, previous_set[i].A, previous_set, next_set)
    if(result) {
      if(result.constructor == Array)
        next_set = next_set.concat(result)
      else
        next_set.push(result)
    }
  }
  sequence.push(next_set)
}

// normalise by frame
if(normaliseEachFrame) {
  for(let i in sequence) {
    let sum = 0
    for(let j in sequence[i])
      sum += sequence[i][j].A
    for(j in sequence[i])
      sequence[i][j].A /= sum
  }
}

console.log(sequence)
console.log("lengths:", sequence.map(frame => frame.length))

// convert frequency set series to a PointTrack
const PointTrack = require("../PointTrack.js")
const pitch = require("../pitch.js")
const make_petalPattern = require("./make_petalPattern")

var track = new PointTrack()
var t = 0
var tOff = step_duration
for(var frame in sequence) {
  let sounds = []
  for(var i in sequence[frame])
    sounds[i] = {
      sound: pitch.fToP(sequence[frame][i].f),
      A: sequence[frame][i].A,
    }
  if(articulation == "block_chords") {
    for(var i in sounds) {
      let note = new PointTrack.Note()
      note.sound = sounds[i]
      note.A = sequence[frame][i].A
      note.t = t
      note.tOff = tOff
      track.mix(note)
    }
    t = tOff
    tOff += step_duration
  } else {
    let frameTrack = make_petalPattern({
      sounds: sounds,
      duration: step_duration,
      numberOfAnchorPoints: 100,
    })
    track.append(frameTrack)
  }
}

console.log(track)

// interpret the PointTrack as sound
const interpretPointTrack = require("../../dsp3/functions/interpretPointTrack.js")
const Osc = require("../../dsp3/components/Osc.js")
const Multiply = require("../../dsp3/components/Multiply.js")

interpretPointTrack(track, (dSamples, p, note) => {
  let osc = new Osc(pitch.pToF(note.sound))
  osc.waveform = "triangle"
  let mult = new Multiply(osc, note.A || 1)
  return mult.OUT.render(dSamples/44100, undefined, undefined, true).then(noteSample =>
    noteSample
      .fadeOutSelf(441)
      //.fadeInSelf(noteSample.lengthInSamples/2)
  )
}, bpm).then(tape => {
  tape
    .normalise()
    .save("Prototyping fsetMappings", "debugging")
})
