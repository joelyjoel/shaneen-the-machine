var utility = require("./utility.js");
var StepTrack = require("./StepTrack.js");
var VoiceTrack = require("./VoiceTrack.js");

function RhythmTrack(model) {
    StepTrack.call(this, model);

    this.map = undefined;
    this.max = 1;

    if(model != undefined && model.constructor == RhythmTrack) {
        this.map = utility.duplicate(model.map);
        this.max = model.max;
    }
}
RhythmTrack.prototype = Object.create(StepTrack.prototype);
RhythmTrack.prototype.construcotr = RhythmTrack;

RhythmTrack.prototype.__defineGetter__("print", function() {
    var s = "";
    for(var i=0; i<this.steps.length; i++) {
        if(this.steps[i] == false) {
            s += "-";
        } else if(this.steps[i] < 10) {
            s += this.steps[i];
        } else {
            s += "?";
        }
    }
    return s;
});

RhythmTrack.prototype.toVoiceTrack = function(p) {
    this.emptyFill();
    var voiceTrack = new VoiceTrack();
    for(var t in this.steps) {
        var step = this.steps[t];
        if(step > this.max) {
            console.log("argh");
            step = (step-1)%this.max + 1;
        }
        if(step != undefined && step.constructor == Number) {
            if(p != undefined) {
                voiceTrack.steps[t] = this.steps[t] + p - 1;
                continue;
            }
            if(this.map == undefined) {
                voiceTrack.steps[t] = this.steps[t];
                continue;
            }
            if(this.map.constructor == Number) {
                voiceTrack.steps[t] = this.steps[t] + this.map -1;
                continue;
            }
            if(this.map.constructor == Array) {
                voiceTrack.steps[t] = this.map[this.steps[t]-1];
                continue;
            }
        }
        if(step === false) {
            voiceTrack.steps[t] = false;
        }
    }

    return voiceTrack;
}

RhythmTrack.prototype.printStep = function(t) {
    var step = this.steps[t];
    if(step === false || step == undefined) {
        return "-";
    }
    if(step.constructor == Number) {
        return step;
    }
    return "?";
}

RhythmTrack.prototype.mix = function(t, what) {
    if(t == undefined) {
        t = 0;
    }
    for(var i=0; i<what.steps.length; i++) {
        if(this.steps[i+t] == false || what.steps[i] > this.steps[i+t]) {
            this.steps[i+t] = what.steps[i];
        }
    }
    return this;
}
RhythmTrack.prototype.toggleMix = function(t, what) {
    if(t == undefined) {
        t = 0;
    }
    for(var i=0; i<what.steps.length; i++) {
        if(what.steps[i]) {
            this.toggle( (t + i)%this.steps.length );
        }
    }
}

RhythmTrack.prototype.toggle = function(t) {
    if(this.steps[t]) {
        this.steps[t] = false;
    } else {
        this.steps[t] = Math.ceil(Math.random()*this.max);
    }
    return this.steps[t];
}
RhythmTrack.prototype.invert = function(t) {
    for(var t=0; t<this.steps.length; t++) {
        this.toggle(t);
    }
}

RhythmTrack.prototype.poke = function(n) {
    var t = Math.floor(Math.random()*this.steps.length);
    this.toggle(t);
    if(n > 1) {
        this.poke(n-1);
    }
}

module.exports = RhythmTrack;
