const PointTrack = require("../PointTrack.js")

function guidify(guide) {
  if(guide) {
    if(guide.isPointTrack || guide.isTrackGroup)
      return guide
    else if(guide.constructor == Number) {
      return new PointTrack().blank(guide)
    }
  } else
    return new PointTrack().blank(16)
}
module.exports = guidify;
