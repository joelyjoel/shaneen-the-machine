const guidify = require("./guidify.js");
const PointTrack = require("../PointTrack.js");

function make_shaker(guide) {
  var functions = [
    make_shaker.syncopatedSubdivisions,
    make_shaker.straight,
    make_shaker.randomised,
    make_shaker.twoAtOnce,
  ];
  var F = functions[Math.floor(Math.random()*functions.length)];
  return F(guide);
}
module.exports = make_shaker;

make_shaker.syncopatedSubdivisions = function(guide) {
  guide = guidify(guide)
  var supd = Math.pow(2, 2 + Math.floor(Math.random()*3));
  var subd = Math.ceil(Math.random()*6);
  var offset = Math.random() * supd/subd;

  var times = [];
  for(var i=0; i<subd; i++) {
    times.push( i * supd/subd + offset);
  }

  var track = PointTrack.fromTs(times, "sh", supd/subd);
  track.d = supd;
  track = track.loop(guide.d);
  track.label = "syncopated shaker";
  return track;
}

make_shaker.straight = function(guide) {
  guide = guidify(guide);
  var subDivision = Math.ceil(Math.random()*8);
  var superDivision = Math.pow(2, 2 + Math.floor(Math.random()*3));

  var times = [];
  for(var i=0; i<subDivision; i++) {
    times.push( i * superDivision/subDivision);
  }

  var track = PointTrack.fromTs(times, "sh", subDivision/superDivision);
  track.d = superDivision;
  track = track.loop(guide.d);
  track.label = "subdivision shaker";
  return track;
}

make_shaker.randomised = function(guide) {
  guide = guidify(guide);

  var superDivision = Math.pow(2, 2+Math.floor(Math.random()*3));
  var n = Math.floor(Math.random()*superDivision * 5)

  var times = [];
  for(var i=0; i<n; i++) {
    times.push(Math.random()*superDivision)
  }

  var track = PointTrack.fromTs(times, "sh");
  track.d = superDivision;
  track = track.loop(guide.d);
  track.label = "randomised shaker";
  return track;
}

make_shaker.twoAtOnce = function(guide) {
  guide = guidify(guide);
  var track = make_shaker(guide).mix(make_shaker(guide));
  track.label = "two shakers at once";
  return track;
}
