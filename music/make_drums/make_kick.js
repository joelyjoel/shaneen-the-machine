const guidify = require("./guidify.js");
const PointTrack = require("../PointTrack.js")

function make_kick(guide) {
  var functions = [
    make_kick.fourFloor,
    make_kick.onAndOff,
    make_kick.sporadicWithAOne,
    make_kick.justOnOne,
  ];
  var F = functions[Math.floor(Math.random()*functions.length)];
  return F(guide);
}
module.exports = make_kick;

make_kick.fourFloor = function(guide) {
  guide = guidify(guide)
  var track = new PointTrack();
  for(var t=0; t<guide.d; t+=4) {
    var note = new PointTrack.Note();
    note.t = t;
    note.d = 4;
    note.sound = "kick";
    track.notes.push(note);
  }
  track.label = "four floor kick";
  return track;
}
make_kick.onAndOff = function(guide) {
  guide = guidify(guide);
  var track = PointTrack.fromTs([0, Math.random()*8], "kick")
  track.d = 8;
  track = track.loop(guide.d);
  track.label = "on- and off-beatkick";
  return track;
}
make_kick.sporadicWithAOne = function(guide) {
  guide = guidify(guide);
  var Ts = [0];
  do {
    Ts.push(Math.floor(Math.random()*16));
  } while(Math.random()<0.75)
  var track = PointTrack.fromTs(Ts, "kick");
  track.d = 16;
  track.notes.push( new PointTrack.Note("kick_1", 0))
  track = track.loop(guide.d);
  track.label = "sporadic kick";
  return track;
}
make_kick.justOnOne = function(guide) {
  guide = guidify(guide);
  var track = new PointTrack();
  track.notes = [new PointTrack.Note("kick", 0, 16)]
  track = track.loop(guide.d);
  track.label = "kick on one";
  return track;
}
