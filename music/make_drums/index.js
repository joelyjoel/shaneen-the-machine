const guidify = require("./guidify.js")

function make_drums(guide) {
  guide = guidify(guide);

  var shakerRhythm = make_drums.shaker(16 * 8)
  .mix(make_drums.shaker(16*4), 4*16);

  var plusKick = shakerRhythm.loop(shakerRhythm.d * 2).mix(
    make_drums.kick(16*8), 16*8
  );

  return plusKick
}
module.exports = make_drums;

make_drums.kick = require("./make_kick.js");
make_drums.shaker = require("./make_shaker.js")
