// Function for making sense of sound/pitch representation in shaneen

// Sometimes sounds are represented like 'kick_2' meaning to keep different
// types of kicks distinct. This function is for seperating these out.

function parseSound(sound) {
  if(sound && sound.constructor == String) {
    var split = sound.split("_");
    return {
      label: split[0],
      n: parseInt(split[1]),
    }
  } else if(sound && sound.constructor == Number)
    return { p: sound }
  else
    return {}
}
module.exports = parseSound;

parseSound.lowestUnusedSound = function(gamut, sound) {
  var label = parseSound(sound).label
  if(label) {
    if(gamut.indexOf(label) == -1)
      return label;
    var n = 1;
    var label_ = label+"_"
    while(gamut.indexOf(label_+n) != -1)
      n++;
    return label_+n;
  } else {
    n = 0;
    while(gamut.indexOf(n) != -1)
      n++;
    return n;
  }
}
