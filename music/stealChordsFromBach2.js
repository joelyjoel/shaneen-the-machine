const fs = require("fs")
const midi = require("./midi.js")
const Chord = require("./Chord.js")
const Scale = require("./Scale.js")
const bach = require("./bach")
const HarmonyTrack = require("./HarmonyTrack.js")
const PointTrack = require("./PointTrack.js")

const findChordProgressionFragments = require('./findChordProgressionFragments')

function* findChordProgressionFragmentsInBach(music) {
  while(true) {
    let music = bach.random().mixDownNumberedTracks()
    for(let frag of findChordProgressionFragments(music))
      yield frag
  }
}
module.exports = findChordProgressionFragmentsInBach
