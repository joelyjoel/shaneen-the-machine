var VoiceTrack = require("./VoiceTrack.js");


for(var i=0; i<32; i++) {
    var myVT = new VoiceTrack().blank(16);
    for(var t=0; t<myVT.steps.length; t++) {
        if(Math.random() < 0.5 || t == 0) {
            myVT.steps[t] = Math.floor(Math.random()*24)+12;
        }
    }
    var pt = myVT.toPointTrack();
    
    var filename = "randoomised bassline "+i+".mid";
    pt.saveMidiFile("/Users/joel/Samples/shaneen_samples/basslines/"+filename);
}
