var StepTrack = require("./StepTrack.js");
var utility = require("./utility.js");
var midiConverter = require("midi-converter");
var fs = require("fs");

function VoiceTrack(model) {
    StepTrack.call(this, model);
}
VoiceTrack.prototype = Object.create(StepTrack.prototype);
VoiceTrack.prototype.constructor = VoiceTrack;

VoiceTrack.prototype.toJsonMidiTrack = function() {
    var events = [];
    var event;
    var step_t = 32;
    var deltaTime = 0;
    var soundingNotes = false;
    for(var t=0; t<this.steps.length; t++) {
        var step = this.steps[t];
        if(step == undefined || step === false) {
            deltaTime += step_t;
            continue;
        }
        if(soundingNotes
        && (step == ";" || step.constructor == Number || step.constructor == Array)) {
            // add note off messages
            for(var i in soundingNotes) {
                event = {
                    "deltaTime": deltaTime,
                    "noteNumber": soundingNotes[i],
                    "type": "channel",
                    "subtype": "noteOff",
                    "velocity": 60
                };
                events.push(event);

                deltaTime = 0;
            }
            soundingNotes = false;
        }
        // add single note
        if(step.constructor == Number) {
            event = {
                "deltaTime": deltaTime,
                "noteNumber": step,
                "channel": 0,
                "type": "channel",
                "subtype": "noteOn",
                "velocity": 60
            };
            events.push(event);
            soundingNotes = [step];
            deltaTime = 0;

            deltaTime += step_t;
            continue;
        } else if(step.constructor == Array) { // add chord
            for(var i in step) {
                event = {
                    "deltaTime": deltaTime,
                    "noteNumber": step[i],
                    "channel": 0,
                    "type": "channel",
                    "subtype": "noteOn",
                    "velocity": 60
                };
                events.push(event);
                soundingNotes = step;
                deltaTime = 0;
            }
            deltaTime += step_t;
            continue;
        }
    }
    if(soundingNotes) {
        // add note off messages
        for(var i in soundingNotes) {
            event = {
                "deltaTime": deltaTime,
                "noteNumber": soundingNotes[i],
                "type": "channel",
                "subtype": "noteOff",
                "velocity": 60
            };
            events.push(event);

            deltaTime = 0;
        }
        soundingNotes = false;
    }
    event = {
        "deltaTime": deltaTime,
        "type": "meta",
        "subtype": "endOfTrack"
    }
    events.push(event);

    return events;
}
VoiceTrack.prototype.toJsonMidiFile = function() {
    return {
        "header": {
            "formatType": 0,
            "trackCount": 1,
            "ticksPerBeat": 128
        },
        "tracks": [this.toJsonMidiTrack()]
    }
}
VoiceTrack.prototype.toMidiFile = function() {
    var json = this.toJsonMidiFile();
    console.log("\n");
    console.dir(json);
    console.log(json.tracks);
    return midiConverter.jsonToMidi(json);
}
VoiceTrack.prototype.saveMidiFile = function(filename) {
    if(filename == undefined) {
        filename = this.label + ".mid";
    }
    fs.writeFileSync(filename, this.toMidiFile(), "binary");
}

module.exports = VoiceTrack;
