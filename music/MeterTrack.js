var TrackGroup = require("./TrackGroup.js");
var TempoPointTrack = require("./TempoPointTrack.js");


function MeterTrack(model) {
    TrackGroup.call(this, model);

    if(!this.tracks.tempo) {
        this.tracks.tempo = new TempoPointTrack();
    }
}
MeterTrack.prototype = Object.create(TrackGroup.prototype);
MeterTrack.prototype.constructor = MeterTrack;


module.exports = MeterTrack;
