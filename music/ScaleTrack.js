var PointTrack = require("./PointTrack.js");

function ScaleTrack(model) {
    PointTrack.call(this, model);
}
ScaleTrack.prototype = Object.create(PointTrack.prototype);
ScaleTrack.prototype.constructor = ScaleTrack;
module.exports = ScaleTrack;

ScaleTrack.prototype.isScaleTrack = true;

ScaleTrack.Note = function(model) {
    PointTrack.Note.call(this, model);
}
ScaleTrack.Note.prototype = Object.create(PointTrack.Note.prototype);
ScaleTrack.Note.prototype.constructor = ScaleTrack.Note;

ScaleTrack.Note.prototype.isScaleTrackNote = true;
