shaneen_faces = ["-.-",">_<","o,o","-o-","'c'","=.=",":-)"];
shaneen_welcomeMessage =  " __________\n"
shaneen_welcomeMessage += "|       "
            + shaneen_faces[Math.floor(Math.random()*shaneen_faces.length)]
            +"|\n";
shaneen_welcomeMessage += "|          | version " +
                Math.round(Math.random()*100)/10
                + "\n";
shaneen_welcomeMessage += "| SHANEEN  |\n";
shaneen_welcomeMessage += "|the machine\n";

console.log(shaneen_welcomeMessage);


exports.utility = require("./utility.js");
exports.midi = require("./midi.js");
exports.cstring = require("./cstring.js");
exports.WeightedSet = require("./WeightedSet.js");
exports.convertTracks = require("./convertTracks.js");
exports.Arp = require("./Arp.js");

//export.DeltaTrack = require("./DeltaTrack.js");
exports.PointTrack = require("./PointTrack.js");
exports.TempoPointTrack = require("./TempoPointTrack.js");
exports.StepTrack = require("./StepTrack.js");
exports.VoiceTrack = require("./VoiceTrack.js");
exports.TrackGroup = require("./TrackGroup.js");
exports.MeterTrack = require("./MeterTrack.js");
exports.RhythmTrack = require("./RhythmTrack.js");
exports.DrumTrack = require("./DrumTrack.js");
exports.ChordTrack = require("./ChordTrack.js");

exports.Chord = require("./Chord.js");
exports.Scale = require("./Scale.js");
exports.HarmonyNetwork = require("./HarmonyNetwork.js");

exports.make_harmony = require("./make_harmony.js");
exports.make_arp = require("./make_arp.js");
