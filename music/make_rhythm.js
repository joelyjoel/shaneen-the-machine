var PointTrack = require("./PointTrack.js");
var DrumTrack = require("./DrumTrack.js");

module.exports = make_rhythm = function(d) {
    var functions = [make_rhythm.basic, make_rhythm.upbeats];
    var F = functions[Math.floor(Math.random()*functions.length)];
    return F(d)
}

make_rhythm.basic = function(D, d) {
    D = D || 0;
    d = d || 1;

    var r = new PointTrack();
    for(var t=0; t<D; t+=d) {
        var note = new PointTrack.Note();
        note.t = t;
        note.d = t+d<D ? d : D-t;
        note.sound = true;
        r.notes.push(note);
    }
    r.d = d;
    r.label = "basic rhythm";
    return r;
}
make_rhythm.upbeats = function(D, d) {
    D = D || 0;
    d = d || 4;

    var r = new PointTrack();
    for(var t=d/2; t<D; t+=d) {
        var note = new PointTrack.Note();
        note.t = t;
        note.d = t+d/2<D ? d/2 : D-t;
        note.sound = true;
        r.notes.push(note);
    }
    r.d = d;
    r.label = "upbeats";
    return r;
}

make_rhythm.fromFrags = function(d, frags) {
    d = d!=undefined ? d : 0;
    var r = new PointTrack();
    r.d = 0;
    while(r.d < d) {
        var frag = frags[Math.floor(Math.random()*frags.length)];
        r.append(frag);
    }
    r = r.cut(0, d);
    r.label = "frag rhythm";
    return r;
}

make_rhythm.kick = function(d) {
    d = d!=undefined ? d : 16;
    var frags = [
        DrumTrack.fromTimeArray([0], 4),
        DrumTrack.fromTimeArray([0,2], 4),
        DrumTrack.fromTimeArray([0,1,3], 4),
        DrumTrack.fromTimeArray([0,3,6], 8),
        DrumTrack.fromTimeArray([0,3], 4),
        DrumTrack.fromTimeArray([0,2,3], 4)
    ];
    var r = make_rhythm.fromFrags(d, frags);
    r.label = "kick rhythm";
    return r;
}

make_rhythm.popVamp = function(d) {
  d = d!=undefined ? d : 16
  var frags = [
    DrumTrack.fromTimeArray([0], 4),
    DrumTrack.fromTimeArray([0,3,6], 8),
    DrumTrack.fromTimeArray([0,3], 4),
    DrumTrack.fromTimeArray([0, 6], 8)
  ]
  var r = make_rhythm.fromFrags(d, frags);
  r.label = "pop vamp rhythm";
  return r;
}

make_rhythm.offBeatHihat = function(d) {
    d = d!=undefined ? d : 16;
    var loop = DrumTrack.fromTimeArray([2], 4);
    var r = loop.loop(d);
    r.poke(d/8);
    //var r = make_rhythm.fromFrags(d, frags);
    r.label = "hihat rhythm";
    return r;
}
