function StepTrack(model) {
    this.steps = new Array();
    this.label = "track";
    if(model != undefined) {
        if(model.isAPointTrack)
            model = model.toStepTrack();

        if(model.isAStepTrack) {
            this.steps = utility.duplicate(model.steps);
            this.label = model.label;
        }
    }
}
module.exports = StepTrack;

var utility = require("./utility.js");
var pitch = require("./pitch.js");
var convertTracks = require("./convertTracks.js");
//var PointTrack = require("./PointTrack.js");

StepTrack.prototype.isAStepTrack = true;

StepTrack.prototype.__defineGetter__("print", function() {
    var printableSteps = new Array();
    for(var t in this.steps) {
        printableSteps[t] = this.printStep(t);
        if(printableSteps[t].length > 4) {
            console.log("uh oh! quite long..", printableSteps[t]);
        }
    }
    return printableSteps.join("\t");
});
StepTrack.prototype.blank = function(d) {
    // makes an empty track of duration d
    if(d == undefined) {
        d = this.d;
    }

    this.steps = new Array();

    for(var t=0; t<d; t++) {
        this.steps[t] = false;
    }

    return this;
}
StepTrack.prototype.zerofy = function(d) {
    this.steps = [];
    return this;
}
StepTrack.prototype.clear = function(t1, t2) {
    if(t1 == undefined) t1 = 0;
    if(t2 == undefined) t2 = this.d;
    for(var t=t1; t<t2; t++) {
        this.steps[t] = false;
    }
    return this;
}
StepTrack.prototype.emptyFill = function(d) {
    // replace any undefined steps with `false` (~) up to duration d
    if(d == undefined) {
        d = this.d;
    }

    for(var t=0; t<d; t++) {
        if(this.steps[t] == undefined) {
            this.steps[t] = false;
        }
    }

    return this;
}
StepTrack.prototype.replaceSlursWithRests = function(from, to) {
    if(from == undefined) {
        from = 0;
    }
    if(to == undefined) {
        to = this.steps.length;
    }

    for(var t in this.steps) {
        if(this.steps[t] === false) {
            this.steps[t] = ";";
        }
    }
    return this;
}
StepTrack.prototype.overwrite = function(what, t) {
    // write something in step t
    if(what == undefined) {
        this.steps[t] = false;
        return this;
    }

    if(what.isAStepTrack) {
        var v = t + what.d
        for(var u=t; u<v; u++) {
            this.overwrite(what.steps[u-t], u);
        }
        return this;
    } else {
        this.steps[t] = what;
        return this;
    }
}
StepTrack.prototype.mix = function(what, t) {
    if(what == undefined) {
        return this;
    }

    if(what.isAStepTrack) {
        var v = t + what.d;
        for(var u=t; u<v; u++) {
            this.mix(what.steps[u-t], u);
        }
        return this;
    } else {
        if(this.changesAt(t)) {
            if(this.steps[t].constructor != Array) {
                this.steps[t] = [this.steps[t]];
            }
            if(what.constructor == Array) {
                for(var i in what) {
                    if(what[i] !== false && what[i] != ";" && what[i] != undefined) {
                        this.steps[t].push(what[i]);
                    }
                }
            } else {
                if(what !== false && what != ";" && what != undefined || this.steps[t] == undefined) {
                    this.steps[t].push(what);
                }
            }
        } else {
            this.steps[t] = what;
        }
    }
}
StepTrack.prototype.append = function(what) {
    this.steps = this.steps.concat(utility.duplicate(what.steps));
    return this;
}
StepTrack.prototype.insert = function(what, t) {
    if(t == undefined) {
        t = 0;
    }

    if(what.constructor.isAStepTrack) {
        what = utility.duplicate(what.steps);
    } else if(what.constructor == Number
    || what.constructor == Array
    || what.constructor == String) {
        what = [utility.duplicate(what)];
    }

    var stepsBefore = this.steps.slice(0, t);
    var stepsAfter = this.steps.slice(t);
    this.steps = stepsBefore.concat(what, stepsAfter);
    return this;
}
StepTrack.prototype.insertBlank = function(d, t) {
    if(t == undefined) {
        t = 0;
    }
    if(d == undefined) {
        d = 1;
    }
    var newbit = new this.constructor().blank(d);
    this.insert(newbit, t);
    return this;
}
StepTrack.prototype.loop = function(d) {
    if(d == undefined) {
        d = this.d * 2;
    }
    var loopSteps = this.steps;
    this.steps = new Array();
    for(var t=0; t<d; t++) {
        this.steps.push( loopSteps[t%loopSteps.length] );
    }
    return this;
}
StepTrack.prototype.setNote = function(what, t) {
    // replace the note at step t
    t = this.noteT(t);
    this.overwrite(what, u);
}
StepTrack.prototype.mixNote = function(what, t) {
    // add to the note at step t
    t = this.noteT(t);
    this.mix(what, t);
}
StepTrack.prototype.rotate = function(angle) {
    if(angle == undefined) {
        angle = Math.floor(Math.random()*this.steps.length);
    }

    var a = this.steps.slice(0, angle);
    var b = this.steps.slice(angle);

    this.steps = b.concat();
}

StepTrack.prototype.note
= StepTrack.prototype.soundAtT = function(t) {
    // returns the note sounding at step t
    return this.steps[this.noteT(t)];
}
StepTrack.prototype.noteT = function(t) {
    // returns the step of the note sounding at step t
    while(t >=0 && (this.steps[t] === false || this.steps[t] == undefined)) {
        t--;
    }
    if(t >= 0) {
        return t;
    }
}
StepTrack.prototype.nextNote = function(t) {
    return this.steps[this.nextNoteT(t)];
}
StepTrack.prototype.nextNoteT = function(t) {
    for(var i=t+1; i<this.steps.length; i++) {
        if(this.changesAt(i)) {
            return i;
        }
    }
    return this.d;
}
StepTrack.prototype.changesAt = function(t) {
    // returns true if a new note begins at step t
    // rename this ( it is also used in Shcore.js)
    return this.steps[t] !== false && this.steps[t] != undefined;
}
StepTrack.prototype.__defineGetter__("d", function() {
    // returns duration in steps
    return this.steps.length;
});
StepTrack.prototype.printStep = function(t) {
    // return textual representation of step t
    var step = this.steps[t];
    if(step == undefined || step === false) {
        return "...";
    } else if(step == ";") {
        return ";";
    }else if(step.constructor == Number) {
        return utility.printPitch(step);
    } else if(step.constructor == String) {
        return step;
    } else if(step.constructor == Array) {
        var printChord = new Array();
        for(var i in step) {
            if(step[i].constructor == Number) {
                printChord.push(pitch.printPitch(step[i]));
            } else if(step[i].constructor == String) {
                printChord.push(step[i]);
            } else {
                printChord.push(step[i].print);
            }
        }
        return printChord.join("<br>")
    } else if(typeof step == "object") {
        return step.print;
    }
    return step;
}
StepTrack.prototype.cut = function(t1, t2) {
    if(t1 == undefined) {
        t1 = 0;
    }
    if(t2 == undefined) {
        t2 = this.d;
    }

    var newStepLine = new StepTrack();
    newStepLine.steps = utility.duplicate( this.steps.slice(t1, t1) );
    newStepLine.label = utility.duplicate(this.label);
    return newStepLine;
}
StepTrack.prototype.toPointTrack = function() {
    return convertTracks.StepTrack_to_PointTrack(this);
}
StepTrack.prototype.__defineGetter__("sounds", function() {
    var list = [];
    for(var i in this.steps) {
        if(this.steps[i] == false || this.steps[i] == ";")
            continue;
        if(this.steps[i].constructor == Array) {
            for(var j in this.steps[i])
                list.push(this.steps[i][j]);
        } else
            list.push(this.steps[i]);
    }
    return list;
});
StepTrack.prototype.__defineSetter__("sound", function(list) {
    list = list.slice();
    for(var i in this.steps) {
        if(this.steps[i] == false || this.steps[i] == ";")
            continue;
        if(this.steps[i].constructor == Array) {
            this.steps[i] = this.steps[i].slice();
            for(var j in this.steps[i])
                this.steps[i][j] = list.shift;
        } else
            this.steps[i] = list.shift();
    }
});
