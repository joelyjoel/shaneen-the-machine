var fs = require("fs");
var midi = require("./midi.js");
var Chord = require("./Chord.js");
var HarmonyNetwork = require("./HarmonyNetwork.js");
var pitch = require("./pitch.js");

var dir = "./resources/bach-chorales/";
var dirContents = fs.readdirSync(dir);
var midiFiles = [];
for(var i in dirContents) {
    var filename = dirContents[i];
    if(filename.slice(filename.length-4) == ".mid") {
        midiFiles.push(dir+filename);
    }
}

var frags = [];

for(var i=0; i<midiFiles.length; i++) {
    var filename = midiFiles[i];
    console.log("analysing "+filename);

    var music = midi.parseMidiFile(filename).mixDownNumberedTracks();
    var frag = [];

    for(var j in music.notes) {
        var t = music.notes[j].t;
        var selection = music.select(t);
        var bass = pitch.pc(selection.bass);
        var op = selection.octaveProfile;
        var chord = Chord.fromOctaveProfile(op);
        chord.bass = bass;
        if(chord != "?") {
            if(chord.print != frag[frag.length-1])
                frag.push(chord.print);
        } else {
            if(frag.length > 1)
                frags.push(frag);
            frag = [];
        }
    }
    if(frag.length > 1) {
        frags.push(frag);
    }
}

var hn = new HarmonyNetwork();
for(var i in frags) {
    frags[i] = frags[i].join(" ");
    hn.absorb(frags[i]);
}
console.log(hn.multiStepSearch(16));

var hmapFile = "// analysis of chord progressions bach uses in the riemenschneider\n" + frags.join("\n");
fs.writeFileSync("./resources/hmaps/bachChoralAbsoluteAnalysis.hmap", hmapFile);
