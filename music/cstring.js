function cstringExtract(cstring) {
    var ignore = /[ \n`|]+/;

    var things = cstring.split(ignore);
    var things2 = new Array();
    for(var i in things) {
        if(things[i] != "") {
            if( things[i][0].match(/[0-9]/) ) {
                things[i] = parseFloat(things[i]);
            }
            things2.push( things[i] );
        }
    }

    return things2;
}
function cstringFirst(cstring) {
    console.log("make cstringFirst() more efficient");
    return cstringExtract(cstring)[0];
}
function cstringLast(cstring) {
    console.log("make cstringLast() more efficient");
    var list = cstringExtract(cstring);
    return list[list.length-1];
}
function cstringPack(cstring, things) {
    var ignore = /[ \n`|]+/;
    var s = "";
    j = 0;
    for(var i=0; i<cstring.length; i++) {
        if( !cstring[i].match(ignore)
        && (cstring[i-1] == undefined || cstring[i-1].match(ignore)) ) {

            if(things[j] != undefined) {
                s += things[j];
            }
            j++;

        }
        if(cstring[i].match(ignore)) {
            s += cstring[i];
        }
    }

    return s;
}
function cstringSet(cstring, i, item) {
    var things = cstringExtract(cstring);

    var search;
    if(typeof things[i] == "number") {
        search = things[i];
        for(var j in things) {
            if(things[j] == search) {
                things[j] = item;
            }
        }
    } else {
        things[i] = item;
    }

    return cstringPack(cstring, things);
}
function cstringSetFirst(cstring, item) {
    return cstringSet(cstring, 0, item);
}
function cstringSetLast(cstring, item) {
    return cstringSet(cstring, cstringExtract(cstring).length-1, item);
}
function cstringDice(cstring, res) {
    // res = res || 1; // what is this "res" thing about??

    var bits = [];
    var ignore = /[ \n|]/;

    cstring = cstring.split("`").join("` ");
    while(cstring.indexOf(": ") != -1)
      cstring = cstring.split(": ").join(":")

    var things = cstring.split(ignore);


    for(var i in things) {
      var chevs = 0;
      var thing = things[i];
      thing = thing.split(":").join(": ")

      while ( thing.indexOf("`") != -1 ) {
        thing = thing.replace("`", "");
        chevs++;
      }

      if(thing == "")
        thing = bits[bits.length-1];
      else
        chevs++

      if(chevs > 0)
        bits.push(thing)

      for(var j=1; j<chevs; j++)
        bits.push(false)
    }

    /*var bits2 = new Array();
    for(var i=0; i<bits.length; i += res) {
        bits2.push(bits[i]);
    }*/

    return bits;
}
function cstringUndice(dicedCstring, res=1, metre=4) {
    var cstring = ""
    for(var i in dicedCstring) {
      if(dicedCstring[i] == dicedCstring[parseInt(i)-1])
        for(var j=0; j<res; j++)
          cstring += "`"
      else {
        if(!(/\s/m).test(cstring[cstring.length-1]) && cstring.length)
          cstring += ' '
        cstring += dicedCstring[i] + ' '
        for(var j=0; j<res-1; j++)
          cstring += "`"
      }

      if(((parseInt(i)+1)*res)%metre == 0)
        cstring += " |";
    }
    return cstring;
}
/*function cstringDouble(cstring) {
    var cstring = cstringDice(cstring, 1);
    cstring = cstringUndice(cstring, 2);
    return cstring;
}*/
function cstringBreakdown(cstring) {

    var ignore = /[ \n`|]+/;

    var bits = [""];

    var haveEncounteredSomething = false;

    for(var c=0; c<cstring.length; c++) {
        if(!cstring[c].match(ignore) && c>=1 && cstring[c-1].match(ignore) && haveEncounteredSomething) {
            bits.push("");
        }
        if(!cstring[c].match(ignore)) {
            haveEncounteredSomething = true;
        }
        bits[bits.length-1] += cstring[c];
    }


    //var sizeOptions = [3,4,5];
    var bitsizes = new Array();
    var n = bits.length;

    if(n < 3) {
        return [cstring];
    }

    console.log(n);

    while(n > 0) {
        if(n < 3) {
            console.log("Joel you've fucked up");
            break;
        }

        var d = Math.floor(3 + Math.random()*3);
        if(n-d >= 3) {
            n -= d;
            bitsizes.push(d);
        }
        if(n == d) {
            n -= d;
            bitsizes.push(d);
        }
    }

    console.log(bitsizes);

    var bigbits = new Array();

    var i = 0;
    for(var j in bitsizes) {
        bigbits.push( bits.slice(i, i+bitsizes[j]).join(""));
        i += bitsizes[j];
    }

    return bigbits;
}

function format(cstring, {barsPerLine=4}={}) {
  let bars = cstring.split(/\s*\|\s*/g)

  let lines = []
  for(let bar=0; bar<bars.length; bar+=barsPerLine)
    lines.push(bars.slice(bar, bar+barsPerLine).join(' | '))

  return lines.join(' |\n')
}

exports.extract = cstringExtract;
exports.first = cstringFirst;
exports.last = cstringLast;
exports.pack = cstringPack;
exports.set = cstringSet;
exports.setFirst = cstringSetFirst;
exports.setLast = cstringSetLast;
exports.dice = cstringDice;
exports.undice = cstringUndice;
//exports.double = cstringDouble;
exports.breakdown = cstringBreakdown;
exports.format = format
