var utility = require("./utility.js");
var VoiceTrack = require("./VoiceTrack.js");
var PointTrack = require("./PointTrack.js");
var StepTrack = require("./StepTrack.js");

exports.PointTrack_to_VoiceTrack = function(oldTrack) {
    var newTrack = new VoiceTrack();
    newTrack.label = oldTrack.label;
    newTrack.instrument = utility.duplicate(oldTrack.instrument);
    for(var i in oldTrack.notes) {
        var t = Math.floor(oldTrack.notes[i].t);
        newTrack.mix(oldTrack.notes[i].sound, t);
    }
    return newTrack;
}

exports.PointTrack_to_StepTrack = function(oldTrack) {
    var newTrack = new StepTrack().blank(oldTrack.d);
    newTrack.label = oldTrack.label;
    for(var i in oldTrack.notes) {
        var t = Math.floor(oldTrack.notes[i].t);
        var note = oldTrack.notes[i];
        newTrack.mix(note.sound, note.t);
    }
    return newTrack;
}

exports.StepTrack_to_PointTrack = function(oldTrack) {
    var newTrack = new PointTrack();
    newTrack.label = oldTrack.label;

    var step, t, tOff;
    var sounding = [];
    for(var i=0; i<oldTrack.steps.length; i++) {
        step = oldTrack.steps[i];
        t = i;
        tOff = i+1;

        if(step == 'true' || step == 'false') {
            console.log("step.sound = BOOL but a string, could be problems here!");
        }

        if(step == false) {
            for(var j in sounding) {
                sounding[j].tOff = tOff;
            }
            continue;
        } else {
            while(sounding.length) {
                var note = sounding.shift();
                note.tOff = t;
            }
            if(step == undefined) {
                continue;
            }

            if(step.constructor == Number
                || step.constructor == String
                || step.constructor == Boolean) {
                var note = new PointTrack.Note();
                note.t = t;
                note.tOff = tOff;
                note.sound = step;
                newTrack.notes.push(note);
                sounding.push(note);
                continue;
            }
            if(step.constructor == Array) {
                for(var j in step) {
                    var note = new PointTrack.Note();
                    note.t = t;
                    note.tOff = tOff;
                    note.sound = step[j];
                    newTrack.notes.push(note);
                    sounding.push(note);
                }
                continue;
            }
            if(step.isAnArpNote) {
                var note = new PointTrack.Note();
                note.t = t;
                note.tOff = tOff;
                note.sound = new step.constructor(step);
                newTrack.notes.push(note);
                sounding.push(note);
                continue;
            }
            console.log("StepTrack_to_PointTrack:: unknown sound type", step);
        }
    }

    newTrack.checkNotesAreSorted();
    return newTrack;
}
