var PointTrack = require("./PointTrack.js");

function ChordTrack(model) {
    PointTrack.call(this, model);
}
ChordTrack.prototype = Object.create(PointTrack.prototype);
ChordTrack.prototype.constructor = ChordTrack;
module.exports = ChordTrack;

ChordTrack.prototype.isChordTrack = true

ChordTrack.Note = PointTrack.Note
