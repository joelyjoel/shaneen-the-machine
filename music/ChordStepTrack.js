var StepTrack = require("./StepTrack.js");
var Chord = require("./Chord.js");
var cstring = require("./cstring.js");

function ChordTrack(model) {
    StepTrack.call(this, model);

    if(model && model.constructor == String)
        this.print = model;
}
ChordTrack.prototype = Object.create(StepTrack.prototype);
ChordTrack.prototype.constructor = ChordTrack;
module.exports = ChordTrack;

ChordTrack.prototype.randomise = function(d) {
    if(d != undefined) {
        this.blank(d);
    }

    for(var t=0; t<this.steps.length; t++) {
        if(Math.random() < 0.50) {
            this.steps[t] = new Chord().randomise();
        } else {
            this.steps[t] = false;
        }
    }
    return this;
}

ChordTrack.prototype.__defineSetter__("print", function(s) {
    var steps = cstring.dice(s);
    for(var i=steps.length-1; i>0; i--) {
        if(steps[i] == steps[i-1])
            steps[i] = false;
    }
    for(var i in steps)
        if(steps[i] && steps[i].constructor == String)
            steps[i] = new Chord(steps[i]);

    this.steps = steps;
});
