const PointTrack = require("./PointTrack.js")

function rhythmify(soundTrack, rhythmTrack) {
  var newTrack = new PointTrack()
  for(var i in rhythmTrack.notes) {
    var newNote = new PointTrack.Note(rhythmTrack.notes[i])
    newNote.sound = soundTrack.soundsAtT(newNote.t)
    newTrack.notes.push(newNote)
  }
  newTrack.label = "rhythmified"
  newTrack.d = rhythmTrack.d
  return newTrack
}
module.exports = rhythmify
