const bach = require('./index.js')
const cstring = require('../cstring')

let harmonicRhythmChunks = [
  '? ```', '? ` ? `',
]

function make_harmonicRhythm(nBars) {
  let loop = []
  for(let i=0; i<nBars; i++)
    loop.push(harmonicRhythmChunks[Math.floor(Math.random()*harmonicRhythmChunks.length)])
  loop = loop.join('| ')

  return loop
}

const randomChoose = (...choices) => {
  return choices[Math.floor(Math.random()*choices.length)]
}

function make_section(
  nBars=randomChoose(8, 12, 16, 20),
  loopSize=randomChoose(2,4,8),
  turnAroundSize=Math.floor(Math.random()*12)/2
) {
  let loop = make_harmonicRhythm(loopSize)

  let nChordsInLoop = (loop.match(/\?/g)||[]).length
  let loopChords = bach.loop(nChordsInLoop)

  loop = cstring.pack(loop, loopChords)

  let loopDiced = cstring.dice(loop)

  let section = new Array(nBars*4)
  let loopedDuration = (nBars-turnAroundSize)*4
  for(let b=0; b<loopedDuration; b++)
    section[b] = loopDiced[b%loopDiced.length]

  let turnAround = make_harmonicRhythm(turnAroundSize)
  let nChordsInTurnAround = cstring.extract(turnAround).length
  let turnAroundChords = bach.markov.connect(
    section[loopedDuration],
    nChordsInTurnAround,
    section[0],
    false
  )
  turnAround = cstring.pack(turnAround, turnAroundChords)
  let turnAroundDiced = cstring.dice(turnAround)
  for(let b=0; b<turnAroundSize*4; b++)
    section[loopedDuration+b] = turnAroundDiced[b]

  return cstring.format(cstring.undice(section, 1, 4))
}
module.exports = make_section
