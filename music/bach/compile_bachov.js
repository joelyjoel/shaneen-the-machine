const bach = require('./bach')
const fs = require('fs')

let table1 = {}
let table2 = {}
let allChords = []

for(let fragment of bach.allChordProgressionFragments()) {
  if(!allChords.includes(fragment[0]))
    allChords.push(fragment[0])

  for(let i=1; i<fragment.length; i++) {
    let A = fragment[i-1] || "-"
    let B = fragment[i] || '-'

    if(!table1[A])
      table1[A] = [B]
    else if(!table1[A].includes(B))
      table1[A].push(B)

    if(!table2[B])
      table2[B] = [A]
    else if(!table2[B].includes(A))
      table2[B].push(A)

    if(!allChords.includes(B))
      allChords.push(B)
  }
}

console.log(table1)
fs.writeFileSync('./music/bach/bachov.markov.json', JSON.stringify({
  forward: table1,
  backward: table2,
  all: allChords,
}))
