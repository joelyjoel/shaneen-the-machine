const fs = require("fs")
const midi = require("../midi.js")
const findChordProgressionFragments = require('../findChordProgressionFragments')

const dir = "./resources/bach-chorales/";
const dirContents = fs.readdirSync(dir);
const midiFileRegex = /\.mid$/
const midiFiles = dirContents.filter(filename => midiFileRegex.test(filename))
  .map(filename => dir + filename);


function randomBachChorale(supressConsole=true) {
  var filename = midiFiles[Math.floor(Math.random()*midiFiles.length)]
  if(!supressConsole)
    console.log("chose:", filename)
  return midi.parseMidiFile(filename, supressConsole)
}

function* allBachChorales() {
  // yield all bach chorales in order
  for(let filepath of midiFiles) {
    console.log(filepath)
    yield midi.parseMidiFile(filepath, true)
  }
}

function* allBachChordProgressionFragments() {
  for(let chorale of allBachChorales()) {
    let mixedDown = chorale.mixDownNumberedTracks()
    for(let fragment of findChordProgressionFragments(mixedDown))
      yield fragment
  }
}

function* findLoopsInChordProgression(chords, n=null) {
  for(var i=0; i<chords.length; i++) {
    var indexOfRepetition = chords.indexOf(chords[i], i+1)
    if(indexOfRepetition != -1 && (!n || n == indexOfRepetition-i))
      yield chords.slice(i, indexOfRepetition)
  }
}

function* findConnectionsInChordProgression(chords, from=null, to=null, n=null) {
  // generate a list of chord progressions found in chords which begin with
  // from and end with to, in n progresssions. If n is null, any number of steps
  // may be used
  if(from) {
    for(
      let i=chords.indexOf(from);
      i!=-1 && (!n || i+n+1<chords.length);
      i=chords.indexOf(from, i+1)
    ) {
      if(n) {
        if(chords[i+n+1] == to || !to)
          // from and n are defined, to either matches or is undefined
          yield chords.slice(i+1, i+n+1)

      } else if(to) {
        // from and to are defined, but not n
        for(let j=chords.indexOf(to, i+1); j!=-1; j=chords.indexOf(to, j+1)) {
          yield chords.slice(i+1, j)
        }
      } else {
        // only from is defined, yield all the chord progressions beginning with from
        throw 'findConnectionsInChordProgression expects at least 3 arguments'
      }
    }
  } else {
    // from is undefined
    if(to) {
      for(let i=chords.indexOf(to, n); i!=-1; i=chords.indexOf(to, i+1)) {
        if(n)
          // to and n are defined
          yield chords.slice(i-n, i)
        else
          throw 'findConnectionsInChordProgression expects at least 3 arguments'
      }
    } else
      throw 'findConnectionsInChordProgression expects at least 3 arguments'
  }
}

function randomBachLoop(n) {
  // randomly find a chord progression of length, n, which loops back on itself
  let tries = 0
  do {
    let chorale = randomBachChorale()
    let frags = findChordProgressionFragments(chorale.mixDownNumberedTracks())

    // list all the loops in this chorale
    let loops = []
    for(let frag of frags)
      for(let loop of findLoopsInChordProgression(frag, n)) {
        let rotationIndex = Math.floor(Math.random() * loop.length)
        loops.push( [...loop.slice(rotationIndex), ...loop.slice(0, rotationIndex)])
      }

    // load all the loops first, otherwise you always get the first loop
    if(loops.length)
      return loops[Math.floor(Math.random()*loops.length)]

  } while(++tries < 100)
}

function randomConnection(from, to, n) {
  // randomly find a chord progression of length n which connects from to to
  if(from instanceof Array)
    from = from[from.length-1]
  if(to instanceof Array)
    to = to[0]

  let tries = 0
  do {
    let chorale = randomBachChorale()
    let frags = findChordProgressionFragments(chorale.mixDownNumberedTracks())

    let solutions = []
    for(let frag of frags)
      // pg abreviates prord gopession
      for(let pg of findConnectionsInChordProgression(frag, from, to, n))
        solutions.push(pg)

    // load all the solutions first, otherwise you always get the first one
    if(solutions.length)
      return solutions[Math.floor(Math.random()*solutions.length)]

  } while(++tries < 100)

  return null
}

module.exports = {
  random: randomBachChorale,
  all: allBachChorales,
  allChordProgressionFragments: allBachChordProgressionFragments,
  loop: randomBachLoop,
  connect: randomConnection,
  markov: require('./bachov')
}
