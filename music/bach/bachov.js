// functions using a markov model of bach's chord progressions
const {forward, backward, all} = require('./bachov.markov.json')

/*function getSolutions(from, to) {
  if(from && to) {
    let forwardOptions = forward[from] || []
    let backwardOptions  = backward[to] || []
    return forwardOptions.filter(chord => backwardOptions.includes(chord))
  } else if(from)
    return forward[from] || []
  else if(to)
    return backward[to] || []
  else
    return all
}*/

function getSolutions(from, to) {
  let toIntersect = []

  if(from instanceof String)
    toIntersect.push(forward[from] || [])
  if(from instanceof Array)
    toIntersect.push(
      ...from.filter(o=>o instanceof String).map(chord => forward[chord])
    )

  if(to instanceof String)
    toIntersect.push(backward[to] || [])
  if(to instanceof Array)
    toIntersect.push(
      ...to.filter(o=>o instanceof String).map(chord => backward[chord])
    )

  if(toIntersect.length) {
    let set = toIntersect[0]
    for(let i=1; i<toIntersect.length; i++)
      set = set.filter(chord => toIntersect[i].includes(chord))
    return set
  } else
    return all.slice()
}

function getSolution(from, to) {
  let options = getSolutions(from, to)
  return options[Math.floor(Math.random()*options.length)]
}

function randomChord() {
  return all[Math.floor(Math.random()*all.length)]
}

function walk(n=4, chords=[]) {
  for(let i=0; i<n; i++)
    chords.push(getSolution(chords[chords.length-1]))
  return chords
}

function check(A, B) {
  return forward[A].includes(B)
}

function connect(from, n=1, to, loop=true) {
  let meetingIndex = Math.floor(Math.random()*n)

  let sequence = new Array(n).fill(null)
  sequence[-1] = from
  sequence[n] = to

  let todo = n
  while(todo) {
    let i = Math.floor(Math.random() * n)

    let solution = getSolution(
      i>1   ? sequence[i-1] : (loop ? [from, sequence[0]] : from),
      i<n-1 ? sequence[i+1] : (loop ? [to, sequence[n-1]] : to),
    )

    if(solution && !sequence[i])
      todo--

    sequence[i] = solution
  }

  return sequence.slice(0, n)
}

module.exports = {
  solutions: getSolutions,
  solution: getSolution,
  random: randomChord,
  walk: walk,
  connect: connect,
}
