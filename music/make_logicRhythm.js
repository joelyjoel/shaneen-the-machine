const logic = require("logicalexpressions")
const PointTrack = require("./PointTrack.js")

function make_logicRhythm(expression, d) {
  d = d || Math.pow(2, expression.largestLetterId + 1)
  var structure = new logic.Structure()

  var binary = []
  for(var t=0; t<d; t++) {
    binary[t] = expression.evaluate(structure)
    structure.next()
  }

  console.log("rhythm:", binary.map((bit) => {
    return bit ? "█" : "-"
  }).join(""))

  var track = PointTrack.fromSteps(binary, 1).mapSounds({true: 60})

  track.label = expression.print()
  return track
}
module.exports = make_logicRhythm
