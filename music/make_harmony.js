function make_chords(n, hn, key) {
    key = key || Scale.random();
    if(!key.isScale) key = new Scale(key);

    var numerals = make_numerals(n, hn);
    var chords = [];
    for(var i in numerals) {
        chords[i] = key.numeralToChord(numerals[i]);
    }
    return chords;
}

function make_numerals(n, hn) {
    hn = hn || "./resources/hmaps/tonality.hmap";
    //hn = hn || "./resources/hmaps/bachChoralAbsoluteAnalysis.hmap"
    if(!hn.isAHarmonyNetwork)
        hn = new HarmonyNetwork(hn);
    var chords = hn.multiStepSearch(n);
    return chords;
}

function make_chordTrack(d, rhythm, hn, key) {
    console.log("calling make_chordTrack")
    var track = new ChordTrack();
    rhythm = rhythm || 8;
    if(rhythm.constructor == Number)
        rhythm = make_rhythm.basic(d, rhythm);

    var n = rhythm.notes.length;
    var chords = make_chords(n);
    for(var i in rhythm.notes) {
        var note = new ChordTrack.Note(rhythm.notes[i]);

        note.sound = chords[i];
        track.notes.push(note);
    }
    track.label = "chord progression";
    return track;
}

function make_harmony(d, rhythm, hn, key) {
    console.log("calling make_harmony");
    key = key || Scale.randomMajor();
    if(key.constructor == String) {
        key = new Scale(key);
    }
    rhythm = rhythm || 8;
    if(rhythm.constructor == Number)
        rhythm = make_rhythm.basic(d, rhythm);

    var track = new HarmonyTrack();
    track.chords = make_chordTrack(d, rhythm, hn, key);
    track.scales = new ScaleTrack();
    var note = new ScaleTrack.Note();
    note.t = 0;
    note.sound = key;
    track.scales.notes = [note];
    return track;
}

module.exports = make_harmony;
make_harmony.make_chords = make_chords;
make_harmony.numerals = make_numerals;
make_harmony.track = make_chordTrack;

var HarmonyNetwork = require("./HarmonyNetwork.js");
var HarmonyTrack = require("./HarmonyTrack.js");
var Scale = require("./Scale.js");
var ChordTrack = require("./ChordTrack.js");
var ScaleTrack = require("./ScaleTrack.js");
var make_rhythm = require("./make_rhythm.js");
