const MelodyProblem = require("./MelodyProblem.js")
const solve = require("./solve.js")

MelodyProblem.solve = solve

module.exports = MelodyProblem
