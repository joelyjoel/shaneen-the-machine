const WeightedSet = require("../WeightedSet.js")
const PitchRange = require("../PitchRange.js")
const Filters = require("./filters.js")
const MelodyProblemConfigs = require("./configs.js")
const MelodyProblem = require("./MelodyProblem.js")

function solveMelodyProblem(problem, config) {
  if(config && config.constructor == String)
    config = MelodyProblemConfigs[config]
  config = Object.assign({}, MelodyProblemConfigs.default, config)

  var workingSet = config.initialRange.pitchSet

  for(var i in config.filters) {
    var filterName = config.filters[i]
    var filter = Filters[filterName]
    var newWorkingSet = new WeightedSet()
    for(var p in workingSet.items) {
      p = parseFloat(p)
      var newWeight = workingSet.items[p] * filter(p, problem, config)
      if(newWeight)
        newWorkingSet.increment(p, newWeight)
    }
    workingSet = newWorkingSet
  }
  //console.log(workingSet)

  return workingSet
}

function solveTrackGroup(group, config, nTries) {
  nTries = nTries || 1
  var numberedTracks = group.numberedTracks
  var notesToSolve = []
  for(var c in numberedTracks) {
    for(var i in numberedTracks[c].notes) {
      var note = numberedTracks[c].notes[i]
      if(note.sound == "?")
        notesToSolve.push(note)
    }
  }

  do {
    notesToSolve.sort(() => {return Math.random()*2-1})
    var failureCount = 0

    for(var i in notesToSolve) {
      var problem = new MelodyProblem(notesToSolve[i], group)
      var solutionSet = solveMelodyProblem(problem, config)
      var solution = solutionSet.roll()
      if(solution)
        notesToSolve[i].sound = solution
      else
        if(notesToSolve[i].sound == "?")
          failureCount++
    }
    nTries--
  } while(nTries > 0 && failureCount > 0)
}

function solveTrack(track, context, config, nTries) {
  nTries = nTries || 1

  var notesToSolve = []
  for(var i in track.notes) {
    if(track.notes[i].sound == "?")
      notesToSolve.push( track.notes[i] )
  }

  do {
    notesToSolve.sort( () => {return Math.random()*2-1} )
    var failureCount = 0
    for(var i in notesToSolve) {
      var problem = new MelodyProblem(notesToSolve[i], context)
      var solutionSet = new solveMelodyProblem(problem, config)
      var solution = solutionSet.roll()
      if(solution)
        notesToSolve[i].sound = solution
      else
        if(notesToSolve[i].sound == "?")
          failureCount++
    }
    nTries--
  } while(nTries > 1 && failureCount > 0)
}

module.exports.melodyProblem = solveMelodyProblem
module.exports.trackGroup = solveTrackGroup
module.exports.track = solveTrack
