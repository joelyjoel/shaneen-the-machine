const PitchRange = require("../PitchRange.js")

exports.default = {
  filters: [
    "harmonyNote",
    "instrumentRange",
    "melodicIntervalBellCurve",
    "parallels",
    "maxMelodicInterval",
    "instrumentRangeBellCurve",
    "banHorizontalUnison",
    "banVerticalUnison",
    "trackAveragePitchBellCurve",
    "scoreMelodicIntervals",
    "dontDoubleThird",
  ],
  initialRange: new PitchRange(48,90),
  maxMelodicInterval: 6,
  melodicIntervalVariance: 0.1,
  forbiddenParallels: [0, 7],
  melodicIntervalScores: {
    '-6': 0,
    6: 0,
    11: 0,
    '-11': 0,
  },
  useIntervalClassesForCheckingParallels: true,
  instrumentRangeVarianceScaleFactor: 1,
  trackAveragePitchVariance: 5,
}

exports.bass = {
  filters: [
    "bassNote",
    "melodicIntervalBellCurve",
    "trackAveragePitchBellCurve",
  ],
  initialRange: new PitchRange(24, 48),
}
