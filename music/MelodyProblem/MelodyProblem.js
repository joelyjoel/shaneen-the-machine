function MelodyProblem(note, context, harmony, loop) {
  // note is a PointTrack.Note
  if(loop == undefined)
    loop = true

  this.tNow = note.t

  if(context.isTrackGroup) {
    this.contextTracks = context.numberedTracks
    if(context.tracks.harmony && !harmony) {
      harmony = context.tracks.harmony
    }
  }
  else if (context.constructor == Array)
    this.contextTracks = context

  if(harmony) {
    if(harmony.isHarmonyTrack) {
      this.harmonyTrack = harmony
      this.chordTrack = harmony.chords
      this.scaleTrack = harmony.scales
    } else if(harmony.isChordTrack) {
      this.chordTrack = harmony
    }
  }

  for(var c in this.contextTracks)
    if(this.contextTracks[c].notes.indexOf(note) != -1)
      this.channel = c
  if(this.channel == undefined) {
    console.log("can't find note in context:", note, context)
    throw "can't find note in context"
  }
  this.mainTrack = this.contextTracks[this.channel]
  this.noteI = this.mainTrack.notes.indexOf(note)

  var trackAverageP = this.mainTrack.averagePitch
  if(trackAverageP)
    this.trackAverageP = trackAverageP

  if(this.mainTrack.instrument) {
    this.instrument = this.mainTrack.instrument
    if(this.instrument.range)
      this.instrumentRange = this.instrument.range
  }

  // find previouse note
  this.soundPre = []
  for(var i=this.noteI-1; i>=0; i--) {
    var candidateNote = this.mainTrack.notes[i]
    if(candidateNote.t < note.t && (!candidateNote.tOff || candidateNote.tOff <= note.t)) {
      this.tPre = candidateNote.t
      this.notePre = candidateNote
      this.soundPre = candidateNote.sound
      break
    }
  }
  if(this.notePre == undefined && loop) {
    var lastNote = this.mainTrack.lastNote
    if(note != lastNote) {
      this.tPre = lastNote.t
      this.notePre = lastNote
      this.soundPre = lastNote.sound
    }
  }
  if(!this.soundPre || this.soundPre.constructor != Array)
    this.soundPre = [this.soundPre]

  // find next note
  this.soundNex = []
  for(var i=this.noteI+1; i<this.mainTrack.notes.length; i++) {
    var candidateNote = this.mainTrack.notes[i]
    if(candidateNote.t > note.t && (!note.tOff || note.tOff <= candidateNote.t)) {
      this.tNex = candidateNote.t
      this.noteNex = candidateNote
      this.soundNex = candidateNote.sound
      break
    }
  }
  if(this.noteNex == undefined && loop) {
    var firstNote = this.mainTrack.firstNote
    if(note != firstNote) {
      this.tNex = firstNote.t
      this.noteNex = firstNote
      this.soundNex = firstNote.sound
    }
  }
  if(!this.soundNex || this.soundNex.constructor != Array)
    this.soundNex = [this.soundNex]

  // find harmonies
  this.harmonyPre = []
  this.harmonyNow = []
  this.harmonyNex = []
  var a, b, c
  for(var c in this.contextTracks) {
    a = this.contextTracks[c].soundsAtT(this.tPre)
    if(c != this.channel)
      b = this.contextTracks[c].soundsAtT(this.tNow)
    else
      b = ["current_test"]
    c = this.contextTracks[c].soundsAtT(this.tNex)

    if(a.length == 0)
      a = [undefined]
    if(b.length == 0)
      b = [undefined]
    if(c.length == 0)
      c = [undefined]

    for(var i in a)
      for(var j in b)
        for(var k in c){
          this.harmonyPre.push(a[i])
          this.harmonyNow.push(b[j])
          this.harmonyNex.push(c[k])
        }
  }

  // find chords
  this.chordPre = this.chordTrack.sound(this.tPre)
  this.chordNow = this.chordTrack.sound(this.tNow)
  this.chordNex = this.chordTrack.sound(this.tNex)
}
module.exports = MelodyProblem

MelodyProblem.prototype.intervalPre = function(p) {
  var intervals = []
  for(var i in this.soundPre) {
    if(this.soundPre[i] && this.soundPre[i].constructor == Number)
      intervals.push( p-this.soundPre[i] )
  }
  return intervals
}
MelodyProblem.prototype.intervalNex = function(p) {
  var intervals = []
  for(var i in this.soundNex) {
    if(this.soundNex[i] && this.soundNex[i].constructor == Number)
      intervals.push( this.soundNex-p )
  }
  return intervals
}
