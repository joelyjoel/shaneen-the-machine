/*
  FILTERS TO MAKE:
    - gausian from track average p
    - notLowestP, lowestP, notHighestP, etc.
    - instrument range bell curve
*/

const pitch = require("../pitch.js")
const gauss = require("./gauss.js")


var filters = {}
module.exports = filters

filters.harmonyNote = function(p, problem, config) {
  if(!problem.chordNow)
    return true
  return problem.chordNow.checkPitch(p)
}

filters.melodicIntervalBellCurve = function(p, problem, config) {
  var intervalPre = problem.intervalPre(p)
  var intervalNex = problem.intervalNex(p)
  var weight = 0
  var n = 0
  for(var i in intervalPre) {
    weight += gauss(intervalPre[i], 0, config.melodicIntervalVariance)
    n++
  }
  for(var i in intervalNex) {
    weight += gauss(intervalNex[i], 0, config.melodicIntervalVariance)
    n++
  }
  if(n == 0)
    return 1
  else return weight / n
}

filters.maxMelodicInterval = function(p, problem, config) {
  var a = 0
  var b = 0
  var intervalPre = problem.intervalPre(p)
  var intervalNex = problem.intervalNex(p)
  for(var i in intervalPre) {
    if(Math.abs(intervalPre[i]) < config.maxMelodicInterval)
      a++
  }
  if(intervalPre.length == 0)
    a = 1
  for(var i in intervalNex) {
    if(Math.abs(intervalNex[i]) < config.maxMelodicInterval)
      b++
  }
  if(intervalNex.length == 0)
    b = 1
  return a/(intervalPre.length || 1) * b/(intervalNex.length || 1)
}

filters.parallels = function(p, problem, config) {
  if(problem.soundPre.length == 0 && problem.soundNex.length == 0)
    return 1

  var parallelCount = 0
  for(var c in problem.harmonyNow){
    if(problem.harmonyNow[c] == "current_test" || !problem.harmonyNow[c] || problem.harmonyNow[c].constructor != Number)
      continue

    var interval = problem.harmonyNow[c]-p

    if(config.useIntervalClassesForCheckingParallels)
      var intervalClass = Math.abs(interval)%12
    else
      var intervalClass = Math.abs(interval)

    if(config.forbiddenParallels.indexOf(intervalClass) != -1) {
      for(var i in problem.soundPre) {
        if(!problem.soundPre[i] || problem.soundPre[i].constructor != Number
        || !problem.harmonyPre[c] || problem.harmonyPre[c].constructor != Number)
          continue
        var intervalPre = problem.harmonyPre[c]-problem.soundPre[i]
        if(intervalPre == interval)
          parallelCount++
      }
      for(var i in problem.soundNex) {
        if(!problem.soundNex[i] || problem.soundNex[i].constructor != Number
        || !problem.harmonyNex[c] || problem.harmonyNex[c].constructor != Number)
          continue
        var intervalNex = problem.harmonyNex[c] - problem.soundNex[i]
        if(intervalNex == interval)
          parallelCount++
      }
    }
  }

  if(parallelCount == 0)
    return 1
  else return 0
}

filters.bassNote = function(p, problem, config) {
  if(!problem.chordNow)
    return 1
  return pitch.pc(p) == problem.chordNow.bassPc
}

filters.instrumentRange = function(p, problem, config) {
  if(problem.instrumentRange)
    return problem.instrument.range.checkPitch(p)
  else
    return 1
}
filters.instrumentRangeBellCurve = function(p, problem, config) {
  if(problem.instrumentRange)
    return gauss(
      p,
      problem.instrumentRange.middle,
      config.instrumentRangeVarianceScaleFactor * problem.instrumentRange.width
    )
  else
    return 1
}
filters.banHorizontalUnison = function(p, problem, config) {
  var intervalPre = problem.intervalPre(p)
  for(var i in intervalPre)
    if(intervalPre[i] == 0)
      return 0
  var intervalNex = problem.intervalNex(p)
  for(var i in intervalNex)
    if(intervalNex[i] == 0)
      return 0
  return 1
}
filters.banVerticalUnison = function(p, problem, config) {
  for(var i in problem.harmonyNow)
    if(problem.harmonyNow[i] == p)
      return 0
  return 1
}

filters.trackAveragePitchBellCurve = function(p, problem, config) {
  if(problem.trackAverageP)
    return gauss(p, problem.trackAverageP, config.trackAveragePitchVariance)
  else
    return 1
}
filters.scoreMelodicIntervals = function(p, problem, config) {

  var weight = 1

  var intervalPre = problem.intervalPre(p)
  for(var i in intervalPre)
    weight *= config.melodicIntervalScores[intervalPre[i]] || 1

  var intervalNex = problem.intervalNex(p)
  for(var i in intervalNex)
    weight *= config.melodicIntervalScores[intervalNex[i]] || 1

  return weight
}

filters.dontDoubleThird = function(p, problem, config) {
  if(problem.chordNow)
    var third = problem.chordNow.third
  if(pitch.pc(p) == third)
    for(var c in problem.harmonyNow)
      if(problem.harmonyNow[c]) {
        var pc = pitch.pc(problem.harmonyNow[c])
        if(pc == third)
          return 0
      }

  return 1
}
