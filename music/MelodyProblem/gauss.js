function f(x, mean, variance) {
  var y = 1/Math.sqrt(2 * Math.PI * variance) * Math.exp(-Math.pow(x-mean, 2)/(2*variance))
  return y
}
module.exports = f
