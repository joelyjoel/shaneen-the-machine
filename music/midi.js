var fs = require("fs");
var midiConverter = require("midi-converter");
var midiRef = require("./midiRef.js");
var PointTrack = require("./PointTrack.js");
var MeterTrack = require("./MeterTrack.js");
var TrackGroup = require("./TrackGroup.js");

parseJsonMidiTrack = function(events, header, meterTrack, supressConsole) {
    if(meterTrack == undefined) {
        meterTrack = new MeterTrack();
    }

    var t = 0;
    var hangingNotes = {};
    var newTrack = new PointTrack();
    for(var i in events) {
        var event = events[i];
        t += event.deltaTime/header.ticksPerBeat * 4;
        if(event.type == "meta") {
            if(event.subtype == "endOfTrack") {
                break;
            } else if(event.subtype == "trackName") {
                newTrack.label = event.text;
                continue;
            } else if(event.subtype == "setTempo") {
                // event.microsecondsPerBeat
                var note = new PointTrack.Note();
                note.t = t;
                note.bpm = 60000000/event.microsecondsPerBeat;
                meterTrack.tracks.tempo.mix(note);
            } else {
                if(!supressConsole)
                  console.log("unknown midi meta event subtype", event.subtype, ":\n", event);
                continue;
            }
        } else if(event.type == "channel") {
            if(event.subtype == "noteOn") {
                var newNote = new PointTrack.Note();
                newNote.t = t;
                newNote.sound = event.noteNumber;
                hangingNotes[newNote.sound] = newNote;
                newTrack.notes.push(newNote);
            } else if(event.subtype == "noteOff") {
                if(hangingNotes[event.noteNumber]) {
                    hangingNotes[event.noteNumber].tOff = t;
                    hangingNotes[event.noteNumber] = undefined;
                }
            } else if(event.subtype == "programChange") {
                if(newTrack.instrument) {
                    console.log( "instrument assignment conflict. midi track contains multiple program changes" );
                }
                newTrack.instrument = midiRef.programNames[event.programNumber];
            } else {
              if(!supressConsole)
                console.log("unknown midi event subtype", event.subtype, ":\n", event);
            }
        }
    }
    return newTrack;
}

parseJsonMidiFile = function parseJsonMidiFile(file, supressConsole) {
    var meterTrack = new MeterTrack();
    var result = new TrackGroup();
    var trackN = 0;
    for(var i in file.tracks) {
      var track = parseJsonMidiTrack(file.tracks[i], file.header, meterTrack, supressConsole);
      if(track.notes.length == 0) {
        if(!supressConsole)
          console.log("skipped track", i, "because it is empty")
        continue;
      }
      result.tracks[trackN] = track
      trackN++
    }
    result.tracks.meter = meterTrack;

    result.checkNotesAreSorted();
    return result;
}

function parseMidiFile(filename, supressConsole) {
    var file = fs.readFileSync(filename, "binary");
    var json = midiConverter.midiToJson(file);
    return parseJsonMidiFile(json, supressConsole);
}

PointTrack_to_jsonMidiTrack = function(track, header) {
    track = new PointTrack(track);
    var events = [];
    for(var i in track.notes) {
        var noteOn = {
            "absoluteTime":Math.round(track.notes[i].t * header.ticksPerBeat/4),
            "noteNumber": track.notes[i].sound,
            "velocity": 60,
            "channel": 0,
            "type": "channel",
            "subtype": "noteOn"
        }
        events.push(noteOn);

        var noteOff = {
            "absoluteTime":Math.round(track.notes[i].tOff * header.ticksPerBeat/4),
            "noteNumber": track.notes[i].sound,
            "velocity": 60,
            "type": "channel",
            "subtype": "noteOff"
        }
        if(track.notes[i].d == undefined)
            noteOff.absoluteTime = noteOn.absoluteTime + 1;
        events.push(noteOff);
    }
    events = events.sort(function(a, b) {
        return a.absoluteTime - b.absoluteTime;
    });

    var lastT = 0;
    for(var i in events) {
        events[i].deltaTime = events[i].absoluteTime-lastT;
        lastT = events[i].absoluteTime;
        delete events[i].absoluteTime;
    }
    events.push({
        "deltaTime": 0,
        "type": "meta",
        "subtype": "endOfTrack"
    })
    return events;
}
PointTrack_to_jsonMidiFile = function(oldTrack) {
    var header = {
        "formatType": 0,
        "trackCount": 1,
        "ticksPerBeat": 128
    }
    var tracks = [PointTrack_to_jsonMidiTrack(oldTrack, header)];
    //[oldTrack.toJsonMidiTrack(header)];
    return {
        "header": header,
        "tracks": tracks
    }
}
PointTrack_saveMidiFile = function(oldTrack, filename) {
    if(filename == undefined) {
        filename = this.label + ".mid";
    }
    var json = PointTrack_to_jsonMidiFile(oldTrack);
    var file = midiConverter.jsonToMidi(json);
    fs.writeFileSync(filename, file, "binary");
    console.log("saved", filename);
}

TrackGroup_to_jsonMidiFile = function(group){
    group = new TrackGroup(group);
    for(var i in group.tracks) {
        if(!group.tracks[i].isAPointTrack) {
            group.tracks[i] = group.tracks[i].toPointTrack();
        }
    }

    var header = {
        "formatType": 0,
        "trackCount": group.tracks.length,
        "ticksPerBeat": 128
    }

    var tracks = [];
    for(var i in group.tracks) {
        var midiTrack = PointTrack_to_jsonMidiTrack(group.tracks[i], header);
        tracks.push(midiTrack);
    }
    return {
        "header": header,
        "tracks": tracks
    }
}

TrackGroup_saveMidiFile = function(group, filename) {
    if(!filename) throw "filename not provided";

    var jsonMidi = TrackGroup_to_jsonMidiFile(group);
    var file = midiConverter.jsonToMidi(jsonMidi);
    fs.writeFileSync(filename, file, "binary");
    console.log("saved:", filename);
}

exports.parseJsonMidiTrack = parseJsonMidiTrack;
exports.parseJsonMidiFile = parseJsonMidiFile;
exports.parseMidiFile = parseMidiFile;
exports.PointTrack_to_jsonMidiTrack = PointTrack_to_jsonMidiTrack;
exports.PointTrack_to_jsonMidiFile = PointTrack_to_jsonMidiFile;
exports.PointTrack_saveMidiFile = PointTrack_saveMidiFile;
exports.TrackGroup_to_jsonMidiFile = TrackGroup_to_jsonMidiFile;
exports.TrackGroup_saveMidiFile = TrackGroup_saveMidiFile;
