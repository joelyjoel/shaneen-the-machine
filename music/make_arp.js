var PointTrack = require("./PointTrack.js");
var Arp = require("./Arp.js");

exports.tranceyThing = function(d) {
    d = d || 16;

    var loop = new PointTrack();
    loop.quick(2, Arp.make_q("absolute-low"));
    loop.quick(1,
        Arp.make_q("harmonyNote"),
        Arp.make_q("stepApproach")
    );
    return loop.loop(d);
}

exports.riff = function(d, riffD) {
    d = d || 16;
    riffD = riffD || 16;
    var loop = new PointTrack().blank(riffD);
    loop.qPoke(Math.random()*riffD);
    return loop.loop(d);
}
