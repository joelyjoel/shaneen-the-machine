const TrackGroup = require("./TrackGroup.js");
const cstring = require("./cstring.js")
const Chord = require("./Chord.js")
const Scale = require("./Scale.js")
const ChordTrack = require("./ChordTrack.js")
const ScaleTrack = require("./ScaleTrack.js")

function HarmonyTrack(model, chevD) {
  if(model && model.constructor == String)
    model = HarmonyTrack.parseString(model, chevD)
  TrackGroup.call(this, model)
}
HarmonyTrack.prototype = Object.create(TrackGroup.prototype);
HarmonyTrack.prototype.constructor = HarmonyTrack;
module.exports = HarmonyTrack;

HarmonyTrack.prototype.isHarmonyTrack = true

HarmonyTrack.prototype.__defineGetter__("chords", function() {
    return this.tracks.chords;
});
HarmonyTrack.prototype.__defineSetter__("chords", function(chords) {
    this.tracks.chords = chords;
});

HarmonyTrack.prototype.__defineGetter__("scales", function() {
    return this.tracks.scales;
});
HarmonyTrack.prototype.__defineSetter__("scales", function(scales) {
    this.tracks.scales = scales;
});

HarmonyTrack.prototype.scale = function(t) {
    if(this.tracks.scales) {
        var note = this.tracks.scales.sound(t);

        if(!note) return undefined;

        if(note.constructor == String)
            note = new Scale(note);
        return note;
    }
}
HarmonyTrack.prototype.chord = function(t) {
  if(this.tracks.chords) {
    var note = this.tracks.chords.sound(t);

    if(!note) return undefined;

    if(note.constructor == String)
        note = new Chord(note);
    return note;
  }
}

HarmonyTrack.prototype.guessOwnScale = function() {
  let scale = Scale.guess(this)
  let note = new ScaleTrack.Note()
  note.t = 0
  note.d = undefined
  note.sound = scale
  this.scales.clear()
  this.scales.mix(note)
}

HarmonyTrack.prototype.__defineGetter__("print", function() {
  var t = 0
  var notes = this.notes.sort((a,b) => {
    if(a.t == b.t) {
      if(b.sound.isScale) {
        return 1
      }
    }
    return a.t-b.t
  })

  var pulse = this.smallestPulse
  if(!pulse)
    return null

  var bits = []
  for(var i in notes) {
    while(notes[i].t > t)
      t += pulse
    bits.push(notes[i].sound.print)
    if(notes[i].sound.isChord)
      t += pulse
  }
  return cstring.format(cstring.undice(cstring.dice(bits.join(' '))))
})

HarmonyTrack.parseString = function(str, chevD) {
  chevD = chevD || 4
  var harmonyTrack = new HarmonyTrack()
  var chordTrack = new ChordTrack()
  var scaleTrack = new ScaleTrack()

  var currentScale = undefined

  var bits = cstring.dice(str)
  for(var i=0; i<bits.length; i++) {
    var t = i * chevD
    if(bits[i]) {
      var scale = Scale.parseScale(bits[i])
      if(scale) {
        currentScale = scale
        var note = new ScaleTrack.Note()
        note.sound = scale
        note.d = chevD
        note.t = t
        scaleTrack.mix(note)
      } else if(scaleTrack.lastNote)
        scaleTrack.lastNote.d += chevD

      var chord = Chord.parseChord(bits[i], currentScale)
      if(chord) {
        var note = new ChordTrack.Note()
        note.sound = chord
        note.d = chevD
        note.t = t
        chordTrack.mix(note)
      }
    } else {
      if(chordTrack.lastNote)
        chordTrack.lastNote.d += chevD
      if(scaleTrack.lastNote)
        scaleTrack.lastNote.d += chevD
    }
  }
  harmonyTrack.scales = scaleTrack
  harmonyTrack.chords = chordTrack
  return harmonyTrack
}
