var TrackGroup = require("./TrackGroup.js");
var Chord = require("./Chord.js");
var cstring = require("./cstring.js");
var ChordTrack = require("./ChordTrack.js");

function Score() {
    TrackGroup.call(this);
}
Score.prototype = Object.create(TrackGroup.prototype);
Score.prototype.constructor = Score;
module.exports = Score;

Score.prototype.scale = function(t) {
    if(this.tracks.scales) {
        var note = this.tracks.scales.soundAtT(t);

        if(!note) return undefined;

        if(note.constructor == String)
            note = new Scale(note);
        return note;
    }
}
Score.prototype.chord = function(t) {
    if(this.tracks.chords) {
        var note = this.tracks.chords.soundAtT(t);

        if(!note) return undefined;

        if(note.constructor == String)
            note = new Chord(note);
        return note;
    }
}
Score.prototype.__defineSetter__("chords", function(prog) {
    this.tracks.chords = new ChordTrack(prog);
});
