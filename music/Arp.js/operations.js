const pitch = require("../pitch.js")

// p - set abosolute midi pitch
exports.p = (p,n) => {
  return n
}
// q
exports.q = (p, n) => {
  return p + n
}
// o
exports.o = (p, n) => {
  return p + (n || 0) * 12
}
// O
exports.O = (p, n) => {
  return pitch.pc(p) + n*12
}
// r
// h
exports.h = (p, n, chord, scale) => {
  return chord.degreeShift(p, n)
}
exports.H = (p, n, chord, scale) => {
  return chord.degreeToP(n)
}
// s
exports.s = (p, n, chord, scale) => {
  return scale.degreeShift(p, n)
}
exports.S = (p, n, chord, scale) => {
  return scale.degreeToP(n)
}

// i
exports.i = (p, n, chord, scale) => {
  // go to closest chord degree n
  var cp = chord.colorProfile
  var targetPc = pitch.pc(cp[n%cp.length])

  var interval = pitch.pc(targetPc-p)
  if(interval > 6)
    interval -=12

  return p + interval
}
// l: Bass
exports.L = (p, n, chord, scale) => {
  var bass = chord.bassPc
  var interval = pitch.pc(bass-p)

  if(n == 0) {
    if(interval > 6) {
      interval -= 12
    }
    return p + interval
  }
  if(n > 0) {
    if(interval == 0)
      n++
    return p + interval + 12*(n-1)
  }
  if(n < 0) {
    return p + interval + 12*(n)
  }
}
