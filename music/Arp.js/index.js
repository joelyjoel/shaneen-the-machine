function Arp(track, harmony) {
    if(track.isScore) {
        harmony = track;
        for(var i in track.tracks) {
            Arp(track.tracks[i], harmony);
        }
        return track;
    }
    if(track.constructor == String) {
      track = new PointTrack(track)
    }
    if(harmony && harmony.constructor == String) {
      harmony = new HarmonyTrack(harmony)
    }

    var maxOrder = highestOrder(track);
    for(var order=0; order<=maxOrder; order++) {
        var e = 0;
        e += computeTrack(track, harmony, track.lastP, order, false);
        e += computeTrack(track, harmony, track.firstP, order, true);
        if(e > 0) do {
            var preE = e;
            e = 0;
            e += computeTrack(track, harmony, track.lastP, order, false);
            e += computeTrack(track, harmony, track.firstP, order, true);
        } while(preE > e);
        if(e > 0) {
            var preP = track.range ? track.range.mid : 60;
            console.log("arp compromising.", {"order":order, "preP":preP});
            do {
                preE = e;
                e = 0;
                e += computeTrack(track, harmony, preP, order, false);
                e += computeTrack(track, harmony, preP, order, true);
            } while(preE > e);
            if(e > 0)
                console.log("Yikes, problem computing arp qs. Order ", order)
        }
    }
    return track;
}
module.exports = Arp;

var pitch = require("../pitch.js");
var Chord = require("../Chord.js");
var Scale = require("../Scale.js");
var make_rhythm = require("../make_rhythm.js");
const HarmonyTrack = require("../HarmonyTrack.js")
const PointTrack = require("../PointTrack.js")
const ArpFunctions = require("./operations.js")

Arp.compute = compute = function(arpQ, p, chord, scale) {
    p = p || 60;

    chord = new Chord(chord);
    scale = new Scale(scale);

  /*  if(arpQ.H)
        p = chord.degreeToP(arpQ.H);

    if(arpQ.S) {
        p = scale.degreeToP(arpQ.S);
    }

    if(arpQ.C)
        p = arpQ.C;

    if(arpQ.o) {
        p += arpQ.o * 12;
    } else if(arpQ.O) { // Question: should O override o, or vice versa?
        p = pitch.pc(p) + arpQ.O * 12;
    }

    if(chord && arpQ.h)
        p = chord.degreeShift(p, arpQ.h);

    if(scale && arpQ.s)
        p = scale.degreeShift(p, arpQ.s);

    if(arpQ.c)
        p += arpQ.c;*/

    for(var i in arpQ.process) {
      //console.log("f:", arpQ.process[i].f)
      var step = arpQ.process[i]
      if(ArpFunctions[step.f])
        p = ArpFunctions[step.f](p, step.val, chord, scale)
      else {
        console.log("unknown arp function:", step)
      }
    }

    return p;
}
Arp.computeTrack = computeTrack = function(track, harmony, preP, order, backwards) {
    if(track.isAPointTrack)
        return computePointTrack(track, harmony, preP, order, backwards);
    else if(track.isAStepTrack)
        return computeStepTrack(track, harmony, preP, order, backwards);
}
Arp.computeStepTrack = computeStepTrack = function(track, harmony, preP, order, backwards) {
    //if(track.constructor == Number) track = harmony.tracks[track];
    order = order || 0;
    if(backwards == undefined) backwards = false;

    var errs = 0;
    var preP;
    for(var t=backwards ? track.steps.length-1 : 0;
    t < track.steps.length && t>=0;
    t += backwards ? -1 : 1) {
        var step = track.steps[t];

        if(step.constructor == Number) {
            preP = step;
            continue;
        }
        if(step.isAnArpQ && step.backwards == backwards && step.order <= order) {
            if(preP == undefined && step.requiresPreP)
                errs++;
            else {
                track.steps[t] = compute(step, preP, harmony.chord(t), harmony.scale(t));
                preP = track.steps[t];
                continue;
            }
        }
        if(step.constructor == Array) {
            for(var t=backwards ? step.length-1 : 0;
            t<step.length && t>=0;
            t += backwards ? -1 : 1) {
                if(step[i].constructor == Number) {
                    preP = step[i];
                    continue;
                }
                if(step[i].isAnArpQ
                && step[i].backwards == backwards
                && step[i].order <= order) {
                    if(preP == undefined && step[i].requiresPreP)
                        errs++;
                    else {
                        step[i] = compute(step[i], preP, harmony.chord(t), harmony.scale(t));
                        preP = step[i];
                    }
                }
            }
            continue;
        }
    }
    return errs;
}
Arp.computePointTrack = computePointTrack = function(track, harmony, preP, order, backwards) {
    order = order || 0;
    if(backwards == undefined) backwards = false;

    var errs = 0;
    var preP;
    for(var i=backwards ? track.notes.length-1 : 0;
    i<track.notes.length && i>=0;
    i += backwards ? -1 : 1) {
        var note = track.notes[i];
        var sound = note.sound;
        var t = note.t;
        if(sound.constructor == Number) {
            var preP = note.sound;
            continue;
        }

        if(sound.isAnArpQ && sound.order <= order && backwards == sound.backwards) {
            if(preP == undefined && sound.requiresPreP) {
                errs++;
                preP = undefined;
                continue;
            } else {
                note.sound = compute(sound, preP, harmony.chord(t), harmony.scale(t));
                preP = note.sound;
                continue;
            }
        }

        if(sound.constructor == Array) {
            for(var t=backwards ? sound.length-1 : 0;
            t<sound.length && t>=0;
            t+= backwards ? -1 : 1) {
                if(sound[i].constructor == Number) {
                    preP = sound[i];
                    continue;
                }
                if(sound[i].isAnArpQ && sound[i].order <= order && backwards == sound[i].backwards) {
                    if(preP == undefined && sound[i].requiresPreP)
                        errs++;
                    else {
                        sound[i] = compute(sound[i], preP, harmony.chord(t), harmony.scale(t));
                        preP = sound[i];
                    }
                }
            }
            continue;
        }
    }
    return errs;
}
Arp.highestOrder = highestOrder = function(track) {
    var sounds = track.sounds;
    winner = -1;
    for(var i in sounds) {
        if(sounds[i].isAnArpQ && sounds[i].order > winner)
            winner = sounds[i].order;
    }
    return winner;
}

Arp.make_q = make_q = function(mode) {
    var q = new Q();
    switch (mode) {
        case "stepApproach":
            q.backwards = true;
            q.s = Math.random()<0.5 ? 1 : -1;
            break;
        case "resolution":
            q.h = 0;
            break
        case "harmonyNote":
            q.h = Math.floor( Math.random() * 7) - 3;
            break;
        case "absolute":
            q.h = Math.floor(Math.random()*4);
            q.O = Math.floor(Math.random()*5)+2;
            break
        case "absolute-low":
          q.h = Math.floor(Math.random()*5);
          q.O = 2;
        default:
            console.log("unknown Arp Q mode:", mode);
    }
    return q;
}
Arp.make_qs = make_qs = function(n, mode) { // maybe make it so you can pass an object as `mode`
    mode = mode || 0;
    if(mode == 0) {
        var sequence = [];
        sequence[0] = make_q("absolute");
        for(var i=1; i<n; i++) {
            if(Math.random()<0.5) {
                sequence[i] = make_q("harmonyNote");
            } else {
                sequence[i] = make_q("stepApproach");
            }
        }
    }
    return sequence;
}

Arp.make_sequence = make_sequence = function(rhythm, mode) {
    if(typeof rhythm == "number") {
        n = rhythm;
        rhythm = make_rhythm(n);
    }
    var n = rhythm.sounds.length;
    var qs = make_qs(n, mode);
    console.log("qs", qs)
    rhythm.sounds = qs;
    return rhythm;
}

function Q(model) {
  this.order = 0;
  this.backwards = false;

  if(model && typeof model == "object")
    this.print = model.print;
    /*for(var i in model)
        this[i] = model[i];*/
  if(model && model.constructor == String)
    this.print = model;
}
Arp.Q = Q;
Q.prototype.isAnArpQ = true;
Q.prototype.isArpQ = true
Q.prototype.__defineGetter__("requiresPreP", function() {
    return (this.O == undefined
        && this.H == undefined
        && this.S == undefined
        && this.C == undefined);
});
Q.prototype.__defineGetter__("print", function() {
    var s = "";

    if(this.requiresPreP && !this.backwards)
        s += "<";

    for(var i in this) {
      if(i.length == 1) {
        s += i;
        if(typeof this[i] == "number")
          s += this[i]

      }
    }
    for(var i=0; i<this.order; i++) {
        s += "*";
    }

    if(this.requiresPreP && this.backwards)
        s += ">";

    return s;
});
Q.prototype.__defineSetter__("print", function(s) {
    this.order = 0;
    this.clear();

    if(s[0] == "_")
      s = s.slice(1)

    var key, val, c;
    val = "";
    for(var i in s) {
        c = s[i];

        if(c.match(/[0-9.\-]/)) {
            val += c;
        } else if(val.length > 0){
            if(key) {
              this[key] = parseFloat(val)
              this.process.push({f: key, val:parseFloat(val)})
            }
            else {
              this.p = parseFloat(val)
              this.process.push({f: "p", val: parseFloat(val)})
            }
            val = "";
            key = ""
        } else if(key) {
          this[key] = true
          this.process.push({f: key, val:0})
          val = ""
          key = ""
        }

        if(c.match(/[a-zA-Z]/)) {
            key = c;
        }

        if(c == "<")
            this.backwards = false;
        if(c == ">")
            this.backwards = true;

        if(c == "*")
            this.order++;
    }
    if(val.length > 0) {
      if(key) {
        this[key] = parseFloat(val)
        this.process.push({f:key, val:parseFloat(val)})
      } else {
        this.p = parseFloat(val)
        this.process.push({f:"p", val:val})
      }
    } else if(key) {
      this[key] = true
      this.process.push({f:key, val:0})
    }
});
Q.prototype.clear = function() {
    for(var i in this) {
        if(i.length == 0)
            delete this[i];
    }
    this.order = 0;
    this.backwards = false;
    this.process = []
}
/*
arpQ {
    O: absolute octave
    o: relative octave
    H: absolute harmony note
    h: relative harmony note
    S: absolute stepwise note
    s: relative stepwise note
    C: absolute chromatic note
    c: relative chromatic

    backwards:
    priority:
}
*/
