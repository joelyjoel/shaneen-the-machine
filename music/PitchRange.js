var WeightedSet = require("./WeightedSet.js");
var utility = require("./utility.js");
const pitch = require("./pitch.js")

function PitchRange(model, hi) {
    if(model != undefined && model.constructor == PitchRange) {
        this.hi = model.hi;
        this.lo = model.lo;
    } else if(model != undefined && model.constructor == Number) {
        // if arguments are numbers
        this.lo = model;
        this.hi = hi;
    } else if(model && model.constructor == String) {
      var split = model.split("~")
      if(split.length == 2) {
        this.lo = pitch.parsePitch(split[0].trim())
        this.hi = pitch.parsePitch(split[1].trim())
      } else {
        console.log("invalid PitchRange model:", model)
        throw "invalid PitchRange model"
      }
    } else if(model && model.constructor == Array) {
      this.lo = pitch.parsePitch(model[0])
      this.hi = pitch.parsePitch(model[1])
    }
}
module.exports = PitchRange;

PitchRange.prototype.__defineGetter__("print", function() {
    return utility.pitch.printPitch(this.lo)+" ~ "+utility.pitch.printPitch(this.hi);
});

PitchRange.prototype.randomP = function() {
    // random integer pitch inclusive
    return this.lo + Math.floor(Math.random() * (this.hi-this.lo+1));
}
PitchRange.prototype.randomMicrotonalP = function() {
    // random microtonal pitch in range
    return this.lo + Math.random()*(this.hi-this.lo);
}
PitchRange.prototype.__defineGetter__("pitchSet", function() {
    var set = new WeightedSet();
    for(var p=this.lo; p<=this.hi; p++) {
        set.increment(p);
    }
    return set;
});

PitchRange.prototype.__defineGetter__("middle", function() {
  return (this.hi + this.lo)/2
})
PitchRange.prototype.__defineGetter__("width", function() {
  return this.hi - this.lo
})

PitchRange.prototype.checkPitch = function(p) {
    return p >= this.lo && p <= this.hi
}

PitchRange.prototype.transpose = function(dP) {
  this.hi += dP
  this.lo += dP
  return this
}
