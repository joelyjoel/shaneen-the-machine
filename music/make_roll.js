var PointTrack = require("./PointTrack.js");

var make_roll = function(d, n, closed) {
    // makes a [PointTrack] rhythm of repeated notes subdividing a duration.
    if(d == undefined) {
        d = 4;
    }
    if(n == undefined) {
        n = d;
    }
    var track = new PointTrack();
    track.label = "subdivision roll: "+d+"/"+n;

    for(var i=0; i<n || (closed && i==n); i++) {
        var note = new PointTrack.Note();
        note.t = d/n * i;
        note.d = d/n;
        note.sound = true;
        track.mix(note);
    }

    return track;
}

module.exports = make_roll;
