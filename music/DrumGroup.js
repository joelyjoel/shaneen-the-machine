var TrackGroup = require("./TrackGroup.js");

function DrumGroup(model) {
    TrackGroup.call(this, model);

    this.label = "drums";

    if(model && model.constructor == String)
        this.preset(model, arguments[1]);
}
DrumGroup.prototype = Object.create(TrackGroup.prototype);
DrumGroup.prototype.constructor = DrumGroup;
module.exports = DrumGroup;

var DrumTrack = require("./DrumTrack.js");

DrumGroup.prototype.__defineGetter__("percset", function() {
    var percset = [];
    for(var i in this.tracks) {
        percset = percset.concat(this.tracks[i].percset);
    }
    return percset;
});

DrumGroup.prototype.preset = function(str, d) {
    d = d!=undefined ? d : this.d;
    switch(str) {
        case "909":
            this.tracks = {
                "kick": new DrumTrack([36, 41]),
                "rim": new DrumTrack(37),
                "snare": new DrumTrack([38, 40]),
                "clap": new DrumTrack(39),
                "hats": new DrumTrack([42, 44, 46]),
                "toms": new DrumTrack([43,45,47,48]),
                "crash": new DrumTrack([49, 50]),
                "ride": new DrumTrack(51)
            }
            break;
    }
    for(var i in this.tracks) {
        this.tracks[i].blank(d);
    }
    return this;
}
DrumGroup.prototype.blank = function(d, n) {
    n = n != undefined ? n : (this.tracks.length || 1);
    d = d != undefined ? d : 0;

    this.tracks = new Object();
    for(var i=0; i<n; i++) {
        this.tracks[i] = new DrumTrack().blank(d);
        this.tracks[i].percset = [i+36,]
        console.log("GRH", this.tracks[i].percset, i+36, [i+36])
    }
    return this;
}
DrumGroup.prototype.swapChannels = function(c1, c2) {
    if(typeof c1 == "number" || typeof c1 == "string")
        c1 = this.tracks[c1];
    if(typeof c2 == "number" || typeof c2 == "string")
        c2 = this.tracks[c2];

    var giv1 = c1.percsetIndexVersion;
    var giv2 = c2.percsetIndexVersion;
    c1.percsetIndexVersion = giv2;
    c2.percsetIndexVersion = giv1;

    return this;
}
DrumGroup.prototype.shadow = function(c1, c2) {
    if(typeof c1 == "number" || typeof c1 == "string")
        c1 = this.tracks[c1];
    if(typeof c2 == "number" || typeof c2 == "string")
        c2 = this.tracks[c2];

    if(c1 == c2) {
        throw("can't shadow a channel by itself");
    }

    for(var i in c1.notes) {
        if(c1.notes[i].t != undefined)
            c2.clear(c1.notes[i].t, c1.notes[i].tOff);
    }
    return this;
}
DrumGroup.prototype.superShadow = function(c) {
    if(typeof c != "number" && typeof c != "string")
        throw "superShadow expects number or string";

    if(c == -1)
        return ;

    for(var i in c.notes) {
        if(c.notes[i].t != undefined)
            this.clear(c.notes[i].t, c.notes[i].tOff, c);
    }
    return this;
}

// Random Modifiers:
DrumGroup.prototype.evolve = function(d, maxPokeSize) {
    maxPokeSize = (maxPokeSize!=undefined) ? maxPokeSize : 3;
    var thisCopy = new DrumGroup(this);
    var newGroup = new DrumGroup(this).zerofy();
    while(newGroup.d < d) {
        newGroup.append(thisCopy);
        thisCopy.omnipoke(Math.random()*maxPokeSize);
    }
    newGroup = newGroup.cut(0, d);
    return newGroup;
}
DrumGroup.prototype.vary = function(d, maxPokeSize) {
    maxPokeSize = (maxPokeSize!=undefined);
    var master = new DrumGroup(this).zerofy();
    while(master.d < d) {
        var cell = new DrumGroup(this);
        cell.omnipoke(Math.random()*maxPokeSize);
        master.append(cell);
    }
    master = master.cut(0, d);
    return master;
}
DrumGroup.prototype.omnipoke = function(n) {
    var options = [
        "poke",
        "randomSwap",
        "randomRotate",
        "shadowPoke",
        "superShadowPoke",
        //"makePoke",
    ];
    var choice = options[Math.floor(Math.random()*options.length)];
    this[choice]();
    if(n>1) return this.omnipoke(n-1);
    else return this;
}
DrumGroup.prototype.poke = function(n, subD) {
    var c = this.randomTrack();
    c.poke(1, subD);
    if(n > 1) return this.poke(n-1, subD);
    else return this;
}
DrumGroup.prototype.randomSwap = function(n) {
    this.swapChannels(this.randomTrack(), this.randomTrack());
    if(n > 1) return this.randomSwap(n-1);
    else return this;
}
DrumGroup.prototype.randomRotate = function(n) {
    this.randomTrack().randomRotate();
    if(n>1) return this.randomRotate(n-1);
    else return this;
}
DrumGroup.prototype.shadowPoke = function(n) {
    var c1 = this.randomTrackName();
    var c2;
    do {
        c2 = this.randomTrackName();
    } while(c1 == c2 && this.numberOfTracks > 1);

    this.shadow(c1, c2);
    if(n > 1) return this.shadowPoke(n-1);
    else return this;
}
DrumGroup.prototype.superShadowPoke = function(n) {
    this.superShadow(this.randomTrackName());
    if(n > 1) return this.superShadowPoke(n-1);
    else return this;
}
DrumGroup.prototype.makePoke = function(n, subD) {
    this.randomTrack().makePoke();
    if(n > 1) return this.makePoke(n-1);
    else return this;
}
