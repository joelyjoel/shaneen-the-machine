var fs = require("fs");
var cstring = require("./cstring.js");
var WeightedSet = require("./WeightedSet.js");

function HarmonyNetwork(source) {
    this.nodes = new Object();

    if(source != undefined && source.constructor == String) {
        if(source.slice(source.length-5) == ".hmap") {
            source = HarmonyNetwork.loadFromFile(source);
        } else {
            this.absorb(source);
        }
    }
    if(source && source.constructor == HarmonyNetwork) {
        for(var i in source.nodes) {
            this.nodes[i] = new HarmonyNetworkNode(source.nodes[i]);
        }
    }
}


HarmonyNetworkNode.prototype.isAHarmonyNetwork = true;
// building
HarmonyNetwork.loadFromFile = function(filename, onload, flatten) {
    if(!HarmonyNetwork.cache[filename]) {
        var file = fs.readFileSync(filename, "utf8");
        var hn = new HarmonyNetwork();
        hn.absorb(file);
        hn.flatten();
        HarmonyNetwork.cache[filename] = hn;
    }
    return new HarmonyNetwork(HarmonyNetwork.cache[filename]);
}
HarmonyNetwork.cache = {};
HarmonyNetwork.prototype.incrementConnection = function(from, to, weight) {
    if(weight == undefined) {
        weight = 1;
    }
    if(this.nodes[from] == undefined) {
        this.nodes[from] = new HarmonyNetworkNode(from);
    }
    if(this.nodes[to] == undefined) {
        this.nodes[to] = new HarmonyNetworkNode(to);
    }

    this.nodes[from].nexSet.increment(to, weight);
    this.nodes[to].preSet.increment(from, weight);
}
HarmonyNetwork.prototype.addSubstitution = function(original, substitution) {
    var originalNode = this.nodes[original];
    for(var chord in originalNode.preSet.items) {
        this.incrementConnection(chord, substitution, originalNode.preSet.items[chord]);
    }
    for(var chord in originalNode.nexSet.items) {
        this.incrementConnection(substitution, chord, originalNode.nexSet.items[chord]);
    }
    return this.nodes[substitution];
}
HarmonyNetwork.prototype.absorb = function(source, weight) {
    if(weight == undefined) {
        weight = 1;
    }
    if(source.constructor == String) {
        source = source.split("\n");
        for(var i in source) {
            if(source[i].indexOf(" = ") != -1) {
                source[i] = source[i].replace(/ /g, "");
                source[i] = source[i].split("=");
                for(var subI=1; subI<source[i].length; subI++) {
                    this.addSubstitution(source[i][0], source[i][subI]);
                }
                continue;
            }
            if(source[i].charAt(0) != "/" && source[i].indexOf("=") == -1) {
                this.absorb(cstring.extract(source[i]), weight);
            }
        }
        return this;
    } else {
        for(var i=0; i<source.length-1; i++) {
            this.incrementConnection(source[i], source[i+1], weight);
        }
        return this;
    }
}
HarmonyNetwork.prototype.flatten = function() {
    for(var i in this.nodes) {
        this.nodes[i].preSet.flatten();
        this.nodes[i].nexSet.flatten();
    }
    return this;
}

// exporting
HarmonyNetwork.prototype.encodeFlattenedJSONVersion = function() {
    var obj = {};
    for(var i in this.nodes) {
        obj[i] = this.nodes[i].nexSet.itemsAsArray;
    }
    return obj;
}

// querying
HarmonyNetwork.prototype.__defineGetter__("allChords", function() {
    return Object.keys(this.nodes);
});
HarmonyNetwork.prototype.__defineGetter__("allChordSet", function() {
    return new WeightedSet(this.allChords);
});
HarmonyNetwork.prototype.randomChord = function() {
    var allChords = this.allChords;
    return allChords[Math.floor(Math.random()*allChords)];
}

HarmonyNetwork.prototype.simpleStrictSearch = function(pre, nex) {
    //console.log("calling simpleStrictSearch", pre, nex);
    if(pre != undefined && this.nodes[pre] == undefined) {
        console.log("unknown chord symbol: ", "'"+pre+"'", this);
        return new WeightedSet();
    }
    if(nex != undefined && this.nodes[nex] == undefined) {
        console.log("unknown chord symbol: ", "\'"+nex+"\'", this);
        return new WeightedSet();
    }

    if(pre != undefined && nex != undefined) {
        var setA = this.nodes[pre].nexSet;
        var setB = this.nodes[nex].preSet;
        return setA.findOverlapWith(setB);
    } else if (pre != undefined) {
        return new WeightedSet(this.nodes[pre].nexSet);
    } else if(nex != undefined) {
        return new WeightedSet(this.nodes[nex].preSet);
    } else {
        return this.allChordSet;
        throw "simpleStrictSearch requires at least one argument";
    }
}
HarmonyNetwork.prototype.multiStepSearch = function(n, pre, nex) {
    var prog = new Array();
    prog[0] = pre;
    prog[n+1] = nex;

    var options = new Array();
    var i = 1;
    var j = n;
    while(i <= j) {
        if(Math.random() < 0.5 && i != 0) {
            if(options[i] == undefined) {
                options[i] = this.simpleStrictSearch(prog[i-1], prog[i+1]).sortRoll();
            }
            if(options[i].length == 0) {
                options[i] = undefined;
                i--;
                if(i != 0) {
                    prog[i] = undefined;
                }
            } else {
                prog[i] = options[i].shift();
                i++;
            }
        } else if(j != n+1) {
            if(options[j] == undefined) {
                options[j] = this.simpleStrictSearch(prog[j-1], prog[j+1]).sortRoll();
            }
            if(options[j].length == 0) {
                options[j] = undefined;
                j++;
                if(j != n+1) {
                    prog[j] = undefined;
                }
            } else {
                prog[j] = options[j].shift();
                j--;
            }
        }
        if(i == 0 && j == n+1) {
            console.log("failure");
            break;
        }
    }

    return prog.slice(1, prog.length-1);
}


// diagnostic
HarmonyNetwork.prototype.__defineGetter__("deadends", function() {
    var report = "";
    for(var chord in this.nodes) {
        if(this.nodes[chord].preSet.pSum == 0) {
            report += "to " + chord + "\n";
        }
        if(this.nodes[chord].nexSet.pSum == 0) {
            report += "from " + chord + "\n";
        }
    }
    return report;
});
HarmonyNetwork.prototype.reportDeadends = function() {
    console.log(this.deadends);
}



function HarmonyNetworkNode(str) {
    if(str != undefined && str.constructor == String) {
        this.str = str;
        this.preSet = new WeightedSet();
        this.nexSet = new WeightedSet();
    } else if(str.constructor == HarmonyNetworkNode) {
        this.str = str.str;
        this.preSet = new WeightedSet(str.preSet);
        this.nexSet = new WeightedSet(str.nexSet);
    }
}

HarmonyNetworkNode.prototype.addNode = function(node) {
    if(this.str != node.str) {
        throw "can't add nodes, they belong to different chords";
    }

    this.preSet.addSet(node.preSet);
    this.nexSet.addSet(node.nexSet);
}

//tonalityHmap = new HarmonyNetwork("tonality.hmap");
module.exports = HarmonyNetwork;
