const make_songVerse = require('../bach/make_songVerse')
const HarmonyTrack = require('../HarmonyTrack')
const PointTrack = require('../PointTrack.js')
const TrackGroup = require('../TrackGroup')
const Arp = require('../Arp.js')
const Q = Arp.Q
const fs = require('fs')

function make_loop() {
  let verse1 = make_songVerse()
  verse1 = '||: ' + verse1.slice(0, verse1.length-2) + " :||"
  verse1 = verse1.split('\n').join('\n    ')
  return verse1
}

let loops = []
let n = 1000
for(let i=0; i<n; i++) {
  console.log(i+'/'+n)
  loops.push(make_loop())
}

fs.writeFileSync('./chords.txt', loops.join('\n\n\n'))
