const stealChordsFromBach = require("../stealChordsFromBach.js")
const satbPad = require("../satbPad.js")

var chords = stealChordsFromBach(4)
console.log(chords.print)

var voiced = satbPad(chords)
voiced.save("stolen from Bach: " + chords.print)

/*
var chords = stealChordProgressionFromBach()

var voiced = satbPad(chords)

voiced.save("stolen from bach: " + chords.print)
*/
