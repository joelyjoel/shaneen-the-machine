const satbPad = require("../satbPad.js")
const rhythmify = require("../rhythmify.js")
const make_rhythm = require("../make_rhythm.js")
const Sampler = require("../../dsp/synths/SamplerPoly.js")
const render = require("../../dsp/Sample.js").record
const make_harmony = require("../make_harmony.js")
const Kick = require("../../dsp/synths/KickMono.js")
const mixx = require("../../dsp/mixx.js")
const Gain = require("../../dsp/Gain.js")

const bpm = 120

var harmony = make_harmony(32)
var chords = satbPad(harmony)
//console.log(chords)

var rhythm = make_rhythm.popVamp(chords.d)
//console.log(rhythm.loop(chord.d))

var rhythmified = rhythmify(chords, rhythm).loopN(4)
console.log(rhythmified)

var T = rhythmified.d/4 * 60*44100/bpm

var kickRhythm = make_rhythm.kick(16).loop(rhythmified.d)

Sampler.choose("").then(function(sampler) {
  sampler.tuneSample()
  sampler.scheduleTrack(rhythmified, bpm)

  var kick = Kick.random()
  kick.scheduleTrack(kickRhythm, bpm)

  var mix = mixx(
    kick,
    new Gain(sampler, -6)
  )

  //sampler.trigger(0)

  render(mix, T)
  .addMeta({
    bpm: bpm,
    sampleUsed: sampler.sample.originalFile
  })
  .normalise()
  .save("testing rhythmify")
})
