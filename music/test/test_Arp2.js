const PointTrack = require("../PointTrack.js")
const Arp = require("../Arp.js")
const stealChordsFromBach = require("../stealChordsFromBach.js")
const argv = require("minimist")(process.argv.slice(2))
const satbPad = require("../satbPad.js")
const TrackGroup = require("../TrackGroup.js")
const HarmonyTrack = require("../HarmonyTrack.js")

var q = new Arp.Q(argv._[0])

var harmony = new HarmonyTrack("C:C````````````")
var arpTrack = new PointTrack(argv._[0])

var arped = Arp(arpTrack, harmony)
console.log(arped)
