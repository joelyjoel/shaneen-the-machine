var PointTrack =  require("../PointTrack.js");

var track1 = new PointTrack();
for(var i=0; i<16; i++) {
  var note = new PointTrack.Note();
  note.t = i
  note.d = 1;
  note.sound = 60+Math.random()*10
  track1.notes.push(note);
}

console.log(track1.notes, track1.gamut)

console.log("Lowest unused sounds =",track1.lowestUnusedSound("kick_1"))

var track2 = track1.mapSounds(function(p) {
  return p+1;
})

track1.append(track2)

console.log("track1 is monophonic:", track1.monophonic)

//track1.save()
