const PointTrack = require("../PointTrack.js")
const HarmonyTrack = require("../HarmonyTrack.js")
const stealChordsFromBach = require("../stealChordsFromBach.js")
const Scale = require("../Scale.js")

var track = stealChordsFromBach(4)

var opStats = track.octaveProfileStatistics

console.log("opStats:", opStats)

var scaleEstimate = Scale.guessFromOctaveProfileStatistics(opStats)
console.log(track.print)

console.log("estimated scale:", scaleEstimate.print)
