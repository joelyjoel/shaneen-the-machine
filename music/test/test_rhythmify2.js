const satbPad = require("../satbPad.js")
const rhythmify = require("../rhythmify.js")
const make_rhythm = require("../make_rhythm.js")
const Sampler = require("../../dsp/synths/SamplerPoly.js")
const render = require("../../dsp/Sample.js").record
const make_harmony = require("../make_harmony.js")
const Kick = require("../../dsp/synths/KickMono.js")
const mixx = require("../../dsp/mixx.js")
const Gain = require("../../dsp/Gain.js")
const SimpleSinePoly = require("../../dsp/synths/SimpleSinePoly.js")
const AdditivePoly = require("../../dsp/synths/AdditivePoly.js")

const bpm = 160

var harmony = make_harmony(32)
var chords = satbPad(harmony)
//console.log(chords)

var rhythm = make_rhythm.popVamp(chords.d)
//console.log(rhythm.loop(chord.d))

var rhythmified = rhythmify(chords, rhythm)

for(var i in rhythmified.notes) {
  rhythmified.notes[i].d *= 0.5
}


var synth = new SimpleSinePoly(1, 4410, 0, 1)//new AdditivePoly([1,2,3,4], 441, 4410, 0, 1)
console.log(synth.label)
synth.scheduleTrack(rhythmified, bpm)
synth.schedule(0, function() {
  console.log("t:", this.clock, ",", this._mix.inputs.length)
  return 4410
})

render(synth, rhythmified.d/4 * 44100*60/bpm)
.normalise()
.save()

console.log(synth._mix.report)
console.log(Object.keys(synth.playing))
