const cstring = require("../cstring.js")
const argv = require("minimist")(process.argv.slice(2))
const Chord = require("../Chord.js")
const Scale = require("../Scale.js")

var str = "g: ` "

var diced = cstring.dice(str)
console.log(diced)

var breakdown = cstring.breakdown(str)
//console.log("breakdown:", breakdown)

for(var i in diced) {
  if(diced[i]) {
    var chord = Chord.parseChord(diced[i])
    var scale = Scale.parseScale(diced[i])
    console.log(diced[i], chord, scale)
  }
}
