const logic = require("logicalexpressions")
const make_logicRhythm = require("../make_logicRhythm.js")

var seed = new logic.Seed(5)
seed.fastForward(Math.floor(Math.random()*10000))

console.log(seed.expression().print())
var rhythm = make_logicRhythm(seed.expression())

console.log(rhythm)

rhythm.save(null, "logicalRhythms")
