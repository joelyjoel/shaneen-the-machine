const MelodyProblem = require("../MelodyProblem")
const TrackGroup = require("../TrackGroup.js")
const solve = require("../MelodyProblem/solve.js")
const demoTrack = require("../../dsp/demoTrack.js")
const HarmonyTrack = require("../HarmonyTrack.js")
const make_harmony = require("../make_harmony.js")


var subD = 8
var harmony = make_harmony(128) /*
var harmony = new HarmonyTrack("C: C``` Am``` F``` G```") //*/

console.log(harmony.print)

var score = TrackGroup.blankGrid(
  harmony.d/subD,
  ["bass", "tenor", "alto", "soprano"],
  subD,
  "?",
)
score.tracks.harmony = harmony

for(var i in score.tracks[0].notes) {
  var problemNote = score.tracks[0].notes[i]
  var problem = new MelodyProblem(problemNote, score, harmony)
  var solutionSet = solve.melodyProblem(problem, "bass")
  problemNote.sound = solutionSet.roll()
}

solve.trackGroup(score, null, 100)


var mixDown = score.mixDownNumberedTracks()
mixDown.removeNonNumberNotes()
mixDown.save("testing melodyProblem")

/*for(var i in mixDown.notes)
  mixDown.notes[i].d *= 0.9

demoTrack(mixDown.loop(mixDown.d * 4))
.normalise()
.addMeta({
  chordProgression: harmony.print,
})
.save("testing melodyProbl  em")*/
