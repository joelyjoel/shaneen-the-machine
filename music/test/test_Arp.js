const PointTrack = require("../PointTrack.js")
const Arp = require("../Arp.js")
const stealChordsFromBach = require("../stealChordsFromBach.js")
const argv = require("minimist")(process.argv.slice(2))
const satbPad = require("../satbPad.js")
const TrackGroup = require("../TrackGroup.js")

var harmony = stealChordsFromBach(8).loopN(8)

var score = new TrackGroup()
var pad = satbPad(harmony)
score.tracks = [ pad ]

for(var i in argv._) {
  var arpSeq = argv._[i]
  var dChev = 1
  var myArpMotif = PointTrack.fromSteps(arpSeq, dChev)//.loop(16)
  var loop = myArpMotif.loop(harmony.d)
  var melody = Arp(loop, harmony)
  score.tracks.push(melody)
}

/*for(var i in myArpMotif.notes) {
  console.log(myArpMotif.notes[i].sound)
}*/

//console.log(myArpMotif.sounds.map((q) => {return q.print}))







score.save("Testing Arp")
