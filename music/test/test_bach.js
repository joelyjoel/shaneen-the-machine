const fs = require('fs')

const bach = require('../bach')
const cstring = require('../cstring')

let harmonicRhythmChunks = [
  '? ```', '? ` ? `'
]

function make_harmonicRhythm(nBars) {
  let loop = []
  for(let i=0; i<nBars; i++)
    loop.push(harmonicRhythmChunks[Math.floor(Math.random()*harmonicRhythmChunks.length)])
  loop = loop.join('| ')

  return loop
}

function make_section(
  nBars=16,
  loopSize=4,
  turnAroundSize=0
) {
  let loop = make_harmonicRhythm(loopSize)

  let nChordsInLoop = (loop.match(/\?/g)||[]).length
  let loopChords = bach.loop(nChordsInLoop)

  loop = cstring.pack(loop, loopChords)

  let loopDiced = cstring.dice(loop)

  let section = new Array(nBars*4)
  let loopedDuration = (nBars-turnAroundSize)*4
  for(let b=0; b<loopedDuration; b++)
    section[b] = loopDiced[b%loopDiced.length]

  let turnAround = make_harmonicRhythm(turnAroundSize)
  let nChordsInTurnAround = cstring.extract(turnAround).length
  let turnAroundChords = bach.markov.connect(
    section[loopedDuration],
    nChordsInTurnAround,
    section[0],
    false
  )
  turnAround = cstring.pack(turnAround, turnAroundChords)
  let turnAroundDiced = cstring.dice(turnAround)
  for(let b=0; b<turnAroundSize*4; b++)
    section[loopedDuration+b] = turnAroundDiced[b]

  return cstring.undice(section, 1, 4)
}



let section = make_section(32, 4, 2).split('| ')

let barsPerLine = 4
let lines = []
for(let bar=0; bar<section.length; bar+=barsPerLine)
  lines.push(section.slice(bar, bar+barsPerLine).join('| '))
section = lines.join('|\n')

console.log(section)
