const satbPad = require("../satbPad.js")
const make_harmony = require("../make_harmony")

var harmony = make_harmony(32)

var pad = satbPad(harmony)
console.log("pad is monophonic: ", pad.monophonic)
console.log(pad)
pad.save("satbPad")
