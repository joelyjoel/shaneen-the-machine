const make_songVerse = require('../bach/make_songVerse')
const HarmonyTrack = require('../HarmonyTrack')
const PointTrack = require('../PointTrack.js')
const TrackGroup = require('../TrackGroup')
const Arp = require('../Arp.js')
const Q = Arp.Q

let verse1 = make_songVerse(8, 4, 2)

let harmony = new HarmonyTrack(verse1)
harmony.loopN(4)
//track.guessOwnScale()

console.log(harmony.print)

let queries = ['LO4', ...Arp.make_qs(7)]
let figuration = new PointTrack().quick(1,
  ...['h-3>', 'h-1>', 'h-2<', 'h-1>', 'h-2>', 'h-1<', 'h-2>', 'H1O6']
  .map(q => new Q(q))
).loop(harmony.d)
let figuration2 = new PointTrack().quick(3, ...queries).loop(harmony.d)


let music = Arp(figuration, harmony)
let music2 = Arp(figuration2, harmony)



/*for(let note of music.notes)
  note.t = Math.floor(note.t/2)*2*/

music.mix(music2)

music.save('testing bach harmony with arp')
