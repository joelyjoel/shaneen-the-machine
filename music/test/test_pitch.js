const pitch = require("../pitch.js")

var testStrings = [
  "C0",
  "Gb-4",
  "F16",
  "Gx-405",
  "G",
  "c5",
]

console.log("\n\n\n")

for(var i in testStrings) {
  var str = testStrings[i]
  var octave = pitch.parseOctave(str)
  var pc = pitch.parsePitchClass(str)
  var p = pitch.parsePitch(str)
  console.log({
    str: str,
    pitchClass: pc,
    octave: octave,
    p: p,
  })
}

console.log("\n\n")

for(var p=-200; p<200; p++) {
  var str = pitch.printPitch(p)
  var parsed = pitch.parsePitch(str)
  if(parsed != p)
    console.log(
      "Problem",
      p,
      str,
      parsed
    )
}
