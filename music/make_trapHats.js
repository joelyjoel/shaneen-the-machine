// (requires at bottom)

function make_trapHats(d) {
    var track = new PointTrack();
    var t = 0;
    while(t < d) {
        var numerator = 8;//* Math.ceil(Math.random()*1);
        //var division = Math.ceil(Math.random()*7);
        var denominator = Math.ceil(Math.random()*9);
        track.mix(make_roll(numerator, denominator), t);
        t += numerator;
    }

    while(Math.random() < 0.5) {
        console.log("subdividing another beat!");
        var noteI = Math.floor(Math.random() * track.notes.length);
        track.rollNote(noteI, 2);
    }
    //track = track.cut(0,d);
    for(var i in track.notes) {
        track.notes[i].sound = 42;
        if(track.notes[i].d >= 2 && Math.random() < 0.2) {
            track.notes[i].sound = 46;
        }
        if(Math.random() < 0.3) {
            //track.notes[i].sound = 44;
        }
    }

    return track;
}
module.exports = make_trapHats;

var PointTrack = require("./PointTrack.js");
var make_roll = require("./make_roll.js");
