var PointTrack = require("./PointTrack.js");

function DrumTrack(model) {
    PointTrack.call(this, model);

    this.label = "drum track";

    if(model!=undefined && model.constructor == Number) // arg is midi number
        this.percset = [model];
    if(model && model.constructor == String) // arg is preset string
        this.preset(model);
    else if(model && model.constructor == Array) // arg is midi # array
        this.percset = utility.duplicate(model);
    else if(model && model.isADrumTrack) // arg is a DrumTrack to copy
        this.percset = model && model.percset ? model.percset.slice() : [true];
}
DrumTrack.prototype = Object.create(PointTrack.prototype);
DrumTrack.prototype.constructor = DrumTrack;
module.exports = DrumTrack;

var make_rhythm = require("./make_rhythm.js");
var utility = require("./utility.js")

DrumTrack.prototype.isADrumTrack = true;
DrumTrack.prototype.preset = function(model) {
    if(typeof model == string) {
        model = this.presets[model];
    }
    if(typeof model == "object") {
        this.percset = model.percset.slice();
    }
}
DrumTrack.prototype.presets = {
    // percset sounds must always be numbers or strings
    "kick": {percset: [36]},
    "hats": {percset: [42,44,46]}
}
DrumTrack.prototype.__defineGetter__("percsetIndexVersion", function() {
    var track = new DrumTrack(this);
    for(var i in this.notes) {
        track.notes[i].percsetIndex = this.percset.indexOf(this.notes[i].sound);
        delete track.notes[i].sound;
    }
    return track;
});
DrumTrack.prototype.__defineSetter__("percsetIndexVersion", function(giv) {
    this.notes = [];
    for(var i in giv.notes) {
        var note = new DrumTrack.Note(giv.notes[i]);

        if(note.percsetIndex >= this.percset.length)
            note.percsetIndex = this.percset.length-1;
        if(note.percsetIndex < 0)
            note.percsetIndex = 0;
        if(note.sound == true)
            note.percsetIndex = 0;

        note.sound = this.percset[note.percsetIndex];
        delete note.percsetIndex;
        //console.log("note", note);
        this.notes.push(note);
    }
    //console.log(this.notes);
});

DrumTrack.prototype.randomSound = function() {
    return this.percset[Math.floor(Math.random()*this.percset.length)];
}
DrumTrack.prototype.makeFunctions = [
    make_rhythm.basic,
    make_rhythm.upbeats
];

DrumTrack.prototype.poke = function(n, subD) {
    subD = subD || 1;
    var t0 = Math.floor(Math.random()*this.d/subD)*subD;
    var t1 = t0 + subD;
    if(this.countAttacks(t0, t1) == 0) {
        var note = new PointTrack.Note();
        note.sound = this.randomSound();
        note.d = subD;
        note.t = t0;
        this.notes.push(note);
    } else {
        this.clear(t0, t1);
    }

    if(n > 1) return this.poke(n-1, subD);
    else return this;
}
DrumTrack.prototype.randomRotate = function() {
    var ammount = Math.floor(Math.random()*this.d);
    //console.log("oh")
    this.rotate(ammount);
    return this;
}
DrumTrack.prototype.makePoke = function(n, subDIn) {
    var subD = subDIn || (Math.pow(2, Math.floor(Math.random()*3)+1));
    var F = this.makeFunctions[Math.floor(Math.random()*this.makeFunctions.length)];
    var made = F(this.d);
    this.percsetIndexVersion = made;
    if(n > 1) return this.makePoke(n-1, subDIn);
    else return this;
}

DrumTrack.fromTimeArray = function(times, d, sound) {
    sound = sound != undefined ? sound : true;
    times = times.sort(function(a,b) {return a-b;});
    var track = new DrumTrack();
    for(var i=0; i<times.length; i++) {
        var note = new DrumTrack.Note();
        note.sound = sound;
        note.t = times[i];
        track.notes.push(note);
    }
    if(d != undefined)
        track.d = d;
    track.label = "rhythm from time array";
    return track;
}
