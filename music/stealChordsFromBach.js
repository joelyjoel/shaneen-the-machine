const fs = require("fs")
const midi = require("./midi.js")
const Chord = require("./Chord.js")
const Scale = require("./Scale.js")
const pitch = require("./pitch.js")
const bach = require("./bach")
const HarmonyTrack = require("./HarmonyTrack.js")
const PointTrack = require("./PointTrack.js")

function findChordProgressionFragments(music) {
  var frag = []
  var frags = []
  for(var i in music.notes) {
    var t = music.notes[i].t
    var selection = music.select(t)
    var bass = pitch.pc(selection.bass)
    var op = selection.octaveProfile
    var chord = Chord.fromOctaveProfile(op)
    chord.bass = bass
    if(chord != "?") {
      if(chord.print != frag[frag.length-1])
        frag.push(chord.print)
    } else {
      if(frag.length > 1)
        frags.push(frag)
      frag = []
    }
  }
  if(frag.length > 1)
    frags.push(frag)

  return frags
}

function stealChordProgression(music, howManyChords) {
  howManyChords = howManyChords || 4
  var frags = findChordProgressionFragments(music)
  frags = frags.filter( (frag) => {
    return frag.length > howManyChords
  })

  if(frags.length == 0)
    return null

  var frag = frags[Math.floor(Math.random()*frags.length)]
  var i0 = Math.floor(Math.random() * (frag.length-howManyChords))
  var hTrack = new HarmonyTrack(frag.slice(i0, i0 + howManyChords).join(" "), 16)
  return hTrack
}

function findChordProgressionLoops(music) {
  var frags = findChordProgressionFragments(music)
  var loops = []
  for(var i in frags) {
    loops = loops.concat(findLoopsInChordProgression(frags[i]))
  }
  return loops
}

function findLoopsInChordProgression(chords) {
  var loops = []
  for(var i=0; i<chords.length; i++) {
    var indexOfRepetition = chords.indexOf(chords[i], i+1)
    if(indexOfRepetition != -1) {
      loops.push( chords.slice(i, indexOfRepetition))
    }
  }
  return loops
}



function stealChordLoopFromBach(howManyChords, rotate=true) {
  var succesful = false
  while(!succesful) {
    // get a random bach chorale and flatten all tracks into one
    var music = bach.random(true).mixDownNumberedTracks()

    // extract chord progressions that return to the same chord
    var loops = findChordProgressionLoops(music)

    // optionally filter by number of chords
    if(howManyChords)
      loops = loops.filter((loop) => {
        return loop.length == howManyChords
      })

    // if there is at least one match exit the loop
    if(loops.length > 1)
      break
  }

  // choose one of the matches
  var loop = loops[Math.floor(Math.random()*loops.length)]

  // optionally rotate the loop by a random index
  if(rotate) {
    let i = Math.floor(Math.random()*loop.length)
    loop = [].concat(loop.slice(i), loop.slice(0, i))
  }

  // convert the loop into a harmony track
  var hTrack = new HarmonyTrack( loop.join(" ") , 16)

  // guess and attach scale to harmony track
  var scaleGuess = Scale.guess(hTrack)
  var scaleNote = new PointTrack.Note()
  scaleNote.t = 0
  scaleNote.sound = scaleGuess
  hTrack.scales.notes.push(scaleNote)

  // return a harmony track
  return hTrack
}
module.exports = stealChordLoopFromBach

function stealChordProgressionFromBach(
  howManyChords=Math.floor(3 + Math.random()*8)
) {
  while(true) {
    var music = bach.random(true).mixDownNumberedTracks()
    var progression = stealChordProgression(music, howManyChords)
    if(progression)
      break
  }
  return progression
}
module.exports.nonLoop = stealChordProgressionFromBach


//var frags = findChordProgressionFragments(music)
//console.log(frags)
