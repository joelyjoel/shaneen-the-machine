const MelodyProblem = require("./MelodyProblem")
const TrackGroup = require("./TrackGroup.js")
const PointTrack = require("./PointTrack.js")
const HarmonyTrack = require("./HarmonyTrack.js")

function satbGroup(harmony) {
  if(harmony.constructor == String) {
    harmony = new HarmonyTrack(harmony)
  }
  var group = TrackGroup.blank(harmony.d, [
    "bass",
    "tenor",
    "alto",
    "soprano",
  ])
  for(var c in group.tracks)
    for(var i in harmony.chords.notes) {
      var note = new PointTrack.Note(harmony.chords.notes[i])
      note.sound = "?"
      group.tracks[c].notes.push(note)
    }
  group.tracks.harmony = harmony

  MelodyProblem.solve.track(group.tracks[0], group, "bass", 5)

  MelodyProblem.solve.trackGroup(group, null, 20)


  return group
}
module.exports.parts = satbGroup

function satbPad(harmony) {
  var parts = satbGroup(harmony)
  var mix = parts.mixDownNumberedTracks()
  mix.removeNonNumberNotes()
  return mix
}
module.exports = satbPad
