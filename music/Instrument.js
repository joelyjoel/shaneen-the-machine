const PitchRange = require("./PitchRange.js")

function Instrument(model) {
  if(model && model.constructor == String) {
    this.name = model
    model = Instrument.templates[model]
  }
  while(model && model.alias) {
    model = Instrument.templates[model.alias]
  }
  if(model && typeof model == "object") {
    Object.assign(this, model)
    if(model.range)
      this.range = new PitchRange(model.range)
    else if(model.transposes) {
      var range = new PitchRange(model.writtenRange)
      range.transpose(model.transposes)
      this.range = range
    }
  }
}
module.exports = Instrument

Instrument.prototype.isInstrument = true

Instrument.random = function() {
  var all = Object.keys(Instrument.templates)
  return new Instrument(all[Math.floor(Math.random()*all.length)])
}

Instrument.templates = {

  // voices
  soprano: {
    range: "C5~C7",
  },
  mezzo_soprano: {
    range: "A4 ~ G6"
  },
  alto: {
    range: "F4~D6",
  },
  tenor: {
    range: "C4 ~ A5",
  },
  baritone: {
    range: "G3 ~ F5",
  },
  bass: {
    range: "E3 ~ E5",
  },
  human_voice: {
    range: "C3 ~ C7"
  },
  voice: {
    alias: "human_voice",
  },

  //WOODWIND
  piccolo: {
    writtenRange: "D5~C8",
    transposes: 12,
  },
  flute: {
    range: "c5~D8",
  },
  alto_flute: {
    writtenRange: "c5~c8",
    transposes: -5,
  },
  oboe: {
    range: "Bb4~A7",
  },
  oboe_damore: {
    writtenRange: "Bb4~E7",
    transposes: -3,
  },
  english_horn: {
    writtenRange: "B4~G7",
    transposes: -7,
  },
  bass_oboe: {
    otherwiseKnownAs: "heckelphone",
    writtenRange: "A4~G7",
    transposes: -12,
  },
  clarinet_Bb: {
    writtenRange:"E4~C8",
    transposes: -2,
  },
  clarinet_Eb: {
    writtenRange:"E4~C8",
    transposes: 3,
  },
  clarinet_A: {
    writtenRange:"E4~C8",
    transposes: -3,
  },
  clarinet_D: {
    writtenRange:"E4~C8",
    transposes: 2,
  },
  clarinet: {
    alias: "clarinet_Bb"
  },
  basset_horn: {
    writtenRange: "C4 ~ G7",
    transposes: -7
  },
  bass_clarinet: {
    writtenRange: "Eb4 ~ G7",
    transposes: -14,
  },
  bassoon: {
    range: "Bb2 ~ Bb5",
  },
  contrabassoon: {
    otherwiseKnownAs: "sarrusophone",
    writtenRange: "Bb2 ~ Bb5",
    transposes: -12,
  },
  soprano_saxophone: {
    writtenRange: "Bb4 ~ G7",
    transposes: -2,
  },
  alto_saxophone: {
    writtenRange: "Bb4 ~ G7",
    transposes: -9,
  },
  tenor_saxophone: {
    writtenRange: "Bb4 ~ G7",
    transposes: -14,
  },
  baritone_saxophone: {
    writtenRange: "Bb4 ~ G7",
    transposes: -21,
  },
  bass_saxophone: {
    writtenRange: "Bb4 ~ G7",
    transposes: -26,
  },

  // brass
  horn: {
    writtenRange: "F#3 ~ C7",
    transposes: -7,
  },
  tenor_wagner_tuba: {
    writtenRange: "C4 ~ G6",
    transposes: -2,
  },
  bass_wagner_tuba: {
    writtenRange: "F3 ~ D6",
    transposes: -7,
  },
  trumpet_C: {
    writtenRange: "F#4 ~ D7",
    transposes: 0
  },
  trumpet_Bb: {
    writtenRange: "F#4 ~ D7",
    transposes: -2
  },
  trumpet_A: {
    writtenRange: "F#4 ~ D7",
    transposes: -3
  },
  trumpet_G: {
    writtenRange: "F#4 ~ D7",
    transposes: 7
  },
  trumpet_F: {
    writtenRange: "F#4 ~ D7",
    transposes: 5
  },
  trumpet_E: {
    writtenRange: "F#4 ~ D7",
    transposes: 4
  },
  trumpet_D: {
    writtenRange: "F#4 ~ D7",
    transposes: 2
  },
  piccolo_trumpet_Bb: {
    writtenRange: "F#4 ~ G6",
    transposes: 10,
  },
  piccolo_trumpet_A: {
    writtenRange: "F#4 ~ G6",
    transposes: 9,
  },
  alto_trombone: {
    range: "A3 ~ G6",
  },
  trombone: {
    range: "E3 ~ F6",
  },
  bass_trombone: {
    range: "Bb2 ~ Bb5",
  },
  contrabass_trombone: {
    range: "Ab1 ~ C6",
  },
  tuba: {
    range: "D2 ~ F5",
  },
  euphonium: {
    writtenRange: "Bb2 ~ Bb4",
    transposes: -2,
  },

  // percussion
  timpani: {
    range: "D3 ~ C5",
  },
  xylophone: {
    range: "F4 ~ C8",
  },
  marimba: {
    range: "C3 ~ C8",
  },
  glockenspiel: {
    writtenRange: "G4 ~ C7",
    transposes: 24,
  },
  vibraphone: {
    range: "F4 ~ F7",
  },
  chimes:  {
    range: "C5 ~ F6",
  },

  // plucked strings
  guitar: {
    writtenRange: "E4 ~ E7",
    transposes: -12
  },
  harp: {
    range: "Cb2 ~ F#8",
  },

  // keyboard
  piano: {
    range: "A1 ~ C9",
  },
  celesta: {
    writtenRange: "C4 ~ C8",
    transposes: 12,
  },
  harpsichord: {
    range: "F2 ~ F7",
  },
  harmonium: {
    range: "F2 ~ F7",
  },
  pipe_organ: {
    range: "C3 ~ C8",
  },
  organ_pedals: {
    writtenRange: "C3 ~ G5",
    transposes: -12,
  },

  // strings
  violin: {
    range: "G4 ~ A8",
  },
  viola: {
    range: "C4 ~ E7",
  },
  cello: {
    range: "C3 ~ C7",
  },
  double_bass: {
    writtenRange: "C3 ~ C6",
    transposes: -12,
  }

}

// main source: http://www.orchestralibrary.com/reftables/rang.html
