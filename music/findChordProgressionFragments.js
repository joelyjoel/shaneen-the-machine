const Chord = require("./Chord.js")
const pitch = require("./pitch.js")

function* findChordProgressionFragments(music) {
  // Attempt to identify chord sequences in any music.
  // Yield each uninterupted sequence as an array of strings

  let frag = [] // current uninterupted sequence

  // loop through all the notes in the music
  for(var note of music.notes) {
    // get all sounds playing concurrently with the note
    let t = note.t // time of the current note
    let selection = music.select(t)

    // find the bass pitch and octave profile
    let bass = pitch.pc(selection.bass)
    let op = selection.octaveProfile

    // find a chord which matches the octave profile
    let chord = Chord.fromOctaveProfile(op)

    if(chord != '?') {
      // Found a chord!
      // set the bassnote of the chord
      chord.bass = bass

      // skip if it is a duplicate of the previous chord
      if(chord.print != frag[frag.length-1])
        frag.push(chord.print)
    } else {
      // if no chord yield the fragment
      if(frag.length > 1)
        yield frag
      frag = []
    }
  }
  if(frag.length > 1)
    return frag
}
module.exports = findChordProgressionFragments
