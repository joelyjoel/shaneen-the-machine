var poet = require("../words/");
var argv = require('minimist')(process.argv.slice(2));
var logbook = require("../logbook.js");

var txt = argv._[0] || "Enter text as a command line argument.";
var myText = new poet.Paragraph(txt);

console.log("\n\n", txt, "\n");
myText.analyse().then(function() {
    //myText.searchAndSolidifyPos(argv.s);
    myText.qify();
    console.log(myText.print, "\n");
    myText.rollQs().then(function() {
        console.log(myText.print, "\n");
        logbook(myText.print);
    })
})


//console.log(myText);
