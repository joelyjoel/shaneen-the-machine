var Sample = require("../sound/Sample.js");
var Osc = require("../sound/ClockworkOsc.js");
var Ramp = require("../sound/ClockworkRamp.js");
var Shaper = require("../sound/ClockworkWaveshaper.js");

var tape = new Sample().blank(44100);
var osc = new Osc(20+Math.random()*20);
osc.db = new Ramp(44100, 0, -60, -1);
osc.pControl = new Ramp(44100, 60, 0, -1.5);
var shaper = new Shaper();
shaper.input = osc;
shaper.drive = new Ramp(44100, 50*Math.random(), 0, -Math.random()*2);

for(var t=0; t<tape.lengthInSamples; t++) {
    tape.channelData[0][t] = shaper.tick();
}
tape.normalise();
console.log("rms:", tape.rms());
tape.save();
