var make_harmony = require("../music/make_harmony.js");
var make_melody = require("../music/make_melody.js");

var chords = make_harmony(32);
console.log(chords);

var mel = make_melody.tranceyThing(chords);
mel.save("make_melody.mid");
//console.log(mel);
