var fs = require("fs");
var parseMidiFile = require("midi-file-parser");
var midiConverter = require("midi-converter");

var file = fs.readFileSync("./complexMidiExample.mid", "binary");

var json = parseMidiFile(file);
fs.writeFileSync("example.json", JSON.stringify(json));

var file2 = midiConverter.jsonToMidi(json);
console.log(file2.constructor);
fs.writeFileSync("bachcopy.mid", file2, "binary");
