var PCM = require("../sound/PatchCordMonkey.js");
var Sample = require("../sound/Sample.js")

var system = PCM.randomOscGroup();

var tape = new Sample().blank(44100);
for(var t=0; t<tape.lengthInSamples; t++)
    tape.channelData[0][t] = system.tick();

tape.normalise();
console.log("RMS:", tape.rms());
tape.save("metallic monkey hit.wav", "hits");
