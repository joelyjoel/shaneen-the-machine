var DrumTrack = require("../music/DrumTrack.js");
var DrumGroup = require("../music/DrumGroup.js");
var make_rhythm = require("../music/make_rhythm.js");

var group = new DrumGroup().blank(16, 16);

group.poke(40);
group = group.evolve(16*8, 40);
group.poke(120, 0.5);

group.mixDownNumberedTracks();
group.save("drum debug.mid");
