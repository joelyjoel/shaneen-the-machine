var ImaginaryLanguage = require("../words/ImaginaryLanguage.js");
var fs = require("fs");
var argv = require("minimist")(process.argv.slice(2));

var lang = argv.l || "not-french";
var dict = new ImaginaryLanguage(lang);
if(dict.vowells.length == 0) {
    dict = new ImaginaryLanguage("prototype").subLanguage();
    dict.name = lang;
}

//var txt = fs.readFileSync("./resources/texts/Alice In Wonderland.txt", "utf-8");
//console.log(txt.length);
var out = dict.translateEnglish(argv._[0] || "Hello, how are you?");
console.log("\n", "english:", argv._[0] || "Hello, how are you?", "\n", lang+":", out, "\n");

dict.save();

//fs.writeFileSync("./Alice in Wonderland [not-french translation].txt", out);
