var StepTrack = require("../music/StepTrack.js");
var PointTrack = require("../music/PointTrack.js");
var VoiceTrack = require("../music/VoiceTrack.js");

var myTrack = new StepTrack().blank(16);
myTrack.steps[0] = true;
myTrack.steps[5] = true;

var myOtherTrack = new PointTrack(myTrack);

console.log(myOtherTrack.notes);


var yetAnotherTrack = new VoiceTrack(myOtherTrack);
console.log(yetAnotherTrack);
