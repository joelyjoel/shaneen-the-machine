var DrumTrack = require("../music/DrumTrack.js");
var DrumGroup = require("../music/DrumGroup.js");
var make_rhythm = require("../music/make_rhythm.js");

var myTrack = make_rhythm.kick(32);

/*var kick = new DrumTrack();
kick.gamut = [36];
kick.gamutIndexVersion = myTrack;
//kick.save("debug kick.mid");

var hihat = new DrumTrack();
hihat.gamut = [42, 44, 46];
hihat.gamutIndexVersion = make_rhythm.offBeatHihat(32);

var kit = new DrumGroup().blank(0, 16);
kit.tracks = [kick, hihat];
kit.mixDownNumberedTracks();
kit.save("debugging drums.mid");
*/

var kit = new DrumGroup("909", 16);
//console.log(kit);
kit.poke(30);
kit.mixDown();
kit.save();
