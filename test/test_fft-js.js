var FFT = require("fft.js");
var Sample = require("../sound/Sample.js");

Sample.readFile("./exampleSample.wav").then(function(samp) {
    /*var signal = samp.channelData[0];
    //console.log(signal);
    var phasors = fft(signal.slice(0,512));
    console.log(phasors.length);

    var frequencies = fftUtil.fftFreq(phasors, 512), // Sample rate and coef is just used for length, and frequency step
        magnitudes = fftUtil.fftMag(phasors);
    console.log(frequencies);

    var both = frequencies.map(function (f, ix) {
        return {frequency: f, magnitude: magnitudes[ix]};
    });



    //console.log(both[both.length-1]);*/
    //console.log(samp.peak());

    for(var t=0; t<samp.lengthInSamples; t++) {
        samp.channelData[0][t] = samp.channelData[1][t] = Math.sin(Math.PI*2 * t/128);
        //samp.channelData[0][t] = samp.channelData[1][t] = Math.sin(t/44100 * Math.PI * 2 * 440);
        //samp.channelData[0][t] = samp.channelData[1][t] = Math.sin(t/44100 * Math.PI * 2 * (2000*t/samp.lengthInSamples));
    }
    samp.saveWavFile("exampleWithNye.wav");

    var anal = samp.calculateFFT(1024);
    console.log(anal[0].frames);
    console.log("samp", samp.peak());
    //var json = JSON.stringify(anal);
    //console.log(anal[0].frequencyBins);

    /*console.log("starting resynthesis");
    var reconstructed = new Sample()
        .blank(samp.lengthInSamples, 1);

    var chan = anal[0];
    /*for(frame in chan.frames) {
        frame = parseInt(frame);
        console.log("frame ", frame)
        var t1 = frame * chan.frameSize;
        var t2 = (frame+1) * chan.frameSize;
        var y = 0;
        for(var t=t1; t<t2; t++) {
            for(var bin in chan.frequencyBins) {
                var f = chan.frequencyBins[bin];
                var A = chan.frames[frame].magnitudes[bin];
                var phase = chan.frames[frame].phases[bin];
                var phasor = chan.frames[frame].phasors[bin];
                /*if(parseInt(bin)%2 == 0) {
                    y += A * Math.sin(Math.PI*2*(f*t - phase));
                } else {
                    y += A * Math.cos(Math.PI*2*(f*t - phase));
                }
                y += phasor.real * Math.sin(Math.PI*2*f);
                y += phasor.im * Math.cos(Math.PI*2*f);
                //phase *= Math.PI * 2;
            }
            y /= 5000;
            //console.log(y);
            reconstructed.channelData[0][t] = y;
            if(Math.abs(y) > 1) {
                console.log("y is too large!", y)
                throw "y is too large!!";
            }
        }
    }*/
    /*var fft = new FFT(chan.frameSize);
    console.log("sick");
    for(var frame in chan.frames) {
        var frame = parseInt(frame);
        var t1 = frame * chan.frameSize;
        var t2 = (frame+1) * chan.frameSize;
        //var buf = new Float32Array(chan.frameSize*2);
        var buf = [];
        fft.inverseTransform(buf, chan.frames[frame].phasors);


        for(var t=t1; t<t2; t++) {
            var a = buf[(t-t1)*2];
            var b = buf[(t-t1)*2 + 1]
            var y = Math.sqrt(a*a + b*b) * 1000;
            reconstructed.channelData[0][t] = y;
            console.log(y);
            //console.log()
        }
    }
    fft.inverseTransform(reconstructed.channelData[0], chan.frames);
    //reconstructed.channelData[0]
    reconstructed.saveWavFile("reconstructedFFTthing.wav");*/
});
