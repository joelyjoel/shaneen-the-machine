var make_clockwork = require("../sound/make_clockwork.js");
var Sample = require("../sound/Sample.js");

var T = 44100;

//var osc = make_clockwork.osc(30);
//osc.A = make_clockwork.ramp(T);
kick = make_clockwork.kickSweep(T);

var tape = Sample.record(kick, T);
tape.normalise();
tape.save();
