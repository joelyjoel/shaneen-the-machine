var poet = require("../words/");
var argv = require("minimist")(process.argv.slice(2));

poet.Word.randomWordFromPOS("vt", 8).then(function(word) {
    console.log("randomWord:", word)
});

var myWord = new poet.Word(argv._[0] || "cake");
myWord.fetchInfo().then(function(w) {
    console.log("done", w);
}, function(w) {
    console.log("error",w)
});
