var clockwork = require("../sound");

var myxer = new clockwork.Mixer();

myxer.inputs = [new clockwork.Osc(440), new clockwork.Osc(200)];
myxer.levels = [1/2, 1/2];
console.log(myxer.levels);

var tape = new clockwork.Sample().blank(44100);
for(var t=0; t<tape.lengthInSamples; t++) {
    console.log(myxer.y);
    tape.channelData[0][t] = myxer.tick();
}
tape.saveWavFile("testingMixer.wav")
