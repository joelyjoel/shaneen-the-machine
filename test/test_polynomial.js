var clockwork = require("../sound/");

var T = 44100/2;

var osc = new clockwork.Osc();
osc.p = 50;
osc.pControl = new clockwork.Polynomial(T);
osc.pControl.randomise(50);
osc.pControl.A = 100;
osc.A = new clockwork.Ramp(T, 1, 0, -Math.random());
var osc2 = new clockwork.Osc();
osc2.p = 50;
osc2.pControl = new clockwork.Polynomial(T);
osc2.pControl.randomise(50);
osc2.pControl.A = 100;
osc2.A = osc;

osc.randomiseWaveform();
osc2.randomiseWaveform();

clockwork.Sample.record(osc2, T).save("polynomial thing.wav");
