var ClockworkOsc = require("../sound/ClockworkOsc.js");
var ClockworkFartMachine = require("../sound/ClockworkFartMachine.js");
var Sample = require("../sound/Sample.js");
var ClockworkRamp = require("../sound/ClockworkRamp.js");


var T = 5 * 44100;
var fm = new ClockworkFartMachine();
fm.intensity = new ClockworkRamp(T/2, 10, 0);
console.log(fm.sockets);

var samTheSample = new Sample(1).blank(T);
for(var t=0; t<T; t++) {
    if((t%Math.floor(44100/16)) == 0 && Math.random()<0.5)
        fm.randomise();
    samTheSample.channelData[0][t] = fm.tick();
}

samTheSample.saveWavFile("./resources/samples/Shaneen Farting.wav");
