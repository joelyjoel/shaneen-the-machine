var Chord = require("../music/Chord.js");
var midi = require("../music/midi.js");
var VoiceTrack = require("../music/VoiceTrack.js");
var pitch = require("../music/pitch.js");
var fs = require("fs");

//console.log(process.cwd())
//console.log(fs.existsSync("./exampleSample.wav"));
var bach = midi.parseMidiFile("./03AchGott.mid");

for(var channel in bach.tracks) {
    //bach.tracks[channel] = new VoiceTrack(bach.tracks[channel]);
}

var track = bach.mixDownNumberedTracks();

var chords = [];
var segD = 1;
for(var t=0; t<track.d; t+=segD) {
    var selection = track.select(t);
    //console.log(selection);
    var op = selection.octaveProfile;
    var chord = Chord.fromOctaveProfile(op);
    var printedOP = [];
    for(var i in op) {
        printedOP[i] = pitch.printPitchClass(op[i]);
    }

    if(chord != "?") {
        chords.push(chord.print);
        console.log(chord.print, printedOP);
    } else {
        chords.push("?");
        console.log(chord, printedOP);
    }

    //fs.writeFileSync("bachAnalysis.txt", chords.join(" "));
}
