var Clockwork = require("../sound/");

var tape = new Clockwork.Sample().blank(441000);

var osc = new Clockwork.Osc(150, "saw", 0.5);
var shaper = new Clockwork.Waveshaper();
shaper.input = osc;
shaper.drive = new Clockwork.Osc(3, "triangle", 0.5, 1);


for(var t=0; t<tape.lengthInSamples; t++) {
    tape.channelData[0][t] = shaper.tick();
}

tape.saveWavFile("testing_ClockworkWaveshaper.wav");
