var Sample = require("../sound/Sample.js");
//var Osc = require("../sound/ClockworkOsc.js");
//console.log(Osc);

Sample.readFile("./exampleSample.wav").then(function(samp) {
    samp.mixDownToMono();
    //console.log("Z", samp);
    var zX = samp.diceByZeroCrossings();
    //console.log("A");
    //var LFO = new Osc(5, "sin", 5, 10)
    //console.log(LFO)
    var samp = Sample.concat(zX, 50);
    //console.log("B");
    samp.save();
});
