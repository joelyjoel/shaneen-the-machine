var Sample = require("../sound/Sample.js");
var argv = require("minimist")(process.argv.slice(2));
var zxsm = require("../sound/zxsm.js");
var zxsmStereo = require("../sound/zxsmStereo.js")


var mysamp = Sample.choose(argv._[0] || "").then(function(samp) {
    samp.mixDownToMono();

    var grainSize = 5+Math.floor(Math.random()*10);
    //console.log(samp.label)
    var out = zxsm.granularScrub(samp, grainSize, 44100 * 60);
    out = zxsmStereo.stepwisePanGrains(out, grainSize);
    //var out = zxsm.timeShiftGrains(samp, grainSize);
    //out = zxsm.wobbleGrainLengths(samp);
    //out = samp;
    //out.mix(samp);
    out.normalise();
    out.save(samp.label);
});
