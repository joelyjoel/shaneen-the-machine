var parseText = require("../words/parseText.js");
var argv = require('minimist')(process.argv.slice(2));
var fs = require("fs");

var txt = argv._[0] || "Enter text as a command line argument";
console.log( parseText(txt) );
//console.log("original:", txt);
