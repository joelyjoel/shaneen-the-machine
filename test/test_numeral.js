var numeral = require("../music/numeral");
var argv = require("minimist")(process.argv.slice(2));

var myNume = argv._[0] || "II7b";
var parsedNume = numeral.parse(myNume);
console.log(myNume, parsedNume);

var andBack = numeral.pack(parsedNume);
console.log(andBack);
