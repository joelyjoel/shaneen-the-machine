var Sample = require("../sound/Sample.js");
var ClockworkOsc = require("../sound/ClockworkOsc.js");
var ClockworkRamp = require("../sound/ClockworkRamp.js");

var osc = new ClockworkOsc();
osc.A = new ClockworkRamp();
osc.p = 60;

var tape = new Sample().blank(44100);

for(var t=0; t<tape.lengthInSamples; t++) {
    tape.channelData[0][t] = osc.tick();
}

tape.saveWavFile("testing_ClockworkRamp.wav");
