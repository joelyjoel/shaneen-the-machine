var HarmonySymbol = require("../music/HarmonySymbol.js");
var Scale = require("../music/Scale.js");
var Chord = require("../music/Chord.js");

console.log(HarmonySymbol);

var myScale = new Scale("Db:");
console.log(myScale.octaveProfile);
console.log(myScale.pToDegree(55));

var myChord = new Chord("Db7");
console.log(myChord.octaveProfile);
console.log(myChord.pToDegree(55));
