const Sample = require("../dsp/Sample.js");

var tape = new Sample().blank(44100*6, 6);

for(var t=0; t<tape.lengthInSamples; t++) {
  var c = Math.floor(t/44100);
  tape.channelData[c][t] = Math.random()*0.4;
}

tape.save("test five point one b4")
