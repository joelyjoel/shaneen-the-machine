var MelodyBrush = require("../music/MelodyBrush.js");
var Score = require("../music/Score.js");
var HarmonyNetwork = require("../music/HarmonyNetwork.js");
var shaneen = require("../music/");

var broosh = new MelodyBrush();
var ctx = new MelodyBrush.Context();

//console.log(broosh.filters);
var key = new shaneen.Scale("C:");


var hn = new HarmonyNetwork("./resources/hmaps/tonality.hmap");
var chords = hn.multiStepSearch(8);
for(var i in chords) {
    chords[i] = key.numeralToChord(chords[i]);
}

var score = new Score().blank(chords.length*8,4);
score.tracks.chords = new shaneen.ChordTrack();
console.log(score.tracks.chords)
for(var i=0; i<chords.length; i++) {
  var note = new shaneen.ChordTrack.Note()
  note.sound = chords[i]
  note.t = i
  note.d = 8
  //  score.tracks.chords.steps[i*8] = chords[i];
}
//score.chords = "C``` ```` Am``` ```` F``` ```` G``` ````";

console.log(score.tracks.chords);
//console.log(score.chord(0));

var numberedTracks = score.numberedTracks;
//console.log("numbered tracks:", numberedTracks);
for(var c in numberedTracks) {
  console.log("c:", c, "nNotes:", numberedTracks[c].notes.length)
  for(var t=0; t<numberedTracks[c].d; t+=4) {
    console.log("stroking");
    broosh.stroke(score, t, c);
        //break;
  }
    //break;
}

for(var c in numberedTracks) {
    if(!numberedTracks[c].isAPointTrack)
        score.tracks[c] = score.tracks[c].toPointTrack();

    //console.log("here:", score.tracks[c], c)
    /*for(var i in score.tracks[c].notes) {
        score.tracks[c].notes[i].t += parseFloat(c);
        score.tracks[c].notes[i].d = 3;
    }*/
}
//console.log(score.matrix);
score.mixDownNumberedTracks();
//console.log(score);

console.dir(score)

score.saveMidiFile("testingMelodyBrush.mid");
//*/
