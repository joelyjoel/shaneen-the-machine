var rwt = require("../words/regularWordTransformations.js");
var poet = require("../words");

function test(name) {
    var from = name.split(">")[0];
    var to = name.split(">")[1];
    var word = poet.Word.randomWordFromPOS(from).then(function(wrd) {
        wrd = wrd.w;
        var transformed = rwt[name](wrd);
        console.log(wrd, "["+name+"]", transformed, "\n");
    });
}

for(var j=0; j<10; j++)
    for(var i in rwt)
        test(i);
