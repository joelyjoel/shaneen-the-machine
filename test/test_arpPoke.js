var PointTrack = require("../music/PointTrack.js");
var make_harmony = require("../music/make_harmony.js");

var harmony = make_harmony(16);
var track = new PointTrack().blank(16);;
track.arpPoke(harmony);
track.arpPoke(harmony);
track.arpPoke(harmony);
track.arpPoke(harmony);
track.arpPoke(harmony);
track.arpPoke(harmony);
track.arpPoke(harmony);
track.arpPoke(harmony);
track.arpPoke(harmony);
track.arpPoke(harmony);
track.arpPoke(harmony);
console.log(track);
track.save();
