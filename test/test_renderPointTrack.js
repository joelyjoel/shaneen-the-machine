var PointTrack = require("../music/PointTrack.js");
var midi = require("../music/midi.js");
var renderPointTrack = require("../sound/renderPointTrack.js");

var bach = midi.parseMidiFile("complexMidiExample.mid");
bach = bach.mixDownNumberedTracks();

for(var i in bach.notes) {
    bach.notes[i].d *= 0.25;
}

var tape = renderPointTrack(bach);
tape.saveWavFile("testing_renderPointTrack.wav");
