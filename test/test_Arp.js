var Arp = require("../music/Arp.js");
var Scale = require("../music/Scale.js");
var Score = require("../music/Score.js");
console.log(Arp);

var myScore = new Score().blank(16, 1);
myScore.chords = "C``` ```` G7``` ````Dm7``````` G7```````";
//myScore.loop(32);
var sequence = Arp.make_sequence(myScore.d);
myScore.tracks[0] = sequence;
Arp(myScore.tracks[0], myScore);
myScore.saveMidiFile("Testing Arp.mid");

myNote = new Arp.Q("H66*");
console.log(myNote, myNote.print);

/*myScore.chords = "C ``````` Dm ```````";
//myScore.tracks[0].steps[0] = new Arp.Note({"O":5, "h":3});
//myScore.tracks[0].steps[8] = new Arp.Note({"s":1});

for(var i=0; i<myScore.tracks[0].steps.length; i++) {
    myScore.tracks[0].steps[i] = new Arp.Note({"h":5, "backwards":Math.random()<1/2});
    if(Math.random() < 0.5) {
        myScore.tracks[0].steps[i] = new Arp.Note({"H":5, "backwards":Math.random()<1/2});
    }
    console.log("sequencing step", myScore.tracks[0].steps[i]);
}
myScore.tracks[0] = myScore.tracks[0].toPointTrack();

var highestOrder = Arp.highestOrder(myScore.tracks[0]);
Arp.fullyComputeTrack(myScore.tracks[0], myScore, undefined, 0, false);

console.log(myScore.tracks[0].notes);*/



//console.log(highestOrder)
/*var arpNote = {"h": 1}
var p = 60;
var newP = Arp.compute(arpNote, p, "C", "C:");
console.log(p, newP);*/


/*var myScale = new Scale("C:");
for(var n=-12; n<24; n+=0.25) {
    var p = myScale.degreeToP(n);
    var deg = myScale.pToDegree(p);
    if(deg != n)
        console.log("n:", n, "p:", p, "deg:", deg);
    //break;
}
*/
