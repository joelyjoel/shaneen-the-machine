
var Clockwork = require("../sound/");

var samp = new Clockwork.Sampler();
//var filename = "./resources/samples/gtts/When I find myself in times of trouble, mother mary comes to me. Speaking words of wisdom, let it be.wav";
var filename = "./philipp.wav";
samp.readFile(filename, 4000, 10000).then(function(sampler) {
    console.log(sampler.sample.lengthInSamples);
    var tape = new Clockwork.Sample().blank(441000);
    //console.log(sampler.sample.sampleRate);

    //sampler.detune = new ClockworkOsc(0.25, "triangle", 12);
    //sampler.rate.equilibrium = 1;

    for(var t=0; t<tape.lengthInSamples; t++) {
        //console.log(":)", t, tape.lengthInSamples);
        if(Math.random()<0.00001 || t%Math.floor(44100/8) == 0) {
            sampler.randomSkip();
            sampler.detune = Math.random()*24 - 12;
        }
        var y = sampler.tick();
        tape.channelData[0][t] = y;
        //console.log(y);
        //console.log(":o", t, tape.lengthInSamples);
    }
    tape.saveWavFile("testing_ClockworkSampler.wav");
});
