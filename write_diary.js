var tts = require("./sound/textToSpeech.js");
var clockwork = require("./sound/");
var argv = require("minimist")(process.argv.slice(2))

var date = new Date();
var dayOfWeek = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"][date.getDay()];
var dayOfMonth = date.getDate();
if(dayOfMonth%10 == 1)
    dayOfMonth += "st";
else if(dayOfMonth%10 == 2)
    dayOfMonth += "nd";
else if(dayOfMonth%10 == 3)
    dayOfMonth += "rd";
else
    dayOfMonth += "th";

var month = ["January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    [date.getMonth()];
var year = date.getFullYear();
var hour = date.getHours();
if(hour > 12) {
    hour -= 12;
}
var ampm = hour >= 12 ? "pm" : "am"
var minute = date.getMinutes();
var time = hour+":"+minute + ampm;
var dateString = time+", "+dayOfWeek + " " + dayOfMonth + " of " + month + " " + year;

var dateSamp;
var dateProm = tts(dateString, "en");

var txt = argv._[0];
var txtSamp;
var entryProm = tts(txt, "en");

var osc = new clockwork.Osc();
var beeps = [];
for(var i=0; i<3; i++) {
    osc.randomiseVibrato();
    osc.f = 300*Math.random();
    osc.randomiseWaveform();
    osc.pahse = 0;

    beeps[i] = new clockwork.Sample().blank(4410*3);
    for(var t=0; t<beeps[i].lengthInSamples; t++) {
        beeps[i].channelData[0][t] = osc.tick();
    }
}

Promise.all([dateProm, entryProm]).then(function(samps) {
    samps[0].repitch(5);
    samps[1].repitch(Math.random()*24-12);
    var d = samps[0].lengthInSamples + samps[1].lengthInSamples
        + beeps[0].lengthInSamples + beeps[1].lengthInSamples + beeps[2].lengthInSamples;

    var master = new clockwork.Sample().blank(d);
    var t = 0;
    master.overwrite(beeps[0], t);
    t += beeps[0].lengthInSamples;
    master.overwrite(samps[0], t);
    t += samps[0].lengthInSamples;
    master.overwrite(beeps[1], t);
    t += beeps[1].lengthInSamples;
    master.overwrite(samps[1], t);
    t += samps[1].lengthInSamples;
    master.overwrite(beeps[2], t);
    t += beeps[2].lengthInSamples;
    console.log("we made it?");
    master.saveWavFile("../../Diary/"+date.toLocaleString()+".wav").then(function() {
        console.log("done");
    });
}, function() {
    console.log("probably failed:()");
});
