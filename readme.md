# Shaneen the Machine
A personified javascript library for composing music, synthesising/manipulating sounds and writing poetry.

## music/
This directory contains functions and classes that deal with algorithmic composition at a symbolic level, rather than a sonic level. It contains two main types of script. There are classes and functions which are designed to be general purpose tools for representing and manipulating musical structures. And there are also the *make_* functions, which use these tools and take a great deal more creative liberty to generate musical material. This exploits the ability in javascript to store functions as object properties inside other functions, building a hierarchical structure. Eg/ *make_rhythm()* calls, at random, any other *make_* function which generates a rhythm and all of these functions are accessible as properties eg/ *make_rhythm.trappyHiHats()*.

### music/Arp.js
An elaborate kind of arpeggiator which reads pitch-queries or *ArpQ*'s which can be embedded in place of pitches in *StepTrack* and *PointTrack* (sub)classes. ArpQ's are a way of notated the harmonic/melodic function of a pitch divorced from its harmony/melodic context. They are stored as javascripts but can also be represented by strings. For example, "<h2" means two harmony notes above the previous note, or ">s-1" means one modal step below the next note.
#### Arp(track, score)
Converts all ArpQ's within *track* into absolute pitches using the context provided by *score*.
#### Arp.make_q(*kind*)
Generates a randomised ArpQ of a certain *kind*:
- "stepApproach"
- "harmonyNote"
- "resolution"
- "absolute"
#### Arp.make_qs(n, mode)
Generates a sequence (as an Array). *mode* is a number which changes how the function chooses ArpQ's.
#### Arp.make_sequence(rhythm, mode)
Generates ArpQ's for each note in a given *rhythm*.

### Chord.js
A subclass of *HarmonySymbol* for storing a chord symbol.

### ChordTrack.js
A subclass of *StepTrack* specialising in storing *Chord* objects. In other words this is a chord progression class.

### convertTracks.js
Contains functions for converting one type of track into another. Most crucially converting between *PointTrack* and *StepTrack*.

### cstring.js

### DrumTrack.js [deprecated] [does not inherit from *TrackGroup*]
Stores and manipulates drum patterns as a series of *RhythmTrack* channels.

### HarmonyNetwork.js
An order-2 markov chain class for generating/analysing chord progressions. Can be used with EITHER roman numeral notation or with jazz chord notation.

### HarmonySymbol.js

### make_gliss.js
### make_rhythm.js
### make_roll.js
### make_trap.js
### make_trapHats.js

### MelodyBrush.js
### MeterTrack.js
### midi.js
### midiRef.js
### numeral.js
### pitch.js
### PitchRange.js
### PointTrack.js
### produce_randomBasslines.js

### music/WeightedSet.js
A simple class efficiently storing string/number values along with associated probability-weights.
#### WeightedSet.prototype.roll() 
choose a random from the set. All probability-weights are first summed (**.pSum**) so that actual probability of a value is its probability-weight divided by the sum of all the probability-weights in the set.
#### WeightedSet.prototype.increment(val, p)
increments the probability-weight of *val* by *p*
#### WeightedSet.prototype.addSet(set2)
merge one set with another, summing the probability-weights.
#### WeightedSet.prototype.multiply(set2)
multiply one set by another



## sound/
## words/