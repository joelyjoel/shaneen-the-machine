const fs = require("fs");
const Path = require("path")
const config = require("./config.js")

function chooseFilename(name, subdir, bigdir) {
    bigdir = config.sampleFolder || "./output/"

    var date = new Date();
    var strMonth = [
        "jan", "feb", "mar", "apr", "may", "jun",
        "jul", "aug", "sep", "oct", "nov", "dec"]
        [date.getMonth()];
    var strDate = date.getDate() + strMonth.toUpperCase() + date.getFullYear();
    strDate = strDate.toUpperCase();


    subdir = subdir || config.sampleInboxSubdir || "inbox";
    var extension = name.slice(name.lastIndexOf("."));
    name = name.slice(0, name.length-extension.length);
    if(name == "")
        name = strDate;


    // check subdir exists, if not then make it
    mkdirIfDoesNotExist(bigdir + subdir)

    var n = 1;
    var filename;
    do {
        filename = bigdir+subdir+"/"+name + (n ? " #"+n : "") + extension;
        n++;
    } while(fs.existsSync(filename));

    return filename;
}
module.exports.chooseFilename = chooseFilename;

function chooseResourceFilename(name, subdir) {
  return chooseFilename(name, subdir, "./resources/")
}
module.exports.chooseResourceFilename = chooseResourceFilename

function mkdirIfDoesNotExist(path) { // (sync)
  path = Path.relative("./", path)
  var bits = path.split(Path.sep)
  var current = ""
  try {
    for(var i in bits) {
      current = Path.resolve(current, bits[i])
      if(!fs.existsSync(current)) {
          fs.mkdirSync(current)
          console.log("made new directory:", current)
      } else {
        //console.log("directory exists", current)
      }
    }
  } catch(e) {
    console.log("error making save directory:", e)
  }
}
module.exports.mkdirIfDoesNotExist = mkdirIfDoesNotExist
