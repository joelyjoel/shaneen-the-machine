const argv = require("minimist")(process.argv.slice(2))

const parseText = require("../parseText.js")
const Paragraph = require("../Paragraph.js")
const getCuttings = require("../getCuttings.js")

getCuttings().then(function(cuttings) {
  var para = new Paragraph(argv._[0] || cuttings[0])
  return para.concatPhrases()
}).then(function(para) {
  return para.analyse()
}).then(function(para) {
  for(var i in para.bits) {
    console.log(para.bits[i].w, para.bits[i]._pos)
  }
})
