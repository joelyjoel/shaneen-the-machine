const anatomicalSong = require("../anatomicalSong.js")
const logbook = require("../../logbook.js")
const textToSpeech = require("../../dsp/textToSpeech.js")
const SampleQ = require("../../dsp3/components/SampleQ.js")
const argv = require("minimist")(process.argv.slice(2))


const lang = argv.l || "en-uk"
const n = argv.n || 1
const p = argv.p || 0
const alt = argv.alt || false

var stanza = anatomicalSong.printLines(n, alt)
var lines = stanza.split("\n")
console.log("\n\n" + stanza, "\n\n")
logbook(stanza)


for(var i in lines) {
  textToSpeech(lines[i], lang).then(function(sample) {
    sample.repitch(p)
    sample.saveResource("single line ("+lang+(p ? " "+p+" semitones" : "")+")", "anatomysong2")
  })

  /*var sq = new SampleQ().gtts(lines[i], lang)
  //sq.grainGap = 1
  sq.trigger()
  sq.onPlaySample = function() {
    console.log(this.i)
  }
  sq.selectionLogic = function(i) {
    return i+1.5
  }

  sq.OUT.render(5).then(function(tape) {
    tape
    .save("single line ("+lang+(p ? " "+p+" semitones" : "")+")", "anatomysong")
  })*/
}
