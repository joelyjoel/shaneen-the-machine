const Inlet = require("./Inlet.js")
const Outlet = require("./Outlet.js")
const Circuit = require("./Circuit.js")

function Unit() {

  this.inWires = []
  this.outWires = []
  this.inlets = {}
  this.outlets = {}
  this.inletOrder = []
  this.outletOrder = []
  this.tickInterval = 1

  this.constructor.timesUsed = (this.constructor.timesUsed || 0) + 1
  this.label = this.constructor.name + this.constructor.timesUsed
}
module.exports = Unit

Unit.signalChunkSize = 256
Unit.sampleRate = 44100

Unit.prototype.isUnit = true
Unit.prototype.sampleRate = Unit.sampleRate

Unit.prototype.tick = function(clock) {
  if(this._tick)
    this._tick() // determines how information is processed within the unit
}


Unit.prototype.addInlet = function(name, args) {
  args = Object.assign({}, args, { name: name, unit: this })
  var inlet = new Inlet(args)
  this.inlets[name] = inlet
  var upperCaseName = name.toUpperCase()
  this.__defineGetter__(upperCaseName, function() {
    return inlet
  })
  if(inlet.chunkSize) {
    this.__defineSetter__(upperCaseName, function(val) {
      if(val.constructor == Number)
        val = new Float32Array(inlet.chunkSize).fill(val)
      this[name] = val
    })
  }
  if(inlet.default)
    this[upperCaseName] = inlet.default
  this.inletOrder.push(inlet)
}
Unit.prototype.addOutlet = function(name, args) {
  args = Object.assign({}, args, { name: name, unit: this })
  var outlet = new Outlet(args)
  this.outlets[name] = outlet
  this.__defineGetter__(name.toUpperCase(), function() {
    return outlet
  })
  this.outletOrder.push(outlet)

  // set default
  if(outlet.nChannels) {
    var val = this[name] = []
    for(var c=0; c<outlet.nChannels; c++)
      if(outlet.chunkSize)
        val[c] = new Float32Array(outlet.chunkSize).fill(0)
      else
        val[c] = 0
  } else {
    if(outlet.chunkSize)
      this[name] = new Float32Array(outlet.chunkSize).fill(0)
    else
      this[name] = 0
  }
}

Unit.prototype.__defineGetter__("defaultInlet", function() {
  return this.inletOrder[0]
})
Unit.prototype.__defineGetter__("defaultOutlet", function() {
  return this.outletOrder[0]
})

Unit.prototype.exploreCircuit = function(circuit) {
  var circuit = circuit || new Circuit()//{wires: [], units:[]}
  if(circuit.units.indexOf(this) == -1)
    circuit.units.push(this)

  for(var i in this.inWires)
    circuit = this.inWires[i].exploreCircuit(circuit)
  for(var i in this.outWires)
    circuit = this.outWires[i].exploreCircuit(circuit)

  return circuit
}

Unit.prototype.findProcessIndex = function(history) {
  history = history || []
  if(history.indexOf(this) != -1) {
    return -1
  }

  if(this.processIndex != undefined)
    return this.processIndex

  history = history.concat([this])

  var winner = -1
  for(var i in this.inWires) {
    var index = this.inWires[i].findProcessIndex(history)
    if(index > winner) {
      winner = index
    }
  }

  this.processIndex = winner + 1
  return winner + 1
}
