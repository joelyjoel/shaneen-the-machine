const Sample = require("./Sample.js")

// temporary!!
function render(outlet, T, nChannels) {
  if(outlet.isUnit || outlet.isPatch)
    outlet = outlet.defaultOutlet
  console.time("rendering " + outlet.label)
  nChannels = nChannels || outlet.nChannels || 1
  var circuit = outlet.unit.exploreCircuit()
  circuit.calculateOrder()
  var tape = new Sample().blank(T, nChannels)
  if(outlet.chunkSize) {
    for(var t=0; t<T; t++) {
      circuit.tick(t)
      if(t%outlet.chunkSize == 0) {
        var signalChunk = outlet.value
        if(outlet.nChannels) {
          for(var c=0; c<outlet.nChannels; c++) {
            for(t2=0; t2<outlet.chunkSize; t2++)
              tape.channelData[c][t+t2] = signalChunk[c][t2]
          }
        } else {
          for(var t2=0; t2<outlet.chunkSize; t2++)
            tape.channelData[0][t+t2] = signalChunk[t2]
        }
      }
    }
  } else {
    for(var t=0; t<T; t++) {
      circuit.tick(t)
      if(!outlet.nChannels) {
        tape.channelData[0][t] = outlet.value
      } else {
        for(var c=0; c<nChannels; c++) {
          tape.channelData[c][t] = outlet.value[c]
        }
      }
    }
  }
  console.timeEnd("rendering " + outlet.label)
  tape.addMeta({
    renderedByDsp2: true,
  })
  return tape
}
module.exports = render
