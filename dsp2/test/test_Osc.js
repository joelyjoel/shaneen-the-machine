const Osc = require("../simple/Osc.js")
const Add = require("../simple/Add.js")
const Multiply = require("../simple/Multiply.js")
const Wire = require("../Wire.js")
const Sample = require("../Sample.js")

const argv = require("minimist")(process.argv.slice(2))

const T = 44100 * (argv.T || 1)
var osc = new Osc(10)

var scale = new Multiply()
new Wire(osc.outlets.out, scale.inlets.a)
scale.b = 100

var add = new Add()
add.a = 300
new Wire(scale.OUT, add.B)

var osc2 = new Osc()
new Wire(add.OUT, osc2.F)


var circuit = osc2.exploreCircuit()
circuit.calculateOrder()
console.log(circuit.order)
var tape = new Float32Array(T)
for(var t=0; t<44100; t++) {
  circuit.tick(t)
  tape[t] = osc2.out
}
console.log(tape)

var samp = new Sample()
samp.channelData = [ tape ]
samp.save("testing dsp2")
