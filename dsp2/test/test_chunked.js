const Osc = require("../chunked/Osc.js")
const Sum = require("../chunked/Sum.js")
const Gain = require("../chunked/Gain.js")
const Wire = require("../Wire.js")
const render = require("../render.js")

const argv = require("minimist")(process.argv.slice(2))
const T = 44100 * (argv.T || 1)

var osc1 = new Osc(10000 + Math.random()*100)
var osc2 = new Osc(10000 + Math.random()*100)
var osc3 = new Osc(20 + Math.random()*25)
osc3.waveTable = Osc.squareTable
var sum1 = Sum.many(osc1, osc2, osc3)

var gain1 = new Gain(-24)
new Wire(sum1, gain1)

render(gain1, T)
.save("testing dsp2 chunking")
