const Wire = require("../Wire.js")
const Unit = require("../Unit.js")

console.log("at least everything loaded!\n")


var unit1 = new Unit()
var unit2 = new Unit()

unit1.addOutlet("y")
unit2.addInlet("x")

unit2.addOutlet("y")
unit1.addInlet("x")


var maWire = new Wire(unit1.outlets.y, unit2.inlets.x)

new Wire(unit2.outlets.y, unit1.inlets.x)

var circuit = maWire.exploreCircuit()
circuit.calculateOrder()
console.log(circuit.printOrder())
