
const Osc = require("../simple/Osc.js")
const Pan = require("../simple/Pan.js")
const Gain = require("../simple/Gain.js")
const render = require("../render.js")
const Wire = require("../Wire.js")
const argv = require("minimist")(process.argv.slice(2))

const T = 44100 * (argv.T || 1)


var osc1 = new Osc(330)
var lfo1 = new Osc(0.1)
var pan1 = new Pan(0)
var gain1 = new Gain(-12)
new Wire(osc1, pan1)
new Wire(lfo1, pan1.PAN)
new Wire(pan1, gain1)

render(gain1.OUT, T, 2)
.save()
