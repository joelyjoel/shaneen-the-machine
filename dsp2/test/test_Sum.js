const Osc = require("../simple/Osc.js")
const Sum = require("../simple/Sum.js")
const Wire = require("../Wire.js")
const render = require("../render.js")

const argv = require("minimist")(process.argv.slice(2))
const T = 44100 * (argv.T || 1)

var osc1 = new Osc(300 * Math.random())
var osc2 = new Osc(400 * Math.random())
var osc3 = new Osc(500 * Math.random())
var osc4 = new Osc(100 * Math.random())

var sum1 = new Sum()
var sum2 = new Sum()
var sum3 = new Sum()

new Wire(osc1.OUT, sum1.A)
new Wire(osc2.OUT, sum1.B)
new Wire(sum1.OUT, sum2.A)
new Wire(osc3.OUT, sum2.B)
new Wire(sum2, sum3.A)
new Wire(osc4, sum3.B)

render(sum3.OUT, T, 1)
.normalise()
.save()
