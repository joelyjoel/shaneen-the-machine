const Circuit = require("./Circuit.js")
const gcd = require("compute-gcd")

function Wire(top, bottom) {
  this.top = top
  this.bottom = bottom
  this.tickInterval = 1
}
module.exports = Wire

Wire.prototype.isWire = true

Wire.prototype.tick = function(clock) {
  var val = this._top.unit[this._top.name]
  if(this._bottom.nChannels && !this._top.nChannels)
    val = [val]
  else if(this._top.nChannels && !this._bottom.nChannels)
    val = val[0]
  this._bottom.unit[this._bottom.name] = val
}

Wire.prototype.exploreCircuit = function(circuit) {
  circuit = circuit || new Circuit()//{wires: [], units: []}

  if(circuit.wires.indexOf(this) == -1) {
    circuit.wires.push(this)
    circuit = this._top.unit.exploreCircuit(circuit)
    circuit = this._bottom.unit.exploreCircuit(circuit)
  }

  return circuit
}

Wire.prototype.__defineGetter__("top", function() {
  return this._top
})
Wire.prototype.__defineSetter__("top", function(top) {
  if(top.isUnit)
    top = top.defaultOutlet
  if(this._top)
    this._top.unit.outWires.splice(this._top.unit.outWires.indexOf(this), 1)
  this._top = top
  if(top && top.unit)
    top.unit.outWires.push(this)
})

Wire.prototype.__defineGetter__("bottom", function() {
  return this._bottom
})
Wire.prototype.__defineSetter__("bottom", function(bottom) {
  if(bottom.isUnit)
    bottom = bottom.defaultInlet
  if(this._bottom)
    this._bottom.unit.inWires.splice(this._bottom.unit.inWires.indexOf(this), 1)
  this._bottom = bottom
  if(bottom && bottom.unit)
    bottom.unit.inWires.push(this)
})

Wire.prototype.findProcessIndex = function(history) {
  history = history || []
  history.push(this)

  var index = this._top.unit.findProcessIndex(history) + 1
  this.processIndex = index
  return index
}

Wire.prototype.__defineGetter__("label", function() {
  return this.constructor.name +
    " (" + this.top.unit.label + "." + this.top.name +
    " -> " +
    this.bottom.unit.label + "." + this.bottom.name +
    ")"
})
