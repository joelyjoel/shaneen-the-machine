const Wire = require("./Wire.js")

function Inlet(model) {
  this.nChannels = 0
  if(model) {
    /*this.name = model.name
    this.unit = model.unit*/
    Object.assign(this, model)
  }
}
module.exports = Inlet

Inlet.prototype.connect = function(outlet) {
  new Wire(outlet, this)
}
