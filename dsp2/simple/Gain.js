const Unit = require("../Unit.js")
const dB = require("../decibel")

function Gain(gain) {
  Unit.call(this)
  this.addInlet("in", {nChannels:true})
  this.addInlet("gain")
  this.addOutlet("out", {nChannels:true})

  this.gain = gain || 0
  this.in = 0
}
Gain.prototype = Object.create(Unit.prototype)
Gain.prototype.constructor = Gain
module.exports = Gain

Gain.prototype._tick = function() {
  var sf = dB(this.gain)
  var out = []
  for(var c in this.in)
    out[c] = this.in[c] * sf
  this.out = out
}
