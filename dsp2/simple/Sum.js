const Unit = require("../Unit.js")

function Sum() {
  Unit.call(this)
  this.addInlet("a", {nChannels: true})
  this.addInlet("b", {nChannels: true})
  this.addOutlet("out", {nChannels: true})
}
Sum.prototype = Object.create(Unit.prototype)
Sum.prototype.constructor = Sum
module.exports = Sum

Sum.prototype.isSum = true

Sum.prototype._tick = function() {
  var y = []
  for(var c=0; c<this.a.length && c<this.b.length; c++) {
    y[c] = (this.a[c] || 0) + (this.b[c] || 0)
  }
  this.out = y
}
