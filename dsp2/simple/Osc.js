const Unit = require("../Unit.js")

function Osc(f) {
  Unit.call(this)
  this.addInlet("f")
  this.addOutlet("out")

  this.f = f || 440
  this.phase = 0
}
Osc.prototype = Object.create(Unit.prototype)
Osc.prototype.constructor = Osc
module.exports = Osc

Osc.prototype.isOsc = true

Osc.prototype._tick = function() {
  this.phase += this.f / this.sampleRate
  this.out = Math.sin(2*Math.PI * this.phase)
}
