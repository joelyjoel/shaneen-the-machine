const Unit = require("../Unit.js")
const dB = require("../decibel.js")

function Pan(pan) { // pan mono to stereo
  Unit.call(this)
  this.addOutlet("out", {nChannels:2})
  this.addInlet("in")
  this.addInlet("pan")

  this.compensationDB = 1.5
  this.pan = pan || 0
}
Pan.prototype = Object.create(Unit.prototype)
Pan.prototype.constructor = Pan
module.exports = Pan

Pan.prototype._tick = function() {
  var compensation = dB((1-Math.abs(this.pan)) * this.compensationDB)
  //this.sfL = (1-pan)/2 * compensation;
  //this.sfR = (1+pan)/2 * compensation;
  this.out = [
    this.in * (1-this.pan)/2 * compensation,
    this.in * (1+this.pan)/2 * compensation,
  ]
}
