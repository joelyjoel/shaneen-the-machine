const Unit = require("../Unit.js")

function Multiply(a, b) {
  Unit.call(this)

  this.addInlet("a")
  this.addInlet("b")
  this.addOutlet("out")

  this.a = a || 1
  this.b = b || 1
}
Multiply.prototype = Object.create(Unit.prototype)
Multiply.prototype.constructor = Multiply
module.exports = Multiply

Multiply.prototype.isMultiply = true

Multiply.prototype._tick = function() {
  this.out = this.a * this.b
}
