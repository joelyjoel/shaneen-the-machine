const Unit = require("../Unit.js")

function Add() {
  Unit.call(this)
  this.addInlet("a")
  this.addInlet("b")
  this.addOutlet("out")
}
Add.prototype = Object.create(Unit.prototype)
Add.prototype.constructor = Add
module.exports = Add

Add.prototype.isAdd = true

Add.prototype._tick = function() {
  this.out = this.a + this.b
}
