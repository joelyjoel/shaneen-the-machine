const Unit = require("../Unit.js")
const dB = require("../decibel.js")

function Gain(gain) {
  Unit.call(this)
  this.tickInterval = Unit.signalChunkSize
  this.addInlet("in", {chunkSize: this.tickInterval, nChannels:true})
  this.addInlet("gain", {chunkSize:this.tickInterval})
  this.addOutlet("out", {chunkSize:this.tickInterval, nChannels:true})
  this.GAIN = gain || 0
}
Gain.prototype = Object.create(Unit.prototype)
Gain.prototype.constructor = Gain
module.exports = Gain

Gain.prototype._tick = function() {
  var out = []
  var sfChunk = new Float32Array(this.tickInterval)
  for(var t=0; t<this.tickInterval; t++)
    sfChunk[t] = dB(this.gain[t])
  for(var c=0; c<this.in.length; c++) {
    var outChunk = out[c] = this.out[c] || new Float32Array(this.tickInterval)
    for(var t=0; t<this.tickInterval; t++)
      outChunk[t] = sfChunk[t] * this.in[c][t]
  }
  console.log(sfChunk)
  this.out = out
}
