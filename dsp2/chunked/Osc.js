const Unit = require("../Unit.js")

const PHI = 2 * Math.PI

function Osc(f) {
  Unit.call(this)
  this.tickInterval = Unit.signalChunkSize
  this.addInlet("f", {chunkSize:this.tickInterval, default:440})
  this.addOutlet("out", {chunkSize:this.tickInterval})

  if(f)
    this.F = f
  this.waveTable = Osc.sineTable
  this.phase = 0
  this.out = new Float32Array(256)
}
Osc.prototype = Object.create(Unit.prototype)
Osc.prototype.constructor = Osc
module.exports = Osc

Osc.prototype._tick = function() {
  for(var t=0; t<this.OUT.chunkSize; t++) {
    this.phase += this.f[t]
    this.phase %= Unit.sampleRate
    var fraction = this.phase%1
    this.out[t] = this.waveTable[Math.floor(this.phase)] * (1-fraction)
                  + this.waveTable[Math.ceil(this.phase)] * fraction
  }
}

Osc.sineTable = new Float32Array(Unit.sampleRate+1)
for(var t=0; t<Osc.sineTable.length; t++) {
  Osc.sineTable[t] = Math.sin(PHI * t/Osc.sineTable.length)
}

Osc.squareTable = new Float32Array(Unit.sampleRate+1)
Osc.squareTable.fill(1, 0, Math.floor(Unit.sampleRate/2))
Osc.squareTable.fill(-1, Math.floor(Unit.sampleRate/2), Osc.squareTable.length)
