const Unit = require("../Unit.js")
const Wire = require("../Wire.js")

function Sum() {
  Unit.call(this)
  this.tickInterval = Unit.signalChunkSize
  this.addInlet("a", {chunkSize:this.tickInterval, nChannels:true, default: 0})
  this.addInlet("b", {chunkSize:this.tickInterval, nChannels:true, default: 0})
  this.addOutlet("out", {chunkSize:this.tickInterval, nChannels:true})
}
Sum.prototype = Object.create(Unit.prototype)
Sum.prototype.constructor = Sum
module.exports = Sum

Sum.many = function() {
  if(arguments.length == 0)
    return arguments[0]
  var sum = new Sum()
  new Wire(arguments[0], sum.A)
  new Wire(arguments[1], sum.B)
  for(var i=2; i<arguments.length; i++) {
    var lastSum = sum
    sum = new Sum()
    new Wire(lastSum.OUT, sum.A)
    new Wire(arguments[i], sum.B)
  }
  return sum
}

Sum.prototype.isSum = true

Sum.prototype._tick = function() {
  var out = []
  for(var c=0; c<this.a.length || c<this.b.length; c++) {
    var chunk = out[c] = this.out[c] || new Float32Array(this.OUT.chunkSize)
    var aChunk = this.a[c]
    var bChunk = this.b[c]
    if(aChunk && bChunk)
      for(var t=0; t<chunk.length; t++)
        chunk[t] = aChunk[t] + bChunk[t]
    else if(aChunk)
      for(var t=0; t<chunk.length; t++)
        chunk[t] = aChunk[t]
    else // if (c < b.length && !aChunk) then bChunk != undefined
      for(var t=0; t<chunk.length; t++)
        chunk[t] = bChunk[t]
  }
  this.out = out
}
