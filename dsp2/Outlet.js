function Outlet(model) {
  //defaults
  this.nChannels = 0

  if(model) {
    /*this.name = model.name
    this.unit = model.unit*/
    Object.assign(this, model)
  }
}
module.exports = Outlet

Outlet.prototype.__defineGetter__("value", function() {
  return this.unit[this.name]
})

Outlet.prototype.__defineGetter__("label", function() {
  return this.unit.label + "." + this.name
})
