function Circuit() {
  this.wires = []
  this.units = []
  this.order = null
  this.startingUnit = null
}
module.exports = Circuit

Circuit.merge = function(/* many */) {
  var newCircuit = new Circuit()
  for(var i in this.arguments) {
    if(this.arguments[i].isCircuit)
      newCircuit.merge(this.arguments[i])
  }
  return newCircuit
}

Circuit.prototype.isCircuit = true

Circuit.prototype.tick = function(clock) {
  if(!this.order)
    this.calculateOrder()
  for(var i in this.order) {
    if(!clock || clock%this.order[i].tickInterval == 0)
      this.order[i].tick(clock)
  }
}

Circuit.prototype.merge = function(circuit) {
  for(var w in circuit.wires) {
    var wire = cicuit.wires[w]
    if(this.wires.indexOf(wire) == -1)
      this.wires.push(wire)
  }
  for(var u in circuit.units) {
    var unit = circuit.units[u]
    if(this.units.indexOf(unit) == -1)
      this.units.push(unit)
  }

  this.recalculateOrder()
}

Circuit.prototype.clearOrder = function() {
  this.order = null
  for(var w in this.wires)
    delete this.wires[w].processIndex
  for(var u in this.units)
    delete this.units[u].processIndex
  return this
}
Circuit.prototype.calculateOrder = function(startingUnit) {
  startingUnit = startingUnit || this.startingUnit || this.units[0]
  startingUnit.findProcessIndex()
  var stuff = this.wires.concat(this.units)
  for(var i in stuff) {
    if(stuff[i].processIndex == undefined) {
      stuff[i].findProcessIndex()
    }
    stuff[i].circuit = this
  }

  var order = stuff.sort(function(a, b) {
    return a.processIndex-b.processIndex
  })

  while(order[0].isWire) {
    order.push(order.shift())
  }

  this.order = order

  return order
}
Circuit.prototype.recalculateOrder = function(startingUnit) {
  this.clearOrder()
  this.calculateOrder(startingUnit)
}

Circuit.prototype.printOrder = function() {
  return this.order.map((obj) => { return obj.label; }).join("\n")
}
